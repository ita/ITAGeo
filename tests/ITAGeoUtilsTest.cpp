#include <ITAGeo/Utils.h>
#include <ITAStopWatch.h>
#include <VistaBase/VistaVector3D.h>
#include <VistaMath/VistaGeometries.h>
#include <VistaTools/VistaRandomNumberGenerator.h>
#include <cassert>
#include <cstdio>

using namespace std;

void TestCalculateDiffractionAperturePoint( );
void TestIsPointOutsideDiffractionWedge( );
void TestIsDiffractionAperturePointInRange( );
void TestRojectPoint( );
bool CompareVistaVectorsEqual( const VistaVector3D&, const VistaVector3D&, const float fPrecisionThreshold = Vista::Epsilon );

int main( int, char** )
{
	TestCalculateDiffractionAperturePoint( );
	TestIsPointOutsideDiffractionWedge( );
	TestIsDiffractionAperturePointInRange( );
	TestRojectPoint( );

	return 0;
}

void TestRojectPoint( )
{
	VistaVector3D v3AnyPoint;
	VistaVector3D v3RojectedPoint;
	const float fArbitraryX = VistaRandomNumberGenerator::GetStandardRNG( )->GenerateFloat( -10.0f, 10.0f );
	VistaVector3D v3VertexStart( 0.0f, 0.0f, 0.0f );
	VistaVector3D v3VertexEnd( 1.0f, 0.0f, 0.0f );
	VistaVector3D v3Face1Normal( 0.0f, 0.0f, 1.0f ); // XY plane

	v3AnyPoint.SetValues( fArbitraryX, 1.0f, 0.0f );
	assert( ITAGeoUtils::RojectPoint( v3VertexStart, v3VertexEnd, v3Face1Normal, v3AnyPoint, v3RojectedPoint ) );
	assert( CompareVistaVectorsEqual( v3RojectedPoint, VistaVector3D( fArbitraryX, 1.0f, .0f ), 10e-6 ) );
	cout << "Rojected point " << v3AnyPoint << " into XY plane at " << v3RojectedPoint << endl;

	v3AnyPoint.SetValues( 0, 1.0f, 1.0f );
	v3AnyPoint.Normalize( );
	v3AnyPoint[Vista::X] = fArbitraryX;
	assert( ITAGeoUtils::RojectPoint( v3VertexStart, v3VertexEnd, v3Face1Normal, v3AnyPoint, v3RojectedPoint ) );
	assert( CompareVistaVectorsEqual( v3RojectedPoint, VistaVector3D( fArbitraryX, 1.0f, .0f ), 10e-6 ) );
	cout << "Rojected point " << v3AnyPoint << " into XY plane at " << v3RojectedPoint << endl;

	v3AnyPoint.SetValues( fArbitraryX, 0.0f, 1.0f );
	assert( ITAGeoUtils::RojectPoint( v3VertexStart, v3VertexEnd, v3Face1Normal, v3AnyPoint, v3RojectedPoint ) );
	assert( CompareVistaVectorsEqual( v3RojectedPoint, VistaVector3D( fArbitraryX, 1.0f, .0f ), 10e-6 ) );
	cout << "Rojected point " << v3AnyPoint << " into XY plane at " << v3RojectedPoint << endl;

	v3AnyPoint.SetValues( 0, -1.0f, 1.0f );
	v3AnyPoint.Normalize( );
	v3AnyPoint[Vista::X] = fArbitraryX;
	assert( ITAGeoUtils::RojectPoint( v3VertexStart, v3VertexEnd, v3Face1Normal, v3AnyPoint, v3RojectedPoint ) );
	assert( CompareVistaVectorsEqual( v3RojectedPoint, VistaVector3D( fArbitraryX, 1.0f, .0f ), 10e-6 ) );
	cout << "Rojected point " << v3AnyPoint << " into XY plane at " << v3RojectedPoint << endl;

	v3AnyPoint.SetValues( fArbitraryX, -1.0f, .0f );
	assert( ITAGeoUtils::RojectPoint( v3VertexStart, v3VertexEnd, v3Face1Normal, v3AnyPoint, v3RojectedPoint ) );
	assert( CompareVistaVectorsEqual( v3RojectedPoint, VistaVector3D( fArbitraryX, 1.0f, .0f ), 10e-6 ) );
	cout << "Rojected point " << v3AnyPoint << " into XY plane at " << v3RojectedPoint << endl;

	v3AnyPoint.SetValues( 0, -1.0f, -1.0f );
	v3AnyPoint.Normalize( );
	v3AnyPoint[Vista::X] = fArbitraryX;
	assert( ITAGeoUtils::RojectPoint( v3VertexStart, v3VertexEnd, v3Face1Normal, v3AnyPoint, v3RojectedPoint ) );
	assert( CompareVistaVectorsEqual( v3RojectedPoint, VistaVector3D( fArbitraryX, 1.0f, .0f ), 10e-6 ) );
	cout << "Rojected point " << v3AnyPoint << " into XY plane at " << v3RojectedPoint << endl;

	v3AnyPoint.SetValues( fArbitraryX, .0f, -1.0f );
	assert( ITAGeoUtils::RojectPoint( v3VertexStart, v3VertexEnd, v3Face1Normal, v3AnyPoint, v3RojectedPoint ) );
	assert( CompareVistaVectorsEqual( v3RojectedPoint, VistaVector3D( fArbitraryX, 1.0f, .0f ), 10e-6 ) );
	cout << "Rojected point " << v3AnyPoint << " into XY plane at " << v3RojectedPoint << endl;

	v3AnyPoint.SetValues( 0, 1.0f, -1.0f );
	v3AnyPoint.Normalize( );
	v3AnyPoint[Vista::X] = fArbitraryX;
	assert( ITAGeoUtils::RojectPoint( v3VertexStart, v3VertexEnd, v3Face1Normal, v3AnyPoint, v3RojectedPoint ) );
	cout << "Rojected point " << v3AnyPoint << " into XY plane at " << v3RojectedPoint << endl;
}

void TestIsDiffractionAperturePointInRange( )
{
	VistaVector3D v3PointOnEdge;
	VistaVector3D v3VertexStart( 10.0f, -23.0f, 0.123f );
	VistaVector3D v3VertexEnd( 44.0f, -13.0f, -33.99f );
	VistaVector3D v3EdgeVec = ( v3VertexEnd - v3VertexStart );

	assert( !ITAGeoUtils::IsDiffractionAperturePointInRange( v3VertexStart, v3VertexEnd, v3VertexStart - .001f * v3EdgeVec ) );
	assert( !ITAGeoUtils::IsDiffractionAperturePointInRange( v3VertexStart, v3VertexEnd, v3VertexStart + 0.0f * v3EdgeVec ) );
	assert( ITAGeoUtils::IsDiffractionAperturePointInRange( v3VertexStart, v3VertexEnd, v3VertexStart + 0.001f * v3EdgeVec ) );
	assert( ITAGeoUtils::IsDiffractionAperturePointInRange( v3VertexStart, v3VertexEnd, v3VertexStart + 0.5f * v3EdgeVec ) );
	assert( ITAGeoUtils::IsDiffractionAperturePointInRange( v3VertexStart, v3VertexEnd, v3VertexStart + 0.999f * v3EdgeVec ) );
	assert( !ITAGeoUtils::IsDiffractionAperturePointInRange( v3VertexStart, v3VertexEnd, v3VertexStart + 1.0f * v3EdgeVec ) );
}

void TestIsPointOutsideDiffractionWedge( )
{
	VistaVector3D v3AnyPoint;
	VistaVector3D v3VertexStart( 0.0f, 0.0f, 0.0f );
	VistaVector3D v3VertexEnd( 0.0f, 0.0f, -1.0f );
	VistaVector3D v3Face1Normal( 0.0f, 1.0f, 0.0f ); // XZ plane
	VistaVector3D v3Face2Normal( 1.0f, 0.0f, 0.0f ); // YZ plane
	// -> all vectors with positive or zero X or positive or zero Y component are outside, Z is arbitrary

	v3AnyPoint.SetValues( 0.001f, 0.001f, 1.1415f );
	assert( ITAGeoUtils::IsPointOutsideDiffractionWedge( v3VertexStart, v3VertexEnd, v3Face1Normal, v3Face2Normal, v3AnyPoint ) );

	v3AnyPoint.SetValues( -0.001f, 0.001f, -1.1415f );
	assert( ITAGeoUtils::IsPointOutsideDiffractionWedge( v3VertexStart, v3VertexEnd, v3Face1Normal, v3Face2Normal, v3AnyPoint ) );

	v3AnyPoint.SetValues( 0.001f, -0.001f, 1.1415f );
	assert( ITAGeoUtils::IsPointOutsideDiffractionWedge( v3VertexStart, v3VertexEnd, v3Face1Normal, v3Face2Normal, v3AnyPoint ) );

	v3AnyPoint.SetValues( -0.001f, -0.001f, -1.1415f );
	assert( ITAGeoUtils::IsPointInsideDiffrationWedge( v3VertexStart, v3VertexEnd, v3Face1Normal, v3Face2Normal, v3AnyPoint ) );
}

void TestCalculateDiffractionAperturePoint( )
{
	VistaVector3D v3SourcePos( 1.3f, -.1f, .16f );
	VistaVector3D v3ReceiverPos( -1.44f, -.2f, -.6f );
	VistaRay rDiffrationEdge( VistaVector3D( 0.0f, 1.0f, 0.0f ), VistaVector3D( 0.0f, 0.0f, -1.0f ) );

	VistaVector3D v3AperturePoint, v3AperturePointReciprocity;

	assert( ITAGeoUtils::CalculateDiffractionAperturePoint( rDiffrationEdge, v3SourcePos, v3ReceiverPos, v3AperturePoint ) );
	assert( ITAGeoUtils::CalculateDiffractionAperturePoint( rDiffrationEdge, v3ReceiverPos, v3SourcePos, v3AperturePointReciprocity ) );
	assert( CompareVistaVectorsEqual( v3AperturePoint, v3AperturePointReciprocity ) );
	cout << "Aperture point found: " << v3AperturePoint << endl;

	v3SourcePos.SetValues( .0f, 1.0f, -1.0f );
	assert( ITAGeoUtils::CalculateDiffractionAperturePoint( rDiffrationEdge, v3SourcePos, v3ReceiverPos, v3AperturePoint ) );
	assert( ITAGeoUtils::CalculateDiffractionAperturePoint( rDiffrationEdge, v3ReceiverPos, v3SourcePos, v3AperturePointReciprocity ) );
	assert( CompareVistaVectorsEqual( v3AperturePoint, v3AperturePointReciprocity ) );
	cout << "Aperture " << v3AperturePoint << " sould be " << v3SourcePos << endl;

	v3ReceiverPos.SetValues( .0f, 1.0f, 1.0f );
	assert( !ITAGeoUtils::CalculateDiffractionAperturePoint( rDiffrationEdge, v3SourcePos, v3ReceiverPos, v3AperturePoint ) );
	assert( !ITAGeoUtils::CalculateDiffractionAperturePoint( rDiffrationEdge, v3ReceiverPos, v3SourcePos, v3AperturePointReciprocity ) );
	cerr << "No aperture point found for " << v3SourcePos << " -> " << v3ReceiverPos << " over " << rDiffrationEdge.GetDir( ) << " through "
	     << rDiffrationEdge.GetOrigin( ) << ", because source and target are lying on edge!" << endl;

	VistaRandomNumberGenerator oRandomNumGenerator;

	ITAStopWatch sw;
	const size_t iLoopCount = 100000;
	for( size_t i = 0; i < iLoopCount; i++ )
	{
		sw.start( );
		v3SourcePos.SetValues( oRandomNumGenerator.GenerateFloat2( ), oRandomNumGenerator.GenerateFloat2( ), oRandomNumGenerator.GenerateFloat2( ) );
		v3ReceiverPos.SetValues( oRandomNumGenerator.GenerateFloat2( ), oRandomNumGenerator.GenerateFloat2( ), oRandomNumGenerator.GenerateFloat2( ) );
		bool bS3R = ITAGeoUtils::CalculateDiffractionAperturePoint( rDiffrationEdge, v3SourcePos, v3ReceiverPos, v3AperturePoint );
		bool bR2S = ITAGeoUtils::CalculateDiffractionAperturePoint( rDiffrationEdge, v3ReceiverPos, v3SourcePos, v3AperturePointReciprocity );
		assert( bS3R == bR2S );
		if( bS3R )
			assert( CompareVistaVectorsEqual( v3AperturePoint, v3AperturePointReciprocity, 10e9 ) ); // nano meters
		sw.stop( );
	}
	cout << "Sucessfully calculated " << iLoopCount << " times the diffraction aperture, statistics are: " << sw << endl;
}

bool CompareVistaVectorsEqual( const VistaVector3D& v3VecA, const VistaVector3D& v3VecB, const float fPrecisionThreshold /* = Vista::Epsilon */ )
{
	assert( v3VecA.CheckForValidity( ) && v3VecB.CheckForValidity( ) );
	const float fLength    = ( v3VecB - v3VecA ).GetLength( );
	bool bLengthAlmostZero = ( fLength < fPrecisionThreshold );
	return bLengthAlmostZero;
}