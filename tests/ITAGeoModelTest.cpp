/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAGeo/SketchUp/Model.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <assert.h>
#include <iostream>

using namespace std;
using namespace ITAGeo;

int main( int, char** )
{
	std::string sBasePath  = "./";
	string sSuffix         = "ac";
	std::string sModelName = "Classroom_empty";
	VistaFileSystemFile oFile( sBasePath + sModelName + "." + sSuffix );

	auto pMatDir = std::make_shared<Material::CMaterialManager>( "." );

	SketchUp::CModel oGeoModel;
	oGeoModel.SetMaterialManager( pMatDir );

	try
	{
		if( oGeoModel.Load( oFile.GetName( ) ) )
			cout << "Successfully loaded file '" << oFile.GetLocalName( ) << "' as geometric model" << endl;

		cout << "Top-level groups: " << oGeoModel.GetNumGroups( ) << endl;
		std::vector<std::string> vsGroupNames( oGeoModel.GetGroupNames( ) );
		for( size_t n = 0; n < vsGroupNames.size( ); n++ )
			cout << "\t" << vsGroupNames[n] << endl;

		cout << "Component definitions: " << oGeoModel.GetNumComponentDefinitions( ) << endl;
		std::vector<std::string> vsCompNames( oGeoModel.GetComponentDefinitionNames( ) );
		for( size_t n = 0; n < vsCompNames.size( ); n++ )
			cout << "\t" << vsCompNames[n] << endl;

		cout << "Top-level component instances: " << oGeoModel.GetNumComponentInstances( ) << endl;

		cout << "Layers: " << oGeoModel.GetNumLayers( ) << endl;
		std::vector<std::string> vsLayers( oGeoModel.GetLayers( ) );
		for( size_t n = 0; n < vsLayers.size( ); n++ )
			cout << "\t" << vsLayers[n] << endl;

		sSuffix = "skp";
		VistaFileSystemFile oOutFile( sBasePath + sModelName + "_out." + sSuffix );
		if( oGeoModel.Store( oOutFile.GetName( ) ) )
			cout << "Successfully stored file '" << oOutFile.GetLocalName( ) << "' as a SketchUp model" << endl;
	}
	catch( const ITAException& e )
	{
		cerr << e << endl;
	}

	return 0;
}
