/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Tests IFC functionality of IfcOpenShell library.
 *
 */

#include <ITAException.h>
#include <iostream>
#include <string>

// IfcOpenShell includes
#include <ifcparse/Ifc2x3.h>
#include <ifcparse/IfcFile.h>
#include <ifcparse/IfcLogger.h>

using namespace std;
using namespace IfcSchema;

static string sIFCFilePath = "SU_TwoCoupledRooms.ifc";

int main( int, char** )
{
	try
	{
		cout << "Loading IFC file path '" << sIFCFilePath << "'" << endl;

		Logger::SetOutput( &std::cout, &std::cout );

		IfcParse::IfcFile oIfcFile;
		if( !oIfcFile.Init( sIFCFilePath ) )
			throw ITAException( ITAException::INVALID_PARAMETER, "IfcFile", "Could not init file " + sIFCFilePath );


		// Do something ...

		IfcBuildingElement::list::ptr lElements = oIfcFile.entitiesByType<IfcBuildingElement>( );
		cout << "\tNum elements: " << lElements->size( ) << endl;

		IfcBuildingElement::list::it it = lElements->begin( );
		while( it != lElements->end( ) )
		{
			const IfcBuildingElement* pElement = *it++;
			std::cout << "\t\tBuilding element info: " << pElement->entity->toString( ) << std::endl;

			if( pElement->is( IfcWindow::Class( ) ) )
			{
				const IfcWindow* window = (IfcWindow*)pElement;
				cout << "[!] Element is a window, trying to get width and height for area calculation." << endl;

				if( window->hasOverallWidth( ) && window->hasOverallHeight( ) )
				{
					const double area = window->OverallWidth( ) * window->OverallHeight( );
					std::cout << "[!] The area of this window is " << area << std::endl;
				}
				else
					cout << "[E] Parameter not available, skipping." << endl;
			}
		}
	}
	catch( const ITAException& e )
	{
		cerr << "Cought exception during parsing of '" << sIFCFilePath << "': " << e << endl;
		return 255;
	}
	catch( ... )
	{
		cerr << "Unrecognized exception during parsing of '" << sIFCFilePath << "'" << endl;
		return 255;
	}

	return 0;
}
