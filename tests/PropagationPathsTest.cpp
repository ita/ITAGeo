#include <ITAGeo/Base.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <cstdio>


using namespace std;
using namespace ITAGeo;

int main( int, char** )
{
	auto pSource = make_shared<CEmitter>( );
	pSource->v3InteractionPoint.SetValues( 1.0f, 2.0f, 3.0f );

	auto pReflection = make_shared<CSpecularReflection>( );
	pReflection->v3InteractionPoint.SetValues( -0.5f, -1.0f, -2.0f );
	pReflection->v3FaceNormal.SetValues( 1.f, 0.f, 0.f, 0.f );
	pReflection->iPolygonID = 0;

	auto pReceiver = make_shared<CSensor>( );
	pReceiver->v3InteractionPoint.SetValues( -1.0f, -2.0f, -3.0f );

	CPropagationPath oPath;
	oPath.sIdentifier = "PathSourceToReflection";
	oPath.push_back( pSource );
	oPath.push_back( pReflection );

	cout << oPath << endl;

	CPropagationPath oPath2;
	oPath2.sIdentifier = "PathReflectionToReceiver";
	oPath2.push_back( pReflection );
	oPath2.push_back( pReceiver );

	CPropagationPathList oPathList;
	oPathList.sIdentifier = "PathListTest";
	oPathList.push_back( oPath );
	oPathList.push_back( oPath2 );

#ifdef WITH_JSON_SUPPORT

	cout << "Testing JSON format I/O for one propagation path" << endl;

	Utils::JSON::Export( oPath, oPath.sIdentifier + ".json" );

	CPropagationPath oPathFromJSON;
	Utils::JSON::Import( oPath, oPath.sIdentifier + ".json" );
	oPathFromJSON.sIdentifier = oPathFromJSON.sIdentifier + "_from_json";

	cout << oPathFromJSON << endl << endl;

	cout << endl << oPathList << endl;

	cout << "Testing JSON format I/O for the propagation path list" << endl;

	Utils::JSON::Export( oPathList, oPathList.sIdentifier + ".json" );

	CPropagationPathList oPathListFromJSON;
	Utils::JSON::Import( oPathListFromJSON, oPathList.sIdentifier + ".json" );
	oPathListFromJSON.sIdentifier = oPathListFromJSON.sIdentifier + "_from_json";

	cout << oPathListFromJSON << endl;


#endif

#ifdef WITH_XML_SUPPORT

	cout << "Testing XML format I/O" << endl;

	oPath.Store( oPath.sIdentifier + ".xml" );

	CPropagationPath oPathFromXML;
	oPathFromXML.Load( oPath.sIdentifier + ".xml" );
	oPathFromXML.sIdentifier = oPath.sIdentifier + "_from_xml";

	cout << oPathFromXML << endl;

#endif

	return 0;
}
