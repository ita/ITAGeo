/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Loads an ITAGeo halfedge mesh model from file and test random located points.
 *
 */

#include <ITAException.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAStopWatch.h>
#include <VistaBase/VistaVector3D.h>
#include <VistaMath/VistaGeometries.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <VistaTools/VistaRandomNumberGenerator.h>
#include <assert.h>
#include <iostream>

using namespace std;
using namespace ITAGeo;

int main( int, char** )
{
	string sInFileName  = "SolidCube";
	string sInExtension = "skp";

	auto pMD = std::make_shared<Material::CMaterialManager>( "./" );

	Halfedge::CMeshModel oHEModel;
	oHEModel.SetMaterialManager( pMD );

	if( oHEModel.Load( sInFileName + "." + sInExtension ) )
		cout << "Sucessfully loaded '" << sInFileName << "' as an halfedge model" << endl;


	// Get min and max values of bounding box of the SolidCube(must be axis aligned)
	VistaVector3D v3Min, v3Max;
	oHEModel.GetBoundingBoxAxisAligned( v3Min, v3Max );
	float fXMin = v3Min[Vista::X];
	float fYMin = v3Min[Vista::Y];
	float fZMin = v3Min[Vista::Z];
	float fXMax = v3Max[Vista::X];
	float fYMax = v3Max[Vista::Y];
	float fZMax = v3Max[Vista::Z];


	// Create a big number of points that are inside the SolidCube
	bool bIsAlwaysInsideRandom  = true;
	bool bIsAlwaysInsideAligned = true;
	VistaVector3D v3DInsidePoint;

	for( int i = 0; i < 10000; i++ )
	{
		v3DInsidePoint[Vista::X] = VistaRandomNumberGenerator::GetStandardRNG( )->GenerateFloat( fXMin, fXMax );
		v3DInsidePoint[Vista::Y] = VistaRandomNumberGenerator::GetStandardRNG( )->GenerateFloat( fYMin, fYMax );
		v3DInsidePoint[Vista::Z] = VistaRandomNumberGenerator::GetStandardRNG( )->GenerateFloat( fZMin, fZMax );

		bIsAlwaysInsideRandom  = bIsAlwaysInsideRandom & oHEModel.GetInsideMeshJordanMethodRandomRay( v3DInsidePoint );
		bIsAlwaysInsideAligned = bIsAlwaysInsideAligned & oHEModel.GetInsideMeshJordanMethodAxisAligned( v3DInsidePoint );
	}

	if( bIsAlwaysInsideRandom )
		cout << "The inside point is always inside the mesh. (RandomRay)" << endl;
	else
		cout << "The inside point is not always inside the mesh. (RandomRay)" << endl;

	if( bIsAlwaysInsideAligned )
		cout << "The inside point is always inside the mesh. (aligned axis)" << endl;
	else
		cout << "The inside point is not always inside the mesh. (aligned axis)" << endl;


	// Create a big number of points that are on the XMin plane the SolidCube
	bIsAlwaysInsideRandom      = true;
	bIsAlwaysInsideAligned     = true;
	bool bIsNeverInsideRandom  = true;
	bool bIsNeverInsideAligned = true;

	for( int i = 0; i < 10000; i++ )
	{
		v3DInsidePoint[Vista::X] = fXMax;
		v3DInsidePoint[Vista::Y] = VistaRandomNumberGenerator::GetStandardRNG( )->GenerateFloat( fYMin, fYMax );
		v3DInsidePoint[Vista::Z] = VistaRandomNumberGenerator::GetStandardRNG( )->GenerateFloat( fZMin, fZMax );

		bIsAlwaysInsideRandom  = bIsAlwaysInsideRandom & oHEModel.GetInsideMeshJordanMethodRandomRay( v3DInsidePoint );
		bIsNeverInsideRandom   = bIsNeverInsideRandom & !oHEModel.GetInsideMeshJordanMethodRandomRay( v3DInsidePoint );
		bIsAlwaysInsideAligned = bIsAlwaysInsideAligned & oHEModel.GetInsideMeshJordanMethodAxisAligned( v3DInsidePoint );
		bIsNeverInsideAligned  = bIsNeverInsideAligned & !oHEModel.GetInsideMeshJordanMethodAxisAligned( v3DInsidePoint );
	}

	if( bIsAlwaysInsideRandom )
		cout << "The inside point is always inside the mesh. (RandomRay)" << endl;
	else
		cout << "The inside point is not always inside the mesh. (RandomRay)" << endl;

	if( bIsAlwaysInsideAligned )
		cout << "The inside point is always inside the mesh. (aligned axis)" << endl;
	else
		cout << "The inside point is not always inside the mesh. (aligned axis)" << endl;

	return 0;
}
