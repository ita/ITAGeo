/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAStopWatch.h>
#include <VistaTools/VistaFileSystemDirectory.h>
#include <VistaTools/VistaFileSystemFile.h>

#pragma warning( disable : 4512 4127 )
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;
#pragma warning( default : 4512 4127 )

#include <assert.h>

using namespace std;


int main( int, char** )
{
	VistaFileSystemFile oInFile( "LOD1_3_F0_H3.gml" );
	assert( oInFile.Exists( ) );

	VistaFileSystemFile oOutFile( "LOD1_3_F0_H3.skp" );
	if( oOutFile.Exists( ) )
		cout << "[Warning] Output file exists, will overwrite." << endl;

	VistaFileSystemDirectory oParentDir( oOutFile.GetParentDirectory( ) );
	assert( oParentDir.Exists( ) );

	ITAStopWatch sw;

	CITAMesh oMesh;
	bool bReadOK = false, bWriteOK = false;
	if( OpenMesh::IO::IOManager( ).can_read( "gml" ) )
	{
		sw.start( );
		bReadOK = OpenMesh::IO::read_mesh( oMesh, oInFile.GetName( ) );
		cout << "Reading took " << sw.stop( ) << endl;

		if( bReadOK )
			cout << "Successfully read mesh from file " << oInFile.GetLocalName( ) << endl;
		else
			cerr << "[Error] Could not read mesh from file " << oInFile.GetLocalName( ) << endl;
	}
	else
		cerr << "[Error] IOManager can not read extension 'gml', available reader extensions are: " << OpenMesh::IO::IOManager( ).qt_read_filters( ) << endl;

	if( OpenMesh::IO::IOManager( ).can_write( "skp" ) )
	{
		sw.start( );
		bWriteOK = OpenMesh::IO::write_mesh( oMesh, oOutFile.GetName( ) );
		cout << "Writing took " << sw.stop( ) << endl;

		if( bWriteOK )
			cout << "Successfully written mesh to file " << oOutFile.GetLocalName( ) << endl;
		else
			cerr << "[Error] Could not write mesh to file " << oOutFile.GetLocalName( ) << endl;
	}
	else
		cerr << "[Error] IOManager can not write extension 'skp', available writer extensions are: " << OpenMesh::IO::IOManager( ).qt_write_filters( ) << endl;

	return 0;
}
