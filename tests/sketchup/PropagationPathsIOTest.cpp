#include <ITAGeo/Base.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>
#include <cassert>

using namespace std;
using namespace ITAGeo;

int main( int, char** )
{
	string sName            = "Building_FlatRoof";
	string sPathListInFile  = sName + "_PathList.json";
	string sGeoModelInFile  = sName + ".skp";
	string sGeoModelOutFile = sName + "_vis.skp";

	// Load Path list
	CPropagationPathList oPathList;
	Utils::JSON::Import( oPathList, sPathListInFile ); // TODO: The 'class' and the type string in the json file do not match. Error in json or in code?

	// Load SketchUp geo model
	SketchUp::CModel oGeoModel;
	oGeoModel.Load( sGeoModelInFile );

	// Load paths in geo model
	for( auto path: oPathList )
	{
		assert( path.size( ) >= 2 );
		oGeoModel.AddPropagationPathVisualization( path, path.GetTypeStr( ) + ": " + path.sIdentifier );
	}

	// Store SketchUp geo model with included paths
	oGeoModel.Store( sGeoModelOutFile );

	return 0;
}