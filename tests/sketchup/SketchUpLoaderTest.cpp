/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAStopWatch.h>
#include <VistaBase/VistaVector3D.h>
#include <VistaMath/VistaGeometries.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <assert.h>
#include <iostream>

using namespace std;
using namespace ITAGeo;

void piptest( ); // why is a pont-in-polygon test in here? @todo move
void load_sketchup_file( );

int main( int, char** )
{
	if( false )
		piptest( );
	if( true )
	{
		try
		{
			load_sketchup_file( );
		}
		catch( ITAException& e )
		{
			cerr << e << endl;
			return 255;
		}
	}

	return 0;
}

void load_sketchup_file( )
{
	string sSKPFilePath = "SolidCube2017.skp";

	VistaFileSystemFile oFile( sSKPFilePath );
	assert( oFile.Exists( ) );

	// CITAGeoHalfedgeMeshModel oModel;
	// if( oModel.Load( sSKPFilePath ) )
	//	cout << "Sucessfully loaded file '" << sSKPFilePath << "' as an halfedge model" << endl;

	// write model to .skp
	/*oModel.Store("SolidCube2017_out.skp");
	oModel.Load("SolidCube2017_out.skp");
	oModel.Store("SolidCube2017_out.off");*/

	SketchUp::CModel model;
	model.Load( "SolidCube2017.skp" );

	CPropagationPath oPropPath;
	oPropPath.resize( 4 );
	oPropPath[0] = std::make_shared<CPropagationAnchor>( VistaVector3D( (double)0, (double)-10, (double)0 ) );
	oPropPath[1] = std::make_shared<CPropagationAnchor>( VistaVector3D( (double)0, (double)-10, (double)100 ) );
	oPropPath[2] = std::make_shared<CPropagationAnchor>( VistaVector3D( (double)100, (double)-10, (double)100 ) );
	oPropPath[3] = std::make_shared<CPropagationAnchor>( VistaVector3D( (double)100, (double)-10, (double)0 ) );
	model.AddPropagationPathVisualization( oPropPath, "test" );

	model.Store( "SolidCube2019_out.skp" );


	/*test writer
	if (oModel.SaveSUModel("SolidCube2017.skp"))
	cout << "Sucessfully wrote SolidCube2017.skp" << endl;
	if (oModel.LoadSUModel("SolidCube2017.skp"))
	cout << "Sucessfully loaded SolidCube2017.skp" << endl;
	if (oModel.SaveSUModel("SolidCube2017.off"))
	cout << "Sucessfully wrote SolidCube2017.off" << endl;
	//oModel.GetNumFaces();*/

	return;
}

void piptest( )
{
	ITAStopWatch sw;

	std::vector<VistaVector3D*> vVertices;
	vVertices.push_back( new VistaVector3D( 9.0f, 10.0f, -21.0f ) );
	vVertices.push_back( new VistaVector3D( -10.0f, 11.0f, -20.0f ) );
	vVertices.push_back( new VistaVector3D( 0.2f, -5, -22.0f ) );

	VistaPolygon polygon( vVertices );

	VistaVector3D vDirection( 0.3f, 0.7f, -9.4f, 1.0f );
	vDirection.Normalize( );

	VistaRay vRay1( VistaVector3D( ), vDirection );
	VistaRay vRay2( VistaVector3D( ), -vDirection );

	VistaVector3D vContactPoint1, vContactPoint2;
	float fScalarProduct;
	sw.start( );
	vRay1.IntersectionTriangle( *( vVertices[0] ), *( vVertices[1] ), *( vVertices[2] ), vContactPoint1 );
	cout << "Intersection test: " << std::to_string( long double( sw.stop( ) ) ) << endl;
	fScalarProduct = vRay1.GetDir( ) * polygon.GetUpVector( );
	vRay2.IntersectionTriangle( *( vVertices[0] ), *( vVertices[2] ), *( vVertices[1] ), vContactPoint2 );
	fScalarProduct = vRay2.GetDir( ) * polygon.GetUpVector( );

	// Extended function returning face normal
	float fPenDepths, fBarycentricAB, fBaricentricAC;
	VistaVector3D vContactPoint3, vFaceNormal;
	sw.start( );
	bool bRet3 =
	    vRay1.IntersectionTriangle( *( vVertices[0] ), *( vVertices[1] ), *( vVertices[2] ), vContactPoint3, vFaceNormal, fPenDepths, fBarycentricAB, fBaricentricAC );
	cout << "Extended intersection test with face normal: " << std::to_string( long double( sw.stop( ) ) ) << endl;

	// Check direction with face culling
	fScalarProduct                 = vFaceNormal * vDirection;
	bool bFaceBacksideIntersection = ( fScalarProduct < 0 );
	if( bRet3 && !bFaceBacksideIntersection )
		cout << "Ray intersection detected." << endl;
	else
		cout << "No ray intersection detected." << endl;

	for( size_t i = 0; i < vVertices.size( ); i++ )
		delete vVertices[i];

	return;
}
