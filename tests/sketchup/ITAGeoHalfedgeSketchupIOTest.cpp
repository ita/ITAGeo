/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAStopWatch.h>
#include <VistaTools/VistaFileSystemDirectory.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <assert.h>

using namespace std;
using namespace ITAGeo;

int main( int iArgsIn, char** ppcArgs )
{
	if( iArgsIn < 3 )
	{
		cout << "Syntax: infile.skp outfile.skp" << endl;
		return 255;
	}

	VistaFileSystemFile oInFile( ppcArgs[1] );
	assert( oInFile.Exists( ) );

	oInFile.GetLocalName( );

	VistaFileSystemFile oOutFile( ppcArgs[2] );
	if( oOutFile.Exists( ) )
		cout << "[Warning] Output file exists, will overwrite." << endl;

	VistaFileSystemDirectory oParentDir( oOutFile.GetParentDirectory( ) );
	assert( oParentDir.Exists( ) );

	ITAStopWatch sw;

	Halfedge::CMeshModel oModel;

	bool bReadOK = false, bWriteOK = false;
	if( Halfedge::CMeshModel::GetSupportedInputFormat( "skp" ) )
	{
		sw.start( );
		bReadOK = oModel.Load( oInFile.GetName( ) );
		cout << "Reading took " << sw.stop( ) << endl;

		if( bReadOK )
			cout << "Successfully read mesh from file " << oInFile.GetLocalName( ) << endl;
		else
			cerr << "[Error] Could not read mesh from file " << oInFile.GetLocalName( ) << endl;
	}
	else
	{
		string s = Halfedge::CMeshModel::GetSupportedInputFormats( );
		cerr << "[Error] IOManager can not read extension 'skp', supported formats:" << endl << s << endl;
	}

	if( Halfedge::CMeshModel::GetSupportedOutputFormat( "skp" ) )
	{
		sw.start( );
		bWriteOK = oModel.Store( oOutFile.GetName( ) );
		cout << "Writing took " << sw.stop( ) << endl;

		if( bWriteOK )
			cout << "Successfully written mesh to file " << oOutFile.GetLocalName( ) << endl;
		else
			cerr << "[Error] Could not write mesh to file " << oOutFile.GetLocalName( ) << endl;
	}
	else
	{
		string s = Halfedge::CMeshModel::GetSupportedOutputFormats( );
		cerr << "[Error] IOManager can not write extension 'skp', supported formats:" << endl << s << endl;
	}

	return 0;
}
