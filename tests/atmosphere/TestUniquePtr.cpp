#include <iostream>
#include <memory>

class IBaseProfile
{
public:
	inline IBaseProfile( ) : IBaseProfile( 0, 0 ) {}
	inline IBaseProfile( const int& iType, const int& iClassType ) : iProfileType( iType ), iClassType( iClassType ) {}


	// IBaseProfile(IBaseProfile&& oOther) {}
	// IBaseProfile& operator=(IBaseProfile&& oOther) { return *this; }

	inline void MyFunc( ) {}
	inline static int StaticFunc( ) { return 0; }

protected:
	int iProfileType;
	int iClassType;
};

class ISpecialProfile : public IBaseProfile
{
public:
	ISpecialProfile( ) = default;
	inline ISpecialProfile( const int& iType, const int& iCType ) : IBaseProfile( iType, iCType ) {}
	virtual void abstact( ) = 0;

	// ISpecialProfile(ISpecialProfile&& oOther) {}
	// ISpecialProfile& operator=(ISpecialProfile&& oOther) { return *this; }
};
class CProfile : public ISpecialProfile
{
public:
	inline CProfile( ) : ISpecialProfile( 1, 2 ) {}

	// CProfile(CProfile&& oOther) {}
	// CProfile& operator=(ISpecialProfile&& oOther) { return *this; }


	virtual void abstact( ) { std::cout << "Hello World" << std::endl; }
};
class CAtmosphere
{
	std::unique_ptr<ISpecialProfile> m_bar;

public:
	CAtmosphere( std::unique_ptr<ISpecialProfile> bar ) : m_bar( std::move( bar ) ) {}

	CAtmosphere( CAtmosphere&& ) {}
	CAtmosphere& operator=( CAtmosphere&& ) { return *this; }
};
int main( )
{
	auto atmos = CAtmosphere( std::make_unique<CProfile>( ) );
	return 0;
}