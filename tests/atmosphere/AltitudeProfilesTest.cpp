/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAGeo/Atmosphere/HumidityProfiles.h>
#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAGeo/Atmosphere/TemperatureProfiles.h>
#include <ITAGeo/Atmosphere/WindProfiles.h>
#include <ITAStopWatch.h>
#include <StratifiedAtmosphere_instrumentation.h>

// #include <VistaTools/VistaFileSystemDirectory.h>
// #include <VistaTools/VistaFileSystemFile.h>
#include <VistaBase/VistaVector3D.h>
#include <assert.h>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <stdio.h>
#include <vector>

using namespace std;
using namespace ITAGeo;


//---Forward declarations
bool testTemperatureProfiles( );
bool testTemperatureProfile( TemperatureProfiles::ITemperatureProfile& profile, vector<double>& altitudes, vector<double>& staticPressure, vector<double>& temperature,
                             vector<double>& temperatureGradient );
bool testTemperatureProfile( TemperatureProfiles::ITemperatureProfile& profile, vector<double>& altitudes, double constStaticPressure, double constTemperature );

bool testWindProfiles( );
bool testWindProfile( WindProfiles::IWindProfile& profile, vector<double>& altitudes, vector<VistaVector3D>& windVectors, vector<VistaVector3D>& windGradientVectors );
bool testWindProfile( WindProfiles::IWindProfile& profile, vector<double>& altitudes, vector<double>& windVelocities, vector<double>& windGradient,
                      VistaVector3D& windDirection );
bool testWindProfile( WindProfiles::IWindProfile& profile, vector<double>& altitudes, double windVelocity, VistaVector3D& windDirection );

bool testHumidityProfiles( );
bool testHumidityProfile( HumidityProfiles::IHumidityProfile& profile, vector<double>& altitudes, double constHumidity );
bool testHumidityProfile( HumidityProfiles::IHumidityProfile& profile, vector<double>& altitudes, vector<double>& humidity );

bool testImportProfile( );
bool testImportPrecalculationProfile( );


void exportAtmosphere( const CStratifiedAtmosphere& atmos, const vector<double>& altitudes, const string& filename )
{
	ofstream myfile( filename );
	myfile << std::fixed << std::setprecision( 40 );

	myfile << "Altitude [m]"
	       << ", "
	       << "Rel. Humidity [%]"
	       << ", "
	       << "Static Pressure [Pa]"
	       << ", "
	       << "Temperature [K]"
	       << ", "
	       << "Temperature Gradient [K/m]"
	       << ", "
	       << "Wind Velocity [m/s]"
	       << ", "
	       << "Wind Velocity Gradient [1/s]"
	       << ", "
	       << "Speed of Sound [m/s]"
	       << ", "
	       << "Speed of Sound Gradient [1/s]" << endl;

	for( int idx = 0; idx < altitudes.size( ); idx++ )
	{
		const double z = altitudes[idx];
		myfile << z << "," << atmos.RelativeHumidity( z ) << ", " << atmos.StaticPressure( z ) << ", " << atmos.Temperature( z ) << ", " << atmos.TemperatureGradient( z )
		       << ", " << atmos.WindVector( z )[Vista::X] << ", " << atmos.WindVectorGradient( z )[Vista::X] << ", " << atmos.SpeedOfSound( z ) << ", "
		       << atmos.SpeedOfSoundGradient( z ) << endl;
	}
}

void exportTestParameters( )
{
	auto humidProfile          = std::make_shared<HumidityProfiles::CConstant>( 50 );
	auto tempProfile           = std::make_shared<TemperatureProfiles::CRoom>( );
	auto windProfile           = std::make_shared<WindProfiles::CZero>( );
	auto homogeneousAtmosphere = CStratifiedAtmosphere( tempProfile, windProfile, humidProfile );

	auto humidProfile2           = std::make_shared<HumidityProfiles::CConstant>( 50 );
	auto tempProfile2            = std::make_shared<TemperatureProfiles::CISA>( );
	auto windProfile2            = std::make_shared<WindProfiles::CLog>( 0.6, 0.1, VistaVector3D( 1, 0, 0 ) );
	auto inhomogeneousAtmosphere = CStratifiedAtmosphere( tempProfile2, windProfile2, humidProfile2 );


	vector<double> altitudes;
	for( double z = 0; z < 10.005; z += 0.1 )
		altitudes.push_back( z );
	for( int z = 11; z <= 10000; z++ )
		altitudes.push_back( z );

	exportAtmosphere( homogeneousAtmosphere, altitudes, "AltitudeProfileTest_homogeneous.txt" );
	exportAtmosphere( inhomogeneousAtmosphere, altitudes, "AltitudeProfileTest_inhomogeneous.txt" );
}


int main( int iArgsIn, char** ppcArgs )
{
	IHTA::Instrumentation::LoggerRegistry::create_default_sinks( );

	cout << "Test Temperature Profiles:" << endl;
	if( testTemperatureProfiles( ) )
		cout << "OK" << endl;
	else
		cout << "ERROR" << endl;
	cout << endl << endl;

	cout << "Test Wind Profiles:" << endl;
	if( testWindProfiles( ) )
		cout << "OK" << endl;
	else
		cout << "ERROR" << endl;
	cout << endl << endl;

	cout << "Test Humidity Profiles:" << endl;
	if( testHumidityProfiles( ) )
		cout << "OK" << endl;
	else
		cout << "ERROR" << endl;
	cout << endl << endl;

	if( testImportProfile( ) )
		cout << "OK" << endl;
	else
		cout << "ERROR" << endl;
	cout << endl << endl;

	cout << "Test Temperature Profiles with precalculation:" << endl;
	if( testImportPrecalculationProfile( ) )
		cout << "OK" << endl;
	else
		cout << "ERROR" << endl;
	cout << endl << endl;

	exportTestParameters( );

	return 0;
}

bool testTemperatureProfiles( )
{
	bool ok = true;
	try
	{
		const int nAltitudes     = 6;
		vector<double> altitudes = { 0, 1, 100, 500, 1000, 2000 };

		double constTemperature    = 11;
		double constStaticPressure = 12;
		auto constProfile          = TemperatureProfiles::CConstant( constTemperature, constStaticPressure );

		double roomTemperature    = 293.15;
		double roomStaticPressure = 101325;
		auto roomProfile          = TemperatureProfiles::CRoom( );

		auto isaProfile                       = TemperatureProfiles::CISA( );
		vector<double> isaTemperatures        = { 288.15, 288.1435, 287.5, 284.9, 281.65, 275.15 };
		vector<double> isaTemperatureGradient = vector<double>( nAltitudes, -0.0065 );
		vector<double> isaPressures = { 101325, 1.013129869269873e+05, 1.001293879491707e+05, 9.546059691667747e+04, 8.987411140590039e+04, 7.949439387767743e+04 };

		cout << "Constant Temperature Model:" << endl;
		ok = ok && testTemperatureProfile( constProfile, altitudes, constStaticPressure, constTemperature );
		cout << "Room Temperature Model:" << endl;
		ok = ok && testTemperatureProfile( roomProfile, altitudes, roomStaticPressure, roomTemperature );
		cout << "ISA Model:" << endl;
		ok = ok && testTemperatureProfile( isaProfile, altitudes, isaPressures, isaTemperatures, isaTemperatureGradient );
	}
	catch( ITAException err )
	{
		cout << err << endl;
		return false;
	}
	return ok;
}
bool testTemperatureProfile( TemperatureProfiles::ITemperatureProfile& profile, vector<double>& altitudes, double constStaticPressure, double constTemperature )
{
	auto staticPressure      = vector<double>( altitudes.size( ), constStaticPressure );
	auto temperature         = vector<double>( altitudes.size( ), constTemperature );
	auto temperatureGradient = vector<double>( altitudes.size( ), 0 );
	return testTemperatureProfile( profile, altitudes, staticPressure, temperature, temperatureGradient );
}
bool testTemperatureProfile( TemperatureProfiles::ITemperatureProfile& profile, vector<double>& altitudes, vector<double>& staticPressure, vector<double>& temperature,
                             vector<double>& temperatureGradient )
{
	bool ok = true;
	cout << "Altitude [m]"
	     << "\t"
	     << "Delta p0 [Pa]"
	     << "\t"
	     << "Delta T [K]"
	     << "\t"
	     << "Delta grad(T) [K/m]" << endl;
	for( int idx = 0; idx < altitudes.size( ); idx++ )
	{
		double deltaP     = profile.StaticPressure( altitudes[idx] ) - staticPressure[idx];
		double deltaT     = profile.Temperature( altitudes[idx] ) - temperature[idx];
		double deltaGradT = profile.TemperatureGradient( altitudes[idx] ) - temperatureGradient[idx];
		ok                = ok && deltaT < 0.0001 && deltaP < 0.0001;
		cout << altitudes[idx] << "\t\t" << deltaP << "\t\t" << deltaT << "\t\t" << deltaGradT << endl;
	}
	cout << "----------" << endl;
	return ok;
}

bool testWindProfiles( )
{
	bool ok = true;
	try
	{
		const int nAltitudes     = 6;
		vector<double> altitudes = { 0, 1, 100, 500, 1000, 2000 };

		auto windDir             = VistaVector3D( 1, 1, 0 );
		double constWindVelocity = 11;
		auto constProfile        = WindProfiles::CConstant( constWindVelocity, windDir );

		double zeroWind  = 0;
		auto zeroProfile = WindProfiles::CZero( );

		double frictionVelocity            = 0.6;
		double surfaceRoughness            = 1;
		auto logProfile                    = WindProfiles::CLog( frictionVelocity, surfaceRoughness, windDir );
		vector<double> logVelocities       = { 0, 0, 6.9078, 9.3219, 10.3616, 11.4014 };
		vector<double> logVelocityGradient = { 0, 0, 0.015, 0.003, 0.0015, 7.5e-04 };

		cout << "Constant Wind:" << endl;
		ok = ok && testWindProfile( constProfile, altitudes, constWindVelocity, windDir );
		cout << "Zero Wind:" << endl;
		ok = ok && testWindProfile( zeroProfile, altitudes, zeroWind, windDir );
		cout << "Log Profile:" << endl;
		ok = ok && testWindProfile( logProfile, altitudes, logVelocities, logVelocityGradient, windDir );
	}
	catch( ITAException err )
	{
		cout << err << endl;
		return false;
	}
	return ok;
}

bool testWindProfile( WindProfiles::IWindProfile& profile, vector<double>& altitudes, double windVelocity, VistaVector3D& windDirection )
{
	auto windVelocities = vector<double>( altitudes.size( ), windVelocity );
	auto windGradient   = vector<double>( altitudes.size( ), 0 );
	return testWindProfile( profile, altitudes, windVelocities, windGradient, windDirection );
}
bool testWindProfile( WindProfiles::IWindProfile& profile, vector<double>& altitudes, vector<double>& windVelocities, vector<double>& windGradient,
                      VistaVector3D& windDirection )
{
	windDirection            = windDirection.GetNormalized( );
	auto windVectors         = vector<VistaVector3D>( altitudes.size( ) );
	auto windGradientVectors = vector<VistaVector3D>( altitudes.size( ) );
	for( int idx = 0; idx < altitudes.size( ); idx++ )
	{
		windVectors[idx]         = windDirection * windVelocities[idx];
		windGradientVectors[idx] = windDirection * windGradient[idx];
	}

	return testWindProfile( profile, altitudes, windVectors, windGradientVectors );
}
bool testWindProfile( WindProfiles::IWindProfile& profile, vector<double>& altitudes, vector<VistaVector3D>& windVectors, vector<VistaVector3D>& windGradientVectors )
{
	bool ok = true;
	cout << "Altitude [m]"
	     << "\t\t"
	     << "Delta v [m/s]"
	     << "\t\t"
	     << "Delta dv/dz [1/s]" << endl;
	for( int idx = 0; idx < altitudes.size( ); idx++ )
	{
		VistaVector3D deltaV     = profile.WindVector( altitudes[idx] ) - windVectors[idx];
		VistaVector3D deltaVGrad = profile.WindVectorGradient( altitudes[idx] ) - windGradientVectors[idx];
		ok                       = ok && deltaVGrad.GetLength( ) < 0.0001 && deltaV.GetLength( ) < 0.0001;
		cout << altitudes[idx] << "\t\t" << deltaV << "\t\t" << deltaVGrad << endl;
	}
	cout << "----------" << endl;
	return ok;
}


bool testHumidityProfiles( )
{
	bool ok = true;
	try
	{
		const int nAltitudes     = 6;
		vector<double> altitudes = { 0, 1, 100, 500, 1000, 2000 };

		double constHumidity = 7;
		auto constProfile    = HumidityProfiles::CConstant( constHumidity );

		cout << "Constant Humidity Model:" << endl;
		ok = ok && testHumidityProfile( constProfile, altitudes, constHumidity );
	}
	catch( ITAException err )
	{
		cout << err << endl;
		return false;
	}
	return ok;
}
bool testHumidityProfile( HumidityProfiles::IHumidityProfile& profile, vector<double>& altitudes, double constHumidity )
{
	auto humidity = vector<double>( altitudes.size( ), constHumidity );
	return testHumidityProfile( profile, altitudes, humidity );
}
bool testHumidityProfile( HumidityProfiles::IHumidityProfile& profile, vector<double>& altitudes, vector<double>& humidity )
{
	bool ok = true;
	cout << "Altitude [m]"
	     << "\t"
	     << "Delta h [%]" << endl;
	for( int idx = 0; idx < altitudes.size( ); idx++ )
	{
		double deltaH = profile.RelativeHumidity( altitudes[idx] ) - humidity[idx];
		ok            = ok && deltaH < 0.01;
		cout << altitudes[idx] << "\t\t" << deltaH << endl;
	}
	cout << "----------" << endl;
	return ok;
}

bool testImportProfile( )
{
	bool ok                         = true;
	vector<double> vfAltitudes      = { 0, 100, 300, 500, 750, 1000 };
	vector<double> vfWeatherData1   = { 0, 1, 2, 3, 4, 5 };
	vector<double> vfWeatherData2   = { 1, 4, 9, 16, 25, 40 };
	vector<double> vfAzimuthDegData = { 0, 0, 45, 90, 100, 105 };

	cout << "Imported Profiles:" << endl;
	try
	{
		auto importedHumidity    = HumidityProfiles::CImport( vfAltitudes, vfWeatherData1 );
		auto importedTemperature = TemperatureProfiles::CImport( vfAltitudes, vfWeatherData1, vfWeatherData2 );
		auto importedWind        = WindProfiles::CImport( vfAltitudes, vfWeatherData1, vfAzimuthDegData );

		const double dEvalValue1 = 250.0;
		const double dEvalValue2 = 350.0;

		cout << "Humidity at " << dEvalValue1 << "m: " << importedHumidity.RelativeHumidity( dEvalValue1 ) << endl;

		cout << "Static pressure at " << dEvalValue2 << "m: " << importedTemperature.StaticPressure( dEvalValue2 ) << endl;
		cout << "Temperature at " << dEvalValue2 << "m: " << importedTemperature.Temperature( dEvalValue2 ) << endl;
		cout << "Temperature grad at " << dEvalValue2 << "m: " << importedTemperature.TemperatureGradient( dEvalValue2 ) << endl;

		cout << "Wind vector at " << dEvalValue2 << "m: " << importedWind.WindVector( dEvalValue2 ) << endl;
		cout << "Wind velocity at " << dEvalValue2 << "m: " << importedWind.WindVector( dEvalValue2 ).GetLength( ) << endl;
		cout << "Wind vector grad at " << dEvalValue2 << "m: " << importedWind.WindVectorGradient( dEvalValue2 ) << endl;
		cout << "Wind vector grad length at " << dEvalValue2 << "m: " << importedWind.WindVectorGradient( dEvalValue2 ).GetLength( ) << endl;
	}
	catch( ITAException err )
	{
		cout << err << endl;
		return false;
	}
	return ok;
}


bool testImportPrecalculationProfile( )
{
	bool ok = true;
	cout << "Imported Profiles with precalculation:" << endl;

	vector<double> vfAltitudes      = { 0, 100, 300, 500, 750, 1000, 5000, 15000 };
	vector<double> vfWeatherData1   = { 0, 1, 2, 3, 4, 5, 6, 7 };
	vector<double> vfWeatherData2   = { 1, 4, 9, 16, 25, 40, 80, 200 };
	vector<double> vfAzimuthDegData = { 0, 0, 45, 90, 100, 105, 110, 110 };
	auto pHumidProfile              = std::make_shared<HumidityProfiles::CImport>( vfAltitudes, vfWeatherData1 );
	auto pTempProfile               = std::make_shared<TemperatureProfiles::CImport>( vfAltitudes, vfWeatherData1, vfWeatherData2 );
	auto pWindProfile               = std::make_shared<WindProfiles::CImport>( vfAltitudes, vfWeatherData1, vfAzimuthDegData );


	auto pHumidProfilePrecalc = std::make_shared<HumidityProfiles::CImport>( vfAltitudes, vfWeatherData1, true );
	auto pTempProfilePrecalc  = std::make_shared<TemperatureProfiles::CImport>( vfAltitudes, vfWeatherData1, vfWeatherData2, true );
	auto pWindProfilePrecalc  = std::make_shared<WindProfiles::CImport>( vfAltitudes, vfWeatherData1, vfAzimuthDegData, true );

	std::cout << "Expecting a logging output warning about exceeding maximum altitude:" << std::endl;
	pHumidProfilePrecalc->RelativeHumidity( 25000 );
	std::cout << std::endl;

	auto oAtmosphere = CStratifiedAtmosphere( pTempProfile, pWindProfile, pHumidProfile );

	const int nAltitutdes = 15001;
	vector<double> vdAltitudes( nAltitutdes );
	for( int z = 0; z < nAltitutdes; z++ )
		vdAltitudes[z] = (double)z;

	std::cout << "Check if data is the same at sampling points. Max deviation in..." << std::endl;
	std::vector<double> vdTemperatureError( vdAltitudes.size( ) );
	std::vector<double> vdTemperatureGradError( vdAltitudes.size( ) );
	std::vector<double> vdWindVelError( vdAltitudes.size( ) );
	std::vector<double> vdWindGradError( vdAltitudes.size( ) );
	double dMaxTemperatureDev = 0, dMaxTemperatureGradDev = 0, dMaxWindVelDev = 0, dMaxWindGradDev = 0;
	for( const double& z: vdAltitudes )
	{
		vdTemperatureError[int( z )]     = pTempProfilePrecalc->Temperature( z ) - pTempProfile->Temperature( z );
		vdTemperatureGradError[int( z )] = pTempProfilePrecalc->TemperatureGradient( z ) - pTempProfile->TemperatureGradient( z );

		vdWindVelError[int( z )]  = ( pWindProfilePrecalc->WindVector( z ) - pWindProfile->WindVector( z ) ).GetLength( );
		vdWindGradError[int( z )] = ( pWindProfilePrecalc->WindVectorGradient( z ) - pWindProfile->WindVectorGradient( z ) ).GetLength( );

		dMaxTemperatureDev     = std::max( dMaxTemperatureDev, std::abs( vdTemperatureError[int( z )] ) );
		dMaxTemperatureGradDev = std::max( dMaxTemperatureGradDev, std::abs( vdTemperatureGradError[int( z )] ) );
		dMaxWindVelDev         = std::max( dMaxWindVelDev, std::abs( vdWindVelError[int( z )] ) );
		dMaxWindGradDev        = std::max( dMaxWindGradDev, std::abs( vdWindGradError[int( z )] ) );
	}
	std::cout << "Temperature: " << dMaxTemperatureDev << "\tTemperature-Grad:" << dMaxTemperatureGradDev << std::endl;
	std::cout << "Wind: " << dMaxWindVelDev << "\tWind-Grad:" << dMaxWindGradDev << std::endl << std::endl;
	ok = dMaxTemperatureDev < DBL_EPSILON && dMaxTemperatureGradDev < DBL_EPSILON && dMaxWindVelDev < DBL_EPSILON && dMaxWindGradDev < DBL_EPSILON;


	std::cout << "Benchmarks:" << std::endl;
	ITAStopWatch swTempNormal, swTempPrecalc, swWindNormal, swWindPrecalc;
	for( const double& z: vdAltitudes )
	{
		swTempNormal.start( );
		pTempProfile->Temperature( z );
		pTempProfile->TemperatureGradient( z );
		swTempNormal.stop( );
		swTempPrecalc.start( );
		pTempProfilePrecalc->Temperature( z );
		pTempProfilePrecalc->TemperatureGradient( z );
		swTempPrecalc.stop( );

		swWindNormal.start( );
		pWindProfile->WindVector( z );
		pWindProfile->WindVectorGradient( z );
		swWindNormal.stop( );
		swWindPrecalc.start( );
		pWindProfilePrecalc->WindVector( z );
		pWindProfilePrecalc->WindVectorGradient( z );
		swWindPrecalc.stop( );
	}
	std::cout << "Temperature:" << std::endl << "Normal: " << swTempNormal.mean( ) << ", Precalc: " << swTempPrecalc.mean( ) << std::endl;
	std::cout << "Wind:" << std::endl << "Normal: " << swWindNormal.mean( ) << ", Precalc: " << swWindPrecalc.mean( ) << std::endl;

	return ok;
}