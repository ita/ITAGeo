/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAGeo/Atmosphere/HumidityProfiles.h>
#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAGeo/Atmosphere/TemperatureProfiles.h>
#include <ITAGeo/Atmosphere/WindProfiles.h>
#include <ITAStopWatch.h>
#include <VistaBase/VistaVector3D.h>
#include <memory>

using namespace std;
using namespace ITAGeo;


//---Forward declarations
bool testInputParameterFilter( );
bool testCreationOfStratifiedMedium( );

int main( int iArgsIn, char** ppcArgs )
{
	cout << "Atmosphere parameter filter test:" << endl;
	if( testInputParameterFilter( ) )
		cout << "OK" << endl;
	else
		cout << "ERROR" << endl;
	cout << endl << endl;


	cout << "Test Generation of valid atmosphere:" << endl;
	if( testCreationOfStratifiedMedium( ) )
		cout << "OK" << endl;
	else
		cout << "ERROR" << endl;
	cout << endl << endl;

	return 0;
}

bool testInputParameterFilter( )
{
	int numExceptions = 0;
	try
	{
		auto windProfile = HumidityProfiles::CConstant( 200 );
	}
	catch( ITAException err )
	{
		numExceptions++;
	}

	try
	{
		auto windProfile = WindProfiles::CLog( 0.6, 0.1, VistaVector3D( 1, 0, 1 ) );
	}
	catch( ITAException err )
	{
		numExceptions++;
	}
	try
	{
		auto windProfile = WindProfiles::CConstant( 1, VistaVector3D( 0, 0, 2 ) );
	}
	catch( ITAException err )
	{
		numExceptions++;
	}

	return numExceptions == 3;
}

bool testCreationOfStratifiedMedium( )
{
	try
	{
		auto tempProfile  = std::make_shared<TemperatureProfiles::CISA>( );
		auto windProfile  = std::make_shared<WindProfiles::CLog>( 0.6, 0.1, VistaVector3D( 1, 0, 0 ) );
		auto humidProfile = std::make_shared<HumidityProfiles::CConstant>( 0.5 );

		auto atmosphere = CStratifiedAtmosphere( tempProfile, windProfile, humidProfile );
	}
	catch( exception )
	{
		return false;
	}

	return true;
}