function AltitudeProfilesTest()

%% Define Atmospheres

inhomAtmos = itaAtmosphere;

%Settings
%(These are the standard settings, itaAtmosphere is already initialized with these values)
inhomAtmos.windProfile = WindProfile.Log; %Enum: Zero, Const, Log
inhomAtmos.tempProfile = TempProfile.ISA; %Enum: Const, ISA

inhomAtmos.z0 = 0.1;                 %Surface Roughness for Log Wind Profile [m]
inhomAtmos.kappa = 0.4;              %Karman constant for Log Wind Profile []
inhomAtmos.vStar = 0.6;              %Friction velocity for Log Wind Profile [m/s]

inhomAtmos.vDirConst = [1 0 0];      %Normal in wind direction []

inhomAtmos.R = 1.4;                  %Air Gas Constant, used to calculate c
inhomAtmos.gamma = 402.8/1.4;        %Ratio of Specific Heats, used to calculate c

homAtmos = inhomAtmos;
homAtmos.windProfile = WindProfile.Zero;
homAtmos.tempProfile = TempProfile.Const;
homAtmos.TConst = 293.15;
homAtmos.constStaticPressure = 101325;

%% Init Data
altitudes = [0:0.1:10  11:10000];

homMatlabData = getMatlabAtmosphereData(altitudes, homAtmos);
homCppData = importCppAtmosphereData('AltitudeProfileTest_homogeneous.txt');

inhomMatlabData = getMatlabAtmosphereData(altitudes, inhomAtmos);
inhomCppData = importCppAtmosphereData('AltitudeProfileTest_inhomogeneous.txt');

%% Plots
createPlots(homMatlabData, homCppData, 'Homogeneous atmosphere')
createPlots(inhomMatlabData, inhomCppData, 'Inhomogeneous atmosphere')

function createPlots(matlabDataMatrix, cppDataMatrix, titleStr)
deltaMatrix = cppDataMatrix - matlabDataMatrix;
deltaZ = abs(deltaMatrix(:,1));
if any(deltaZ > 1e-10)
    warning('Large mismatch in altitude values')
end

z = matlabDataMatrix(:,1);

deltaHumidity = deltaMatrix(:,2);
deltaP0 = deltaMatrix(:,3);
deltaT = deltaMatrix(:,4);
deltaGradT = deltaMatrix(:,5);
deltaV = deltaMatrix(:,6);
deltaGradV = deltaMatrix(:,7);
deltaC = deltaMatrix(:,8);
deltaGradC = deltaMatrix(:,9);

deltaPlot(z, deltaHumidity, '\Delta h [%]', titleStr);
deltaPlot(z, deltaP0, '\Delta p_0 [Pa]', titleStr);
deltaPlot(z, deltaT, '\Delta T [K]', titleStr);
deltaPlot(z, deltaGradT, '\Delta grad(T) [K/m]', titleStr);
deltaPlot(z, deltaV, '\Delta v [m/s]', titleStr);
deltaPlot(z, deltaGradV, '\Delta grad(v) [1/m]', titleStr);
deltaPlot(z, deltaC, '\Delta c [m/s]', titleStr);
deltaPlot(z, deltaGradC, '\Delta grad(c) [1/m]', titleStr);

deltaPlot(z, deltaHumidity./matlabDataMatrix(:,2)*100, '\eta h [%]', titleStr);
deltaPlot(z, deltaP0./matlabDataMatrix(:,3)*100, '\eta p_0 [%]', titleStr);
deltaPlot(z, deltaT./matlabDataMatrix(:,4)*100, '\eta T [%]', titleStr);
deltaPlot(z, deltaGradT./matlabDataMatrix(:,5)*100, '\eta grad(T) [%]', titleStr);
deltaPlot(z, deltaV./matlabDataMatrix(:,6)*100, '\eta v [%]', titleStr);
deltaPlot(z, deltaGradV./matlabDataMatrix(:,7)*100, '\eta grad(v) [%]', titleStr);
deltaPlot(z, deltaC./matlabDataMatrix(:,8)*100, '\eta c [%]', titleStr);
deltaPlot(z, deltaGradC./matlabDataMatrix(:,9)*100, '\eta grad(c) [%]', titleStr);

function deltaPlot(z, deltaValues, ylabelStr, titleStr, deltaValues2, ylabelStr2)

if nargin == 4; deltaValues2 = 0; end
if ~any(abs(deltaValues) > 0) && ~any(abs(deltaValues2) > 0)
    return;
end

figure;
plot(z, deltaValues);
xlabel('z [m]'); ylabel(ylabelStr);
title(titleStr);

if nargin == 4; return; end

yyaxis right
plot(z, deltaValues2)
ylabel(ylabelStr2)


function [dataMatrix] = getMatlabAtmosphereData(altitudes, atmosphere)

dataMatrix = zeros(numel(altitudes), 9);

for idx = 1:numel(altitudes)
    z = altitudes(idx);
    dataMatrix(idx, 1) = z;
    dataMatrix(idx, 2) = atmosphere.humidity(z);
    dataMatrix(idx, 3) = atmosphere.staticPressure(z);
    dataMatrix(idx, 4) = atmosphere.T(z);
    %dataMatrix(idx, 5) = atmosphere.(z);
    dataMatrix(idx, 6) = atmosphere.windVec(z)*[1 0 0]';
    dataMatrix(idx, 7) = atmosphere.derivedWindVec(z) * [1 0 0]';
    dataMatrix(idx, 8) = atmosphere.speedOfSound(z);
    dataMatrix(idx, 9) = atmosphere.derivedSpeedOfSound(z);
end

function dataMatrix = importCppAtmosphereData(filename)
dataMatrix = readmatrix(filename);

