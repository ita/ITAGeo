/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAGeo/Atmosphere/HumidityProfiles.h>
#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAGeo/Atmosphere/TemperatureProfiles.h>
#include <ITAGeo/Atmosphere/WindProfiles.h>
#include <ITAGeo/Utils/JSON/Atmosphere.h>
#include <ITAStopWatch.h>

// #include <VistaTools/VistaFileSystemDirectory.h>
// #include <VistaTools/VistaFileSystemFile.h>
#include <VistaBase/VistaVector3D.h>
#include <assert.h>
#include <math.h>
#include <sstream>
#include <stdexcept>
#include <stdio.h>
#include <vector>

using namespace std;
using namespace ITAGeo;


//---Forward declarations
bool testWriteAndReadJSON( );
bool testWriteAndReadJSON( CStratifiedAtmosphere& atmosphere );

bool testImportFromMatlabViaJSON( );
bool testImportFromMatlabViaJSON( const std::string& sFilePath );

bool testImportFromMatlabWithPrecalc( );

int main( int iArgsIn, char** ppcArgs )
{
	cout << "Test Generation of JSON text:" << endl;
	if( testWriteAndReadJSON( ) )
		cout << "SUMMARY: all OK" << endl;
	else
		cout << "SUMMARY: >= 1 ERROR" << endl;
	cout << endl << endl;

	cout << "Test Import of JSON files:" << endl;
	if( testImportFromMatlabViaJSON( ) )
		cout << "SUMMARY: all OK" << endl;
	else
		cout << "SUMMARY: >= 1 ERROR" << endl;
	cout << endl << endl;

	testImportFromMatlabWithPrecalc( );

	return 0;
}

std::string printJSONString( CStratifiedAtmosphere& atmosphere )
{
	cout << "JSON string:" << endl;
	// std::stringstream jsonStr = atmosphere.ToJSON();
	std::stringstream jsonStr = Utils::JSON::Encode( atmosphere );
	cout << jsonStr.str( ) << endl;
	return jsonStr.str( );
}

bool testWriteAndReadJSON( )
{
	bool ok = true;
	try
	{
		auto humidProfile = std::make_shared<HumidityProfiles::CConstant>( 1 );
		auto tempProfile  = std::make_shared<TemperatureProfiles::CConstant>( 4, 2 );
		auto windProfile  = std::make_shared<WindProfiles::CConstant>( 42, VistaVector3D( 1, 0, 0 ) );
		auto atmosphere1  = CStratifiedAtmosphere( tempProfile, windProfile, humidProfile );
		ok                = ok && testWriteAndReadJSON( atmosphere1 );


		auto humidProfile2 = std::make_shared<HumidityProfiles::CConstant>( 2 );
		auto tempProfile2  = std::make_shared<TemperatureProfiles::CRoom>( );
		auto windProfile2  = std::make_shared<WindProfiles::CZero>( );
		auto atmosphere2   = CStratifiedAtmosphere( tempProfile2, windProfile2, humidProfile2 );
		ok                 = ok && testWriteAndReadJSON( atmosphere2 );


		auto humidProfile3 = std::make_shared<HumidityProfiles::CConstant>( 3 );
		auto tempProfile3  = std::make_shared<TemperatureProfiles::CISA>( );
		auto windProfile3  = std::make_shared<WindProfiles::CLog>( 0.6, 0.1, VistaVector3D( 1, 0, 0 ) );
		auto atmosphere3   = CStratifiedAtmosphere( tempProfile3, windProfile3, humidProfile3 );
		ok                 = ok && testWriteAndReadJSON( atmosphere3 );

		vector<double> vdAltitudes    = { 0, 1, 100, 500, 1000, 2000 };
		vector<double> vdWeatherData1 = { 0, 1, 2, 3, 4, 5 };
		vector<double> vdWeatherData2 = { 1, 2, 3, 4, 5, 6 };
		vector<double> vdWeatherData3 = { 2, 3, 4, 5, 6, 7 };
		auto importedHumidity         = std::make_shared<HumidityProfiles::CImport>( vdAltitudes, vdWeatherData1 );
		auto importedTemperature      = std::make_shared<TemperatureProfiles::CImport>( vdAltitudes, vdWeatherData1, vdWeatherData2 );
		auto importedWind             = std::make_shared<WindProfiles::CImport>( vdAltitudes, vdWeatherData1, vdWeatherData2, vdWeatherData3 );
		auto atmosphereImport         = CStratifiedAtmosphere( importedTemperature, importedWind, importedHumidity );
		ok                            = ok && testWriteAndReadJSON( atmosphereImport );
	}
	catch( exception& err )
	{
		return false;
	}

	return ok;
}

bool testWriteAndReadJSON( CStratifiedAtmosphere& atmosphere )
{
	try
	{
		cout << "Encode(): ";
		std::string jsonStr = printJSONString( atmosphere );
		cout << "Encode(): OK" << endl;

		cout << "Decode(): ";
		Utils::JSON::Decode( atmosphere, jsonStr );
		cout << "OK" << endl;
	}
	catch( ITAException& err )
	{
		cout << "ERROR" << endl << err << endl;
		return false;
	}
	catch( exception& err )
	{
		cout << "ERROR" << endl << err.what( ) << endl;
		return false;
	}
	return true;
}

bool testImportFromMatlabViaJSON( )
{
	bool ok = true;
	try
	{
		ok = ok && testImportFromMatlabViaJSON( "atmosphere_const.json" );
		ok = ok && testImportFromMatlabViaJSON( "atmosphere_isa_zero.json" );
		ok = ok && testImportFromMatlabViaJSON( "atmosphere_isa_log.json" );
		ok = ok && testImportFromMatlabViaJSON( "atmosphere_import.json" );
	}
	catch( ITAException& err )
	{
		cout << err << endl;
		ok = false;
	}


	return ok;
}
bool testImportFromMatlabViaJSON( const std::string& sFilePath )
{
	bool ok = true;
	try
	{
		auto humidProfile = std::make_shared<HumidityProfiles::CConstant>( 1 );
		auto tempProfile  = std::make_shared<TemperatureProfiles::CConstant>( 4, 2 );
		auto windProfile  = std::make_shared<WindProfiles::CConstant>( 42, VistaVector3D( 1, 0, 0 ) );

		auto atmosphere = CStratifiedAtmosphere( tempProfile, windProfile, humidProfile );

		Utils::JSON::Import( atmosphere, sFilePath );
		printJSONString( atmosphere );
		cout << "OK" << endl;
	}
	catch( ITAException& err )
	{
		cout << "ERROR" << endl;
		cout << err << endl;
		ok = false;
	}
	return ok;
}

bool testImportFromMatlabWithPrecalc( )
{
	bool ok = true;
	try
	{
		auto atmosphere = CStratifiedAtmosphere( );
		Utils::JSON::Import( atmosphere, "atmosphere_with_precalc.json" );

		atmosphere.RelativeHumidity( 100.1 );
		atmosphere.Temperature( 100.1 );
		atmosphere.WindVector( 100.1 );
		cout << "OK" << endl;
	}
	catch( ITAException& err )
	{
		cout << "ERROR" << endl;
		cout << err << endl;
		ok = false;
	}
	return ok;
}