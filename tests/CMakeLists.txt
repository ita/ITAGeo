cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

project (ITAGeoTest)

# ######################################################################################################################

add_executable (PropagationPathsTest PropagationPathsTest.cpp)
target_link_libraries (PropagationPathsTest PUBLIC ITAGeo::ITAGeo)

set_property (TARGET PropagationPathsTest PROPERTY FOLDER "Tests/ITAGeo")
set_property (TARGET PropagationPathsTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS PropagationPathsTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (ITAGeoHalfedgeMeshModelTest ITAGeoHalfedgeMeshModelTest.cpp)
target_link_libraries (ITAGeoHalfedgeMeshModelTest PUBLIC ITAGeo::ITAGeo)

set_property (TARGET ITAGeoHalfedgeMeshModelTest PROPERTY FOLDER "Tests/ITAGeo")
set_property (
	TARGET ITAGeoHalfedgeMeshModelTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
)

install (TARGETS ITAGeoHalfedgeMeshModelTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (PointCloudExporter PointCloudExporter.cpp)
target_link_libraries (PointCloudExporter PUBLIC ITAGeo::ITAGeo)

set_property (TARGET PointCloudExporter PROPERTY FOLDER "Tests/ITAGeo")
set_property (TARGET PointCloudExporter PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS PointCloudExporter RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (ITAGeoModelTest ITAGeoModelTest.cpp)
target_link_libraries (ITAGeoModelTest PUBLIC ITAGeo::ITAGeo)

set_property (TARGET ITAGeoModelTest PROPERTY FOLDER "Tests/ITAGeo")
set_property (TARGET ITAGeoModelTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS ITAGeoModelTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (ITAUrbanModelTest ITAUrbanModelTest.cpp)
target_link_libraries (ITAUrbanModelTest PUBLIC ITAGeo::ITAGeo)

set_property (TARGET ITAUrbanModelTest PROPERTY FOLDER "Tests/ITAGeo")
set_property (TARGET ITAUrbanModelTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS ITAUrbanModelTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (ITAGeoUtilsTest ITAGeoUtilsTest.cpp)
target_link_libraries (ITAGeoUtilsTest PUBLIC ITAGeo::ITAGeo)

set_property (TARGET ITAGeoUtilsTest PROPERTY FOLDER "Tests/ITAGeo")
set_property (TARGET ITAGeoUtilsTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS ITAGeoUtilsTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################
