#include <ITAException.h>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_translate_exception.hpp>
#include <catch2/generators/catch_generators_all.hpp>

CATCH_TRANSLATE_EXCEPTION( const ITAException& ex )
{
	return ex.ToString( );
}

#define GENERATE_RANDOM_STRING( length ) \
	GENERATE( map( []( const std::vector<int>& i ) { return std::string( i.begin( ), i.end( ) ); }, chunk( length, take( length, random( 32, 122 ) ) ) ) )

TEST_CASE( "Factorials are computed", "[factorial]" )
{
	auto str = GENERATE_RANDOM_STRING( 10 );
	REQUIRE( true );
}