#include "test_definitions.h"

#include <ITAGeo/Material/MaterialManager.h>

using namespace ITAGeo::Material;

TEST_CASE( "ITAGeo::Material::CMaterialManager/AddMaterialsFromFolder", "[CMaterialManager]" )
{
	CMaterialManager oMaterialManager;
	SECTION( "LoadSuccess" )
	{
		REQUIRE_NOTHROW( oMaterialManager.AddMaterialsFromFolder( ITAGEO_TEST_RESOURCE_DIR "/material_database" ) );
		REQUIRE( oMaterialManager.HasMaterial( "TestMaterial1" ) );
		REQUIRE( oMaterialManager.HasMaterial( "TestMaterial2" ) );
		REQUIRE( oMaterialManager.HasMaterial( "TestMaterial3" ) );
		REQUIRE_FALSE( oMaterialManager.HasMaterial( "TestMaterial4" ) );
	}
	SECTION( "RecursiveLoadSuccess" )
	{
		REQUIRE_NOTHROW( oMaterialManager.AddMaterialsFromFolder( ITAGEO_TEST_RESOURCE_DIR "/material_database", true ) );
		REQUIRE( oMaterialManager.HasMaterial( "TestMaterial1" ) );
		REQUIRE( oMaterialManager.HasMaterial( "TestMaterial2" ) );
		REQUIRE( oMaterialManager.HasMaterial( "TestMaterial3" ) );
		REQUIRE( oMaterialManager.HasMaterial( "TestMaterial4" ) );
	}
	SECTION( "LoadFail" )
	{
		REQUIRE_THROWS_AS( oMaterialManager.AddMaterialsFromFolder( ITAGEO_TEST_RESOURCE_DIR "/NOT_EXISTING" ), ITAException );
	}
}

TEST_CASE( "ITAGeo::Material::CMaterialManager/GetNumMaterials", "[CMaterialManager]" )
{
	CMaterialManager oMaterialManagerEmpty;

	REQUIRE( oMaterialManagerEmpty.GetNumMaterials( ) == 0 );

	CMaterialManager oMaterialManager( ITAGEO_TEST_RESOURCE_DIR "/material_database" );

	REQUIRE( oMaterialManager.GetNumMaterials( ) == 3 );
}

TEST_CASE( "ITAGeo::Material::CMaterialManager/HasMaterial", "[CMaterialManager]" )
{
	CMaterialManager oMaterialManager( ITAGEO_TEST_RESOURCE_DIR "/material_database" );

	REQUIRE( oMaterialManager.HasMaterial( "TestMaterial1" ) );
	REQUIRE( oMaterialManager.HasMaterial( "TestMaterial2" ) );
	REQUIRE( oMaterialManager.HasMaterial( "TestMaterial3" ) );
	REQUIRE_FALSE( oMaterialManager.HasMaterial( "TestMaterial4" ) );
}

TEST_CASE( "ITAGeo::Material::CMaterialManager/GetMaterial", "[CMaterialManager]" )
{
	CMaterialManager oMaterialManager( ITAGEO_TEST_RESOURCE_DIR "/material_database" );

	REQUIRE( oMaterialManager.GetMaterial( "TestMaterial1" ) != nullptr );
	REQUIRE( oMaterialManager.GetMaterial( "TestMaterial2" ) != nullptr );
	REQUIRE( oMaterialManager.GetMaterial( "TestMaterial3" ) != nullptr );
	REQUIRE( oMaterialManager.GetMaterial( "TestMaterial4" ) == nullptr );
}

TEST_CASE( "ITAGeo::Material::CMaterialManager/AddMaterial", "[CMaterialManager]" )
{
	CMaterialManager oMaterialManagerToCopyMaterialFrom( ITAGEO_TEST_RESOURCE_DIR "/material_database" );
	std::shared_ptr<ITAGeo::Material::IMaterial> pMaterial = oMaterialManagerToCopyMaterialFrom.GetMaterial( "TestMaterial1" );

	CMaterialManager oMaterialManager;
	std::string sMaterialName = "TestMaterial1";

	oMaterialManager.AddMaterial( sMaterialName, pMaterial );

	REQUIRE( oMaterialManager.GetNumMaterials( ) == 1 );
	REQUIRE( oMaterialManager.HasMaterial( sMaterialName ) );
	REQUIRE( oMaterialManager.GetMaterial( sMaterialName ) != nullptr );
}