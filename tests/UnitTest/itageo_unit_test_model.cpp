#include "test_definitions.h"

#include <ITAGeo/Model.h>
#include <ITAGeo/Utils/JSON/PropagationPath.h>


TEST_CASE( "ITAGeo::CModel/Empty", "[CModel]" )
{
	ITAGeo::CModel oModel;
	REQUIRE_THROWS( oModel.GetOpenMesh( ) );
}

TEST_CASE( "ITAGeo::CModel/Load", "[CModel]" )
{
	ITAGeo::CModel oModel;

	REQUIRE_FALSE( oModel.Load( "NOT_A_PATH" ) );

	REQUIRE_THROWS_AS( oModel.Load( ITAGEO_TEST_RESOURCE_DIR "/demo_rebuild.UNKNOWN_FILE_ENDING" ), ITAException );

	REQUIRE_THROWS_AS( oModel.Load( ITAGEO_TEST_RESOURCE_DIR "/broke_file.dae" ), ITAException );

	REQUIRE( oModel.Load( ITAGEO_TEST_RESOURCE_DIR "/demo_rebuild.dae" ) );

#ifdef URBAN_MODEL_WITH_SKETCHUP_SUPPORT
	REQUIRE( oModel.Load( ITAGEO_TEST_RESOURCE_DIR "/sketchup/1x1x1_cube.skp" ) );
#endif
}

TEST_CASE( "ITAGeo::CModel/GetOpenMesh", "[CModel]" )
{
	ITAGeo::CModel oModel;

	REQUIRE_THROWS_AS( oModel.GetOpenMesh( ), ITAException );

	oModel.Load( ITAGEO_TEST_RESOURCE_DIR "/demo_rebuild.dae" );
	REQUIRE( oModel.GetOpenMesh( ) != nullptr );
}

TEST_CASE( "ITAGeo::CModel/Store", "[CModel]" )
{
	ITAGeo::CModel oModel;
	// currently no scene loaded. Path irrelevant
	REQUIRE_FALSE( oModel.Store( "./viz_out.dae", "collada" ) );

	oModel.Load( ITAGEO_TEST_RESOURCE_DIR "/demo_rebuild.dae" );

	// no visualisation loaded. Path irrelevant
	REQUIRE_FALSE( oModel.Store( "./viz_out.dae", "collada" ) );

	std::vector<ITAGeo::CEmitter> vEmitters { ITAGeo::CEmitter( VistaVector3D( 0.0, 0.4, 3.5 ) ), ITAGeo::CEmitter( VistaVector3D( 3.5, 1.0, 0.0 ) ) };
	std::vector<ITAGeo::CSensor> vSensors { ITAGeo::CSensor( VistaVector3D( 9.7, 0.9, 10.0 ) ), ITAGeo::CSensor( VistaVector3D( 0.0, 0.0, 0.0 ) ) };
	ITAGeo::CPropagationPathList vPathList { };
	ITAGeo::Utils::JSON::Import( vPathList, ITAGEO_TEST_RESOURCE_DIR "/demo_rebuild.json" );

	oModel.AddVisualization( vPathList, vEmitters, vSensors );

	SECTION( "Successful" )
	{
		REQUIRE( oModel.Store( "./viz_out.dae", "collada" ) );
	}
	SECTION( "Failure" )
	{
		REQUIRE_FALSE( oModel.Store( "NOT_A_PATH" ) );

		REQUIRE_THROWS_AS( oModel.Store( "./viz_out.dae", "NOT_A_FILE_FORMAT" ), ITAException );
	}
}
