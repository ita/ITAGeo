#include "ITAGeo/SketchUp/Assimp_Sketchup_Importer.h"
#include "assimpInfo.h"
#include "assimp_compare.h"
#include "test_definitions.h"

#include <assimp/Importer.hpp>
#include <assimp/material.h>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

TEST_CASE( "ITAGeo::SketchUpImport/ImportFail", "[SketchUpImport][Optional]" )
{
	Assimp::Importer aiImporterSkp;
	aiImporterSkp.RegisterLoader( new CSketchUpImporter );

	auto postproc = aiProcess_JoinIdenticalVertices | aiProcess_DropNormals | aiProcess_FindDegenerates | aiProcess_PreTransformVertices;

	auto skp_scene = aiImporterSkp.ReadFile( std::string( ITAGEO_TEST_RESOURCE_DIR "/sketchup/not_a_skp.skp" ), postproc );

	REQUIRE( skp_scene == nullptr );
}

TEST_CASE( "ITAGeo::SketchUpImport/Import", "[SketchUpImport][Optional]" )
{
	Assimp::Importer aiImporterSkp, aiImporterDae;
	aiImporterSkp.RegisterLoader( new CSketchUpImporter );

	auto postproc = aiProcess_JoinIdenticalVertices | aiProcess_DropNormals | aiProcess_FindDegenerates | aiProcess_PreTransformVertices;

	std::vector<std::string> testMeshes {
		"1x1x1_cube", "1x1x1_cube_component", "1x1x1_cube_group", "1x1x1_cube_nested_component", "1x1x1_cube_two_component",
		"colors"
		/* "complex_face" */
	};

	for( auto&& testMesh: testMeshes )
	{
		DYNAMIC_SECTION( "Testing Mesh " << testMesh )
		{
			auto skp_scene = aiImporterSkp.ReadFile( std::string( ITAGEO_TEST_RESOURCE_DIR "/sketchup/" ) + testMesh + ".skp", postproc );
			INFO( aiImporterSkp.GetErrorString( ) );

			INFO( aiImporterSkp.GetErrorString( ) );
			REQUIRE( skp_scene );

			auto dae_scene = aiImporterDae.ReadFile( std::string( ITAGEO_TEST_RESOURCE_DIR "/sketchup/" ) + testMesh + ".dae", postproc | aiProcess_OptimizeMeshes );

			INFO( aiImporterDae.GetErrorString( ) );
			REQUIRE( dae_scene );

#if FALSE
			Assimp_Info( dae_scene );
			Assimp_Info( skp_scene );
#endif

			compare( dae_scene, skp_scene );
		}
	}
}

TEST_CASE( "ITAGeo::SketchUpImport/CanRead", "[SketchUpImport][Optional]" )
{
	Assimp::Importer aiImporterSkp;

	auto postproc = aiProcess_JoinIdenticalVertices | aiProcess_DropNormals | aiProcess_FindDegenerates | aiProcess_PreTransformVertices;

	SECTION( "Cannot read" )
	{
		auto skp_scene = aiImporterSkp.ReadFile( std::string( ITAGEO_TEST_RESOURCE_DIR "/sketchup/" ) + "1x1x1_cube.skp", postproc );

		REQUIRE( skp_scene == nullptr );
	}

	SECTION( "Can read" )
	{
		aiImporterSkp.RegisterLoader( new CSketchUpImporter );
		auto skp_scene = aiImporterSkp.ReadFile( std::string( ITAGEO_TEST_RESOURCE_DIR "/sketchup/" ) + "1x1x1_cube.skp", postproc );

		REQUIRE( skp_scene != nullptr );
	}
}