#pragma once

#include "test_definitions.h"

#include <assimp/material.h>
#include <assimp/mesh.h>
#include <assimp/scene.h>

#define CHECK_OPACITY FALSE

static std::ostream& operator<<( std::ostream& os, const aiFace& face )
{
	os << "[";
	for( unsigned int i = 0; i < face.mNumIndices; i++ )
	{
		os << face.mIndices[i];
		if( i < face.mNumIndices - 1 )
		{
			os << ", ";
		}
		else
		{
			os << "]\n";
		}
	}
	return os;
}

static std::ostream& operator<<( std::ostream& os, const aiVector3D& toDump )
{
	os << "( " << toDump.x << ", " << toDump.y << ", " << toDump.z << ")";
	return os;
}

static std::ostream& operator<<( std::ostream& os, const aiMatrix4x4& toDump )
{
	for( auto i = 0; i < 4; i++ )
	{
		for( auto j = 0; j < 4; j++ )
		{
			os << toDump[i][j] << '\t';
		}
		os << '\n';
	}
	return os;
}

struct IndicesMatcher : public Catch::Matchers::MatcherGenericBase
{
	using indicesVector       = std::vector<int>;
	using indicesVectorVector = std::vector<indicesVector>;

	const indicesVectorVector& m_expected;

	IndicesMatcher( const indicesVectorVector& expected ) : m_expected { expected } {}

	bool match( const indicesVectorVector& toCompare ) const
	{
		if( m_expected.size( ) != toCompare.size( ) )
		{
			return false;
		}
		return std::is_permutation( m_expected.begin( ), m_expected.end( ), toCompare.begin( ),
		                            []( const indicesVector& expected, const indicesVector& toCompare )
		                            {
			                            if( expected.size( ) != toCompare.size( ) )
			                            {
				                            return false;
			                            }
			                            return std::is_permutation( expected.begin( ), expected.end( ), toCompare.begin( ) );
		                            } );
	}

	std::string describe( ) const override { return "Face indices comparison: " + ::Catch::Detail::stringify( m_expected ); }
};

auto EqualsIndices( const IndicesMatcher::indicesVectorVector& expected ) -> IndicesMatcher
{
	return IndicesMatcher { expected };
}

struct VerticesMatcher : public Catch::Matchers::MatcherGenericBase
{
	const std::vector<aiVector3D>& m_expected;

	VerticesMatcher( const std::vector<aiVector3D>& expected ) : m_expected { expected } {}

	bool match( const std::vector<aiVector3D>& toCompare ) const
	{
		if( m_expected.size( ) != toCompare.size( ) )
		{
			return false;
		}
		return std::is_permutation( m_expected.begin( ), m_expected.end( ), toCompare.begin( ),
		                            []( const aiVector3D& expected, const aiVector3D& toCompare ) { return expected.Equal( toCompare ); } );
	}

	std::string describe( ) const override { return "Vectices vector comparison: " + ::Catch::Detail::stringify( m_expected ); }
};

auto EqualsVertices( const std::vector<aiVector3D>& expected ) -> VerticesMatcher
{
	return VerticesMatcher { expected };
}

struct MeshesMatcher : public Catch::Matchers::MatcherGenericBase
{
	const std::vector<const aiMesh*>& m_expected;
	const std::unordered_map<int, int>& m_materialsMap;

	MeshesMatcher( const std::vector<const aiMesh*>& expected, const std::unordered_map<int, int>& materialsMap )
	    : m_expected { expected }
	    , m_materialsMap { materialsMap }
	{
	}

	bool match( const std::vector<const aiMesh*>& toCompare ) const
	{
		if( m_expected.size( ) != toCompare.size( ) )
		{
			return false;
		}
		return std::is_permutation( m_expected.begin( ), m_expected.end( ), toCompare.begin( ),
		                            [&]( const aiMesh* expected, const aiMesh* toCompare ) { return compareMesh( expected, toCompare ); } );
	}

	bool compareMesh( const aiMesh* expected, const aiMesh* toCompare ) const
	{
		if( expected == toCompare )
		{
			return false;
		}

		if( expected == nullptr )
		{
			return false;
		}

		if( toCompare == nullptr )
		{
			return false;
		}

		if( m_materialsMap.at( expected->mMaterialIndex ) != toCompare->mMaterialIndex )
		{
			return false;
		}

		// compare vertices
		if( expected->mNumVertices != toCompare->mNumVertices )
		{
			return false;
		}

		if( !expected->HasPositions( ) )
		{
			return false;
		}
		if( !toCompare->HasPositions( ) )
		{
			return false;
		}

		std::vector<aiVector3D> expectedVertices, toCompareVertices;
		expectedVertices.reserve( expected->mNumVertices );
		toCompareVertices.reserve( toCompare->mNumVertices );

		for( unsigned int i = 0; i < expected->mNumVertices; i++ )
		{
			expectedVertices.push_back( expected->mVertices[i] );
			toCompareVertices.push_back( toCompare->mVertices[i] );
		}

		if( !EqualsVertices( expectedVertices ).match( toCompareVertices ) )
		{
			return false;
		}

		// create a map between the indices of the expected vertices and the vertex indices of toCompare
		std::unordered_map<int, int> verticesMap;

		for( unsigned int i = 0; i < expected->mNumVertices; i++ )
		{
			verticesMap[i] = std::distance( toCompareVertices.begin( ), std::find_if( toCompareVertices.begin( ), toCompareVertices.end( ),
			                                                                          [&expected = expectedVertices.at( i )]( const aiVector3D& toCompare )
			                                                                          { return expected.Equal( toCompare ); } ) );
		}

		// compare faces
		if( expected->mNumFaces != toCompare->mNumFaces )
		{
			return false;
		}

		std::vector<std::vector<int>> expectedFaces, toCompareFaces;
		expectedFaces.reserve( expected->mNumFaces );
		toCompareFaces.reserve( toCompare->mNumFaces );

		for( unsigned int i = 0; i < expected->mNumFaces; i++ )
		{
			const auto& expectedFace  = expected->mFaces[i];
			const auto& toCompareFace = toCompare->mFaces[i];

			if( &expectedFace == &toCompareFace )
			{
				return false;
			}
			if( &expectedFace == nullptr || &toCompareFace == nullptr )
			{
				return false;
			}

			if( expectedFace.mNumIndices != toCompareFace.mNumIndices )
			{
				return false;
			}
			if( expectedFace.mIndices == nullptr || toCompareFace.mIndices == nullptr )
			{
				return false;
			}

			std::vector<int> expectedIndices, toCompareIndices;
			expectedIndices.reserve( expectedFace.mNumIndices );
			toCompareIndices.reserve( toCompareFace.mNumIndices );

			for( unsigned int i = 0; i < expectedFace.mNumIndices; i++ )
			{
				expectedIndices.push_back( verticesMap.at( expectedFace.mIndices[i] ) );
				toCompareIndices.push_back( toCompareFace.mIndices[i] );
			}

			expectedFaces.push_back( expectedIndices );
			toCompareFaces.push_back( toCompareIndices );
		}

		if( !EqualsIndices( expectedFaces ).match( toCompareFaces ) )
		{
			return false;
		}

		return true;
	}

	std::string describe( ) const override { return "Meshes comparison failed"; }
};

auto EqualsMeshes( const std::vector<const aiMesh*>& expected, const std::unordered_map<int, int>& materialsMap ) -> MeshesMatcher
{
	return MeshesMatcher { expected, materialsMap };
}

struct MaterialMatcher : public Catch::Matchers::MatcherGenericBase
{
	const std::vector<const aiMaterial*>& m_expected;

	MaterialMatcher( const std::vector<const aiMaterial*>& expected ) : m_expected { expected } {}

	bool match( const std::vector<const aiMaterial*>& toCompare ) const
	{
		if( m_expected.size( ) != toCompare.size( ) )
		{
			return false;
		}
		return std::is_permutation( m_expected.begin( ), m_expected.end( ), toCompare.begin( ),
		                            []( const aiMaterial* expected, const aiMaterial* toCompare )
		                            {
			                            if( expected == toCompare )
			                            {
				                            return false;
			                            }
			                            if( expected == nullptr || toCompare == nullptr )
			                            {
				                            return false;
			                            }

			                            std::string expectedName( expected->GetName( ).C_Str( ) );
			                            std::string toCompareName( toCompare->GetName( ).C_Str( ) );
			                            if( expectedName == "material" ) // sketchup default material name
			                            {
				                            if( AI_DEFAULT_MATERIAL_NAME != toCompareName )
				                            {
					                            return false;
				                            }
			                            }
			                            else
			                            {
				                            if( expectedName.find( toCompareName ) == std::string::npos && toCompareName.find( expectedName ) == std::string::npos )
				                            {
					                            return false;
				                            }
			                            }

			                            aiColor3D expectedColor( 0.f ), toCompareColor( 0.f );

			                            expected->Get( AI_MATKEY_COLOR_DIFFUSE, expectedColor );
			                            toCompare->Get( AI_MATKEY_COLOR_DIFFUSE, toCompareColor );

			                            if( !Catch::Matchers::WithinRel( expectedColor.r ).match( toCompareColor.r ) ||
			                                !Catch::Matchers::WithinRel( expectedColor.g ).match( toCompareColor.g ) ||
			                                !Catch::Matchers::WithinRel( expectedColor.b ).match( toCompareColor.b ) )
			                            {
				                            return false;
			                            }

#if CHECK_OPACITY
			                            float expectedOpacity, toCompareOpacity;

			                            expected->Get( AI_MATKEY_OPACITY, expectedOpacity );
			                            toCompare->Get( AI_MATKEY_OPACITY, toCompareOpacity );

			                            if( !Catch::Matchers::WithinRel( expectedOpacity ).match( toCompareOpacity ) )
			                            {
				                            return false;
			                            }

			                            WARN( "Todo" );
#endif
			                            return true;
		                            } );
	}

	std::string describe( ) const override { return "Materials vector comparison: " + ::Catch::Detail::stringify( m_expected ); }
};

auto EqualsMaterials( const std::vector<const aiMaterial*>& expected ) -> MaterialMatcher
{
	return MaterialMatcher { expected };
}

static void compare( const aiMesh* expected, const aiMesh* toCompare )
{
	if( expected == toCompare )
	{
		FAIL( "Given pointers point to the same mesh." );
	}
	REQUIRE( expected != nullptr );
	REQUIRE( toCompare != nullptr );

	REQUIRE( expected->mMaterialIndex == toCompare->mMaterialIndex );

	// compare vertices
	REQUIRE( expected->mNumVertices == toCompare->mNumVertices );
	REQUIRE( expected->HasPositions( ) );
	REQUIRE( toCompare->HasPositions( ) );

	std::vector<aiVector3D> expectedVertices, toCompareVertices;
	expectedVertices.reserve( expected->mNumVertices );
	toCompareVertices.reserve( toCompare->mNumVertices );

	const float scaleInchToMeter = 39.3700787f;

	for( unsigned int i = 0; i < expected->mNumVertices; i++ )
	{
		expectedVertices.push_back( expected->mVertices[i] );
		toCompareVertices.push_back( toCompare->mVertices[i] );
	}

	REQUIRE_THAT( toCompareVertices, EqualsVertices( expectedVertices ) );

	// create a map between the indices of the expected vertices and the vertex indices of toCompare
	std::unordered_map<int, int> verticesMap;

	for( unsigned int i = 0; i < expected->mNumVertices; i++ )
	{
		verticesMap[i] = std::distance( toCompareVertices.begin( ),
		                                std::find_if( toCompareVertices.begin( ), toCompareVertices.end( ),
		                                              [&expected = expectedVertices.at( i )]( const aiVector3D& toCompare ) { return expected.Equal( toCompare ); } ) );
	}

	// compare faces
	REQUIRE( expected->mNumFaces == toCompare->mNumFaces );

	std::vector<std::vector<int>> expectedFaces, toCompareFaces;
	expectedFaces.reserve( expected->mNumFaces );
	toCompareFaces.reserve( toCompare->mNumFaces );

	for( unsigned int i = 0; i < expected->mNumFaces; i++ )
	{
		const auto& expectedFace  = expected->mFaces[i];
		const auto& toCompareFace = toCompare->mFaces[i];

		if( &expectedFace == &toCompareFace )
		{
			FAIL( "Given pointers point to the same face." );
		}
		REQUIRE( &expectedFace != nullptr );
		REQUIRE( &toCompareFace != nullptr );

		REQUIRE( expectedFace.mNumIndices == toCompareFace.mNumIndices );
		REQUIRE( expectedFace.mIndices != nullptr );
		REQUIRE( toCompareFace.mIndices != nullptr );

		std::vector<int> expectedIndices, toCompareIndices;
		expectedIndices.reserve( expectedFace.mNumIndices );
		toCompareIndices.reserve( toCompareFace.mNumIndices );

		for( unsigned int i = 0; i < expectedFace.mNumIndices; i++ )
		{
			expectedIndices.push_back( verticesMap.at( expectedFace.mIndices[i] ) );
			toCompareIndices.push_back( toCompareFace.mIndices[i] );
		}

		expectedFaces.push_back( expectedIndices );
		toCompareFaces.push_back( toCompareIndices );
	}

	REQUIRE_THAT( toCompareFaces, EqualsIndices( expectedFaces ) );
}

static void compare( const aiMaterial* expected, const aiMaterial* toCompare )
{
	if( expected == toCompare )
	{
		FAIL( "Given pointers point to the same material." );
	}
	REQUIRE( expected != nullptr );
	REQUIRE( toCompare != nullptr );

	std::string expectedName( expected->GetName( ).C_Str( ) );
	std::string toCompareName( toCompare->GetName( ).C_Str( ) );
	if( expectedName == "material" ) // sketchup default material name
	{
		REQUIRE( AI_DEFAULT_MATERIAL_NAME == toCompareName );
	}
	else
	{
		REQUIRE( ( expectedName.find( toCompareName ) != std::string::npos || toCompareName.find( expectedName ) != std::string::npos ) );
	}

	aiColor3D expectedColor( 0.f ), toCompareColor( 0.f );

	expected->Get( AI_MATKEY_COLOR_DIFFUSE, expectedColor );
	toCompare->Get( AI_MATKEY_COLOR_DIFFUSE, toCompareColor );

	REQUIRE_THAT( toCompareColor.r, Catch::Matchers::WithinRel( expectedColor.r ) );
	REQUIRE_THAT( toCompareColor.g, Catch::Matchers::WithinRel( expectedColor.g ) );
	REQUIRE_THAT( toCompareColor.b, Catch::Matchers::WithinRel( expectedColor.b ) );

#if CHECK_OPACITY
	float expectedOpacity, toCompareOpacity;

	expected->Get( AI_MATKEY_OPACITY, expectedOpacity );
	toCompare->Get( AI_MATKEY_OPACITY, toCompareOpacity );

	REQUIRE_THAT( toCompareOpacity, Catch::Matchers::WithinRel( expectedOpacity ) );

	WARN( "Todo" );
#endif
}

static bool nodeHierarchyHasMeshes( const aiNode* node )
{
	if( node->mNumMeshes > 0 )
	{
		return true;
	}

	for( auto i = 0; i < node->mNumChildren; ++i )
	{
		if( nodeHierarchyHasMeshes( node->mChildren[i] ) )
		{
			return true;
		}
	}
	return false;
}

static bool childNodesHaveMeshes( const aiNode* node )
{
	for( auto i = 0; i < node->mNumChildren; ++i )
	{
		if( nodeHierarchyHasMeshes( node->mChildren[i] ) )
		{
			return true;
		}
	}
	return false;
}

static void compare( const aiNode* expected, const aiNode* toCompare )
{
	if( expected == toCompare )
	{
		FAIL( "Given pointers point to the same node." );
	}
	REQUIRE( expected != nullptr );
	REQUIRE( toCompare != nullptr );

	// The name of the nodes is irrelevant here, so it is not tested.

	REQUIRE( expected->mNumMeshes == toCompare->mNumMeshes );

	if( expected->mNumMeshes > 0 )
	{
		for( auto i = 0; i < expected->mNumMeshes; ++i )
		{
			REQUIRE( expected->mMeshes[i] == toCompare->mMeshes[i] );
		}
	}

	if( childNodesHaveMeshes( expected ) && childNodesHaveMeshes( toCompare ) )
	{
		REQUIRE( expected->mNumChildren == toCompare->mNumChildren );
	}

	if( expected->mParent != nullptr && toCompare->mParent != nullptr ) // not root node
	{
		REQUIRE( expected->mTransformation == toCompare->mTransformation );
	}

	if( expected->mNumChildren == 1 && toCompare->mNumChildren == 1 )
	{
		compare( expected->mChildren[0], toCompare->mChildren[0] );
	}
	if( expected->mNumChildren > 1 && toCompare->mNumChildren > 1 )
	{
		for( auto i = 0; i < expected->mNumChildren; ++i )
		{
			// this cannot take into consideration when the nodes have a different order.
			compare( expected->mChildren[i], toCompare->mChildren[i] );
		}
	}
}

static void compare( const aiScene* expected, const aiScene* toCompare )
{
	if( expected == toCompare )
	{
		FAIL( "Given pointers point to the same scene." );
	}
	REQUIRE( expected != nullptr );
	REQUIRE( toCompare != nullptr );

	// compare materials
	REQUIRE( expected->mNumMaterials == toCompare->mNumMaterials );

	if( expected->mNumMaterials > 0 )
	{
		REQUIRE( expected->mMaterials != nullptr );
		REQUIRE( toCompare->mMaterials != nullptr );
	}

	std::vector<const aiMaterial*> expectedMaterials, toCompareMaterials;
	expectedMaterials.reserve( expected->mNumMaterials );
	toCompareMaterials.reserve( toCompare->mNumMaterials );

	for( unsigned int i = 0; i < expected->mNumMaterials; i++ )
	{
		expectedMaterials.push_back( expected->mMaterials[i] );
		toCompareMaterials.push_back( toCompare->mMaterials[i] );
	}

	// create a map between the indices of the expected material and the material indices of toCompare
	std::unordered_map<int, int> materialsMap;

	for( unsigned int i = 0; i < expected->mNumMaterials; i++ )
	{
		materialsMap[i] = std::distance( toCompareMaterials.begin( ), std::find_if( toCompareMaterials.begin( ), toCompareMaterials.end( ),
		                                                                            [&expected = expectedMaterials.at( i )]( const aiMaterial* toCompare )
		                                                                            {
			                                                                            std::string expectedName( expected->GetName( ).C_Str( ) );
			                                                                            std::string toCompareName( toCompare->GetName( ).C_Str( ) );

			                                                                            if( expectedName == "material" )
			                                                                            {
				                                                                            expectedName = AI_DEFAULT_MATERIAL_NAME;
			                                                                            }

			                                                                            if( expectedName.find( toCompareName ) != std::string::npos ||
			                                                                                toCompareName.find( expectedName ) != std::string::npos )
			                                                                            {
				                                                                            return true;
			                                                                            }
			                                                                            return false;
		                                                                            } ) );
	}

	if( expected->mNumMaterials > 1 )
	{
		REQUIRE_THAT( toCompareMaterials, EqualsMaterials( expectedMaterials ) );
	}
	else if( expected->mNumMaterials == 1 )
	{
		compare( expected->mMaterials[0], toCompare->mMaterials[0] );
	}

	// compare meshes
	REQUIRE( expected->mNumMeshes == toCompare->mNumMeshes );

	if( expected->mNumMeshes > 1 )
	{
		std::vector<const aiMesh*> expectedMeshes, toCompareMeshes;
		expectedMeshes.reserve( expected->mNumMeshes );
		toCompareMeshes.reserve( toCompare->mNumMeshes );

		for( unsigned int i = 0; i < expected->mNumMeshes; i++ )
		{
			expectedMeshes.push_back( expected->mMeshes[i] );
			toCompareMeshes.push_back( toCompare->mMeshes[i] );
		}

		REQUIRE_THAT( toCompareMeshes, EqualsMeshes( expectedMeshes, materialsMap ) );
	}
	else if( expected->mNumMeshes == 1 )
	{
		compare( expected->mMeshes[0], toCompare->mMeshes[0] );
	}

	// compare node structure
	compare( expected->mRootNode, toCompare->mRootNode );
}
