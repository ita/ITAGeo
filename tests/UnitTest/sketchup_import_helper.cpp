#include "ITAGeo/SketchUp/Assimp_Sketchup_Helper.h"
#include "test_definitions.h"

TEST_CASE( "ITAGeo::SketchUpImport/Helper/ComponentDefOrder", "[SketchUpImport][Optional]" )
{
	using namespace AssimpSkpHelper;

	Component comp1 { "comp1", {} };
	Component comp2 { "comp2", {} };
	Component comp3 { "comp3", {} };
	Component comp4 { "comp4", {} };
	Component comp5 { "comp5", {} };

	comp1.entity.instances.push_back( { "comp2", "", {} } );
	comp1.entity.instances.push_back( { "comp2", "", {} } );
	comp1.entity.instances.push_back( { "comp3", "", {} } );

	comp2.entity.instances.push_back( { "comp4", "", {} } );

	std::vector<Component> componentDefs { comp1, comp2, comp3, comp4, comp5 };

	SECTION( "All components used" )
	{
		auto order = buildComponentDefinitionOrder( componentDefs, { "comp1", "comp2", "comp3", "comp4", "comp5" } );

		std::vector<std::string> orderVector( order.begin( ), order.end( ) );
		REQUIRE_THAT( orderVector, Catch::Matchers::Equals( std::vector<std::string> { "comp4", "comp2", "comp3", "comp1", "comp5" } ) );
	}

	SECTION( "No components used" )
	{
		auto order = buildComponentDefinitionOrder( componentDefs, { } );

		std::vector<std::string> orderVector( order.begin( ), order.end( ) );
		REQUIRE_THAT( orderVector, Catch::Matchers::Equals( std::vector<std::string> { } ) );
	}


	SECTION( "Subset of components used" )
	{
		SECTION( "Recursive component" )
		{
			auto order = buildComponentDefinitionOrder( componentDefs, { "comp1" } );

			std::vector<std::string> orderVector( order.begin( ), order.end( ) );
			REQUIRE_THAT( orderVector, Catch::Matchers::Equals( std::vector<std::string> { "comp4", "comp2", "comp3", "comp1" } ) );
		}

		SECTION( "Recursive component" )
		{
			auto order = buildComponentDefinitionOrder( componentDefs, { "comp5" } );

			std::vector<std::string> orderVector( order.begin( ), order.end( ) );
			REQUIRE_THAT( orderVector, Catch::Matchers::Equals( std::vector<std::string> { "comp5" } ) );
		}
	}
}

CATCH_REGISTER_ENUM( SUResult, SU_ERROR_NONE, SU_ERROR_NULL_POINTER_INPUT, SU_ERROR_INVALID_INPUT, SU_ERROR_NULL_POINTER_OUTPUT, SU_ERROR_INVALID_OUTPUT,
                     SU_ERROR_OVERWRITE_VALID, SU_ERROR_GENERIC, SU_ERROR_SERIALIZATION, SU_ERROR_OUT_OF_RANGE, SU_ERROR_NO_DATA, SU_ERROR_INSUFFICIENT_SIZE,
                     SU_ERROR_UNKNOWN_EXCEPTION, SU_ERROR_MODEL_INVALID, SU_ERROR_MODEL_VERSION, SU_ERROR_LAYER_LOCKED, SU_ERROR_DUPLICATE, SU_ERROR_PARTIAL_SUCCESS,
                     SU_ERROR_UNSUPPORTED, SU_ERROR_INVALID_ARGUMENT, SU_ERROR_ENTITY_LOCKED, SU_ERROR_INVALID_OPERATION )

TEST_CASE( "ITAGeo::SketchUpImport/Helper/checkSUResult", "[SketchUpImport][Optional]" )
{
	using namespace AssimpSkpHelper;

	std::vector<SUResult> suResults {
		SU_ERROR_NONE,
		SU_ERROR_NULL_POINTER_INPUT,
		SU_ERROR_INVALID_INPUT,
		SU_ERROR_NULL_POINTER_OUTPUT,
		SU_ERROR_INVALID_OUTPUT,
		SU_ERROR_OVERWRITE_VALID,
		SU_ERROR_GENERIC,
		SU_ERROR_SERIALIZATION,
		SU_ERROR_OUT_OF_RANGE,
		SU_ERROR_NO_DATA,
		SU_ERROR_INSUFFICIENT_SIZE,
		SU_ERROR_UNKNOWN_EXCEPTION,
		SU_ERROR_MODEL_INVALID,
		SU_ERROR_MODEL_VERSION,
		SU_ERROR_LAYER_LOCKED,
		SU_ERROR_DUPLICATE,
		SU_ERROR_PARTIAL_SUCCESS,
		SU_ERROR_UNSUPPORTED,
		SU_ERROR_INVALID_ARGUMENT,
		SU_ERROR_ENTITY_LOCKED,
		SU_ERROR_INVALID_OPERATION,
	};

	for( auto&& suResult: suResults )
	{
		auto suResultString = Catch::StringMaker<SUResult>( ).convert( suResult );
		DYNAMIC_SECTION( "Testing SUResult: " << suResultString )
		{
			if( suResult == SU_ERROR_NONE )
			{
				REQUIRE_NOTHROW( checkSUResult( suResult ) );
			}
			else
			{
				REQUIRE_THROWS_WITH( checkSUResult( suResult ), Catch::Matchers::ContainsSubstring( suResultString ) );
			}
		}
	}
}