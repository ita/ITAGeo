#include <ITAException.h>
#include <ITAGeo/SketchUp/Model.h>
#include <VistaBase/VistaVector3D.h>
#include <assert.h>
#include <iostream>

using namespace std;
using namespace ITAGeo;

int main( int, char** )
{
	const int nPoints           = 28;
	const int nValues           = nPoints * ( 1 + 3 + 1 );
	double dPointArray[nValues] = { 1,  -0.248, 0.358,  3.013, 1,  2,  -1.644, 0.353,  3.013, 2,  3,  -1.595, -4.465, 3.013, 3,  4,  -1.022, -3.845, 3.013, 4,
		                            5,  0.982,  -3.835, 3.013, 5,  6,  0.982,  -4.477, 3.013, 6,  7,  6.901,  -4.477, 3.013, 7,  8,  6.852,  1.744,  3.013, 8,
		                            9,  -0.278, 1.74,   3.013, 9,  10, -0.265, 1.542,  1.992, 10, 11, -0.269, 0.557,  2.003, 11, 12, 6.841,  1.527,  2.555, 12,
		                            13, 6.849,  -0.013, 2.58,  13, 14, 6.848,  -0.532, 2.587, 14, 15, 6.871,  -2.086, 2.599, 15, 16, 6.87,   -2.603, 2.599, 16,
		                            17, 6.884,  -4.139, 2.605, 17, 18, -1.022, -4.465, 3.013, 18, 19, -0.248, 0.358,  0,     19, 20, -1.644, 0.353,  0,     20,
		                            21, -1.595, -4.465, 0,     21, 22, -1.022, -3.845, 0,     22, 23, 0.982,  -3.835, 0,     23, 24, 0.982,  -4.477, 0,     24,
		                            25, 6.901,  -4.477, 0,     25, 26, 6.852,  1.744,  0,     26, 27, -0.278, 1.74,   0,     27, 28, -1.022, -4.465, 0,     28 };

	std::vector<VistaVector3D> vvPointCloud;
	for( int n = 0; n < nPoints; n++ )
		vvPointCloud.push_back( VistaVector3D( &dPointArray[n * 5 + 1] ) );

	CPropagationPath oVisualizationPath;
	for( int n = 0; n < nPoints; n++ )
	{
		auto pAnchor = std::make_shared<CPropagationAnchor>( vvPointCloud[n] );
		oVisualizationPath.push_back( pAnchor );
	}

	SketchUp::CModel oModel;
	oModel.AddPropagationPathVisualization( oVisualizationPath, "PointCloudPolyLine" );
	oModel.Store( "PointCloudExport.skp" );

	return 0;
}
