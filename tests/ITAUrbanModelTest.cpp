/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAException.h>
#include <ITAGeo/Material/MaterialManager.h>
#include <ITAGeo/SketchUp/Model.h>
#include <ITAGeo/Urban/Model.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <assert.h>
#include <iostream>

using namespace std;
using namespace ITAGeo;

int main( int, char** )
{
	std::string sBasePath  = "D:/Users/erraji/sketchup/";
	string sSuffix         = "skp";
	std::string sModelName = "SolidCube2";
	VistaFileSystemFile oFile( sBasePath + sModelName + "." + sSuffix );

	auto pMaterialDirectory = std::make_shared<Material::CMaterialManager>( "./" );

	Urban::CModel oUrbanModel;


	try
	{
		if( oUrbanModel.Load( oFile.GetName( ) ) )
			cout << "Successfully loaded file '" << oFile.GetLocalName( ) << "' as geometric urban model" << endl;

		cout << "Number of building meshes: " << oUrbanModel.GetNumBuildings( ) << endl;
		std::vector<std::string> vsGroupNames( oUrbanModel.GetBuildingNames( ) );
		for( size_t n = 0; n < vsGroupNames.size( ); n++ )
			cout << "\t\"" << vsGroupNames[n] << "\"" << endl;


		VistaFileSystemFile oOutFile( sBasePath + sModelName + "_out." + sSuffix );
		if( oUrbanModel.Store( oOutFile.GetName( ) ) )
			cout << "Successfully stored file '" << oOutFile.GetLocalName( ) << "' as a " << sSuffix << " model" << endl;
	}
	catch( const ITAException& e )
	{
		cerr << e << endl;
	}

	return 0;
}
