#include <ITAGeo/CityGML/Conversions.h>
#include <ITAGeo/Halfedge/CityGML/Reader.h>

// ITA includes
#include <ITAException.h>

// Vista includes
#include <VistaMath/VistaGeometries.h>
#include <VistaTools/VistaFileSystemFile.h>

// OpenMesh includes
#pragma warning( disable : 4512 4127 )
#include <OpenMesh/Core/IO/IOManager.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#pragma warning( default : 4512 4127 )

// libcitygml
#pragma warning( disable : 4201 )
#include <citygml/citygml.h>
#include <citygml/citymodel.h>
#include <citygml/cityobject.h>
#include <citygml/geometry.h>
#include <citygml/polygon.h>
#include <citygml/vecs.hpp>
#pragma warning( default : 4201 )

#ifdef WITH_XERCES_SUPPORT
// Xerces-c
#	include <xercesc/sax/SAXParseException.hpp>
#endif

typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;

CITAGeoHalfedgeCityGMLReader::CITAGeoHalfedgeCityGMLReader( )
{
	OpenMesh::IO::IOManager( ).register_module( this );
}

CITAGeoHalfedgeCityGMLReader::~CITAGeoHalfedgeCityGMLReader( ) {}

bool CITAGeoHalfedgeCityGMLReader::read( const std::string& sCityGMLFilePath, OpenMesh::IO::BaseImporter& oBaseImporter, OpenMesh::IO::Options& )
{ /*
	try
	{
	    citygml::ParserParams oParserParams;
	    std::shared_ptr< citygml::CityGMLLogger > pLogger = nullptr;

	    std::shared_ptr< const citygml::CityModel > pCityModel = citygml::load( sCityGMLFilePath, oParserParams, pLogger );

	    std::map< int, CITAMesh::VertexHandle > mTopLevelVertices;

	    const citygml::ConstCityObjects objects = pCityModel->getAllCityObjectsOfType( citygml::CityObject::CityObjectsType::COT_Building ); // @todo There is a problem
with this method - looks like "dll hell" for( auto object : objects )
	    {

	        for( unsigned int n = 0; n < object->getGeometriesCount(); n++ )
	        {
	            const citygml::Geometry& geo = object->getGeometry( n );
	            for( unsigned int p = 0; p < geo.getPolygonsCount(); p++ )
	            {
	                std::shared_ptr< const citygml::Polygon > poly( geo.getPolygon( p ) ); // CityGML polygon
	                const std::vector< TVec3d >& voVertices( poly->getVertices() );
	                std::vector< CITAMesh::VertexHandle > vhVertices( voVertices.size() );

	                const std::vector< unsigned int >& vuiIndices = poly->getIndices();
	                for( auto i : vuiIndices )
	                {
	                    CITAMesh::VertexHandle hVertex; // OpenMesh vertex
	                    std::map< int, CITAMesh::VertexHandle >::iterator it = mTopLevelVertices.find( i );
	                    if( it == mTopLevelVertices.end() )
	                    {
	                        const TVec3d& vCityGMLPoint3D( poly->getVertices()[ i ] );
	                        hVertex = oBaseImporter.add_vertex( CityGMLPoint3DToITAMeshPoint( vCityGMLPoint3D ) );
	                        mTopLevelVertices[ i ] = hVertex; // add new vertex to map
	                        vhVertices[ i ] = hVertex;
	                    }
	                }

	                oBaseImporter.add_face( vhVertices );
	            }
	        }
	    }

	    return true;

	}
#ifdef WITH_XERCES_SUPPORT
	catch( SAXParseException& e )
	{
	    ITA_EXCEPT_INVALID_PARAMETER( "Could not parse CityGML file " + sCityGMLFilePath + ": " + e.str() );
	}
#endif
	catch( ... )
	{
	    ITA_EXCEPT_INVALID_PARAMETER( "Could not parse CityGML file " + sCityGMLFilePath );
	}

	return false; */
	return true;
};

bool CITAGeoHalfedgeCityGMLReader::read( std::istream&, OpenMesh::IO::BaseImporter&, OpenMesh::IO::Options& )
{
	ITA_EXCEPT1( NOT_IMPLEMENTED, "instream reader is not implemented" );
}

CITAGeoHalfedgeCityGMLReader g_oCityGMLReaderInstance; //!< Global reader instance
