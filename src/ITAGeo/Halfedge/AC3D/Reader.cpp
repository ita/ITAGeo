#include <ITAGeo/Halfedge/AC3D/Helper.h>
#include <ITAGeo/Halfedge/AC3D/Reader.h>

// ITA includes
#include <ITAException.h>

// Vista includes
#include <VistaBase/VistaVector3D.h>
#include <VistaMath/VistaGeometries.h>
#include <VistaTools/VistaFileSystemFile.h>

#pragma warning( disable : 4512 4127 )
// OpenMesh includes
#include <OpenMesh/Core/IO/IOManager.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#pragma warning( default : 4512 4127 )

// STL
#include <fstream>
#include <vector>

using namespace ITAGeo::Halfedge::AC3D;

typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;

CReader::CReader( )
{
	OpenMesh::IO::IOManager( ).register_module( this );
}

CReader::~CReader( ) {}

bool CReader::read( const std::string& sFilePath, OpenMesh::IO::BaseImporter& oBaseImporter, OpenMesh::IO::Options& oOptions )
{
	VistaFileSystemFile oFile( sFilePath );
	if( oFile.Exists( ) == false )
		ITA_EXCEPT1( IO_ERROR, "Could not load AC3D file " + sFilePath );

	std::ifstream ifsAC3D( sFilePath );

	std::string sAC3DBeginningSequence;
	ifsAC3D >> sAC3DBeginningSequence;

	if( sAC3DBeginningSequence != "AC3Db" )
		ITA_EXCEPT1( IO_ERROR, "File " + sFilePath + " does not have 'AC3Db' beginning sequence." );

	bool bStreamImplRet = read( ifsAC3D, oBaseImporter, oOptions );
	return bStreamImplRet;
}

bool CReader::read( std::istream& ssInStream, OpenMesh::IO::BaseImporter& oBaseImporter, OpenMesh::IO::Options& )
{
	std::string sToken, sSecondToken;

	while( ssInStream >> sToken )
	{
		if( sToken == "MATERIAL" )
		{
			CAC3DObject::CMaterial oMat;
			ReadAC3DMaterialLine( ssInStream, oMat );
			m_voMaterial.push_back( oMat );
			oBaseImporter.add_texture_information( int( m_voMaterial.size( ) ) - 1, oMat.sMaterialID );
		}
		else if( sToken == "OBJECT" )
		{
			CAC3DObject oWorldObject;
			ssInStream >> oWorldObject.sObjectType;
			ssInStream >> sSecondToken;
			if( sSecondToken == "kids" )
			{
				int iNumKids;
				ssInStream >> iNumKids;
				oWorldObject.voKids.resize( iNumKids );

				for( size_t i = 0; i < oWorldObject.voKids.size( ); i++ )
				{
					CAC3DObject& oAC3DObject( oWorldObject.voKids[i] );
					ssInStream >> sSecondToken;
					if( sSecondToken == "OBJECT" )
						ssInStream >> oAC3DObject.sObjectType;
					if( oAC3DObject.sObjectType == "poly" )
						ReadAC3DObject( ssInStream, oAC3DObject );
					else if( oAC3DObject.sObjectType == "group" )
						ReadAC3DObject( ssInStream, oAC3DObject );
					else
						ITA_EXCEPT1( IO_ERROR, "Unkown AC3D object '" + oAC3DObject.sObjectType + "'" );
				}
				ReadAC3DObject( ssInStream, oWorldObject );

				ImportAC3DWorldObject( oWorldObject, oBaseImporter, m_voMaterial );
			}
		}
		else
			ITA_EXCEPT1( IO_ERROR, "Could not interpret AC3D token '" + sToken + "'" );
	}

	return true;
}

CReader g_oAC3DReaderInstance; //!< Global reader instance
