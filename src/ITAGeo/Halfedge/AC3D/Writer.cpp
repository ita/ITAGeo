#include <ITAGeo/Halfedge/AC3D/Writer.h>


// OpenMesh includes
#pragma warning( disable : 4512 4127 )
#include <OpenMesh/Core/IO/IOManager.hh>
#include <OpenMesh/Core/Mesh/Handles.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#pragma warning( default : 4512 4127 )

// ITA includes
#include <ITAException.h>

// Vista includes
#include <VistaTools/VistaFileSystemFile.h>

// STL includes
#include <fstream>
#include <vector>

// Using namespace
using namespace ITAGeo::Halfedge::AC3D;

typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;

CWriter::CWriter( )
{
	OpenMesh::IO::IOManager( ).register_module( this );
	sModelName = "ITAGeoHalfedgeAC3DGenericWriter";
}

CWriter::~CWriter( ) {}

bool CWriter::write( const std::string& sFilePath, OpenMesh::IO::BaseExporter& oBaseExporter, OpenMesh::IO::Options iOptions, std::streamsize iPresition ) const
{
	VistaFileSystemFile oFile( sFilePath );
	if( oFile.IsDirectory( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "File target is a directory" );
	if( oFile.IsReadOnly( ) && oFile.Exists( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "File target is read-only" );

	auto os = std::ofstream( sFilePath.c_str( ) );
	return CWriter::write( os, oBaseExporter, iOptions, iPresition );
}

bool CWriter::write( std::ostream& os, OpenMesh::IO::BaseExporter& oBaseExporter, OpenMesh::IO::Options, std::streamsize ) const
{
	std::ostream& out( os );

	out << "AC3Db" << std::endl;

	out << "MATERIAL \"default\" rgb 0 0.328 0.621  amb 0.2 0.2 0.2  emis 0 0 0  spec 0.5 0.5 0.5  shi 10  trans 0.5" << std::endl;

	out << "OBJECT world" << std::endl;
	out << "kids " << 1 << std::endl;

	out << "OBJECT poly" << std::endl;
	out << "name \"polygon_object\"" << std::endl;

	out << "numvert " << oBaseExporter.n_vertices( ) << std::endl;
	for( unsigned j = 0; j < oBaseExporter.n_vertices( ); j++ )
	{
		CITAMesh::Point pPoint = oBaseExporter.point( OpenMesh::VertexHandle( int( j ) ) );
		out << pPoint[0] << " " << pPoint[1] << " " << pPoint[2] << std::endl;
	}

	out << "numsurf " << oBaseExporter.n_faces( ) << std::endl;
	for( int n = 0; n < oBaseExporter.n_faces( ); n++ )
	{
		std::vector<OpenMesh::VertexHandle> vhVertices;
		OpenMesh::FaceHandle hFace = OpenMesh::FaceHandle( int( n ) );
		unsigned int nFaceVertices = oBaseExporter.get_vhandles( hFace, vhVertices ); // Warning: assumes, that OpenMesh face handles are ascending (0..n-1)
		assert( nFaceVertices == unsigned int( vhVertices.size( ) ) );

		out << "SURF 0x10" << std::endl;
		out << "mat 0" << std::endl; // Materials are not well supported by OpenMesh base exporter

		out << "refs " << nFaceVertices << std::endl;
		for( size_t j = 0; j < vhVertices.size( ); j++ )
		{
			OpenMesh::VertexHandle hVertex( vhVertices[j] );
			out << hVertex.idx( ) << " 0 0" << std::endl;
		}
	}

	out << "kids 0" << std::endl;

	return true;
}

CWriter g_oAC3DWriterInstance; //!< Global writer instance, registers skp file extension
