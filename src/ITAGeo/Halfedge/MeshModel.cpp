// ITAGeo includes
#include <ITAException.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/Material/Material.h>
#ifdef URBAN_MODEL_WITH_SKETCHUP_SUPPORT
#	include <ITAGeo/SketchUp/Helper.h>
#	include <ITAGeo/SketchUp/Materials.h>
#	include <ITAGeo/SketchUp/MeshConversions.h>
#	include <ITAGeo/SketchUp/Model.h>
#endif
#include <ITAGeo/Utils.h>

// OpenMesh includes
#pragma warning( disable : 4512 4127 )
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Utils/PropertyManager.hh>
#pragma warning( default : 4512 4127 )

// Vista includes
#include <VistaTools/VistaFileSystemDirectory.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <VistaTools/VistaRandomNumberGenerator.h>

// STL
#include <iosfwd>
#include <map>

// Namespaces
using namespace ITAGeo;
using namespace ITAGeo::Halfedge;


CMeshModel::CMeshModel( )
{
	m_prModelMeshData = std::make_unique<CITAMesh>( );
	m_prModelMeshData->request_face_texture_index( );
	// create global CAcousticFaceProperty for all openMesh Faces called "Acoustics::Material::Face",
	// can be accessed everywhere with the same function, returns property manager
	OpenMesh::getOrMakeProperty<CITAMesh::FaceHandle, ITAGeo::Material::CAcousticFaceProperty>( *m_prModelMeshData, "Acoustics::Material::Face" );
}

CMeshModel::~CMeshModel( ) {}

std::string CMeshModel::GetSupportedInputFormats( )
{
	return OpenMesh::IO::IOManager( ).qt_read_filters( );
}

std::string CMeshModel::GetSupportedOutputFormats( )
{
	return OpenMesh::IO::IOManager( ).qt_write_filters( );
}

bool CMeshModel::GetSupportedOutputFormat( const std::string& sFormatID )
{
	return OpenMesh::IO::IOManager( ).can_read( sFormatID );
}

bool CMeshModel::GetSupportedInputFormat( const std::string& sFormatID )
{
	return OpenMesh::IO::IOManager( ).can_write( sFormatID );
}

bool CMeshModel::Load( const std::string& sSKPFilePath )
{
	VistaFileSystemFile oFile( sSKPFilePath );
	if( oFile.Exists( ) == false )
		ITA_EXCEPT1( INVALID_PARAMETER, "File '" + sSKPFilePath + "' does not exist or is invalid" );

	bool bSuccess = OpenMesh::IO::read_mesh( *( m_prModelMeshData ), oFile.GetName( ) );


	// Enrich acoustic material
	CITAMesh* pMesh         = m_prModelMeshData.get( );
	CITAMesh::FaceIter f_it = pMesh->faces_begin( );

	// get our manager for material for each face
	auto mMaterialPropertyManager = OpenMesh::getOrMakeProperty<OpenMesh::FaceHandle, ITAGeo::Material::CAcousticFaceProperty>( *pMesh, "Acoustics::Material::Face" );
	// get texture id map which is created on OpenMesh::load, see OpenMesh Doku
	auto mtextureMapPropertyManager = OpenMesh::getOrMakeProperty<OpenMesh::MeshHandle, std::map<int, std::string>>( *pMesh, "TextureMapping" );
	auto mTextures                  = *mtextureMapPropertyManager; // access the underlying property of the mesh

	// translate from texture id map to Materials
	while( f_it != pMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *f_it++ );
		if( pMesh->has_face_texture_index( ) )
		{
			int iTextureIndex         = pMesh->texture_index( hFace );
			std::string sMaterialName = mTextures[iTextureIndex];
			if( m_pMaterialManager && m_pMaterialManager->HasMaterial( sMaterialName ) )
				mMaterialPropertyManager[hFace].SetMaterial( m_pMaterialManager->GetMaterial( sMaterialName ) );
		}
	}

	return bSuccess;
}

bool CMeshModel::Store( const std::string& sOutFilePath, bool bOverwite /*=true*/ ) const
{
	VistaFileSystemFile oFile( sOutFilePath );
	VistaFileSystemDirectory oParentDir( oFile.GetParentDirectory( ) );
	if( oParentDir.Exists( ) == false )
		ITA_EXCEPT1( INVALID_PARAMETER, "Target folder '" + oParentDir.GetName( ) + "' does not exist or is invalid" );

	if( oFile.Exists( ) && bOverwite == false )
		ITA_EXCEPT1( MODAL_EXCEPTION, "File '" + oFile.GetName( ) + "' already exists and shall not be overwritten" );

	const CITAMesh& oMesh( *( m_prModelMeshData ) );
	bool bSuccess = OpenMesh::IO::write_mesh( oMesh, oFile.GetName( ) );

	return bSuccess;
}

CMeshModelShared CMeshModel::GetTransformedMesh( const VistaTransformMatrix& oTransformMatrix ) const
{
	auto pModel = std::make_shared<CMeshModel>( );


	auto pMesh = this->ConstGetMesh( );

	// An error will occur if mesh doesn't have any vertices
	if( pMesh->n_vertices( ) == 0 )
		return nullptr;

	// Set name of transformed mesh
	pModel->SetName( this->GetName( ) );

	// Iterate over all faces of the mesh, then iterate over all vertices of the current face
	std::map<const int, CITAMesh::VertexHandle> mTopLevelVertices;
	CITAMesh::ConstFaceIter f_it = pMesh->faces_begin( );
	while( f_it != pMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *f_it++ );


		CITAMesh::ConstFaceVertexIter fv_it = pMesh->cfv_begin( hFace );
		std::vector<CITAMesh::VertexHandle> vhVerticesTransformed;
		while( fv_it != pMesh->cfv_end( hFace ) )
		{
			CITAMesh::VertexHandle hVertex( *fv_it++ );

			CITAMesh::VertexHandle hVertexTransformed;

			std::map<const int, CITAMesh::VertexHandle>::key_type key( hVertex.idx( ) );
			std::map<const int, CITAMesh::VertexHandle>::iterator it = mTopLevelVertices.find( key );
			if( it == mTopLevelVertices.end( ) )
			{
				// Add new vertex
				// Get current Point, change type to VistaVector3D, transform point and change the type back
				CITAMesh::Point pPoint            = pMesh->point( hVertex );
				VistaVector3D vPoint              = VistaVector3D( float( pPoint[0] ), float( pPoint[1] ), float( pPoint[2] ) );
				VistaVector3D vPointTransformed   = oTransformMatrix.TransformPoint( vPoint );
				CITAMesh::Point pPointTransformed = CITAMesh::Point( float( vPointTransformed[0] ), float( vPointTransformed[1] ), float( vPointTransformed[2] ) );

				hVertexTransformed = pModel->GetMesh( )->add_vertex( pPointTransformed );

				mTopLevelVertices.insert( std::pair<const int, CITAMesh::VertexHandle>( hVertex.idx( ), hVertexTransformed ) );
			}
			else
			{
				// Connect already registered vertex
				hVertexTransformed = it->second;
			}
			vhVerticesTransformed.push_back( hVertexTransformed );
		}

		pModel->GetMesh( )->add_face( vhVerticesTransformed );
	}

	return pModel;
}

void CMeshModel::CopyFrom( const CMeshModel& oMesh )
{
	this->m_prModelMeshData.reset( new CITAMesh( *oMesh.ConstGetMesh( ) ) );
	m_sModelName = oMesh.m_sModelName;
}

bool CMeshModel::IsWaterproofMesh( ) const
{
	const CITAMesh* pMesh             = m_prModelMeshData.get( );
	CITAMesh::ConstHalfedgeIter he_it = pMesh->halfedges_sbegin( );
	while( he_it != pMesh->halfedges_end( ) )
	{
		CITAMesh::HalfedgeHandle hHalfedge( *he_it++ );
		if( pMesh->is_boundary( hHalfedge ) )
			return false;
	}
	return true;
}

bool CMeshModel::IsValidTriangleMesh( ) const
{
	const CITAMesh* pMesh = m_prModelMeshData.get( );

	// return pMesh->is_triangles(); // always returns false ... own impl required.

	CITAMesh::ConstFaceIter cf_it = pMesh->faces_sbegin( );
	while( cf_it != pMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *cf_it++ );
		uint iFaceValence = pMesh->valence( hFace );
		if( iFaceValence != 3 )
			return false;
	}
	return true;
}

bool CMeshModel::TriangulateMesh( )
{
	if( IsValidTriangleMesh( ) )
		return true;

	CITAMesh* pMesh = m_prModelMeshData.get( );

	pMesh->triangulate( );

	return true;
}

bool CMeshModel::SetFaceAcousticMaterial( int iFaceID, std::shared_ptr<Material::IMaterial> pMaterial )
{
	CITAMesh* pMesh = m_prModelMeshData.get( );
	CITAMesh::FaceHandle hFace( iFaceID );
	auto mMaterialPropertyManager = OpenMesh::getOrMakeProperty<OpenMesh::FaceHandle, ITAGeo::Material::CAcousticFaceProperty>( *pMesh, "Acoustics::Material::Face" );
	mMaterialPropertyManager[hFace].SetMaterial( pMaterial );
	return true;
}

bool CMeshModel::InvertFaceNormals( )
{
	CITAMesh* pMesh = m_prModelMeshData.get( );

	pMesh->update_face_normals( );

	CITAMesh::VertexIter v_it = pMesh->vertices_begin( );
	while( v_it != pMesh->vertices_end( ) )
	{
		CITAMesh::VertexHandle hVertex( *v_it++ );
		pMesh->calc_vertex_normal( hVertex );
		pMesh->set_normal( hVertex, -pMesh->normal( hVertex ) );
	}

	return true;
}

const CITAMesh* CMeshModel::ConstGetMesh( ) const
{
	return m_prModelMeshData.get( );
}

CITAMesh* CMeshModel::GetMesh( )
{
	return m_prModelMeshData.get( );
}

void CMeshModel::GetBoundingBoxAxisAligned( VistaVector3D& v3Min, VistaVector3D& v3Max ) const
{
	const CITAMesh* pMesh = m_prModelMeshData.get( );

	if( pMesh->n_vertices( ) == 0 )
	{
		v3Max.SetToZeroVector( );
		v3Min.SetToZeroVector( );
		return;
	}

	CITAMesh::VertexIter v_it = pMesh->vertices_begin( );

	CITAMesh::VertexHandle hFirstVertex( *v_it++ );
	VistaVector3D v3FirstVertexPoint( pMesh->point( hFirstVertex ).data( ) );
	v3Max = v3FirstVertexPoint;
	v3Min = v3FirstVertexPoint;

	while( v_it != pMesh->vertices_end( ) )
	{
		CITAMesh::VertexHandle hVertex( *v_it++ );
		VistaVector3D v3VertexPoint( pMesh->point( hVertex ).data( ) );

		for( int i = 0; i < 3; i++ )
		{
			if( v3VertexPoint[i] > v3Max[i] )
				v3Max[i] = v3VertexPoint[i];
			if( v3VertexPoint[i] < v3Min[i] )
				v3Min[i] = v3VertexPoint[i];
		}
	}
}

void CMeshModel::GetBoundingSphere( VistaVector3D& v3Centroid, float& fRadius ) const
{
	const CITAMesh* pMesh = m_prModelMeshData.get( );

	v3Centroid = GetBarycenter( );
	fRadius    = 0.0f;

	CITAMesh::VertexIter v_it = pMesh->vertices_begin( );
	while( v_it != pMesh->vertices_end( ) )
	{
		CITAMesh::VertexHandle hVertex( *v_it++ );
		VistaVector3D v3VertexPointOuter( pMesh->point( hVertex ).data( ) );
		const float fDistance = ( v3VertexPointOuter - v3Centroid ).GetLength( );
		if( fDistance > fRadius )
			fRadius = fDistance;
	}
}

VistaVector3D CMeshModel::GetBarycenter( ) const
{
	VistaVector3D v3Barycenter( 0.0f, 0.0f, 0.0f );
	const CITAMesh* pMesh = m_prModelMeshData.get( );

	if( pMesh->n_vertices( ) == 0 )
		return v3Barycenter;

	CITAMesh::ConstVertexIter v_it = pMesh->vertices_begin( );
	while( v_it != pMesh->vertices_end( ) )
	{
		CITAMesh::VertexHandle hVertex( *v_it++ );
		VistaVector3D v3VertexPoint( pMesh->point( hVertex ).data( ) );
		v3Barycenter += v3VertexPoint;
	}

	return v3Barycenter / float( pMesh->n_vertices( ) );
}

bool CMeshModel::GetInsideBoundingBoxAxisAligned( const VistaVector3D& v3Pos ) const
{
	VistaVector3D v3Min, v3Max;
	GetBoundingBoxAxisAligned( v3Min, v3Max );

	if( ( v3Pos[Vista::X] < v3Min[Vista::X] ) || ( v3Pos[Vista::X] > v3Max[Vista::X] ) )
		return false;
	if( ( v3Pos[Vista::Y] < v3Min[Vista::Y] ) || ( v3Pos[Vista::Y] > v3Max[Vista::Y] ) )
		return false;
	if( ( v3Pos[Vista::Z] < v3Min[Vista::Z] ) || ( v3Pos[Vista::Z] > v3Max[Vista::Z] ) )
		return false;

	return true;
}

bool CMeshModel::GetInsideBoundingSphere( const VistaVector3D& v3Pos ) const
{
	VistaVector3D v3Centroid;
	float fRadius = 0.0f;
	GetBoundingSphere( v3Centroid, fRadius );

	return bool( ( v3Centroid - v3Pos ).GetLength( ) < fRadius );
}

bool CMeshModel::GetInsideMeshJordanMethodRandomRay( const VistaVector3D& v3Pos ) const
{
	if( !IsWaterproofMesh( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Model is not waterproof, can't use Jordan point-in-polygon method" );

	VistaVector3D v3Dir;
	while( v3Dir.GetLength( ) == 0.0f )
	{
		v3Dir[Vista::X] = (float)VistaRandomNumberGenerator::GetStandardRNG( )->GenerateDouble( -1, 1 );
		v3Dir[Vista::Y] = (float)VistaRandomNumberGenerator::GetStandardRNG( )->GenerateDouble( -1, 1 );
		v3Dir[Vista::Z] = (float)VistaRandomNumberGenerator::GetStandardRNG( )->GenerateDouble( -1, 1 );
	}

	VistaRay rRandomRay( v3Pos, v3Dir );

	auto pMesh  = this->ConstGetMesh( );
	auto rFaces = pMesh->faces( );

	// Counter for number of intersections
	int iCounter = 0;

	// Iterate over faces, check point in polygon and count even or odd hits
	for( auto hFace: rFaces )
	{
		VistaPolygon oPolygon;

		ITAGeoUtils::VistaPolygonFromOpenMeshFace( pMesh, hFace, oPolygon );

		VistaVector3D v3IntersectionPoint;
		bool bIntersection = ITAGeoUtils::RayConvexPolygonIntersectionTest( rRandomRay, oPolygon, v3IntersectionPoint, ECulling::NONE );

		if( bIntersection )
			iCounter++;
	}

	// Points inside a mesh have an odd number of intersections
	if( iCounter % 2 == 0 )
		return false;
	else
		return true;
}

bool CMeshModel::GetInsideMeshJordanMethodAxisAligned( const VistaVector3D& v3Pos ) const
{
	// Directions for ray in x, y and z direction
	VistaVector3D v3DirX( 1, 0, 0, 1 );
	VistaVector3D v3DirY( 0, 1, 0, 1 );
	VistaVector3D v3DirZ( 0, 0, 1, 1 );

	// Counters for number of intersections
	int iCountX = 0;
	int iCountY = 0;
	int iCountZ = 0;

	// Rays
	VistaRay rRayX( v3Pos, v3DirX );
	VistaRay rRayY( v3Pos, v3DirY );
	VistaRay rRayZ( v3Pos, v3DirZ );

	// Mesh and its faces
	auto pMesh  = this->ConstGetMesh( );
	auto rFaces = pMesh->faces( );

	// Iterate over faces, check point in polygon and count even or odd hits
	for( auto hFace: rFaces )
	{
		VistaPolygon oPolygon;

		ITAGeoUtils::VistaPolygonFromOpenMeshFace( pMesh, hFace, oPolygon );

		VistaVector3D v3IntersectionPoint;

		// Intersection test for x direction
		bool bIntersectionX = ITAGeoUtils::RayConvexPolygonIntersectionTest( rRayX, oPolygon, v3IntersectionPoint, ECulling::NONE );
		if( bIntersectionX )
			iCountX++;

		// Intersection test for y direction
		bool bIntersectionY = ITAGeoUtils::RayConvexPolygonIntersectionTest( rRayY, oPolygon, v3IntersectionPoint, ECulling::NONE );
		if( bIntersectionY )
			iCountY++;

		// Intersection test for z direction
		bool bIntersectionZ = ITAGeoUtils::RayConvexPolygonIntersectionTest( rRayZ, oPolygon, v3IntersectionPoint, ECulling::NONE );
		if( bIntersectionZ )
			iCountZ++;
	}

	// Booleans for the inside tests of each direction and the boolean state of the majority
	bool bInsideX        = iCountX % 2 != 0;
	bool bInsideY        = iCountY % 2 != 0;
	bool bInsideZ        = iCountZ % 2 != 0;
	bool bInsideMajority = ( bInsideX & bInsideY ) || ( bInsideX & bInsideZ ) || ( bInsideY & bInsideZ ); // NMK: bitwise and correct?

	return bInsideMajority;
}

void CMeshModel::FilterVisiblePaths( CPropagationPathList& oPathListIn, CPropagationPathList& oPathListOut ) const
{
	CPropagationPathList oPathListCopy;

	// Add identifier of path list
	oPathListCopy.sIdentifier = oPathListIn.sIdentifier;

	// Iterate over each path of oPathListIn and add visible paths to oPathListOut
	for( auto oPath: oPathListIn )
	{
		if( IsPathVisible( oPath ) )
			oPathListCopy.push_back( oPath );
	}

	oPathListOut = oPathListCopy;
}

bool CMeshModel::IsPathVisible( const CPropagationPath& oPath ) const
{
	auto pMesh = this->ConstGetMesh( );

	// Search for intersections between two neighbored anchors and repeat it for all neighbored anchors of the path
	for( int i = 0; i < oPath.GetNumAnchors( ) - 1; i++ )
	{
		VistaVector3D v3StartPoint = oPath[i]->v3InteractionPoint;
		VistaVector3D v3EndPoint   = oPath[i + 1]->v3InteractionPoint;

		// Iterate intersection search over all faces
		auto faceIter = pMesh->faces_begin( );
		while( faceIter != pMesh->faces_end( ) )
		{
			CITAMesh::FaceHandle hFace( *faceIter++ );

			// If one of the path sections crosses one of the faces, the whole path won't be visible
			if( ITAGeoUtils::IsLineIntersectingFace( v3StartPoint, v3EndPoint, pMesh, hFace ) == EIntersecting::BETWEEN )
				return false;
		}
	}

	// No intersections occur
	return true;
}

size_t ITAGeo::Halfedge::CMeshModel::GetNumFaces( ) const
{
	return m_prModelMeshData->n_faces( );
}


//---CMeshModelList----------------------------------------------------------------------------------------------------------------------------

bool ITAGeo::Halfedge::CMeshModelList::Load( const std::string& sModelFilePath, bool bLoadTopLevelMesh /* = true */ )
{
	VistaFileSystemFile f( sModelFilePath );

	if( !f.Exists( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Could not load mesh model file. '" + sModelFilePath + "', does not exist." );

	std::string sFileBaseName = f.GetLocalName( );
	size_t idx                = sFileBaseName.find_last_of( '.' );

	std::string sFileEnding = sFileBaseName.substr( idx + 1, sFileBaseName.size( ) - 1 );

	bool bCompleteLoaded = false;

	if( sFileBaseName.compare( "skp" ) )
	{
#ifdef URBAN_MODEL_WITH_SKETCHUP_SUPPORT
		SketchUp::CModel oSketchUpModel;
		oSketchUpModel.SetMaterialManager( m_pMaterialManager );
		oSketchUpModel.Load( sModelFilePath );

		// Local copy of CMeshModelShared
		Halfedge::CMeshModelShared pMeshCopy;

		// If bLoadTopLevelMesh is set, add top level mesh
		if( bLoadTopLevelMesh )
		{
			// Get the mesh models
			pMeshCopy = std::make_shared<CMeshModel>( );
			pMeshCopy->CopyFrom( *oSketchUpModel.GetTopLevelMesh( ) );
			this->push_back( pMeshCopy );
			this->at( 0 )->SetName( "TopLevelMesh" );
		}

		// Add each grouped mesh model to vector
		for( auto pMeshModel: oSketchUpModel.GetGroupMeshModels( ) )
		{
			// Get the mesh models
			pMeshCopy = std::make_shared<CMeshModel>( );
			pMeshCopy->CopyFrom( *pMeshModel );
			this->push_back( pMeshCopy );
		}

		// Add component mesh models to the meshes as grouped mesh models
		for( auto pMeshModel: oSketchUpModel.GetComponentMeshModels( ) )
		{
			// Get the mesh models
			pMeshCopy = std::make_shared<CMeshModel>( );
			pMeshCopy->CopyFrom( *pMeshModel );
			this->push_back( pMeshCopy );
		}

		// Add association of mesh names to their indices
		for( int i = 0; i < this->size( ); i++ )
			m_mapMeshNameToIndex[this->at( i )->GetName( )] = i;


		bCompleteLoaded = true;
#else
		ITA_EXCEPT1( NOT_IMPLEMENTED, "Loading Sketchup models is not supported with this build of ITAGeo" );
#endif
	}

#ifdef URBAN_MODEL_WITH_CITYGML_SUPPORT
	if( sFileBaseName.compare( "gml" ) )
	{
		ITA_EXCEPT1( NOT_IMPLEMENTED, "Loading CityGML models as urban models is currently not supported" );
	}
#endif

	// Set number of faces
	for( auto pMeshModel: *this )
		m_iTotalNumFaces += pMeshModel->GetMesh( )->n_faces( );

	return bCompleteLoaded;
}

bool ITAGeo::Halfedge::CMeshModelList::Store( const std::string& sMeshModelFilePath, bool bOverwrite /* = true*/ ) const
{
	VistaFileSystemFile oFile( sMeshModelFilePath );
	VistaFileSystemDirectory oParentDir( oFile.GetParentDirectory( ) );
	if( oParentDir.Exists( ) == false )
		ITA_EXCEPT1( INVALID_PARAMETER, "Target folder '" + oParentDir.GetName( ) + "' does not exist or is invalid" );

	if( oFile.Exists( ) && bOverwrite == false )
		ITA_EXCEPT1( MODAL_EXCEPTION, "File '" + oFile.GetName( ) + "' already exists and shall not be overwritten" );


	std::string sFileBaseName = oFile.GetLocalName( );
	size_t idx                = sFileBaseName.find_last_of( '.' );

	std::string sFileEnding = sFileBaseName.substr( idx + 1, sFileBaseName.size( ) - 1 );


	if( sFileBaseName.compare( "skp" ) )
	{
#ifdef URBAN_MODEL_WITH_SKETCHUP_SUPPORT
		SketchUp::CModel oSketchUpModel;

		for( auto pMeshModel: *this )
			oSketchUpModel.AddGroupMeshModel( pMeshModel.get( ) );


		oSketchUpModel.Store( sMeshModelFilePath, bOverwrite );

		return true;
#else
		ITA_EXCEPT1( NOT_IMPLEMENTED, "Storing Sketchup models is not supported with this build of ITAGeo" );
#endif
	}
	else
		ITA_EXCEPT_NOT_IMPLEMENTED;

	return false;
}

void ITAGeo::Halfedge::CMeshModelList::CopyFrom( const CMeshModelList& vpMeshModelList )
{
	// Reset mesh model list
	this->clear( );
	m_mapMeshNameToIndex.clear( );
	m_iTotalNumFaces = 0;

	for( int i = 0; i < vpMeshModelList.size( ); i++ )
	{
		auto pMeshModel = vpMeshModelList[i];

		Halfedge::CMeshModelShared pCurrentCopy = std::make_shared<CMeshModel>( );
		pCurrentCopy->CopyFrom( *pMeshModel );

		this->push_back( pCurrentCopy );
		m_mapMeshNameToIndex[this->at( i )->GetName( )] = i;
	}

	for( auto pMeshModel: *this )
	{
		m_iTotalNumFaces += pMeshModel->GetMesh( )->n_faces( );
	}
}

void ITAGeo::Halfedge::CMeshModelList::FilterVisiblePaths( CPropagationPathList& oPathListIn, CPropagationPathList& oPathListOut ) const
{
	CPropagationPathList oPathListCopy;

	// Add identifier of path list
	oPathListCopy.sIdentifier = oPathListIn.sIdentifier;

	// Iterate over each path of oPathListIn and add visible paths to oPathListOut
	for( auto oPath: oPathListIn )
	{
		if( IsPathVisible( oPath ) )
			oPathListCopy.push_back( oPath );
	}

	oPathListOut = oPathListCopy;
}

bool ITAGeo::Halfedge::CMeshModelList::IsPathVisible( const CPropagationPath& oPath ) const
{
	// Search for intersections between two neighbored anchors and repeat it for all neighbored anchors of the path
	for( int i = 0; i < oPath.GetNumAnchors( ) - 1; i++ )
	{
		VistaVector3D v3StartPoint = oPath[i]->v3InteractionPoint;
		VistaVector3D v3EndPoint   = oPath[i + 1]->v3InteractionPoint;

		// Iterate intersection search over all mesh models
		for( int iteration = 0; iteration < this->size( ); iteration++ )
		{
			// Iterate intersection search over all faces of current mesh model
			auto pMesh    = this->at( iteration )->GetMesh( );
			auto faceIter = pMesh->faces_begin( );
			while( faceIter != pMesh->faces_end( ) )
			{
				CITAMesh::FaceHandle hFace( *faceIter++ );

				// If one of the path sections crosses one of the faces, the whole path won't be visible
				if( ITAGeoUtils::IsLineIntersectingFace( v3StartPoint, v3EndPoint, pMesh, hFace ) == EIntersecting::BETWEEN )
					return false;
			}
		}
	}

	// No intersections occur
	return true;
}
