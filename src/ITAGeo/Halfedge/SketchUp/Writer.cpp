#include <ITAGeo/Halfedge/SketchUp/Writer.h>
#include <ITAGeo/SketchUp/Conversions.h>
#include <ITAGeo/SketchUp/Helper.h>


// OpenMesh includes
#pragma warning( disable : 4512 4127 )
#include <OpenMesh/Core/IO/IOManager.hh>
#include <OpenMesh/Core/Mesh/Handles.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#pragma warning( default : 4512 4127 )

// ITA includes
#include <ITAException.h>

// Sketchup includes
#include <SketchUpAPI/geometry.h>
#include <SketchUpAPI/initialize.h>
#include <SketchUpAPI/model/component_definition.h>
#include <SketchUpAPI/model/component_instance.h>
#include <SketchUpAPI/model/drawing_element.h>
#include <SketchUpAPI/model/edge.h>
#include <SketchUpAPI/model/entities.h>
#include <SketchUpAPI/model/face.h>
#include <SketchUpAPI/model/geometry_input.h>
#include <SketchUpAPI/model/group.h>
#include <SketchUpAPI/model/material.h>
#include <SketchUpAPI/model/model.h>
#include <SketchUpAPI/model/vertex.h>
#include <SketchUpAPI/transformation.h>
#include <SketchUpAPI/unicodestring.h>

// Vista includes
#include <VistaTools/VistaFileSystemFile.h>

// Using namespace
using namespace ITAGeo::Halfedge::SketchUp;

typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;

CWriter::CWriter( )
{
	OpenMesh::IO::IOManager( ).register_module( this );
	sModelName = "ITAGeoHalfedgeSketchUpGenericWriter";
	iSUVersion = (int)SUModelVersion_SU2015;
}

CWriter::~CWriter( ) {}

bool CWriter::write( const std::string& _filename, OpenMesh::IO::BaseExporter& oBaseExporter, OpenMesh::IO::Options, std::streamsize ) const
{
	VistaFileSystemFile oFile( _filename );
	if( oFile.IsDirectory( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "File target is a directory" );
	if( oFile.IsReadOnly( ) && oFile.Exists( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "File target is read-only" );

	SUInitialize( );

	SUResult sur      = SU_ERROR_NONE;
	SUModelRef rModel = SU_INVALID;

	if( ( sur = SUModelCreate( &rModel ) ) != SU_ERROR_NONE )
		ITA_EXCEPT1( INVALID_PARAMETER, "Could not create SU model with error code: " + std::to_string( long double( sur ) ) );

	sur = SUModelSetName( rModel, sModelName.c_str( ) );

	SUEntitiesRef rEntities = SU_INVALID;
	SUModelGetEntities( rModel, &rEntities );

	SUGeometryInputRef rGeoInput = SU_INVALID;
	SUGeometryInputCreate( &rGeoInput );

	// Geometry
	std::vector<SUPoint3D> vVertices( oBaseExporter.n_vertices( ) );
	for( size_t i = 0; i < oBaseExporter.n_vertices( ); ++i )
	{
		SUPoint3D& rSUPoint( vVertices[i] );
		CITAMesh::Point pPoint = oBaseExporter.point( OpenMesh::VertexHandle( int( i ) ) );
		rSUPoint               = ITAMeshPointToSUPoint3D( pPoint );
	}

	if( vVertices.size( ) > 0 )
	{
		if( ( sur = SUGeometryInputSetVertices( rGeoInput, vVertices.size( ), &( vVertices[0] ) ) ) != SU_ERROR_NONE )
			ITA_EXCEPT1( INVALID_PARAMETER, "SUGeometryInputSetVertices: " + std::to_string( long double( sur ) ) );
	}

	for( size_t n = 0; n < oBaseExporter.n_faces( ); ++n )
	{
		std::vector<OpenMesh::VertexHandle> vhVertices;
		OpenMesh::FaceHandle hFace = OpenMesh::FaceHandle( int( n ) );
		unsigned int nFaceVertices = oBaseExporter.get_vhandles( hFace, vhVertices ); // Warning: assumes, that OpenMesh face handles are ascending (0..n-1)
		assert( nFaceVertices == unsigned int( vhVertices.size( ) ) );

		SULoopInputRef rLoop = SU_INVALID;
		if( ( sur = SULoopInputCreate( &rLoop ) ) != SU_ERROR_NONE )
			ITA_EXCEPT1( INVALID_PARAMETER, "Could not create loop with error code: " + std::to_string( long double( sur ) ) );

		for( size_t j = 0; j < vhVertices.size( ); ++j )
		{
			size_t iVertexIndex = vhVertices[j].idx( );
			if( ( sur = SULoopInputAddVertexIndex( rLoop, iVertexIndex ) ) != SU_ERROR_NONE )
				ITA_EXCEPT1( INVALID_PARAMETER, "Could not add vertex index with error code: " + std::to_string( long double( sur ) ) );
		}

		size_t iFaceIndex;
		if( ( sur = SUGeometryInputAddFace( rGeoInput, &rLoop, &iFaceIndex ) ) != SU_ERROR_NONE )
			ITA_EXCEPT1( INVALID_PARAMETER, "Could not create face with error code: " + std::to_string( long double( sur ) ) );

		SULoopInputRelease( &rLoop );

		const OpenMesh::Vec4ui& v4FaceColor( oBaseExporter.colorAi( hFace ) );
		SUMaterialRef rFaceMaterial = SU_INVALID;
		SU_EXC( SUMaterialCreate( &rFaceMaterial ) );
		SUColor oSUColor;
		oSUColor.red   = unsigned char( v4FaceColor[0] );
		oSUColor.green = unsigned char( v4FaceColor[1] );
		oSUColor.blue  = unsigned char( v4FaceColor[2] );
		oSUColor.alpha = unsigned char( v4FaceColor[3] );
		SU_EXC( SUMaterialSetColor( rFaceMaterial, &oSUColor ) );
		SU_EXC( SUModelAddMaterials( rModel, 1, &rFaceMaterial ) );

		SUMaterialInput oMatInput;
		oMatInput.num_uv_coords = 0;
		oMatInput.material      = rFaceMaterial;
		SU_EXC( SUGeometryInputFaceSetFrontMaterial( rGeoInput, iFaceIndex, &oMatInput ) );
	}

	if( vVertices.size( ) > 0 )
	{
		if( ( sur = SUEntitiesFill( rEntities, rGeoInput, true ) ) != SU_ERROR_NONE )
			ITA_EXCEPT1( INVALID_PARAMETER, "Could not fill geo input, error code: " + std::to_string( long double( sur ) ) );
	}

	SUGeometryInputRelease( &rGeoInput );

	SUModelSaveToFileWithVersion( rModel, _filename.c_str( ), (SUModelVersion)iSUVersion );

	SUModelRelease( &rModel );

	SUTerminate( );

	return true;
}

bool CWriter::write( std::ostream&, OpenMesh::IO::BaseExporter&, OpenMesh::IO::Options, std::streamsize ) const
{
	ITA_EXCEPT1( NOT_IMPLEMENTED, "ostream writer not implemented" );
}

CWriter g_oSUWriterInstance; //!< Global writer instance, registers skp file extension
