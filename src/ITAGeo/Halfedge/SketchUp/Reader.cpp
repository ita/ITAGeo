#include <ITAGeo/Halfedge/SketchUp/Reader.h>
#include <ITAGeo/SketchUp/Conversions.h>
#include <ITAGeo/SketchUp/Helper.h>

#pragma warning( disable : 4512 4127 )


// OpenMesh includes
#include <OpenMesh/Core/IO/IOManager.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

#pragma warning( default : 4512 4127 )

// ITA includes
#include <ITAException.h>

// Sketchup includes
#include <SketchUpAPI/geometry.h>
#include <SketchUpAPI/initialize.h>
#include <SketchUpAPI/model/component_definition.h>
#include <SketchUpAPI/model/component_instance.h>
#include <SketchUpAPI/model/drawing_element.h>
#include <SketchUpAPI/model/edge.h>
#include <SketchUpAPI/model/entities.h>
#include <SketchUpAPI/model/face.h>
#include <SketchUpAPI/model/group.h>
#include <SketchUpAPI/model/material.h>
#include <SketchUpAPI/model/model.h>
#include <SketchUpAPI/model/vertex.h>
#include <SketchUpAPI/transformation.h>
#include <SketchUpAPI/unicodestring.h>

// Vista includes
#include <VistaMath/VistaGeometries.h>
#include <VistaTools/VistaFileSystemFile.h>

// STL
#include <list>

// Using namespace
using namespace ITAGeo::Halfedge::SketchUp;

typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;

CReader::CReader( )
{
	OpenMesh::IO::IOManager( ).register_module( this );
}

CReader::~CReader( ) {}

bool CReader::read( const std::string& sFilePath, OpenMesh::IO::BaseImporter& oBaseImporter, OpenMesh::IO::Options& )
{
	SUInitialize( );
	SUResult sur;

	SUModelRef rModel = SU_INVALID;
	if( ( sur = SUModelCreateFromFile( &rModel, sFilePath.c_str( ) ) ) != SU_ERROR_NONE )
		ITA_EXCEPT1( INVALID_PARAMETER, "SUModelCreateFromFile: Encountered SU error code " + std::to_string( long double( sur ) ) );

	int iMinorVersion, iMajorVersion, iBuild;
	if( ( sur = SUModelGetVersion( rModel, &iMajorVersion, &iMinorVersion, &iBuild ) ) != SU_ERROR_NONE )
		ITA_EXCEPT1( INVALID_PARAMETER, "SUModelGetVersion: Encountered SU error code " + std::to_string( long double( sur ) ) );

	if( iMajorVersion == 2014 )
		iSUVersion = SUModelVersion_SU2014;
	if( iMajorVersion == 2015 )
		iSUVersion = SUModelVersion_SU2015;

	SUStringRef sSUModelName = SU_INVALID;
	SUStringCreate( &sSUModelName );
	if( ( sur = SUModelGetName( rModel, &sSUModelName ) ) != SU_ERROR_NONE )
		ITA_EXCEPT1( INVALID_PARAMETER, "SUModelGetName: Encountered SU error code " + std::to_string( long double( sur ) ) );
	sModelName = SUStringToStdString( sSUModelName );

	SUModelUnits iSUUnits;
	if( ( sur = SUModelGetUnits( rModel, &iSUUnits ) ) != SU_ERROR_NONE )
		ITA_EXCEPT1( INVALID_PARAMETER, "SUModelGetUnits: Encountered SU error code " + std::to_string( long double( sur ) ) );

	SUEntitiesRef rModelEntities = SU_INVALID;
	if( ( sur = SUModelGetEntities( rModel, &rModelEntities ) ) != SU_ERROR_NONE )
		ITA_EXCEPT1( INVALID_PARAMETER, "SUModelGetEntities: Encountered SU error code " + std::to_string( long double( sur ) ) );

	size_t nNumEntityInstances = 0;
	sur                        = SUEntitiesGetNumInstances( rModelEntities, &nNumEntityInstances );
	if( nNumEntityInstances > 0 )
		std::cerr << "Found instances of entities that will be ignored. Currently, only top-level faces are considered." << std::endl;

	size_t nNumFaces = 0;
	sur              = SUEntitiesGetNumFaces( rModelEntities, &nNumFaces );

	if( nNumFaces == 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Sketchup file does not contain top-level faces" );

	std::vector<SUFaceRef> vrFaces( nNumFaces );
	size_t nActualNumFaces = 0;
	sur                    = SUEntitiesGetFaces( rModelEntities, nNumFaces, &vrFaces[0], &nActualNumFaces );
	assert( nActualNumFaces == nNumFaces );

	// Iterate over faces and translate into MyMesh representation
	SUPoint3D vSUPoint3D;
	std::map<const void*, CITAMesh::VertexHandle> mTopLevelVertices;
	std::map<std::string, int> mTextures;
	int iTextureIndex = 0;
	for( size_t n = 0; n < nNumFaces; n++ )
	{
		const SUFaceRef& rFace( vrFaces[n] );


		// Geometry

		size_t nNumEdges = 0;
		sur              = SUFaceGetNumEdges( rFace, &nNumEdges );

		std::vector<SUEdgeRef> vrEdges( nNumEdges );
		size_t nActualEdgeCount = 0;
		if( nNumEdges )
			sur = SUFaceGetEdges( rFace, nNumEdges, &vrEdges[0], &nActualEdgeCount );

		size_t nNumVertices = 0, nActualNumVertices = 0;
		sur = SUFaceGetNumVertices( rFace, &nNumVertices );
		std::vector<SUVertexRef> vrVertices( nNumVertices );
		sur = SUFaceGetVertices( rFace, nNumVertices, &vrVertices[0], &nActualNumVertices );

		std::vector<CITAMesh::VertexHandle> vhVertices( nNumVertices );
		for( size_t m = 0; m < nNumVertices; m++ )
		{
			const SUVertexRef& rVertex( vrVertices[m] ); // SU vertex
			CITAMesh::VertexHandle hVertex;              // OpenMesh vertex

			std::map<const void*, CITAMesh::VertexHandle>::key_type key( rVertex.ptr );
			std::map<const void*, CITAMesh::VertexHandle>::iterator it = mTopLevelVertices.find( key );
			if( it == mTopLevelVertices.end( ) )
			{
				// Add new vertex
				sur     = SUVertexGetPosition( rVertex, &vSUPoint3D );
				hVertex = oBaseImporter.add_vertex( SUPoint3DToITAMeshPoint( vSUPoint3D, iSUUnits ) );
				mTopLevelVertices.insert( std::pair<const void*, CITAMesh::VertexHandle>( rVertex.ptr, hVertex ) );
			}
			else
			{
				// Connect already registered vertex
				hVertex = it->second;
			}
			vhVertices[m] = hVertex;
		}


		// Add face

		CITAMesh::FaceHandle hFace = oBaseImporter.add_face( vhVertices );
		if( !hFace.is_valid( ) )
		{
			std::cerr << "Invalid face handle, could not add face from vertices list during SketchUp file import. Vertices index list: [ ";
			for( auto v: vhVertices )
				std::cout << v.idx( ) << " ";
			std::cout << " ]" << std::endl;
			continue;
		}


		// Material / textures

		SUMaterialRef rMaterial = SU_INVALID;
		sur                     = SUFaceGetFrontMaterial( rFace, &rMaterial );
		if( sur != SU_ERROR_NO_DATA )
		{
			std::string sMaterialName;
			SUStringRef rMaterialName = SU_INVALID;
			SUStringCreate( &rMaterialName );
			if( SUMaterialGetName( rMaterial, &rMaterialName ) == SU_ERROR_NONE )
				sMaterialName = SUStringToStdString( rMaterialName );

			if( mTextures.find( sMaterialName ) == mTextures.end( ) )
				mTextures[sMaterialName] = iTextureIndex++;

			oBaseImporter.set_face_texindex( hFace, mTextures[sMaterialName] );
		}
	}

	// Add material information

	for( auto a: mTextures )
		oBaseImporter.add_texture_information( a.second, a.first );

	// Release
	if( ( sur = SUModelRelease( &rModel ) ) != SU_ERROR_NONE )
		ITA_EXCEPT1( INVALID_PARAMETER, "Encountered SU error code " + std::to_string( long double( sur ) ) );

	SUTerminate( );

	return true;
};

bool CReader::read( std::istream&, OpenMesh::IO::BaseImporter&, OpenMesh::IO::Options& )
{
	ITA_EXCEPT1( NOT_IMPLEMENTED, "instream reader is not implemented" );
}

CReader g_oSUReaderInstance; //!< Global reader instance
