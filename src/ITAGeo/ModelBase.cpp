#include <ITAGeo/ModelBase.h>

// ITABase includes
#include <ITAException.h>


// STL include
#include <assert.h>
#include <iomanip>


void ITAGeo::CModelBase::SetMaterialManager( std::shared_ptr<Material::CMaterialManager> pMaterialManager )
{
	m_pMaterialManager = pMaterialManager;
}

std::ostream& operator<<( std::ostream& oIn, const ITAGeo::CModelBase& oModel )
{
	oIn << "Name: " << oModel.GetName( ) << std::endl;
	return oIn;
}

void ITAGeo::CModelBase::SetName( const std::string& sName )
{
	m_sModelName = sName;
}

std::string ITAGeo::CModelBase::GetName( ) const
{
	return m_sModelName;
}
