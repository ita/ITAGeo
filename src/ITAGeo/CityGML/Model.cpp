// ITAGeo includes
#include <ITAGeo/CityGML/Conversions.h>
#include <ITAGeo/CityGML/Model.h>


// ITABase includes
#include <ITAException.h>
#include <ITAStopWatch.h>

// libcitygml
#include <citygml/citygml.h>
#include <citygml/citymodel.h>
#include <citygml/cityobject.h>
#include <citygml/envelope.h>
#include <citygml/geometry.h>
#include <citygml/linearring.h>
#include <citygml/linestring.h>
#include <citygml/object.h>
#include <citygml/polygon.h>
#include <citygml/tesselator.h>

#ifdef ITA_GEO_WITH_XERCES_SUPPORT
// Xerces-c
#	include <xercesc/sax/SAXParseException.hpp>
#endif

// OpenMesh includes
#pragma warning( disable : 4512 4127 )
#include <OpenMesh/Core/IO/IOManager.hh>
#include <OpenMesh/Core/IO/importer/ImporterT.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#pragma warning( default : 4512 4127 )

typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;


// STL
#include <cassert>
#include <iostream>
#include <math.h>
#include <memory>
#include <string>


class ITAGeo::CCityGMLPtr
{
public:
	std::shared_ptr<const citygml::CityModel> pCityModel;
	std::shared_ptr<citygml::CityGMLLogger> pLogger = nullptr;
};


//!  Resolved for Polygons without Holes // throws Polygon Connectivity error at seemingly random points.
void ITAGeo::CCityGMLModel::GetBuildingMesh( const ITAGeo::CCityGMLModel::CCityBuilding* pBuilding, ITAGeo::Halfedge::CMeshModel* pMeshModel, const VistaVector3D v3Shift,
                                             const bool bCreateConnectedMesh )
{
	const citygml::CityObject& oBuilding = m_pCityModel->pCityModel->getRootCityObject( pBuilding->iNum - 1 );
	CITAMesh* pMesh                      = pMeshModel->GetMesh( );

	std::vector<TVec3d> voPointCloud;
	std::vector<CITAMesh::VertexHandle> vvhVertices;

	CITAMesh::Point vpGlobalShift( v3Shift[Vista::X], v3Shift[Vista::Y], v3Shift[Vista::Z] );

	std::cout << "Building mesh for object " << oBuilding.getId( ) << std::endl;

	for( unsigned int j = 0; j < oBuilding.getChildCityObjectsCount( ); j++ )
	{
		for( unsigned int k = 0; k < oBuilding.getChildCityObject( j ).getGeometriesCount( ); k++ )
		{
			for( unsigned int l = 0; l < oBuilding.getChildCityObject( j ).getGeometry( k ).getPolygonsCount( ); l++ )
			{
				const citygml::CityObject& oBuildingPart         = oBuilding.getChildCityObject( j );
				std::shared_ptr<const citygml::Polygon> pPolygon = oBuildingPart.getGeometry( k ).getPolygon( l );
				std::vector<TVec3d> vPolygonVertices             = pPolygon->getVertices( );
				assert( vPolygonVertices.size( ) > 2 );

				//! Actual Mesh conversion
				std::vector<CITAMesh::VertexHandle> vFaceVertices;

				// Multiple faces can share the same vertices, following loop makes sure only new vertices are added to mesh
				for( unsigned int m = 0; m < vPolygonVertices.size( ) - 1; m++ )
				{
					TVec3d& v( vPolygonVertices[m] );

					bool bSkip = false;
					for( size_t n = 0; n < voPointCloud.size( ); n++ )
					{
						const TVec3d& p( voPointCloud[n] );
						if( v.x == p.x && v.y == p.y && v.z == p.z )
						{
							bSkip = true;
							vFaceVertices.push_back( vvhVertices[n] );
						}
					}

					if( !bSkip )
					{
						CITAMesh::VertexHandle vh = pMesh->add_vertex( CityGMLPoint3DToITAMeshPoint( v ) + vpGlobalShift );

						if( bCreateConnectedMesh )
							voPointCloud.push_back( v );
						vvhVertices.push_back( vh );

						vFaceVertices.push_back( vh );
					}
				}

				// Unique check on vertex handles for new face
				std::set<CITAMesh::VertexHandle> sVertexSet( vFaceVertices.begin( ), vFaceVertices.end( ) );
				std::vector<CITAMesh::VertexHandle> vPrunedFaceVertices;
				vPrunedFaceVertices.assign( sVertexSet.begin( ), sVertexSet.end( ) );

				if( vPrunedFaceVertices.size( ) != vFaceVertices.size( ) )
				{
					std::cerr << "Found non-unique entries in linear ring at " << oBuildingPart.getId( ) << ", trying to add pruned building part." << std::endl;
					if( vPrunedFaceVertices.size( ) > 2 )
						pMesh->add_face( vPrunedFaceVertices );
					continue;
				}

				pMesh->add_face( vFaceVertices );
			}
		}
	}
}

std::vector<const ITAGeo::CCityGMLModel::CCityBuilding*> ITAGeo::CCityGMLModel::GetBuildings( ) const
{
	std::vector<const ITAGeo::CCityGMLModel::CCityBuilding*> buildings;
	for( size_t n = 0; n < m_voBuildings.size( ); n++ )
		buildings.push_back( &m_voBuildings[n] );
	return buildings;
}

ITAGeo::CCityGMLModel::CCityGMLModel( ) : m_pCityModel( new ITAGeo::CCityGMLPtr )
{
	SetName( "Unnamed_CityGML" );
}

ITAGeo::CCityGMLModel::~CCityGMLModel( )
{
	// m_pCityModel->pCityModel = nullptr;
	// m_pCityModel->pLogger = nullptr;
	// delete m_pCityModel;
}

bool ITAGeo::CCityGMLModel::Load( const std::string& sCityGMLFilePath )
{
	citygml::ParserParams oParserParams;

	try
	{
		m_pCityModel->pCityModel                          = citygml::load( sCityGMLFilePath, oParserParams, m_pCityModel->pLogger );
		const citygml::ConstCityObjects& pCityRootObjects = m_pCityModel->pCityModel->getRootCityObjects( );

		for( unsigned int i = 0; i < pCityRootObjects.size( ); i++ )
		{
			const citygml::CityObject* Object = pCityRootObjects[i];
		}
		return true;
	}
#ifdef ITA_GEO_WITH_XERCES_SUPPORT
	catch( SAXParseException& e )
	{
		ITA_EXCEPT_INVALID_PARAMETER( "Could not parse CityGML file " + sCityGMLFilePath + ": " + e.str( ) );
	}
#endif
	catch( ... )
	{
		ITA_EXCEPT_INVALID_PARAMETER( "Could not parse CityGML file " + sCityGMLFilePath );
	}

	return true;
}

bool ITAGeo::CCityGMLModel::Store( const std::string& sGMLFilePath, bool bOverwrite /* = true */ ) const
{
	ITA_EXCEPT1( NOT_IMPLEMENTED, "Storing GML files is not supported, yet" );
	return false;
}

void ITAGeo::CCityGMLModel::CalculateObjectCentroids( )
{
	CCityBuilding oCCityObject;
	const citygml::ConstCityObjects& pCityRootObjects = m_pCityModel->pCityModel->getRootCityObjects( );

	for( unsigned int i = 0; i < pCityRootObjects.size( ); i++ )
	{
		const citygml::CityObject* pCityObject = pCityRootObjects[i];
		std::vector<TVec3d> vPolygonCentroids;
		std::vector<TVec3d> vObjectVertices;

		for( unsigned int j = 0; j < pCityObject->getChildCityObjectsCount( ); j++ )
		{
			for( unsigned int k = 0; k < pCityObject->getChildCityObject( j ).getGeometriesCount( ); k++ )
			{
				for( unsigned int l = 0; l < pCityObject->getChildCityObject( j ).getGeometry( k ).getPolygonsCount( ); l++ )
				{
					std::shared_ptr<const citygml::Polygon> pPolygon = pCityObject->getChildCityObject( j ).getGeometry( k ).getPolygon( l );
					std::vector<TVec3d> vPolygonVertices             = pPolygon->getVertices( );
					vPolygonVertices.pop_back( ); // CityGML Polygons are defined by a line ring that connects all vertices counterclockwise.
					// By definiton the last vertex has to equal the first. Deleting the last vertex to ommit duplicates.

					// Calculate Centroid of CityGML Polygon by averaging out points
					float fPolygonCentroid_X = 0;
					float fPolygonCentroid_Y = 0;
					float fPolygonCentroid_Z = 0;

					for( unsigned int f = 0; f < vPolygonVertices.size( ); f++ )
					{
						vObjectVertices.push_back( vPolygonVertices[f] );

						// sum all x,y,z coordinates of polygon vertices,
						fPolygonCentroid_X += vPolygonVertices[f].x;
						fPolygonCentroid_Y += vPolygonVertices[f].y;
						fPolygonCentroid_Z += vPolygonVertices[f].z;
					}

					const TVec3d v3PolygonCentroid( fPolygonCentroid_X / vPolygonVertices.size( ), fPolygonCentroid_Y / vPolygonVertices.size( ),
					                                fPolygonCentroid_Z / vPolygonVertices.size( ) );
					vPolygonCentroids.push_back( v3PolygonCentroid );
				}
			}
		}

		// Calculate Object Centroid by averaging all Polygon Centroids of Object
		float fObjectCentroid_X = 0;
		float fObjectCentroid_Y = 0;
		float fObjectCentroid_Z = 0;

		for( unsigned int f = 0; f < vPolygonCentroids.size( ); f++ )
		{
			fObjectCentroid_X += vPolygonCentroids[f].x;
			fObjectCentroid_Y += vPolygonCentroids[f].y;
			fObjectCentroid_Z += vPolygonCentroids[f].z;
		}

		const TVec3d v3TObjectCentroid( fObjectCentroid_X / vPolygonCentroids.size( ), fObjectCentroid_Y / vPolygonCentroids.size( ),
		                                fObjectCentroid_Z / vPolygonCentroids.size( ) );
		const VistaVector3D v3ObjectCentroid( v3TObjectCentroid.x, v3TObjectCentroid.y, v3TObjectCentroid.z );

		// Defining Object Boundingsphere by finding Object Vertex that is the farthest from Centroid and using this distance as radius for the Boundingsphere
		float fMaxDistance = 0;
		for( unsigned int j = 0; j < vObjectVertices.size( ); j++ )
		{
			float fVertex_Centroid_Distance = sqrt( pow( vObjectVertices[j].x - v3TObjectCentroid.x, 2 ) + pow( vObjectVertices[j].y - v3TObjectCentroid.y, 2 ) +
			                                        pow( vObjectVertices[j].z - v3TObjectCentroid.z, 2 ) );
			if( fVertex_Centroid_Distance > fMaxDistance )
			{
				fMaxDistance = fVertex_Centroid_Distance;
			}
		}

		// Fill Object Attributes
		oCCityObject.iNum                  = i + 1;
		oCCityObject.sID                   = pCityObject->getId( );
		oCCityObject.sType                 = pCityObject->getTypeAsString( );
		oCCityObject.v3CenterPos           = v3ObjectCentroid;
		oCCityObject.fBoundingSphereRadius = fMaxDistance;
		m_voBuildings.push_back( oCCityObject );
	}
}

std::vector<double> ITAGeo::CCityGMLModel::GetCityCenter( ) const
{
	auto p                      = m_pCityModel->pCityModel->getEnvelope( ).getLowerBound( );
	const std::vector<double> v = { p.x, p.y, p.z };
	return v;
}

/**
 *@brief Creates list of objects that are relevant to the sound propagation, based on maximum propagation distance and sender & receiver positions
 *
 *@param[in] dDetourDistance maximum propagation distance
 *@param[in] v3SourcePosition Sound Source Position
 *@param[in] v3ReceiverPosition Sound Receiver Position
 *@param[in] vpResidualObjects Empty vector of CityObjects, which is filled with the relevant Objects
 *
 **/
void ITAGeo::CCityGMLModel::DistanceCulling( const double dPropagationDistance, const ITAGeo::CPropagationAnchor& oSource, const ITAGeo::CPropagationAnchor& oTarget,
                                             std::vector<const ITAGeo::CCityGMLModel::CCityBuilding*>& vpRemainingObjects )
{
	float fRx, fRy, fRz;
	oTarget.v3InteractionPoint.GetValues( fRx, fRy, fRz );
	float fSx, fSy, fSz;
	oSource.v3InteractionPoint.GetValues( fSx, fSy, fSz );

	// construct Ellipsoid
	// Sender & Receiver positions are the foci of the ellipsoid
	VistaVector3D v3EllipsoidCenter;

	v3EllipsoidCenter.SetValues( ( fRx + fSx ) / 2, ( fRy + fSy ) / 2, ( fRz + fSz ) / 2 );

	float fCx, fCy, fCz;
	v3EllipsoidCenter.GetValues( fCx, fCy, fCz );

	// Calculate Length of Ellipsoid Axis
	VistaVector3D v3CenterToFocus = oTarget.v3InteractionPoint - v3EllipsoidCenter;

	float fCenterToFocusDistance = v3CenterToFocus.GetLength( );
	float fSemiMajorAxis         = dPropagationDistance / 2;
	float fSemiMinorAxis         = sqrt( pow( fSemiMajorAxis, 2 ) - pow( fCenterToFocusDistance, 2 ) );


	// Translate Foci Position relative to EllipsoidCenter
	VistaVector3D v3ReceiverPosT = oTarget.v3InteractionPoint - v3EllipsoidCenter;
	VistaVector3D v3SourcePosT   = oSource.v3InteractionPoint - v3EllipsoidCenter;

	// Always rotate in such a way, that the foci are on the x-axis
	// e.g.: Foci are at 1/1/0 and -1/-1/0, so rotate to 1.41/0/0 & -1.41/0/0
	VistaVector3D v3TargetAxis( 1.0, 0, 0 );

	// create Quaternions for rotation
	VistaQuaternion qRotationQuat( v3ReceiverPosT, v3TargetAxis );
	VistaQuaternion qRotationQuatInv = qRotationQuat.GetInverted( );

	// Rotate Sender & Receiver to X-Axis
	VistaQuaternion qReceiverPosR = qRotationQuat * v3ReceiverPosT * qRotationQuatInv;
	float fRecCoords[4];
	qReceiverPosR.GetValues( fRecCoords );
	VistaVector3D v3ReceiverPosTR( fRecCoords[0], fRecCoords[1], fRecCoords[2] );

	VistaQuaternion qSourcePosR = qRotationQuat * v3SourcePosT * qRotationQuatInv;
	float fSourCoords[4];
	qSourcePosR.GetValues( fSourCoords );
	VistaVector3D v3SourcePosTR( fSourCoords[0], fSourCoords[1], fSourCoords[2] );

	// Culling Loop
	// ITAStopWatch OuterCircleWatch, InnerCircleWatch, EllipsoidBSWatch, EllipsoidCTWatch;
	// int OCCount = 0, ICCount = 0 , ELCount = 0, NCount = 0;

	for( unsigned int i = 0; i < m_voBuildings.size( ); i++ )
	{
		const CCityBuilding& oBuilding( m_voBuildings[i] );

		// translate Centerposition relative to Ellipsoid Center
		VistaVector3D v3TranslatedCenterPos = oBuilding.v3CenterPos - v3EllipsoidCenter;

		// skip if outside the sphere around EllipsoidCenter where the semi major axis is the radius

		// OuterCircleWatch.start();
		bool OuterCircleTest = v3TranslatedCenterPos.GetLength( ) > ( fSemiMajorAxis + oBuilding.fBoundingSphereRadius );
		// OuterCircleWatch.stop();
		if( OuterCircleTest )
		{
			// OCCount++;
			continue;
		}

		// include in list if within the sphere around EllipsoidCenter where the semi minor axis is the radius
		// InnerCircleWatch.start();
		bool InnerCircleTest = v3TranslatedCenterPos.GetLength( ) < ( fSemiMinorAxis + oBuilding.fBoundingSphereRadius );
		// InnerCircleWatch.stop();
		if( InnerCircleTest )
		{
			// ICCount++;
			vpRemainingObjects.push_back( &m_voBuildings[i] );
			continue;
		}

		// run coordinate transform and test if building within ellipsoid
		VistaQuaternion qFinalTransformCenterPos = qRotationQuat * v3TranslatedCenterPos * qRotationQuatInv;
		float fCenterCoords[4];
		qFinalTransformCenterPos.GetValues( fCenterCoords );
		VistaVector3D v3FinalCenterPos( fCenterCoords[0], fCenterCoords[1], fCenterCoords[2] );


		// test if boundingsphere of object intersects ellipsoid
		// EllipsoidBSWatch.start();
		bool ELBS = oBuilding.BoundingSphereInsideEllipse( v3FinalCenterPos, oBuilding.fBoundingSphereRadius, fSemiMajorAxis, fSemiMinorAxis );
		// EllipsoidBSWatch.stop();
		if( ELBS )
		{
			// ELCount++;
			vpRemainingObjects.push_back( &m_voBuildings[i] );
			continue;
		}

		// NCount++;
	}
	/*std::cout << std::fixed << std::setprecision(10) << OuterCircleWatch.mean() <<  " sig: " << OuterCircleWatch.std_deviation() << std::endl;
	std::cout << std::fixed << std::setprecision(10) << InnerCircleWatch.mean() << " sig: " << InnerCircleWatch.std_deviation() << std::endl;
	std::cout << std::fixed << std::setprecision(10) << EllipsoidBSWatch.mean() << " sig: " << EllipsoidBSWatch.std_deviation() << std::endl;
	std::cout << "Skipped " << OCCount << " Objects because of OCTEST." << std::endl;
	std::cout << "ICTEST added " << ICCount << " Objects to scope. " << std::endl;
	std::cout << "ELTEST added " << ELCount << " Objects to scope." << std::endl;
	std::cout << NCount << " Objects are inside OC but outside of EL" << std::endl; */
}

bool ITAGeo::CCityGMLModel::CCityBuilding::CenterInsideEllipse( const float fCoordinates[4], const float fSemiMajorAxis, const float fSemiMinorAxis ) const
{
	return ( pow( fCoordinates[0], 2 ) / pow( fSemiMajorAxis, 2 ) + pow( fCoordinates[1], 2 ) / pow( fSemiMinorAxis, 2 ) +
	         pow( fCoordinates[2], 2 ) / pow( fSemiMinorAxis, 2 ) ) <= 1;
}

bool ITAGeo::CCityGMLModel::CCityBuilding::BoundingSphereInsideEllipse( const VistaVector3D& v3CentroidPosition, const float fBSRadius, const float fSemiMajorAxis,
                                                                        const float fSemiMinorAxis ) const
{
	VistaVector3D v3LowestPointBoundingSphere = v3CentroidPosition * ( 1 - fBSRadius / v3CentroidPosition.GetLength( ) );
	float fLPBSx, fLPBSy, fLPBSz;
	v3LowestPointBoundingSphere.GetValues( fLPBSx, fLPBSy, fLPBSz );

	return ( pow( fLPBSx, 2 ) / pow( fSemiMajorAxis, 2 ) + pow( fLPBSy, 2 ) / pow( fSemiMinorAxis, 2 ) + pow( fLPBSz, 2 ) / pow( fSemiMinorAxis, 2 ) ) <= 1;
}
