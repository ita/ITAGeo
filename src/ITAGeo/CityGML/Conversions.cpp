#include <ITAGeo/CityGML/Conversions.h>

// ITABase includes
#include <ITAException.h>


CITAMesh::Point CityGMLPoint3DToITAMeshPoint( TVec3d& vCityGMLPoint )
{
	return CITAMesh::Point( float( vCityGMLPoint.x ), float( vCityGMLPoint.y ), float( vCityGMLPoint.z ) );
}
