// ITA includes
#include "ITAGeo/Model.h"

#ifdef URBAN_MODEL_WITH_SKETCHUP_SUPPORT
#	include "SketchUp/Assimp_Sketchup_Importer.h"
#endif

#include <ITAException.h>
#include <ITAGeo/Utils.h>

// assimp includes
#include <assimp/Exporter.hpp>
#include <assimp/Importer.hpp>
#include <assimp/ObjMaterial.h>
#include <assimp/SceneCombiner.h>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>

// OpenMesh includes
#include <OpenMesh/Core/Utils/PropertyManager.hh>

// STL includes
#include <algorithm>
#include <filesystem>
#include <map>

// This define will later be removed once the ITAGEO_WARN class is implemented
#define ITAGEO_WARN( e ) std::cout << e << '\n';


ITAGeo::CModel::CModel( const CModel& other )
{
	this->m_sName                 = other.m_sName + "_copy";
	this->m_pMaterialManager      = other.m_pMaterialManager;
	this->m_oVisualisationObjects = other.m_oVisualisationObjects;
	if( other.m_pScene.get( ) != nullptr )
	{
		aiScene** pOut = new aiScene*;
		aiCopyScene( other.m_pScene.get( ), pOut );
		this->m_pScene = std::unique_ptr<aiScene>( *pOut );
	}
}

ITAGeo::CModel& ITAGeo::CModel::operator=( const CModel& other )
{
	this->m_sName                 = other.m_sName + "_copy";
	this->m_pMaterialManager      = other.m_pMaterialManager;
	this->m_oVisualisationObjects = other.m_oVisualisationObjects;
	this->m_pScene.release( );

	if( other.m_pScene != nullptr )
	{
		aiScene** pOut = new aiScene*;
		aiCopyScene( other.m_pScene.get( ), pOut );
		this->m_pScene = std::unique_ptr<aiScene>( *pOut );
	}
	return *this;
}


bool ITAGeo::CModel::Load( const std::string& sPath )
{
	/// Check if file path exists. Check if extension is supported by assimp importer. Then Import the scene via the assimp importer. We
	/// copy and save the scene because the importer still owns the loaded scene. If the importer runs out of scope he destroys the imported scene

	std::filesystem::path fsPath( sPath );

	// does file exist?
	if( !std::filesystem::exists( fsPath ) )
		return false;

	Assimp::Importer aiImporter;
#ifdef URBAN_MODEL_WITH_SKETCHUP_SUPPORT
	aiImporter.RegisterLoader( new CSketchUpImporter );
#endif

	// fileformat supported?
	std::string sSupportedFileFormats;
	aiImporter.GetExtensionList( sSupportedFileFormats );

	unsigned int iExtension = sSupportedFileFormats.find( fsPath.extension( ).string( ) );
	if( iExtension == std::string::npos )
		return false;

	// remove unnecessary components from the import file
	aiImporter.SetPropertyInteger( AI_CONFIG_PP_RVC_FLAGS, aiComponent::aiComponent_ANIMATIONS | aiComponent::aiComponent_BONEWEIGHTS | aiComponent::aiComponent_CAMERAS |
	                                                           aiComponent::aiComponent_LIGHTS | aiComponent::aiComponent_TANGENTS_AND_BITANGENTS |
	                                                           aiComponent::aiComponent_TEXCOORDS );

	// Lets read!
	// '| aiProcess_JoinIdenticalVertices' - using this causes the exporter to crash in export due to some memory deletion problem. I dont really know why but the main
	// problem is that I am creating a scene of my own and then merging it with a scene of assimp importer. Not using the flag currently did not yield any problems
	// but maybe needed later. I would check visualization generation first. Maybe we need normals or smth like this idk
	const aiScene* sceneImport =
	    aiImporter.ReadFile( fsPath.string( ), aiProcess_FindDegenerates | aiProcess_SortByPType | aiProcess_RemoveComponent | aiProcess_DropNormals );
	if( sceneImport == nullptr )
	{
		std::string sError = aiImporter.GetErrorString( );
		ITA_EXCEPT1( ITAException::PARSE_ERROR, std::string( "Assimp was unable to load '" ) + fsPath.string( ) + "'. It returned the following Error: " + sError );
	}

	// Copy and save imported scene. Release existing scene first.
	if( this->m_pScene.get( ) != nullptr )
		this->m_pScene.release( );

	aiScene** pOut = new aiScene*;
	aiCopyScene( sceneImport, pOut );
	this->m_pScene = std::unique_ptr<aiScene>( *pOut );

	this->m_sName = fsPath.filename( ).string( );

#ifdef _DEBUG
	// "debug" printout, taken from assimp
	if( this->m_pScene->mNumMeshes )
	{
		std::cout << "Meshes:  (name) [vertices / faces | primitive_types]" << std::endl;
	}
	for( unsigned int i = 0; i < this->m_pScene->mNumMeshes; ++i )
	{
		const aiMesh* mesh = this->m_pScene->mMeshes[i];
		printf( "    %d (%s)", i, mesh->mName.C_Str( ) );
		printf( ": [%d / %d |", mesh->mNumVertices, mesh->mNumFaces );
		const unsigned int ptypes = mesh->mPrimitiveTypes;
		if( ptypes & aiPrimitiveType_POINT )
		{
			printf( " point" );
		}
		if( ptypes & aiPrimitiveType_LINE )
		{
			printf( " line" );
		}
		if( ptypes & aiPrimitiveType_TRIANGLE )
		{
			printf( " triangle" );
		}
		if( ptypes & aiPrimitiveType_POLYGON )
		{
			printf( " polygon" );
		}
		printf( "]\n" );
	}
#endif

	return true;
}

bool ITAGeo::CModel::Store( const std::string& sPath, const std::string& sFormatExtension /* = "" */ ) const
{
	// Prechecks
	if( !m_oVisualisationObjects.has_value( ) || this->m_pScene == nullptr )
		return false;

	std::filesystem::path fsPath( sPath );

	if( !std::filesystem::exists( fsPath.parent_path( ) ) )
		return false;

	// Create and Export
	aiScene* aiVisualizationScene = this->CreateVisualizationScene( );

	aiScene** aiMergedScene = new aiScene* { nullptr };
	// scene merger deletes all input scenes but we dont want to lose our Model scene
	aiScene** aiTempLoadedScene = new aiScene* { nullptr };
	aiCopyScene( this->m_pScene.get( ), aiTempLoadedScene );
	auto to_be_merged = std::vector<aiScene*> { *aiTempLoadedScene, aiVisualizationScene };
	Assimp::SceneCombiner::MergeScenes( aiMergedScene, to_be_merged );

	Assimp::Exporter aiExporter;
	// take given file extension of derive from path. Path.extension returns ".EXTENSION" but assimp exporter wants without dot
	std::string sFormatID = sFormatExtension != "" ? sFormatExtension : fsPath.extension( ).string( ).substr( 1 );

	aiReturn aiExportStatus = aiExporter.Export( *aiMergedScene, sFormatID, sPath );
	if( aiExportStatus != aiReturn_SUCCESS )
	{
		std::string sError = aiExporter.GetErrorString( );
		ITA_EXCEPT1( ITAException::PARSE_ERROR, std::string( "Assimp was unable to store '" ) + fsPath.string( ) + "'. It returned the following Error: " + sError );
	}
	return true;
}

std::unique_ptr<CITAMesh> ITAGeo::CModel::GetOpenMesh( ) const
{
	if( this->m_pScene == nullptr )
		ITA_EXCEPT1( ITAException::CONFIG_ERROR, "Model does not contain a loaded scene to convert to OpenMesh. Please load a model first!" );

	// traverse whole hierachy and add each mesh with its local plus global rotation

	std::unique_ptr<CITAMesh> pMesh = std::make_unique<CITAMesh>( );
	// create face property, in AddToMesh the property is retrieved to add material to openmesh faces
	OpenMesh::getOrMakeProperty<CITAMesh::FaceHandle, ITAGeo::Material::CAcousticFaceProperty>( *pMesh, "Acoustics::Material::Face" );

	aiNode* aiStartNode            = this->m_pScene->mRootNode;
	aiMatrix4x4 aiRotationIdentity = aiMatrix4x4( );
	// map used to map from assimp to openmesh handles. Assimp can have duplicate vertices which is forbidden in openmesh
	std::map<aiVector3D, CITAMesh::VertexHandle>* pGlobalMeshVertexHandleMap = new std::map<aiVector3D, CITAMesh::VertexHandle>( );

	this->RecursivelyAddNodesToMesh( aiStartNode, pMesh.get( ), aiRotationIdentity, pGlobalMeshVertexHandleMap );

	return pMesh;
};


void ITAGeo::CModel::SetMaterialManager( std::shared_ptr<ITAGeo::Material::CMaterialManager> pMaterialManager )
{
	this->m_pMaterialManager = pMaterialManager;
}

void ITAGeo::CModel::AddVisualization( ITAGeo::CPropagationPathList& vPropagationPaths, std::vector<ITAGeo::CEmitter>& vEmitters, std::vector<ITAGeo::CSensor>& vSensors )
{
	if( !this->m_oVisualisationObjects.has_value( ) )
		this->m_oVisualisationObjects = SVisHelper { };

	this->m_oVisualisationObjects->vPropagationPaths = vPropagationPaths;
	this->m_oVisualisationObjects->vEmitters         = vEmitters;
	this->m_oVisualisationObjects->vSensors          = vSensors;
}


///----------------------------------------------------------------------------------------------------------------
///--------------Private-------------------------------------------------------------------------------------------
///----------------------------------------------------------------------------------------------------------------


void ITAGeo::CModel::RecursivelyAddNodesToMesh( const aiNode* aiCurrentNode, CITAMesh* pMesh, aiMatrix4x4 m4CurrentTransformation,
                                                std::map<aiVector3D, CITAMesh::VertexHandle>* pGlobalMeshVertexHandleMap ) const
{
	m4CurrentTransformation *= aiCurrentNode->mTransformation;
	std::vector<unsigned int> vMeshIndices;

	// add own meshes
	if( aiCurrentNode->mNumMeshes != 0 )
	{
		for( unsigned int i = 0; i < aiCurrentNode->mNumMeshes; i++ )
		{
			vMeshIndices.push_back( aiCurrentNode->mMeshes[i] );
		}
		this->AddToMesh( pMesh, vMeshIndices, m4CurrentTransformation, pGlobalMeshVertexHandleMap );
	}

	// recursively add children
	for( unsigned int i = 0; i < aiCurrentNode->mNumChildren; i++ )
	{
		this->RecursivelyAddNodesToMesh( aiCurrentNode->mChildren[i], pMesh, m4CurrentTransformation, pGlobalMeshVertexHandleMap );
	}

	return;
};

void ITAGeo::CModel::AddToMesh( CITAMesh* pMesh, std::vector<unsigned int>& vMeshIndices, aiMatrix4x4 m4CurrentTransformation,
                                std::map<aiVector3D, CITAMesh::VertexHandle>* pGlobalMeshVertexHandleMap ) const
{
	aiScene* aiScene              = this->m_pScene.get( );
	aiMesh** aimMeshes            = aiScene->mMeshes;
	auto mMaterialPropertyManager = OpenMesh::getProperty<CITAMesh::FaceHandle, ITAGeo::Material::CAcousticFaceProperty>( *pMesh, "Acoustics::Material::Face" );
	std::vector<CITAMesh::VertexHandle> vFaceHandles;

	for( unsigned int i: vMeshIndices )
	{
		aiMesh* aiMesh = aimMeshes[i];

		// skipping meshes with lines or points
		if( ( aiMesh->mPrimitiveTypes & aiPrimitiveType_LINE ) || ( aiMesh->mPrimitiveTypes & aiPrimitiveType_POINT ) )
			continue;

		// convert vertices to openmesh handle. If already converted skip
		for( unsigned int j = 0; j < aiMesh->mNumVertices; j++ )
		{
			aiVector3D aiVertex = aiMesh->mVertices[j];

			// If Vertex already in global map skip it
			if( pGlobalMeshVertexHandleMap->count( aiVertex ) > 0 )
				continue;

			// transforming vertex according to current transformation
			aiVector3D aiRotatedVertex = m4CurrentTransformation * aiVertex;
			CITAMesh::Point omVertex( (float)aiRotatedVertex.x, (float)aiRotatedVertex.y, (float)aiRotatedVertex.z );
			CITAMesh::VertexHandle omVertexHandle = pMesh->add_vertex( omVertex );
			if( !pMesh->is_valid_handle( omVertexHandle ) )
			{
				std::string sWarning = "Openmesh invalid vertex. Conversion from assimp mesh '" + std::string( aiMesh->mName.C_Str( ) ) +
				                       "' to openmesh failed. A vertex with these coordinates most likely already exists. Check your model for duplicate vertices. "
				                       "Skipping vertex (May lead to wrong faces).";
				ITAGEO_WARN( sWarning );
				continue;
			}

			// the vertex maps to the transformed openmesh vertex (handle)
			( *pGlobalMeshVertexHandleMap )[aiVertex] = omVertexHandle;
		}

		// get material of mesh. If none then nullptr
		aiMaterial* aiFaceMaterial = aiScene->mMaterials[aiMesh->mMaterialIndex];

		// create openmesh face
		for( unsigned int j = 0; j < aiMesh->mNumFaces; j++ )
		{
			vFaceHandles.clear( );
			aiFace face = aiMesh->mFaces[j];
			for( unsigned int k = 0; k < face.mNumIndices; k++ )
			{
				unsigned int ind = face.mIndices[k];
				aiVector3D vec   = aiMesh->mVertices[ind];
				vFaceHandles.push_back( ( *pGlobalMeshVertexHandleMap )[vec] );
			}

			CITAMesh::FaceHandle omFaceHandle = pMesh->add_face( vFaceHandles );

			// valid openmesh face?
			if( !pMesh->is_valid_handle( omFaceHandle ) )
			{
				std::string sWarning = "Openmesh invalid face. Complex edge or complex vertex found. Conversion from assimp mesh '" +
				                       std::string( aiMesh->mName.C_Str( ) ) +
				                       "' to openmesh failed. A face/edge with these vertices most likely already exists. Check your model for faces/edge/verticies that "
				                       "lie inside each other. Skipping face.";
				ITAGEO_WARN( sWarning );
				continue;
			}

			// assign material to face if database with same material is available
			if( this->GetMaterialManager( ) == nullptr || aiFaceMaterial == nullptr )
				continue;

			aiString aiMaterialName = aiFaceMaterial->GetName( );
			// if material with aiMaterialName does not exists a nullptr is assigned
			mMaterialPropertyManager[omFaceHandle].SetMaterial( this->GetMaterialManager( )->GetMaterial( aiMaterialName.C_Str( ) ) );
		}
	}

	return;
};


/// VISUALISATION ------------------------------
aiScene* const ITAGeo::CModel::CreateVisualizationScene( ) const
{
	/// GET STUFF THAT WE NEED FOR SCENE -----------------------------------------------------------------------------------------------
	std::vector<VistaVector3D> vEmitterOrigins;
	std::vector<VistaVector3D> vSensorOrigins;

	for( auto oEmitter: this->m_oVisualisationObjects->vEmitters )
		vEmitterOrigins.push_back( oEmitter.v3InteractionPoint );

	for( auto oSensor: this->m_oVisualisationObjects->vSensors )
		vSensorOrigins.push_back( oSensor.v3InteractionPoint );

	// get meshes
	const std::vector<aiMesh*> vEmitterMeshes = CreateVisualizationCubes( vEmitterOrigins );
	const std::vector<aiMesh*> vSensorMeshes  = CreateVisualizationCubes( vSensorOrigins );
	const LineHierachyMap mLineMeshes         = CreateVisulizationLines( );

	// get their size
	unsigned int iNumEmitters = vEmitterMeshes.size( );
	unsigned int iNumSensors  = vSensorMeshes.size( );
	unsigned int iNumLines    = 0;
	for( const auto& [_, mLocalOrders]: mLineMeshes )             // outer map
		for( const auto& [pairOrders, vMeshLists]: mLocalOrders ) // inner map
			iNumLines += vMeshLists.size( );


	/// START TO CREATE VIUSALISATION SCENE --------------------------------------------------------------------------------------------
	aiScene* aiVisualizationScene    = new aiScene;
	aiVisualizationScene->mMetaData  = new aiMetadata( );
	aiVisualizationScene->mNumMeshes = iNumEmitters + iNumSensors + iNumLines;
	// mesh order is first all emitter meshes, then all sensor meshes, then all line meshes
	aiVisualizationScene->mMeshes = new aiMesh*[aiVisualizationScene->mNumMeshes];


	// copy meshes into scene, lines more complex as it is in this map structure
	std::copy( vEmitterMeshes.begin( ), vEmitterMeshes.end( ), &aiVisualizationScene->mMeshes[0] );
	std::copy( vSensorMeshes.begin( ), vSensorMeshes.end( ), &aiVisualizationScene->mMeshes[iNumEmitters] );
	unsigned int iuNumAlreadyCopiedLines = 0;
	for( const auto& [_, mLocalOrders]: mLineMeshes )             // outer map
		for( const auto& [pairOrders, vMeshLists]: mLocalOrders ) // inner map
		{
			std::copy( vMeshLists.begin( ), vMeshLists.end( ), &aiVisualizationScene->mMeshes[iNumEmitters + iNumSensors + iuNumAlreadyCopiedLines] );
			iuNumAlreadyCopiedLines += vMeshLists.size( );
		}


	// set material in scene
	unsigned int uiNumLineMaterials     = GetSumUpTo( mLineMeshes.size( ) + 1 );
	aiVisualizationScene->mNumMaterials = uiNumLineMaterials + 2; // line materials plus emitter and sensor material
	aiVisualizationScene->mMaterials    = CreateSceneMaterials( mLineMeshes.size( ) + 1 );

	// manually add material index, Lines already have the correct material index assigned.
	std::for_each( vEmitterMeshes.begin( ), vEmitterMeshes.end( ), [&uiNumLineMaterials]( aiMesh* aiEmitter ) { aiEmitter->mMaterialIndex = uiNumLineMaterials + 0; } );
	std::for_each( vSensorMeshes.begin( ), vSensorMeshes.end( ), [&uiNumLineMaterials]( aiMesh* aiSensor ) { aiSensor->mMaterialIndex = uiNumLineMaterials + 1; } );


	/// CREATE HIERACHY ----------------------------------------------------------------------------------------------------------------
	aiVisualizationScene->mRootNode               = new aiNode( "Visualisation" );
	aiVisualizationScene->mRootNode->mNumMeshes   = 0;
	aiVisualizationScene->mRootNode->mMeshes      = new unsigned[0] { };
	aiVisualizationScene->mRootNode->mMetaData    = new aiMetadata( );
	aiVisualizationScene->mRootNode->mNumChildren = 3;
	aiVisualizationScene->mRootNode->mChildren    = new aiNode*[aiVisualizationScene->mRootNode->mNumChildren];

	// create emitter node
	aiNode* aiEmittersNode     = new aiNode( "Emitters" );
	aiEmittersNode->mParent    = aiVisualizationScene->mRootNode;
	aiEmittersNode->mNumMeshes = iNumEmitters;
	aiEmittersNode->mMeshes    = new unsigned[aiEmittersNode->mNumMeshes];
	for( unsigned int i = 0; i < iNumEmitters; i++ )
		aiEmittersNode->mMeshes[i] = i;
	aiEmittersNode->mMetaData                     = new aiMetadata( );
	aiVisualizationScene->mRootNode->mChildren[0] = aiEmittersNode;

	// create sensor node
	aiNode* aiSensorsNode     = new aiNode( "Sensors" );
	aiSensorsNode->mParent    = aiVisualizationScene->mRootNode;
	aiSensorsNode->mNumMeshes = iNumSensors;
	aiSensorsNode->mMeshes    = new unsigned[aiSensorsNode->mNumMeshes];
	for( unsigned int i = 0; i < iNumSensors; i++ )
		aiSensorsNode->mMeshes[i] = iNumEmitters + i;
	aiSensorsNode->mMetaData                      = new aiMetadata( );
	aiVisualizationScene->mRootNode->mChildren[1] = aiSensorsNode;

	// create top level line node
	aiNode* aiLinesNode                           = new aiNode( "Lines" );
	aiLinesNode->mParent                          = aiVisualizationScene->mRootNode;
	aiLinesNode->mNumChildren                     = mLineMeshes.size( );
	aiLinesNode->mChildren                        = new aiNode*[aiLinesNode->mNumChildren];
	aiLinesNode->mMetaData                        = new aiMetadata( );
	aiVisualizationScene->mRootNode->mChildren[2] = aiLinesNode;


	// create and connect all lines. One node for each line. split up in orders
	unsigned int uiCombinedCurrentChild  = 0;
	unsigned int uiLineMeshCurrentOffset = 0;
	for( const auto& [uiCombinedOrder, mLocalOrders]: mLineMeshes )
	{
		// create combined Order node
		std::string sNameCombinedOrder                 = "Combined order of " + std::to_string( uiCombinedOrder );
		aiNode* aiCombinedOrderNode                    = new aiNode( sNameCombinedOrder );
		aiCombinedOrderNode->mParent                   = aiLinesNode; // add our parent
		aiCombinedOrderNode->mNumChildren              = mLocalOrders.size( );
		aiCombinedOrderNode->mChildren                 = new aiNode*[aiCombinedOrderNode->mNumChildren];
		aiCombinedOrderNode->mMetaData                 = new aiMetadata( );
		aiLinesNode->mChildren[uiCombinedCurrentChild] = aiCombinedOrderNode; // add TO OUR parent
		uiCombinedCurrentChild++;

		unsigned int uiLocalCurrentChild = 0;
		for( const auto& [pairOrders, vMeshLists]: mLocalOrders )
		{
			// create local order nodes, attach them and add lines
			std::string sNameLocalOrder    = "Reflection order: " + std::to_string( pairOrders.first ) + ", Diffraction order: " + std::to_string( pairOrders.second );
			aiNode* aiLocalOrderNode       = new aiNode( sNameLocalOrder );
			aiLocalOrderNode->mParent      = aiCombinedOrderNode; // add our parent
			aiLocalOrderNode->mNumChildren = vMeshLists.size( );
			aiLocalOrderNode->mChildren    = new aiNode*[aiLocalOrderNode->mNumChildren];
			aiLocalOrderNode->mMetaData    = new aiMetadata( );
			aiCombinedOrderNode->mChildren[uiLocalCurrentChild] = aiLocalOrderNode; // add TO OUR parent
			uiLocalCurrentChild++;

			// add all paths as a single node
			for( unsigned int j = 0; j < vMeshLists.size( ); j++ )
			{
				aiMesh* aiSingleLine         = vMeshLists[j]; // get for name of path
				aiNode* aiSingleLineNode     = new aiNode( aiSingleLine->mName.C_Str( ) );
				aiSingleLineNode->mParent    = aiLocalOrderNode;
				aiSingleLineNode->mNumMeshes = 1;
				// The Line meshes were stored by iterating over all global and then local order nodes and putting them into one list.
				// We are iterating over the same nodes and therefor only have to store an offset to the current line mesh. See Copy mesh into scene code above
				aiSingleLineNode->mMeshes      = new unsigned[aiSingleLineNode->mNumMeshes] { iNumEmitters + iNumSensors + uiLineMeshCurrentOffset };
				aiSingleLineNode->mMetaData    = new aiMetadata( );
				aiLocalOrderNode->mChildren[j] = aiSingleLineNode;
				uiLineMeshCurrentOffset++;
			}
		}
	}


	// we do not have to delete our meshes, as the Scene is now owner of them. We only copied the pointers, not the meshes.
	// If the scene is delete all meshes and everything else is deleted as well.

	return aiVisualizationScene;
}

const std::vector<aiMesh*> ITAGeo::CModel::CreateVisualizationCubes( const std::vector<VistaVector3D>& vOrigins ) const
{
	std::vector<aiMesh*> vOutMeshes;
	for( auto v3Origin: vOrigins )
	{
		float iOriginOffset = 0.2; // cube size
		float x, y, z;
		v3Origin.GetValues( x, y, z );

		aiMesh* aiCubeMesh       = new aiMesh( );
		aiCubeMesh->mNumVertices = 8;
		aiCubeMesh->mVertices    = new aiVector3D[aiCubeMesh->mNumVertices] {
            aiVector3D( x - iOriginOffset, y - iOriginOffset, z + iOriginOffset ), aiVector3D( x + iOriginOffset, y - iOriginOffset, z + iOriginOffset ),
            aiVector3D( x + iOriginOffset, y + iOriginOffset, z + iOriginOffset ), aiVector3D( x - iOriginOffset, y + iOriginOffset, z + iOriginOffset ),
            aiVector3D( x - iOriginOffset, y - iOriginOffset, z - iOriginOffset ), aiVector3D( x + iOriginOffset, y - iOriginOffset, z - iOriginOffset ),
            aiVector3D( x + iOriginOffset, y + iOriginOffset, z - iOriginOffset ), aiVector3D( x - iOriginOffset, y + iOriginOffset, z - iOriginOffset )
		};

		aiFace aiFaceUp;
		aiFace aiFaceDown;
		aiFace aiFaceLeft;
		aiFace aiFaceRight;
		aiFace aiFaceFront;
		aiFace aiFaceBack;
		aiFaceUp.mNumIndices    = 4;
		aiFaceDown.mNumIndices  = 4;
		aiFaceLeft.mNumIndices  = 4;
		aiFaceRight.mNumIndices = 4;
		aiFaceFront.mNumIndices = 4;
		aiFaceBack.mNumIndices  = 4;

		aiFaceUp.mIndices    = new unsigned int[4] { 0, 1, 2, 3 };
		aiFaceDown.mIndices  = new unsigned int[4] { 7, 6, 5, 4 };
		aiFaceLeft.mIndices  = new unsigned int[4] { 2, 1, 5, 6 };
		aiFaceRight.mIndices = new unsigned int[4] { 0, 3, 7, 4 };
		aiFaceFront.mIndices = new unsigned int[4] { 1, 0, 4, 5 };
		aiFaceBack.mIndices  = new unsigned int[4] { 3, 2, 6, 7 };

		aiCubeMesh->mNumFaces       = 6;
		aiCubeMesh->mFaces          = new aiFace[aiCubeMesh->mNumFaces] { aiFaceUp, aiFaceDown, aiFaceLeft, aiFaceRight, aiFaceFront, aiFaceBack };
		aiCubeMesh->mPrimitiveTypes = aiPrimitiveType_POLYGON;
		vOutMeshes.push_back( aiCubeMesh );
	}
	return vOutMeshes;
}

const std::map<unsigned int, std::map<std::pair<unsigned int, unsigned int>, std::vector<aiMesh*>>> ITAGeo::CModel::CreateVisulizationLines( ) const
{
	auto fVistaVectorToAIVector = []( const VistaVector3D& v3In )
	{
		float x, y, z;
		v3In.GetValues( x, y, z );
		return aiVector3D( x, y, z );
	};

	// Assign names to lines. This may be done already but lets play it safe.
	CPropagationPathList oPathListWithIdentifiers = this->m_oVisualisationObjects->vPropagationPaths;
	ITAGeoUtils::AddIdentifier( oPathListWithIdentifiers );

	LineHierachyMap mLinesWithOrder;

	// iterate over all paths and create a line mesh. Then add this line mesh to the correct combined and local order
	for( const CPropagationPath& oPath: oPathListWithIdentifiers )
	{
		/// create line mesh
		unsigned int iNumVertices = oPath.size( );
		unsigned int iNumLines    = iNumVertices - 1;

		aiMesh* aiLineMesh          = new aiMesh( );
		aiLineMesh->mNumVertices    = iNumVertices;
		aiLineMesh->mVertices       = new aiVector3D[aiLineMesh->mNumVertices];
		aiLineMesh->mNumFaces       = iNumLines;
		aiLineMesh->mFaces          = new aiFace[aiLineMesh->mNumFaces];
		aiLineMesh->mPrimitiveTypes = aiPrimitiveType_LINE;
		aiLineMesh->mName           = oPath.sIdentifier;

		// add vertices and lines (faces)
		for( unsigned int i = 0; i < iNumVertices - 1; i++ )
		{
			aiLineMesh->mVertices[i]          = fVistaVectorToAIVector( oPath.at( i )->v3InteractionPoint );
			aiLineMesh->mFaces[i].mNumIndices = 2;
			aiLineMesh->mFaces[i].mIndices    = new unsigned int[2] { i, i + 1 };
		}
		// add last vertex
		aiLineMesh->mVertices[iNumVertices - 1] = fVistaVectorToAIVector( oPath.at( iNumVertices - 1 )->v3InteractionPoint );


		/// Write mesh into map structur with correct order ----------------------------------------------------------------------------

		// get order
		std::pair<unsigned int, unsigned int> pairPathOrders = ITAGeoUtils::GetPathOrders( oPath );
		unsigned int uiCombinedOrder                         = pairPathOrders.first + pairPathOrders.second;

		// add material index. We assume a starting index of zero and have to handle that later in the CreateVisualisationScenen function
		aiLineMesh->mMaterialIndex = GetMaterialIndex( pairPathOrders );

		// does combined order exist? if not add it and local order with it
		if( mLinesWithOrder.count( uiCombinedOrder ) == 0 )
		{
			std::map<std::pair<unsigned int, unsigned int>, std::vector<aiMesh*>> mLocalOrder;
			mLocalOrder[pairPathOrders]      = std::vector<aiMesh*> { aiLineMesh };
			mLinesWithOrder[uiCombinedOrder] = mLocalOrder;
			continue;
		}

		// combined order exist. Get map and check if local order already exists. If not add it, else pushback into existing list
		std::map<std::pair<unsigned int, unsigned int>, std::vector<aiMesh*>>& mLocalOrder = mLinesWithOrder[uiCombinedOrder];

		if( mLocalOrder.count( pairPathOrders ) == 0 )
			mLocalOrder[pairPathOrders] = std::vector<aiMesh*> { aiLineMesh };
		else
			mLocalOrder[pairPathOrders].push_back( aiLineMesh );
	}

	const auto iHighestCombinedOrder =
	    std::max_element( mLinesWithOrder.begin( ), mLinesWithOrder.end( ), []( const auto& p1, const auto& p2 ) { return p1.first < p2.first; } )->first;

	if( ( iHighestCombinedOrder + 1 ) > mLinesWithOrder.size( ) )
	{
		for( auto i = 0; i < iHighestCombinedOrder + 1; ++i )
		{
			if( mLinesWithOrder.count( i ) == 0 )
				mLinesWithOrder[i] = std::map<std::pair<unsigned int, unsigned int>, std::vector<aiMesh*>> { };
		}
	}

	return mLinesWithOrder;
}


///  MATERIAL STUFF ---------------------------
aiMaterial* ITAGeo::CModel::CreateAiMaterial( float fRed, float fGreen, float fBlue ) const
{
	// See here for property descriptions: https://assimp-docs.readthedocs.io/en/latest/usage/use_the_lib.html#material-system

	std::string sName = "lineColour"; // doesnt look like assimp exports the name
	int iShadingMode  = aiShadingMode::aiShadingMode_Flat;
	int iObjIllum     = 0; // no idea what this is or which value it needs
	aiColor3D aiAmbientColor( 0.1, 0.1, 0.1 );
	aiColor3D aiDiffuseColor( fRed, fGreen, fBlue );
	aiColor3D aiSpecularColor( 0.4, 0.4, 0.4 );
	aiColor3D aiEmissiveColor( 0, 0, 0 );
	float fShininess = 0.0;
	float fOpacity   = 1.0;
	aiColor3D aiTransparentColor( 0, 0, 0 );
	int iAnisotropyFactor = 0; // no defaul value from assimp website
	float fRefracti       = 1.0;

	aiMaterial* aiMat = new aiMaterial;
	aiMat->AddProperty( &sName, 1, AI_MATKEY_NAME );
	aiMat->AddProperty( &iShadingMode, 1, AI_MATKEY_SHADING_MODEL );
	aiMat->AddProperty( &iObjIllum, 1, AI_MATKEY_OBJ_ILLUM );
	aiMat->AddProperty( &aiAmbientColor, 1, AI_MATKEY_COLOR_AMBIENT );
	aiMat->AddProperty( &aiDiffuseColor, 1, AI_MATKEY_COLOR_DIFFUSE );
	aiMat->AddProperty( &aiSpecularColor, 1, AI_MATKEY_COLOR_SPECULAR );
	aiMat->AddProperty( &aiEmissiveColor, 1, AI_MATKEY_COLOR_EMISSIVE );
	aiMat->AddProperty( &fShininess, 1, AI_MATKEY_SHININESS );
	aiMat->AddProperty( &fOpacity, 1, AI_MATKEY_OPACITY );
	aiMat->AddProperty( &aiTransparentColor, 1, AI_MATKEY_COLOR_TRANSPARENT );
	aiMat->AddProperty( &iAnisotropyFactor, 1, AI_MATKEY_ANISOTROPY_FACTOR );
	aiMat->AddProperty( &fRefracti, 1, AI_MATKEY_REFRACTI );

	return aiMat;
};

aiMaterial** ITAGeo::CModel::CreateSceneMaterials( const unsigned int uiCombinedOrder ) const
{
	unsigned int uiNumLineMaterials = GetSumUpTo( uiCombinedOrder );
	unsigned int uiNumCubeMaterials = 2; // one emitter and one sensor
	unsigned int uiNumMaterials     = uiNumLineMaterials + uiNumCubeMaterials;
	aiMaterial** aiLineMaterials    = new aiMaterial*[uiNumMaterials];

	unsigned int uiCurrentIteration = 0;
	// first all line materials, then add emitter and sensor behind them
	for( unsigned int i = 0; i < uiCombinedOrder; i++ )
	{
		for( unsigned int j = 0; j <= i; j++ )
		{
			const float fRed                    = 1.0 / std::pow( 2, ( j ) );     // specular reflection. first strongest
			const float fGreen                  = 1.0 / std::pow( 2, ( i - j ) ); // diffraction. first lowest, then strongest
			const float fBlue                   = 0.0;
			aiLineMaterials[uiCurrentIteration] = CreateAiMaterial( fRed, fGreen, fBlue );
			uiCurrentIteration++;
		}
	}

	assert( uiNumLineMaterials == uiCurrentIteration );

	// add emitter and sensor materials, hardcoded atm
	aiLineMaterials[uiNumLineMaterials + 0] = this->CreateAiMaterial( 0.78431, 0.21569, 0.0 ); // emitter(s)
	aiLineMaterials[uiNumLineMaterials + 1] = this->CreateAiMaterial( 0, 0.19608, 0.7451 );    // sensor(s)

	return aiLineMaterials;
}

const unsigned int ITAGeo::CModel::GetMaterialIndex( const std::pair<unsigned int, unsigned int>& pairPathOrders ) const
{
	// The idea is, that each possible pair has a material even if we do not use it.
	// The combined order of 0 has only one possible material, for the pair 0,0,
	// The combined order of 1 has two possible materials, for 1,0 and 0,1
	// The combined order of 2 has three possible materials, for 2,0 and 1,1 and 0,2
	// And so forth. Each order has one more material then the one before. The sum of the pair is the combined order. Then we have to
	// figure out what combination (in the current combined order) it is. First value (reflection) goes down from max combined order down to 0. This leads to combined
	// order - reflection order

	unsigned int uiCombinedOrder = pairPathOrders.first + pairPathOrders.second;
	unsigned int uiLocalIndex    = uiCombinedOrder - pairPathOrders.first;
	unsigned int uiCombinedIndex = GetSumUpTo( uiCombinedOrder );

	return uiCombinedIndex + uiLocalIndex;
}


// used in visualisation @NMK: utils, static, or smth like that
unsigned int ITAGeo::CModel::GetSumUpTo( const unsigned int uiCombinedOrder ) const
{
	// starting at zero because our model may have only a direct path = combined order of 0
	unsigned int uiCombinedIndex = 0;
	for( unsigned int i = 0; i <= uiCombinedOrder; i++ )
		uiCombinedIndex += i;
	return uiCombinedIndex;
}