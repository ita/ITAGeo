#include <ITAGeo/SketchUp/Conversions.h>

// ITABase includes
#include <ITAException.h>

// Constants
static const double g_dInchesToMeterScale = 39.3700787f;
static const double g_dFeetToMeterScale   = 3.28084f;

double GetSIScaleFromSUUnits( const SUModelUnits& iUnits )
{
	double dScale = 1.0f;
	if( iUnits == SUModelUnits_Centimeters )
		dScale = 0.01f;
	if( iUnits == SUModelUnits_Feet )
		dScale = g_dFeetToMeterScale;
	if( iUnits == SUModelUnits_Inches )
		dScale = g_dInchesToMeterScale;
	if( iUnits == SUModelUnits_Millimeters )
		dScale = 0.001f;
	if( iUnits == SUModelUnits_Meters )
		dScale = g_dInchesToMeterScale;
	return dScale;
}

CITAMesh::Point SUPoint3DToITAMeshPoint( const SUPoint3D& vSUPoint, SUModelUnits iUnits /*=SUModelUnity_Inches*/ )
{
	const double dScale = GetSIScaleFromSUUnits( iUnits );
	return CITAMesh::Point( float( vSUPoint.x / dScale ), float( vSUPoint.y / dScale ), float( vSUPoint.z / dScale ) );
}

VistaVector3D SUPoint3DToVistaVector3D( const SUPoint3D& vSUPoint, SUModelUnits iUnits /*=SUModelUnity_Inches*/ )
{
	const double dScale = GetSIScaleFromSUUnits( iUnits );
	return VistaVector3D( float( vSUPoint.x / dScale ), float( vSUPoint.y / dScale ), float( vSUPoint.z / dScale ) );
}

CITAMesh::Point VistaVectorToITAMeshPoint( const VistaVector3D& vVPoint )
{
	return CITAMesh::Point( float( vVPoint[0] ), float( vVPoint[1] ), float( vVPoint[2] ) );
}

SUPoint3D VistaVectorToSUPoint( const VistaVector3D& vVPoint, SUModelUnits iUnits /*=SUModelUnity_Inches*/ )
{
	const double dScale = GetSIScaleFromSUUnits( iUnits );
	SUPoint3D vSUPoint;
	vSUPoint.x = vVPoint[0] * dScale;
	vSUPoint.y = vVPoint[1] * dScale;
	vSUPoint.z = vVPoint[2] * dScale;
	return vSUPoint;
}

SUPoint3D ITAMeshPointToSUPoint3D( const CITAMesh::Point& vITAPoint, SUModelUnits iUnits /*=SUModelUnity_Inches*/ )
{
	const double dScale = GetSIScaleFromSUUnits( iUnits );
	SUPoint3D vSUPoint;
	vSUPoint.x = vITAPoint[0] * dScale;
	vSUPoint.y = vITAPoint[1] * dScale;
	vSUPoint.z = vITAPoint[2] * dScale;
	return vSUPoint;
}

VistaTransformMatrix SUTransformToVistaTransformMatrix( const SUTransformation& tTransform, SUModelUnits iUnits /*=SUModelUnity_Inches*/ )
{
	// SU uses column-major form, Vista uses row-major. Convert via transposition, then scale to SI units.
	VistaTransformMatrix tVistaTransform( tTransform.values );
	tVistaTransform.Transpose( );
	tVistaTransform.SetTranslation( tVistaTransform.GetTranslation( ) / float( GetSIScaleFromSUUnits( iUnits ) ) );
	return tVistaTransform;
}

SUTransformation VistaTransformMatrixToSUTransform( const VistaTransformMatrix& oVistaTransform, SUModelUnits iUnits /*=SUModelUnity_Inches*/ )
{
	// SU uses column-major form, Vista uses row-major. Convert via transposition, then scale to SI units.
	const double dScale = GetSIScaleFromSUUnits( iUnits );
	SUTransformation tTransform;
	oVistaTransform.GetTransposedValues( tTransform.values );
	tTransform.values[12] *= dScale;
	tTransform.values[13] *= dScale;
	tTransform.values[14] *= dScale;
	return tTransform;
}

CITAMesh::Point SUPoint3DToITAMeshPointTransformed( const SUPoint3D& vSUPoint, const std::vector<SUTransformation>& vtTransform,
                                                    SUModelUnits iUnits /*=SUModelUnity_Inches*/ )
{
	VistaTransformMatrix oVistaTransform = SUTransformVectorToVistaTransformMatrix( vtTransform, iUnits );
	VistaVector3D vPoint                 = SUPoint3DToVistaVector3D( vSUPoint, iUnits );
	vPoint                               = oVistaTransform.TransformPoint( vPoint );
	return VistaVectorToITAMeshPoint( vPoint );
}

VistaTransformMatrix SUTransformVectorToVistaTransformMatrix( const std::vector<SUTransformation>& vtSUTransform, SUModelUnits iUnits /*=SUModelUnity_Inches*/ )
{
	if( vtSUTransform.empty( ) )
		return VistaTransformMatrix( );

	VistaTransformMatrix tMergedTransform = SUTransformToVistaTransformMatrix( vtSUTransform[0], iUnits );
	for( size_t t = 1; t < vtSUTransform.size( ); t++ )
		tMergedTransform *= SUTransformToVistaTransformMatrix( vtSUTransform[t], iUnits );

	return tMergedTransform;
}
