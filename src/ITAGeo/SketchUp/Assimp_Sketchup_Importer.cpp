#include "Assimp_Sketchup_Importer.h"

#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <iostream>
#include <memory>
#include <unordered_map>

static const aiImporterDesc desc = { "SketchUp Importer", "", "", "", aiImporterFlags_SupportBinaryFlavour, 0, 0, 0, 0, "skp" };

CSketchUpImporter::CSketchUpImporter( )
{
	m_mSUAccessMutex.lock( );
	SUInitialize( );
}

CSketchUpImporter::~CSketchUpImporter( )
{
	m_mSUAccessMutex.unlock( );
	SUTerminate( );
};

bool CSketchUpImporter::CanRead( const std::string& filename, Assimp::IOSystem* pIOHandler, bool checkSig ) const
{
	SUModelRef rModel = SU_INVALID;
	SUModelLoadStatus eStatus;
	SUResult res = SUModelCreateFromFileWithStatus( &rModel, filename.c_str( ), &eStatus );
	SUModelRelease( &rModel );
	return res == SU_ERROR_NONE;
}

const aiImporterDesc* CSketchUpImporter::GetInfo( ) const
{
	return &desc;
}

void CSketchUpImporter::InternReadFile( const std::string& filename, aiScene* pScene, Assimp::IOSystem* pIOHandler )
{
	SUResult res;

	SUModelRef rModel = SU_INVALID;
	SUModelLoadStatus eStatus;
	res = SUModelCreateFromFileWithStatus( &rModel, filename.c_str( ), &eStatus );
	AssimpSkpHelper::checkSUResult( res );

	auto materials = AssimpSkpHelper::readAllMaterialsFromSU( rModel );

	auto components = AssimpSkpHelper::readComponentDefinitionsFromSU( rModel );

	SUEntitiesRef rModelEntities = SU_INVALID;
	res                          = SUModelGetEntities( rModel, &rModelEntities );
	AssimpSkpHelper::checkSUResult( res );

	auto entities = AssimpSkpHelper::readEntityFromSU( rModelEntities );

	// Create the root node of the scene
	auto sName        = AssimpSkpHelper::readModelNameFromSU( rModel );
	pScene->mRootNode = new aiNode;

	// apply scaling for sketchups inch basis
	const float scaleInchToMeter = 1 / 39.3700787f;
	pScene->mRootNode->mTransformation *= aiMatrix4x4( scaleInchToMeter, 0, 0, 0, 0, scaleInchToMeter, 0, 0, 0, 0, scaleInchToMeter, 0, 0, 0, 0, 1 );

	// apply transform from sketchups  right-handed z-up coordinate system to openGLs right-handed y-up coordinate system
	pScene->mRootNode->mTransformation *= aiMatrix4x4( 1, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1 );

	if( !sName.empty( ) )
	{
		// Set the name of the scene
		pScene->mRootNode->mName.Set( sName );
	}
	else
	{
		pScene->mRootNode->mName.Set( "ID1" );
	}

	AssimpSkpHelper::createAssimpDataFromSkpData( pScene, materials, components, entities );

	SUModelRelease( &rModel );
}
