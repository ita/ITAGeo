// ITAGeo includes
#include <ITAGeo/Halfedge/MeshModel.h>
#include <ITAGeo/SketchUp/Conversions.h>
#include <ITAGeo/SketchUp/Helper.h>
#include <ITAGeo/SketchUp/Materials.h>
#include <ITAGeo/SketchUp/MeshConversions.h>
#include <ITAGeo/SketchUp/Model.h>

// ITABase includes
#include <ITAException.h>

// Vista includes
#include <VistaTools/VistaFileSystemDirectory.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <algorithm>

// OpenMesh includes
#include <OpenMesh/Core/Utils/PropertyManager.hh>

// Typedefs
typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;

using namespace ITAGeo;
using namespace ITAGeo::SketchUp;


CModel::CModel( )
{
	m_pTopLevelMeshModel = std::make_shared<ITAGeo::Halfedge::CMeshModel>( );

	m_iSUVersion = SUModelVersion::SUModelVersion_SU2016;
	SetName( "Unnamed_ITAGeoSketchUpModel" );
}

CModel::~CModel( ) {}

void CModel::SetSketchUpVersion( int iVersion )
{
	m_iSUVersion = iVersion;
}

int CModel::GetSketchUpVersion( ) const
{
	return m_iSUVersion;
}

bool CModel::Load( const std::string& sSKPFilePath )
{
	VistaFileSystemFile oFile( sSKPFilePath );
	if( oFile.Exists( ) == false )
		ITA_EXCEPT1( INVALID_PARAMETER, "File '" + sSKPFilePath + "' does not exist or is invalid" );

	SUInitialize( );
	SUResult sur;

	SUModelRef rModel = SU_INVALID;
	SU_EXC( SUModelCreateFromFile( &rModel, sSKPFilePath.c_str( ) ) );

	int iSUVersionMajor, iSUVersionMinor, iBuild;
	SU_EXC( SUModelGetVersion( rModel, &iSUVersionMajor, &iSUVersionMinor, &iBuild ) );

	SUStringRef rModelName = SU_INVALID;
	SU_EXC( SUStringCreate( &rModelName ) );
	SU_EXC( SUModelGetName( rModel, &rModelName ) );
	SetName( SUStringToStdString( rModelName ) );
	SU_EXC( SUStringRelease( &rModelName ) );


	// Layers

	size_t nNumLayers;
	SU_EXC( SUModelGetNumLayers( rModel, &nNumLayers ) );
	if( nNumLayers > 0 )
	{
		size_t nActualNumLayers;
		std::vector<SULayerRef> vrLayers( nNumLayers );
		SU_EXC( SUModelGetLayers( rModel, nNumLayers, &vrLayers[0], &nActualNumLayers ) );

		for( size_t l = 0; l < nNumLayers; l++ )
		{
			SULayerRef rLayer( vrLayers[l] );
			SUStringRef sSULayer = SU_INVALID;
			SU_EXC( SUStringCreate( &sSULayer ) );
			SU_EXC( SULayerGetName( rLayer, &sSULayer ) );
			std::string sLayerName = SUStringToStdString( sSULayer );
			SU_EXC( SUStringRelease( &sSULayer ) );

			AddLayer( sLayerName );
		}
	}

	// Top-level mesh (not exploded)

	SUEntitiesRef rModelEntities = SU_INVALID;
	SU_EXC( SUModelGetEntities( rModel, &rModelEntities ) );

	// Top-level mesh (do not explode connected groups and component instances here)
	CITAMesh* pTLMesh = m_pTopLevelMeshModel->GetMesh( );
	SU_EXC( SUEntitiesToITAMesh( rModelEntities, pTLMesh, false, m_pMaterialManager ) );


	// Top-level groups (exploded)

	size_t nNumGroups;
	SU_EXC( SUEntitiesGetNumGroups( rModelEntities, &nNumGroups ) );

	if( nNumGroups > 0 )
	{
		size_t nActualGroups;
		std::vector<SUGroupRef> vrGroups( nNumGroups );
		SU_EXC( SUEntitiesGetGroups( rModelEntities, nNumGroups, &vrGroups[0], &nActualGroups ) );

		m_vpMeshGroups.resize( nNumGroups );
		for( size_t n = 0; n < nNumGroups; n++ )
		{
			auto pHEMeshModel = std::make_shared<Halfedge::CMeshModel>( );
			m_vpMeshGroups[n] = pHEMeshModel;

			const SUGroupRef& rGroup( vrGroups[n] );
			SUEntitiesRef rGroupEntities;
			SU_EXC( SUGroupGetEntities( rGroup, &rGroupEntities ) );

			SUTransformation tTransform;
			SU_EXC( SUGroupGetTransform( rGroup, &tTransform ) );
			std::vector<SUTransformation> vtTransform;
			vtTransform.push_back( tTransform );

			SUStringRef sName = SU_INVALID;
			SU_EXC( SUStringCreate( &sName ) );
			SU_EXC( SUGroupGetName( rGroup, &sName ) );
			pHEMeshModel->SetName( SUStringToStdString( sName ) );


			// Recursion (explode connected groups and component instances, and add to current group mesh)
			CITAMesh* pGroupMesh = pHEMeshModel->GetMesh( );
			SU_EXC( SUEntitiesToITAMesh( rGroupEntities, pGroupMesh, true, vtTransform, m_pMaterialManager ) );
		}
	}


	// Component definitions

	size_t nNumCompDefs;
	SU_EXC( SUModelGetNumComponentDefinitions( rModel, &nNumCompDefs ) );

	if( nNumCompDefs > 0 )
	{
		size_t nActualCompDefs;
		std::vector<SUComponentDefinitionRef> vrCompDefs( nNumCompDefs );
		SU_EXC( SUModelGetComponentDefinitions( rModel, nNumCompDefs, &vrCompDefs[0], &nActualCompDefs ) );
		SUStringRef rCompDefName = SU_INVALID;
		SU_EXC( SUStringCreate( &rCompDefName ) );

		for( size_t i = 0; i < nActualCompDefs; i++ )
		{
			SUEntitiesRef rCompDefEntities;
			SU_EXC( SUComponentDefinitionGetEntities( vrCompDefs[i], &rCompDefEntities ) );

			size_t nNumCompInst;
			SU_EXC( SUEntitiesGetNumInstances( rCompDefEntities, &nNumCompInst ) );

			CHalfedgeMeshModelComponentDefinition oCompDef;
			oCompDef.pMesh = std::make_shared<Halfedge::CMeshModel>( );

			// Recursion.
			SU_EXC( SUEntitiesToITAMesh( rCompDefEntities, oCompDef.pMesh->GetMesh( ), true ) );
			SU_EXC( SUComponentDefinitionGetName( vrCompDefs[i], &rCompDefName ) );

			oCompDef.sName          = SUStringToStdString( rCompDefName );
			oCompDef.iNumReferences = 0;
			m_voMeshComponentDefinitions.push_back( oCompDef );
		}

		SU_EXC( SUStringRelease( &rCompDefName ) );
	}


	// Component instances

	size_t nNumCompInst;
	SU_EXC( SUEntitiesGetNumInstances( rModelEntities, &nNumCompInst ) );

	if( nNumCompInst > 0 )
	{
		size_t nActualInst;
		std::vector<SUComponentInstanceRef> vrCompInst( nNumCompInst );
		SU_EXC( SUEntitiesGetInstances( rModelEntities, nNumCompInst, &vrCompInst[0], &nActualInst ) );

		for( size_t i = 0; i < nActualInst; i++ )
		{
			CHalfedgeMeshModelComponentInstance oCompInst;

			SUStringRef rCompDefName = SU_INVALID;
			SU_EXC( SUStringCreate( &rCompDefName ) );

			SUComponentDefinitionRef rCompDef = SU_INVALID;
			SU_EXC( SUComponentInstanceGetDefinition( vrCompInst[i], &rCompDef ) );
			SU_EXC( SUComponentDefinitionGetName( rCompDef, &rCompDefName ) );

			std::string sTargetCompDefName = SUStringToStdString( rCompDefName );

			// Transform matrix
			SUTransformation tTransform;
			SU_EXC( SUComponentInstanceGetTransform( vrCompInst[i], &tTransform ) );
			oCompInst.oTransformMatrix = SUTransformToVistaTransformMatrix( tTransform );

			// Connect component definition
			bool bDefinitionForInstanceFound = false;
			for( size_t j = 0; j < m_voMeshComponentDefinitions.size( ); j++ )
			{
				CHalfedgeMeshModelComponentDefinition& oGlobalCompDef( m_voMeshComponentDefinitions[j] );
				if( oGlobalCompDef.sName == sTargetCompDefName )
				{
					oCompInst.pComponentDefinition = oGlobalCompDef.pMesh;
					oGlobalCompDef.iNumReferences++;
					bDefinitionForInstanceFound = true;
					break;
				}
			}

			if( !bDefinitionForInstanceFound )
				ITA_EXCEPT1( INVALID_PARAMETER, "Could not find SU component definition" );

			SU_EXC( SUStringRelease( &rCompDefName ) );

			m_voMeshComponentInstances.push_back( oCompInst ); // Add
		}
	}

	// Release

	if( ( sur = SUModelRelease( &rModel ) ) != SU_ERROR_NONE )
		ITA_EXCEPT1( INVALID_PARAMETER, "Encountered SU error code " + std::to_string( long double( sur ) ) );

	SUTerminate( );

	return true;
}

bool CModel::Store( const std::string& sSKPFilePath, bool bOverwite /*=true*/ ) const
{
	VistaFileSystemFile oFile( sSKPFilePath );
	VistaFileSystemDirectory oParentDir( oFile.GetParentDirectory( ) );
	if( oParentDir.Exists( ) == false )
		ITA_EXCEPT1( INVALID_PARAMETER, "Target folder '" + oParentDir.GetName( ) + "' does not exist or is invalid" );

	if( oFile.Exists( ) && bOverwite == false )
		ITA_EXCEPT1( MODAL_EXCEPTION, "File '" + oFile.GetName( ) + "' already exists and shall not be overwritten" );


	// Set up SU content

	SUInitialize( );

	SUModelRef rNewModel = SU_INVALID;

	SU_EXC( SUModelCreate( &rNewModel ) );

	std::string sName = m_pTopLevelMeshModel->GetName( );
	SU_EXC( SUModelSetName( rNewModel, sName.c_str( ) ) );

	SUEntitiesRef rEntities = SU_INVALID;
	SU_EXC( SUModelGetEntities( rNewModel, &rEntities ) );

	CITAMesh* pMesh = m_pTopLevelMeshModel->GetMesh( );
	SU_EXC( ITAMeshToSUEntities( pMesh, &rEntities ) );


	// Set unit

	SUModelUnits rUnits = SUModelUnits::SUModelUnits_Meters;
	SU_EXC( SUModelGetUnits( rNewModel, &rUnits ) );


	// Add Layers

	std::vector<SULayerRef> vrLayer;
	vrLayer.resize( m_vsLayers.size( ) );
	for( size_t i = 0; i < m_vsLayers.size( ); i++ )
	{
		SULayerRef& rLayer( vrLayer[i] );
		const std::string& sLayerName( m_vsLayers[i] );
		SU_EXC( SULayerCreate( &rLayer ) );
		SU_EXC( SULayerSetName( rLayer, sLayerName.c_str( ) ) );
	}
	if( vrLayer.size( ) > 0 )
		SU_EXC( SUModelAddLayers( rNewModel, vrLayer.size( ), &vrLayer[0] ) );


	// Groups

	for( size_t i = 0; i < m_vpMeshGroups.size( ); i++ )
	{
		pMesh = m_vpMeshGroups[i]->GetMesh( );

		SUGroupRef rGroup = SU_INVALID;
		SU_EXC( SUGroupCreate( &rGroup ) );

		SUEntitiesRef rGroupEntities = SU_INVALID;
		SU_EXC( SUGroupGetEntities( rGroup, &rGroupEntities ) );
		SU_EXC( ITAMeshToSUEntities( pMesh, &rGroupEntities ) );
		SU_EXC( SUEntitiesAddGroup( rEntities, rGroup ) );
		SU_EXC( SUGroupSetName( rGroup, m_vpMeshGroups[i]->GetName( ).c_str( ) ) );
	}


	// Component Definitions

	std::vector<SUComponentDefinitionRef> vrCompDef;
	for( size_t i = 0; i < m_voMeshComponentDefinitions.size( ); i++ )
	{
		SUComponentDefinitionRef rCompDef = SU_INVALID;
		SU_EXC( SUComponentDefinitionCreate( &rCompDef ) );

		SUEntitiesRef rCompDefEntities = SU_INVALID;
		SU_EXC( SUComponentDefinitionGetEntities( rCompDef, &rCompDefEntities ) );

		pMesh = m_voMeshComponentDefinitions[i].pMesh->GetMesh( );
		SU_EXC( ITAMeshToSUEntities( pMesh, &rCompDefEntities ) );
		SU_EXC( SUComponentDefinitionSetName( rCompDef, m_voMeshComponentDefinitions[i].sName.c_str( ) ) );

		vrCompDef.push_back( rCompDef );
	}

	if( vrCompDef.size( ) > 0 )
		SU_EXC( SUModelAddComponentDefinitions( rNewModel, vrCompDef.size( ), &vrCompDef[0] ) );


	// Component Instances

	for( size_t i = 0; i < m_voMeshComponentInstances.size( ); i++ )
	{
		SUComponentInstanceRef rCompInst = SU_INVALID;
		SUTransformation tTransform      = VistaTransformMatrixToSUTransform( m_voMeshComponentInstances[i].oTransformMatrix );
		bool bDefinitionFound            = false;
		for( size_t j = 0; j < vrCompDef.size( ); j++ )
		{
			if( m_voMeshComponentInstances[i].pComponentDefinition == m_voMeshComponentDefinitions[j].pMesh )
			{
				SU_EXC( SUComponentDefinitionCreateInstance( vrCompDef[j], &rCompInst ) );
				SU_EXC( SUComponentInstanceSetTransform( rCompInst, &tTransform ) );
				SU_EXC( SUEntitiesAddInstance( rEntities, rCompInst, NULL ) );
				bDefinitionFound = true;
				break;
			}
		}
		if( !bDefinitionFound )
			ITA_EXCEPT1( IO_ERROR, "SU: couldn't find definition of component instance" );
	}


	// Add visualization meshes (with individual layers)

	for( auto key: m_mAcousticVisualizationMeshes )
	{
		const std::string& sVisLayerName( key.first );
		std::vector<const Halfedge::CMeshModel*> vpHMM( key.second );

		SULayerRef rVisLayer = SU_INVALID;
		SU_EXC( SULayerCreate( &rVisLayer ) );
		SU_EXC( SULayerSetName( rVisLayer, sVisLayerName.c_str( ) ) );
		SU_EXC( SUModelAddLayers( rNewModel, 1, &rVisLayer ) );

		for( size_t n = 0; n < vpHMM.size( ); n++ )
		{
			const Halfedge::CMeshModel* pHMM( vpHMM[n] );

			const CITAMesh* pVisMesh = pHMM->ConstGetMesh( );
			SU_EXC( ITAMeshToSUEntities( pVisMesh, &rEntities, &rVisLayer ) );
		}
	}

	for( auto key: m_mGeoPropPathsVisualization )
	{
		ITAGeo::CPropagationPathList voGeoPropPathsVisualization( key.second );

		// Propagation path visualization
		SULayerRef rPropLayer = SU_INVALID;
		const std::string& sPropLayerName( key.first );
		SU_EXC( SULayerCreate( &rPropLayer ) );
		SU_EXC( SULayerSetName( rPropLayer, sPropLayerName.c_str( ) ) );
		SU_EXC( SUModelAddLayers( rNewModel, 1, &rPropLayer ) );

		std::vector<SUCurveRef> vrCurves( voGeoPropPathsVisualization.size( ) );
		for( size_t n = 0; n < voGeoPropPathsVisualization.size( ); n++ )
		{
			const CPropagationPath& oPath( voGeoPropPathsVisualization[n] );

			if( oPath.size( ) <= 1 )
				continue;

			SUMaterialRef rMaterial = SU_INVALID;
			SU_EXC( SUMaterialCreate( &rMaterial ) );
			SUColor oSUColor;


			// Color and alpha values

			// Set alpha value according to length of path with 10% as minimum value and 255 as maximum value
			float fMaxLength = voGeoPropPathsVisualization.GetMaxLength( );
			float fAlpha     = 0.0f;
			if( fMaxLength > 0.0f )
			{
				fAlpha = std::max( 0.1f, 1.0f - (float)oPath.GetLength( ) / fMaxLength );
				fAlpha = std::min( 255.0f, fAlpha * 255.0f );
			}

			// Color mixing

			// Monte Carlo rays are blue
			int iNumRaySteps = oPath.GetNumRaySteps( );
			float fBlue      = 255.0f * iNumRaySteps / float( oPath.size( ) - 2 );

			// Diffractions are yellow
			int iNumDiffractions = oPath.GetNumDiffractions( );
			float fRed           = 255.0f * iNumDiffractions / float( oPath.size( ) - 2 );

			int iNumReflections = oPath.GetNumReflections( );
			float fGreen        = 255.0f * ( iNumReflections + iNumDiffractions ) / float( oPath.size( ) - 2 );

			// Direct path (always red)
			if( oPath.GetNumAnchors( ) == 2 )
			{
				fRed   = 230.0f;
				fGreen = 0.0f;
				fBlue  = 0.0f;
			}

			oSUColor.alpha = SUByte( fAlpha );
			oSUColor.red   = SUByte( fRed );   // First wavefront / direct sound
			oSUColor.green = SUByte( fGreen ); // Reflection, yellow (red+green) = diffr
			oSUColor.blue  = SUByte( fBlue );  // Late reverb / ray tracing

			SU_EXC( SUMaterialSetColor( rMaterial, &oSUColor ) );
			SUModelAddMaterials( rNewModel, 1, &rMaterial );

			std::vector<SUEdgeRef> vrEdges( oPath.size( ) - 1 );
			bool bSkip        = false;
			int iSegmentCount = 0;
			for( size_t m = 0; m < vrEdges.size( ); m++ )
			{
				auto pSourceAnchor( oPath[m] );
				auto pTargetAnchor( oPath[m + 1] );

				if( pSourceAnchor == pTargetAnchor )
					ITA_EXCEPT_INVALID_PARAMETER( "A propagation path can not have the same anchors in a row." );
				if( pSourceAnchor->v3InteractionPoint == pTargetAnchor->v3InteractionPoint )
				{
					if( m == vrEdges.size( ) - 2 )
					{
						std::cout << "During SketchUp model export: source and target anchor of propagation path have same interaction point at "
						          << pSourceAnchor->v3InteractionPoint << ", skipping this path." << std::endl;
						bSkip = true;
						break;
					}

					std::cout << "During SketchUp model export: source and target anchor of propagation path have same interaction point at "
					          << pSourceAnchor->v3InteractionPoint << ", skipping this segment of the path." << std::endl;
					continue;
				}

				SUPoint3D v3SUStart = VistaVectorToSUPoint( pSourceAnchor->v3InteractionPoint );
				SUPoint3D v3SUEnd   = VistaVectorToSUPoint( pTargetAnchor->v3InteractionPoint );

				SU_EXC( SUEdgeCreate( &vrEdges[iSegmentCount], &v3SUStart, &v3SUEnd ) );

				SUDrawingElementRef rDrawingElement = SUEdgeToDrawingElement( vrEdges[iSegmentCount] );
				SU_EXC( SUDrawingElementSetMaterial( rDrawingElement, rMaterial ) );
				SU_EXC( SUDrawingElementSetLayer( rDrawingElement, rPropLayer ) );

				iSegmentCount++;
			}
			if( !bSkip )
				SU_EXC( SUCurveCreateWithEdges( &vrCurves[n], &vrEdges[0], iSegmentCount ) );
		}

		if( vrCurves.size( ) > 0 )
			SU_EXC( SUEntitiesAddCurves( rEntities, vrCurves.size( ), &vrCurves[0] ) );
	}


	// Add emitters

	for( auto keyLabel: m_mGeoEmittersVisualization )
	{
		// Add emitter label
		SULayerRef rEmitterLayer = SU_INVALID;
		const std::string& sEmitterLayerName( keyLabel.first );
		SU_EXC( SULayerCreate( &rEmitterLayer ) );
		SU_EXC( SULayerSetName( rEmitterLayer, sEmitterLayerName.c_str( ) ) );
		SU_EXC( SUModelAddLayers( rNewModel, 1, &rEmitterLayer ) );

		// Add material
		SUMaterialRef rMaterial = SU_INVALID;
		SU_EXC( SUMaterialCreate( &rMaterial ) );
		SUColor oSUColor;
		oSUColor.red   = SUByte( 200.0f );
		oSUColor.green = SUByte( 55.0f );
		oSUColor.blue  = SUByte( 0.0f );
		SU_EXC( SUMaterialSetColor( rMaterial, &oSUColor ) );
		SUMaterialSetName( rMaterial, "ITA_Acoustics_DefaultEmitter" );
		SUModelAddMaterials( rNewModel, 1, &rMaterial );

		std::vector<SUCurveRef> vrCurves;
		for( auto keyName: keyLabel.second )
		{
			auto oEmitter = keyName.second;

			OpenMesh::Vec3f emitterPos( oEmitter.v3InteractionPoint[0], oEmitter.v3InteractionPoint[1], oEmitter.v3InteractionPoint[2] );

			// Length of cube edges in meters
			float fLength = 0.3f;

			SUGroupRef rGroup = SU_INVALID;

			AddCubeToSUGroup( rGroup, emitterPos, fLength );

			SUDrawingElementRef rGroupDrawing = SUGroupToDrawingElement( rGroup );
			SUDrawingElementSetLayer( rGroupDrawing, rEmitterLayer );
			SUDrawingElementSetMaterial( rGroupDrawing, rMaterial );

			SU_EXC( SUEntitiesAddGroup( rEntities, rGroup ) );
			SU_EXC( SUGroupSetName( rGroup, keyName.first.c_str( ) ) );
		}
	}

	// Add sensors

	for( auto keyLabel: m_mGeoSensorsVisualization )
	{
		// Add sensor label
		SULayerRef rSensorLayer = SU_INVALID;
		const std::string& sSensorLayerName( keyLabel.first );
		SU_EXC( SULayerCreate( &rSensorLayer ) );
		SU_EXC( SULayerSetName( rSensorLayer, sSensorLayerName.c_str( ) ) );
		SU_EXC( SUModelAddLayers( rNewModel, 1, &rSensorLayer ) );

		// Add material
		SUMaterialRef rSensorMaterial = SU_INVALID;
		SU_EXC( SUMaterialCreate( &rSensorMaterial ) );
		SUColor oSUColor;
		oSUColor.red   = SUByte( 0.0f );
		oSUColor.green = SUByte( 50.0f );
		oSUColor.blue  = SUByte( 190.0f );
		SU_EXC( SUMaterialSetColor( rSensorMaterial, &oSUColor ) );
		SUMaterialSetName( rSensorMaterial, "ITA_Acoustics_DefaultSensor" );
		SUModelAddMaterials( rNewModel, 1, &rSensorMaterial );

		std::vector<SUCurveRef> vrCurves;
		for( auto keyName: keyLabel.second )
		{
			auto oSensor = keyName.second;

			OpenMesh::Vec3f sensorPos( oSensor.v3InteractionPoint[0], oSensor.v3InteractionPoint[1], oSensor.v3InteractionPoint[2] );

			// Length of cube edges in meters
			float fLength = 0.3f;

			SUGroupRef rGroup = SU_INVALID;

			AddCubeToSUGroup( rGroup, sensorPos, fLength );

			SUDrawingElementRef rGroupDrawing = SUGroupToDrawingElement( rGroup );
			SUDrawingElementSetLayer( rGroupDrawing, rSensorLayer );
			SUDrawingElementSetMaterial( rGroupDrawing, rSensorMaterial );

			SU_EXC( SUEntitiesAddGroup( rEntities, rGroup ) );
			SU_EXC( SUGroupSetName( rGroup, keyName.first.c_str( ) ) );
		}
	}


	// Store to file

	SUModelVersion iSUVersion = (SUModelVersion)m_iSUVersion;
	SUResult sur              = SUModelSaveToFileWithVersion( rNewModel, sSKPFilePath.c_str( ), iSUVersion );
	if( sur == SU_ERROR_SERIALIZATION )
	{
		ITA_EXCEPT1( IO_ERROR, "SUModelSaveToFileWithVersion: could not store SKP file, maybe file still open in SketchUp?" );
	}
	else if( sur != SU_ERROR_NONE )
	{
		ITA_EXCEPT1( INVALID_PARAMETER, "SUModelSaveToFileWithVersion: Could not write to file with error code: " + std::to_string( long double( sur ) ) );
	}

	SU_EXC( SUModelRelease( &rNewModel ) );

	SUTerminate( );

	return true;
}

void ITAGeo::SketchUp::CModel::AddCubeToSUGroup( SUGroupRef& rGroup, const OpenMesh::Vec3f& v3CubePos, const float fLength ) const
{
	// Vertices of cube
	std::vector<OpenMesh::Vec3f> v3Vertices( 8 );

	// Mesh, where faces of cube are created
	CITAMesh* pMesh = new CITAMesh( );
	OpenMesh::getOrMakeProperty<CITAMesh::FaceHandle, ITAGeo::Material::CAcousticFaceProperty>( *pMesh, "Acoustics::Material::Face" );

	// Creation of group ref
	SU_EXC( SUGroupCreate( &rGroup ) );
	SUEntitiesRef rGroupEntities = SU_INVALID;

	// Vertices of cube
	v3Vertices[0] = v3CubePos + OpenMesh::Vec3f( -fLength / 2, -fLength / 2, -fLength / 2 );
	v3Vertices[1] = v3CubePos + OpenMesh::Vec3f( -fLength / 2, -fLength / 2, fLength / 2 );
	v3Vertices[2] = v3CubePos + OpenMesh::Vec3f( -fLength / 2, fLength / 2, fLength / 2 );
	v3Vertices[3] = v3CubePos + OpenMesh::Vec3f( -fLength / 2, fLength / 2, -fLength / 2 );
	v3Vertices[4] = v3CubePos + OpenMesh::Vec3f( fLength / 2, fLength / 2, -fLength / 2 );
	v3Vertices[5] = v3CubePos + OpenMesh::Vec3f( fLength / 2, fLength / 2, fLength / 2 );
	v3Vertices[6] = v3CubePos + OpenMesh::Vec3f( fLength / 2, -fLength / 2, fLength / 2 );
	v3Vertices[7] = v3CubePos + OpenMesh::Vec3f( fLength / 2, -fLength / 2, -fLength / 2 );

	// Vertex handles in vector form
	std::vector<OpenMesh::VertexHandle> vVertexHandles;
	for( auto vertex: v3Vertices )
		vVertexHandles.push_back( pMesh->add_vertex( vertex ) );

	// Add the vertices to the corresponding six faces of the cube
	std::vector<OpenMesh::VertexHandle> vVertexHandlesFace1;
	vVertexHandlesFace1.push_back( vVertexHandles[0] );
	vVertexHandlesFace1.push_back( vVertexHandles[1] );
	vVertexHandlesFace1.push_back( vVertexHandles[2] );
	vVertexHandlesFace1.push_back( vVertexHandles[3] );

	std::vector<OpenMesh::VertexHandle> vVertexHandlesFace2;
	vVertexHandlesFace2.push_back( vVertexHandles[1] );
	vVertexHandlesFace2.push_back( vVertexHandles[6] );
	vVertexHandlesFace2.push_back( vVertexHandles[5] );
	vVertexHandlesFace2.push_back( vVertexHandles[2] );

	std::vector<OpenMesh::VertexHandle> vVertexHandlesFace3;
	vVertexHandlesFace3.push_back( vVertexHandles[3] );
	vVertexHandlesFace3.push_back( vVertexHandles[2] );
	vVertexHandlesFace3.push_back( vVertexHandles[5] );
	vVertexHandlesFace3.push_back( vVertexHandles[4] );

	std::vector<OpenMesh::VertexHandle> vVertexHandlesFace4;
	vVertexHandlesFace4.push_back( vVertexHandles[0] );
	vVertexHandlesFace4.push_back( vVertexHandles[3] );
	vVertexHandlesFace4.push_back( vVertexHandles[4] );
	vVertexHandlesFace4.push_back( vVertexHandles[7] );

	std::vector<OpenMesh::VertexHandle> vVertexHandlesFace5;
	vVertexHandlesFace5.push_back( vVertexHandles[7] );
	vVertexHandlesFace5.push_back( vVertexHandles[6] );
	vVertexHandlesFace5.push_back( vVertexHandles[1] );
	vVertexHandlesFace5.push_back( vVertexHandles[0] );

	std::vector<OpenMesh::VertexHandle> vVertexHandlesFace6;
	vVertexHandlesFace6.push_back( vVertexHandles[4] );
	vVertexHandlesFace6.push_back( vVertexHandles[5] );
	vVertexHandlesFace6.push_back( vVertexHandles[6] );
	vVertexHandlesFace6.push_back( vVertexHandles[7] );

	// Add the six faces to the mesh
	pMesh->add_face( vVertexHandlesFace1 );
	pMesh->add_face( vVertexHandlesFace2 );
	pMesh->add_face( vVertexHandlesFace3 );
	pMesh->add_face( vVertexHandlesFace4 );
	pMesh->add_face( vVertexHandlesFace5 );
	pMesh->add_face( vVertexHandlesFace6 );


	// Add mesh to the SuGroup
	SU_EXC( SUGroupGetEntities( rGroup, &rGroupEntities ) );
	SU_EXC( ITAMeshToSUEntities( pMesh, &rGroupEntities ) );

	delete pMesh;
}

void CModel::AddGroupMeshModel( const Halfedge::CMeshModel* pMesh )
{
	auto pMeshCopy = std::make_shared<Halfedge::CMeshModel>( );
	pMeshCopy->CopyFrom( *pMesh );
	m_vpMeshGroups.push_back( pMeshCopy );
}

void CModel::InvertAllFaceNormals( )
{
	m_pTopLevelMeshModel->InvertFaceNormals( );
	for( auto pMesh: m_vpMeshGroups )
		pMesh->InvertFaceNormals( );
}

size_t CModel::GetNumGroups( ) const
{
	return m_vpMeshGroups.size( );
}

std::vector<std::string> CModel::GetGroupNames( ) const
{
	std::vector<std::string> vsNames( GetNumGroups( ) );
	for( size_t n = 0; n < GetNumGroups( ); n++ )
		vsNames[n] = m_vpMeshGroups[n]->GetName( );
	return vsNames;
}

std::vector<const Halfedge::CMeshModel*> CModel::GetGroupMeshModels( ) const
{
	std::vector<const Halfedge::CMeshModel*> vpMeshGroups;
	for( auto& pMesh: m_vpMeshGroups )
		vpMeshGroups.push_back( pMesh.get( ) );

	return vpMeshGroups;
}

std::vector<Halfedge::CMeshModelShared> CModel::GetComponentMeshModels( ) const
{
	std::vector<ITAGeo::Halfedge::CMeshModelShared> vpMeshInstances;
	for( auto& pMeshModel: m_voMeshComponentInstances )
	{
		ITAGeo::Halfedge::CMeshModelShared pTransformedModel = pMeshModel.pComponentDefinition->GetTransformedMesh( pMeshModel.oTransformMatrix );
		vpMeshInstances.push_back( pTransformedModel );
	}

	return vpMeshInstances;
}

size_t CModel::GetNumComponentInstances( ) const
{
	return m_voMeshComponentInstances.size( );
}

size_t CModel::GetNumComponentDefinitions( ) const
{
	return m_voMeshComponentDefinitions.size( );
}

std::vector<std::string> CModel::GetComponentDefinitionNames( ) const
{
	std::vector<std::string> vsNames( GetNumComponentDefinitions( ) );
	for( size_t n = 0; n < GetNumComponentDefinitions( ); n++ )
		vsNames[n] = m_voMeshComponentDefinitions[n].sName;
	return vsNames;
}

size_t CModel::GetNumLayers( ) const
{
	return m_vsLayers.size( );
}

std::vector<std::string> CModel::GetLayers( ) const
{
	return m_vsLayers;
}

const ITAGeo::Halfedge::CMeshModel* CModel::GetTopLevelMesh( ) const
{
	return m_pTopLevelMeshModel.get( );
}

bool CModel::AddLayer( std::string& sLayerName )
{
	for( size_t n = 0; n < m_vsLayers.size( ); n++ )
		if( sLayerName == m_vsLayers[n] )
			return false;
	m_vsLayers.push_back( sLayerName );
	return true;
}

void CModel::AddEmitterVisualization( const CPropagationAnchor& oEmitter, const std::string& sEmitterName /* = "Emitter" */,
                                      const std::string& sLayerName /*= "Emitter"*/ )
{
	m_mGeoEmittersVisualization[sLayerName][sEmitterName] = oEmitter;
}

void CModel::AddSensorVisualization( const CPropagationAnchor& oEmitter, const std::string& sSensorName /* = "Sensor" */, const std::string& sLayerName /*= "Sensor"*/ )
{
	m_mGeoSensorsVisualization[sLayerName][sSensorName] = oEmitter;
}

void CModel::AddPropagationPathVisualization( const CPropagationPath& oPath, const std::string& sLayerName /* = "Layer0" */ )
{
	if( sLayerName.empty( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Layer name can't be empty" );

	m_mGeoPropPathsVisualization[sLayerName].push_back( oPath );
}

void CModel::AddVisualizationMesh( const Halfedge::CMeshModel* pVisMesh, const std::string& sLayerName /* = "Layer0" */ )
{
	m_mAcousticVisualizationMeshes[sLayerName].push_back( pVisMesh );
}

bool CModel::TriangulateMeshes( )
{
	if( !m_pTopLevelMeshModel->TriangulateMesh( ) )
		return false;

	for( size_t i = 0; i < m_voMeshComponentDefinitions.size( ); i++ )
	{
		if( !m_voMeshComponentDefinitions[i].pMesh->TriangulateMesh( ) )
			return false;
	}

	for( size_t i = 0; i < m_vpMeshGroups.size( ); i++ )
	{
		if( !m_vpMeshGroups[i]->TriangulateMesh( ) )
			return false;
	}

	return true;
}

bool CModel::IsWaterproof( ) const
{
	if( !m_pTopLevelMeshModel->IsWaterproofMesh( ) )
		return false;

	for( size_t i = 0; i < m_voMeshComponentDefinitions.size( ); i++ )
	{
		if( !m_voMeshComponentDefinitions[i].pMesh->IsWaterproofMesh( ) )
			return false;
	}

	for( size_t i = 0; i < m_vpMeshGroups.size( ); i++ )
	{
		if( !m_vpMeshGroups[i]->IsWaterproofMesh( ) )
			return false;
	}

	return true;
}
