#include "Assimp_Sketchup_Helper.h"

#include <assimp/DefaultLogger.hpp>
#include <assimp/scene.h>
#include <iostream>

namespace AssimpSkpHelper
{
	const std::string DEFAULT_MATERIAL_NAME( AI_DEFAULT_MATERIAL_NAME );

	void checkSUResult( SUResult err )
	{
		if( err != SU_ERROR_NONE )
		{
			std::ostringstream os;
			switch( err )
			{
				case SU_ERROR_NULL_POINTER_INPUT:
					os << "SU_ERROR_NULL_POINTER_INPUT - ";
					os << "A pointer for a required input was NULL.";
					break;
				case SU_ERROR_INVALID_INPUT:
					os << "SU_ERROR_INVALID_INPUT - ";
					os << "An API object input to the function was not created properly.";
					break;
				case SU_ERROR_NULL_POINTER_OUTPUT:
					os << "SU_ERROR_NULL_POINTER_OUTPUT - ";
					os << "A pointer for a required output was NULL. ";
					break;
				case SU_ERROR_INVALID_OUTPUT:
					os << "SU_ERROR_INVALID_OUTPUT - ";
					os << "An API object to be written with output from the function was not created properly.";
					break;
				case SU_ERROR_OVERWRITE_VALID:
					os << "SU_ERROR_OVERWRITE_VALID - ";
					os << "Indicates that an input object reference already references an object where it was expected to be SU_INVALID.";
					break;
				case SU_ERROR_GENERIC:
					os << "SU_ERROR_GENERIC - ";
					os << "Indicates an unspecified error.";
					break;
				case SU_ERROR_SERIALIZATION:
					os << "SU_ERROR_SERIALIZATION - ";
					os << "Indicates an error occurred during loading or saving of a file.";
					break;
				case SU_ERROR_OUT_OF_RANGE:
					os << "SU_ERROR_OUT_OF_RANGE - ";
					os << "An input contained a value that was outside the range of allowed values.";
					break;
				case SU_ERROR_NO_DATA:
					os << "SU_ERROR_NO_DATA - ";
					os << "The requested operation has no data to return to the user. This usually occurs when a request is made for data that is only available "
					      "conditionally.";
					break;
				case SU_ERROR_INSUFFICIENT_SIZE:
					os << "SU_ERROR_INSUFFICIENT_SIZE - ";
					os << "Indicates that the size of an output parameter is insufficient.";
					break;
				case SU_ERROR_UNKNOWN_EXCEPTION:
					os << "SU_ERROR_UNKNOWN_EXCEPTION - ";
					os << "An unknown exception occurred.";
					break;
				case SU_ERROR_MODEL_INVALID:
					os << "SU_ERROR_MODEL_INVALID - ";
					os << "The model requested is invalid and cannot be loaded.";
					break;
				case SU_ERROR_MODEL_VERSION:
					os << "SU_ERROR_MODEL_VERSION - ";
					os << "The model cannot be loaded or saved due to an invalid version.";
					break;
				case SU_ERROR_LAYER_LOCKED:
					os << "SU_ERROR_LAYER_LOCKED - ";
					os << "The layer that is being modified is locked.";
					break;
				case SU_ERROR_DUPLICATE:
					os << "SU_ERROR_DUPLICATE - ";
					os << "The user requested an operation that would result in duplicate data.";
					break;
				case SU_ERROR_PARTIAL_SUCCESS:
					os << "SU_ERROR_PARTIAL_SUCCESS - ";
					os << "The requested operation was not fully completed but it returned an intermediate successful result.";
					break;
				case SU_ERROR_UNSUPPORTED:
					os << "SU_ERROR_UNSUPPORTED - ";
					os << "The requested operation is not supported.";
					break;
				case SU_ERROR_INVALID_ARGUMENT:
					os << "SU_ERROR_INVALID_ARGUMENT - ";
					os << "An argument contains invalid information.";
					break;
				case SU_ERROR_ENTITY_LOCKED:
					os << "SU_ERROR_ENTITY_LOCKED - ";
					os << "The entity being modified is locked.";
					break;
				case SU_ERROR_INVALID_OPERATION:
					os << "SU_ERROR_INVALID_OPERATION - ";
					os << "The requested operation is invalid.";
					break;
				default:
					os << "Unknown Error #" << err;
			}

			throw std::runtime_error( os.str( ) );
		}
	}

	std::string readModelNameFromSU( const SUModelRef& rModel )
	{
		CSUString name;
		SUModelGetName( rModel, name );
		return name.utf8( );
	}

	std::vector<Material> readAllMaterialsFromSU( const SUModelRef& rModel )
	{
		std::vector<Material> retMaterials;

		SUResult res;

		size_t nNumMaterials = 0;
		res                  = SUModelGetNumMaterials( rModel, &nNumMaterials );
		checkSUResult( res );

		if( nNumMaterials > 0 )
		{
			std::vector<SUMaterialRef> materials( nNumMaterials );
			res = SUModelGetMaterials( rModel, nNumMaterials, materials.data( ), &nNumMaterials );
			checkSUResult( res );

			for( auto&& material: materials )
			{
				retMaterials.push_back( readMaterialFromSU( material ) );
			}
		}

		return retMaterials;
	}

	Material readMaterialFromSU( const SUMaterialRef& rMaterial )
	{
		SUResult res;
		Material mat;

		if( SUIsInvalid( rMaterial ) )
		{
			throw std::runtime_error( "skp: found invalid material" );
		}

		mat.name = readMaterialNameFromSU( rMaterial );

		SUMaterialType type;
		res = SUMaterialGetType( rMaterial, &type );

		if( ( type == SUMaterialType_Colored ) || ( type == SUMaterialType_ColorizedTexture ) )
		{
			res = SUMaterialGetColor( rMaterial, &mat.color );
			checkSUResult( res );
		}

		bool has_alpha = false;
		res            = SUMaterialGetUseOpacity( rMaterial, &has_alpha );
		checkSUResult( res );
		if( has_alpha )
		{
			double alpha = 0;
			res          = SUMaterialGetOpacity( rMaterial, &alpha );
			checkSUResult( res );
			mat.has_alpha = true;
			mat.alpha     = alpha;
		}

		if( ( type == SUMaterialType_Textured ) || ( type == SUMaterialType_ColorizedTexture ) )
		{
			SUTextureRef texture = SU_INVALID;
			res                  = SUMaterialGetTexture( rMaterial, &texture );
			checkSUResult( res );

			mat.has_texture = true;

			ASSIMP_LOG_WARN( "skp: Texture import not supported yet." );
		}

		return mat;
	}

	std::string readMaterialNameFromSU( const SUMaterialRef& material )
	{
		CSUString name;
		SUResult res = SUMaterialGetNameLegacyBehavior( material, name );
		checkSUResult( res );
		return name.utf8( );
	}

	Entity readEntityFromSU( const SUEntitiesRef& rEntities )
	{
		Entity entity;
		SUResult res;

		// Component instances
		size_t nNumInstances = 0;
		res                  = SUEntitiesGetNumInstances( rEntities, &nNumInstances );
		checkSUResult( res );

		if( nNumInstances > 0 )
		{
			std::vector<SUComponentInstanceRef> instances( nNumInstances );
			res = SUEntitiesGetInstances( rEntities, nNumInstances, &instances[0], &nNumInstances );
			checkSUResult( res );

			for( auto&& instance: instances )
			{
				auto& retInstance                   = entity.instances.emplace_back( );
				SUComponentDefinitionRef definition = SU_INVALID;
				res                                 = SUComponentInstanceGetDefinition( instance, &definition );
				checkSUResult( res );

				ASSIMP_LOG_WARN( "skp: Layer import not supported yet" );

				SUMaterialRef material = SU_INVALID;
				res                    = SUDrawingElementGetMaterial( SUComponentInstanceToDrawingElement( instance ), &material );
				if( res == SU_ERROR_NO_DATA )
				{
					retInstance.material_name = DEFAULT_MATERIAL_NAME;
				}
				else
				{
					checkSUResult( res );
					retInstance.material_name = readMaterialNameFromSU( material );
				}

				CSUString name;
				res = SUComponentDefinitionGetName( definition, name );
				checkSUResult( res );
				retInstance.name = name.utf8( );

				res = SUComponentInstanceGetTransform( instance, &retInstance.transform );
				checkSUResult( res );
			}
		}

		// Groups
		size_t nNumGroups = 0;
		res               = SUEntitiesGetNumGroups( rEntities, &nNumGroups );
		checkSUResult( res );

		if( nNumGroups > 0 )
		{
			std::vector<SUGroupRef> groups( nNumGroups );
			res = SUEntitiesGetGroups( rEntities, nNumGroups, &groups[0], &nNumGroups );
			checkSUResult( res );

			for( auto&& group: groups )
			{
				auto& retGroup = entity.groups.emplace_back( );

				SUEntitiesRef group_entities = SU_INVALID;
				res                          = SUGroupGetEntities( group, &group_entities );
				checkSUResult( res );

				retGroup.entity = readEntityFromSU( group_entities );

				res = SUGroupGetTransform( group, &retGroup.transform );
				checkSUResult( res );
			}
		}

		// Faces
		size_t nNumFaces = 0;
		res              = SUEntitiesGetNumFaces( rEntities, &nNumFaces );
		checkSUResult( res );

		if( nNumFaces > 0 )
		{
			std::vector<SUFaceRef> faces( nNumFaces );
			res = SUEntitiesGetFaces( rEntities, nNumFaces, &faces[0], &nNumFaces );
			checkSUResult( res );

			for( auto&& face: faces )
			{
				if( SUIsInvalid( face ) )
				{
					continue;
				}

				std::string front_material, back_material;

				SUMaterialRef su_front_material = SU_INVALID;
				res                             = SUFaceGetFrontMaterial( face, &su_front_material );
				if( res == SU_ERROR_NO_DATA )
				{
					front_material = DEFAULT_MATERIAL_NAME;
				}
				else
				{
					checkSUResult( res );
					front_material = readMaterialNameFromSU( su_front_material );
				}

				SUMaterialRef su_back_material = SU_INVALID;
				res                            = SUFaceGetBackMaterial( face, &su_back_material );
				if( res == SU_ERROR_NO_DATA )
				{
					back_material = DEFAULT_MATERIAL_NAME;
				}
				else
				{
					checkSUResult( res );
					back_material = readMaterialNameFromSU( su_back_material );
				}

				ASSIMP_LOG_WARN( "skp: Texture import not supported yet." );

				// Find out how many loops the face has
				size_t num_loops = 0;
				res              = SUFaceGetNumInnerLoops( face, &num_loops );
				checkSUResult( res );
				num_loops++; // add the outer loop

				if( num_loops == 1 )
				{
					auto& retFace = entity.faces.emplace_back( );

					retFace.front_material = front_material;
					retFace.back_material  = back_material;

					// Simple Face
					SULoopRef outer_loop = SU_INVALID;
					res                  = SUFaceGetOuterLoop( face, &outer_loop );
					checkSUResult( res );

					size_t num_vertices;
					res = SULoopGetNumVertices( outer_loop, &num_vertices );
					checkSUResult( res );

					if( num_vertices > 0 )
					{
						std::vector<SUVertexRef> vertices( num_vertices );
						res = SULoopGetVertices( outer_loop, num_vertices, &vertices[0], &num_vertices );
						checkSUResult( res );

						for( auto&& vertex: vertices )
						{
							auto& retVertex = retFace.vertices.emplace_back( );
							SUVertexGetPosition( vertex, &retVertex );
						}
					}
				}
				else
				{
					// If this is a complex face with one or more holes in it
					// we tessellate it into triangles using the polygon mesh class, then
					// export each triangle as a face.

					// Create a mesh from face.
					SUMeshHelperRef mesh_ref = SU_INVALID;
					res                      = SUMeshHelperCreate( &mesh_ref, face );
					checkSUResult( res );

					// Get the vertices
					size_t num_vertices = 0;
					res                 = SUMeshHelperGetNumVertices( mesh_ref, &num_vertices );
					checkSUResult( res );

					if( num_vertices == 0 )
					{
						// todo correct?
						continue;
					}

					std::vector<SUPoint3D> vertices( num_vertices );
					res = SUMeshHelperGetVertices( mesh_ref, num_vertices, &vertices[0], &num_vertices );
					checkSUResult( res );

					// Get triangle indices.
					size_t num_triangles = 0;
					res                  = SUMeshHelperGetNumTriangles( mesh_ref, &num_triangles );
					checkSUResult( res );

					const size_t num_indices = 3 * num_triangles;
					size_t num_retrieved     = 0;
					std::vector<size_t> indices( num_indices );
					res = SUMeshHelperGetVertexIndices( mesh_ref, num_indices, &indices[0], &num_retrieved );
					checkSUResult( res );

					ASSIMP_LOG_WARN( "skp: Complex face, no texture import/UV" );

					for( size_t i_triangle = 0; i_triangle < num_triangles; i_triangle++ )
					{
						auto& retFace = entity.faces.emplace_back( );

						retFace.front_material = front_material;
						retFace.back_material  = back_material;

						// Three points in each triangle
						for( size_t i = 0; i < 3; i++ )
						{
							auto& retVertex = retFace.vertices.emplace_back( );

							// Get vertex
							size_t index = indices[i_triangle * 3 + i];
							retVertex    = vertices[index];
						}
					}
				}
			}
		}

		ASSIMP_LOG_WARN( "skp: Edge import not supported yet" );

		ASSIMP_LOG_WARN( "skp: Curve import not supported yet" );

		return entity;
	}

	std::vector<Component> readComponentDefinitionsFromSU( const SUModelRef& rModel )
	{
		std::vector<Component> retComponents;

		SUResult res;
		size_t num_comp_defs = 0;
		res                  = SUModelGetNumComponentDefinitions( rModel, &num_comp_defs );
		checkSUResult( res );

		if( num_comp_defs > 0 )
		{
			std::vector<SUComponentDefinitionRef> comp_defs( num_comp_defs );
			res = SUModelGetComponentDefinitions( rModel, num_comp_defs, &comp_defs[0], &num_comp_defs );
			checkSUResult( res );

			for( auto&& comp_def: comp_defs )
			{
				auto& ret_comp = retComponents.emplace_back( );
				CSUString name;
				res = SUComponentDefinitionGetName( comp_def, name );
				checkSUResult( res );

				ret_comp.name = name.utf8( );

				SUEntitiesRef entities = SU_INVALID;
				SUComponentDefinitionGetEntities( comp_def, &entities );

				ret_comp.entity = readEntityFromSU( entities );
			}
		}

		return retComponents;
	}

	void createAssimpDataFromSkpData( aiScene* pScene, const std::vector<Material>& materials, const std::vector<Component>& componentDefinitions, const Entity& entity )
	{
		std::vector<aiMesh*> meshes;
		std::vector<aiNode*> nodes;

		std::set<std::string> usedComponents, usedMaterials;
		collectUsedComponentsAndMaterials( entity, usedComponents, usedMaterials );

		auto convertedComponentDefinitions = createComponentDefinitions( componentDefinitions, usedComponents, usedMaterials, meshes, pScene );

		nodes.push_back( createAssimpNodeFromEntity( entity, meshes, pScene, convertedComponentDefinitions, usedMaterials ) );

#if FALSE
		auto node = nodes.back( );

		for( auto&& mesh: meshes )
		{
			for( auto id_vert = 0; id_vert < mesh->mNumVertices; id_vert++ )
			{
				auto vert = mesh->mVertices[id_vert];
				std::cout << "Vert ID:" << id_vert << "\tPos: " << vert.x << ' ' << vert.y << ' ' << vert.z << '\n';
			}
			std::cout << '\n';

			for( auto id_face = 0; id_face < mesh->mNumFaces; id_face++ )
			{
				auto face = mesh->mFaces[id_face];
				std::cout << "Face ID:" << id_face << "\tVerts: ";
				for( auto idx = 0; idx < face.mNumIndices; idx++ )
				{
					std::cout << face.mIndices[idx] << ' ';
				}
				std::cout << '\n';
			}
		}
#endif

		pScene->mRootNode->addChildren( nodes.size( ), nodes.data( ) );

		// appendChildToParentNode( pScene->mRootNode, newNode );

		// Create mesh pointer buffer for this scene
		if( pScene->mNumMeshes > 0 )
		{
			pScene->mMeshes = new aiMesh*[meshes.size( )];
			for( size_t index = 0; index < meshes.size( ); ++index )
			{
				pScene->mMeshes[index] = meshes[index];
			}
		}

		createAssimpMaterials( pScene, materials, usedMaterials );
	}

	aiMesh* createAssimpMesh( const std::vector<const Face*>& faces, int materialIndex )
	{
		std::unique_ptr<aiMesh> pMesh( new aiMesh );

		for( auto&& face: faces )
		{
			pMesh->mNumFaces++;
			pMesh->mNumVertices += face->vertices.size( );

			if( face->vertices.size( ) == 1 )
			{
				pMesh->mPrimitiveTypes |= aiPrimitiveType_POINT;
			}
			else if( face->vertices.size( ) == 2 )
			{
				pMesh->mPrimitiveTypes |= aiPrimitiveType_LINE;
			}
			else if( face->vertices.size( ) > 3 )
			{
				pMesh->mPrimitiveTypes |= aiPrimitiveType_POLYGON;
			}
			else
			{
				pMesh->mPrimitiveTypes |= aiPrimitiveType_TRIANGLE;
			}
		}

		if( pMesh->mNumFaces > 0 )
		{
			pMesh->mFaces    = new aiFace[pMesh->mNumFaces];
			pMesh->mVertices = new aiVector3D[pMesh->mNumVertices];

			// Set material (here?)
			pMesh->mMaterialIndex = materialIndex;

			size_t faceCounter   = 0;
			size_t vertexCounter = 0;
			for( auto&& face: faces )
			{
				aiFace& f           = pMesh->mFaces[faceCounter++];
				f.mNumIndices       = face->vertices.size( );
				f.mIndices          = new unsigned int[f.mNumIndices];
				size_t indexCounter = 0;
				for( auto&& vertex: face->vertices )
				{
					aiVector3D& vert           = pMesh->mVertices[vertexCounter];
					vert.x                     = vertex.x;
					vert.y                     = vertex.y;
					vert.z                     = vertex.z;
					f.mIndices[indexCounter++] = vertexCounter++;
				}
			}
		}

		return pMesh.release( );
	}

	aiNode* createAssimpNodeFromEntity( const Entity& entity, std::vector<aiMesh*>& meshes, aiScene* pScene,
	                                    const std::unordered_map<std::string, std::unique_ptr<aiNode>>& componentDefinitions, const std::set<std::string>& usedMaterials )
	{
		auto pNode = std::make_unique<aiNode>( );

		// Add instances
		if( !entity.instances.empty( ) )
		{
			std::vector<aiNode*> instanceNodes;
			instanceNodes.reserve( entity.groups.size( ) ); // at least number of instances
			for( auto&& instance: entity.instances )
			{
				auto componentDefinitionForInstance = componentDefinitions.at( instance.name ).get( ); // Do not take ownership here!!

				instanceNodes.push_back( deepCopyNode( componentDefinitionForInstance ) );

				instanceNodes.back( )->mTransformation = convertSUTransformation( instance.transform );
			}
			pNode->addChildren( instanceNodes.size( ), instanceNodes.data( ) );
		}

		// Add group
		if( !entity.groups.empty( ) )
		{
			std::vector<aiNode*> groupNodes;
			groupNodes.reserve( entity.groups.size( ) );
			for( auto&& group: entity.groups )
			{
				groupNodes.push_back( createAssimpNodeFromEntity( group.entity, meshes, pScene, componentDefinitions, usedMaterials ) );

				groupNodes.back( )->mTransformation = convertSUTransformation( group.transform );
			}
			pNode->addChildren( groupNodes.size( ), groupNodes.data( ) );
		}

		auto prevMeshNumber = meshes.size( );
		// Add entity geometry
		if( !entity.faces.empty( ) )
		{
			std::unordered_map<std::string, std::vector<const Face*>> facesPerMaterial;

			for( auto&& face: entity.faces )
			{
				if( face.front_material != face.back_material )
				{
					facesPerMaterial[face.front_material].push_back( &face );

					if( face.back_material != DEFAULT_MATERIAL_NAME )
					{
						facesPerMaterial[face.back_material].push_back( &face );
					}
				}
				else
				{
					facesPerMaterial[face.front_material].push_back( &face );
				}
			}

			size_t counter = 0;
			for( auto& [key, val]: facesPerMaterial )
			{
				meshes.push_back( createAssimpMesh( val, std::distance( usedMaterials.begin( ), std::find( usedMaterials.begin( ), usedMaterials.end( ), key ) ) ) );
			}
		}

		// Add Mesh instances to scene and node
		const size_t meshSizeDiff = meshes.size( ) - prevMeshNumber;
		if( meshSizeDiff > 0 )
		{
			pNode->mMeshes    = new unsigned int[meshSizeDiff];
			pNode->mNumMeshes = static_cast<unsigned int>( meshSizeDiff );
			size_t index      = 0;
			for( size_t i = prevMeshNumber; i < meshes.size( ); ++i )
			{
				pNode->mMeshes[index] = pScene->mNumMeshes;
				pScene->mNumMeshes++;
				++index;
			}
		}

		return pNode.release( );
	}

	void appendChildToParentNode( aiNode* pParent, aiNode* pChild )
	{
		// Checking preconditions
		if( nullptr == pParent || nullptr == pChild )
		{
			throw std::runtime_error( "Node tree seems to be corrupted." );
		}

		// Assign parent to child
		pChild->mParent = pParent;

		// Copy node instances into parent node
		pParent->mNumChildren++;
		pParent->mChildren[pParent->mNumChildren - 1] = pChild;
	}

	aiMatrix4x4 convertSUTransformation( const SUTransformation& trans )
	{
		aiMatrix4x4 matrix;

		for( auto i = 0; i < 4; i++ )
		{
			for( auto j = 0; j < 4; j++ )
			{
				matrix[i][j] = trans.values[i + 4 * j];
			}
		}

		return matrix;
	}

	void collectUsedComponentsAndMaterials( const Entity& entity, std::set<std::string>& usedComponents, std::set<std::string>& usedMaterials )
	{
		for( auto&& instance: entity.instances )
		{
			usedComponents.insert( instance.name );
			usedMaterials.insert( instance.material_name );
		}

		for( auto&& group: entity.groups )
		{
			collectUsedComponentsAndMaterials( group.entity, usedComponents, usedMaterials );
		}

		for( auto&& face: entity.faces )
		{
			usedMaterials.insert( face.front_material );
			usedMaterials.insert( face.back_material );
		}
	}

	std::unordered_map<std::string, std::unique_ptr<aiNode>> createComponentDefinitions( const std::vector<Component>& componentDefinitions,
	                                                                                     const std::set<std::string>& usedComponents,
	                                                                                     const std::set<std::string>& usedMaterials, std::vector<aiMesh*>& meshes,
	                                                                                     aiScene* pScene )
	{
		std::unordered_map<std::string, std::unique_ptr<aiNode>> definitions;

		auto componentOrder = buildComponentDefinitionOrder( componentDefinitions, usedComponents );

		// compose ordered component def vector
		for( auto&& nextComponent: componentOrder )
		{
			auto nextComponentDefIterator = std::find_if( componentDefinitions.begin( ), componentDefinitions.end( ),
			                                              [&nextComponent]( const Component& comp ) { return comp.name == nextComponent; } );

			// This check is probably redundant but just in case
			if( nextComponentDefIterator != componentDefinitions.end( ) )
			{
				auto& definition = definitions[nextComponentDefIterator->name];
				definition       = std::unique_ptr<aiNode>( createAssimpNodeFromEntity( nextComponentDefIterator->entity, meshes, pScene, definitions, usedMaterials ) );
			}
		}

		return definitions;
	}

	std::list<std::string> buildComponentDefinitionOrder( const std::vector<Component>& componentDefinitions, const std::set<std::string>& usedComponents )
	{
		std::list<std::string> componentOrder;

		for( auto&& compDef: componentDefinitions )
		{
			if( usedComponents.find( compDef.name ) != usedComponents.end( ) )
			{
				// Depth first search
				if( std::find( componentOrder.begin( ), componentOrder.end( ), compDef.name ) == componentOrder.end( ) )
				{
					componentOrder.splice( componentOrder.end( ), componentDefinitionOrderHelper( compDef, componentDefinitions ) );
				}
			}
		}

		return componentOrder;
	}

	std::list<std::string> componentDefinitionOrderHelper( const Component& component, const std::vector<Component>& componentDefinitions )
	{
		std::list<std::string> componentOrder;

		for( auto&& instanceUse: component.entity.instances )
		{
			auto componentDefinitionForInstance = std::find_if( componentDefinitions.begin( ), componentDefinitions.end( ),
			                                                    [&instanceUse]( const Component& comp ) { return comp.name == instanceUse.name; } );

			if( componentDefinitionForInstance != componentDefinitions.end( ) )
			{
				if( std::find( componentOrder.begin( ), componentOrder.end( ), componentDefinitionForInstance->name ) == componentOrder.end( ) )
				{
					componentOrder.splice( componentOrder.end( ), componentDefinitionOrderHelper( *componentDefinitionForInstance, componentDefinitions ) );
				}
			}
		}

		componentOrder.push_back( component.name );

		return componentOrder;
	}

	aiNode* deepCopyNode( aiNode* source )
	{
		auto dest = std::make_unique<aiNode>( );

		// flat copy
		*dest.get( ) = *source;

		// copy mesh indices
		dest->mMeshes = new unsigned int[dest->mNumMeshes];
		memcpy( dest->mMeshes, source->mMeshes, sizeof( unsigned int ) * dest->mNumMeshes );

		// copy child nodes
		dest->mChildren = new aiNode*[dest->mNumChildren];
		for( unsigned int i = 0; i < dest->mNumChildren; i++ )
		{
			dest->mChildren[i] = deepCopyNode( source->mChildren[i] );
		}

		// need to set the mParent fields to the created aiNode.
		for( unsigned int i = 0; i < dest->mNumChildren; i++ )
		{
			dest->mChildren[i]->mParent = dest.get( );
		}

		return dest.release( );
	}

	void createAssimpMaterials( aiScene* pScene, const std::vector<Material>& materials, const std::set<std::string>& usedMaterials )
	{
		if( nullptr == pScene )
		{
			return;
		}

		pScene->mNumMaterials = usedMaterials.size( );

		pScene->mMaterials = new aiMaterial*[pScene->mNumMaterials];

		size_t matIndex = 0;
		for( auto&& usedMaterial: usedMaterials )
		{
			Material material;

			if( usedMaterial == DEFAULT_MATERIAL_NAME )
			{
				// create default material
				material.name        = DEFAULT_MATERIAL_NAME;
				material.color.red   = 255;
				material.color.green = 255;
				material.color.blue  = 255;
				material.color.alpha = 255;
				material.has_color   = true;
				// material.alpha       = 0;
				// material.has_alpha   = true;
			}
			else
			{
				auto materialIter =
				    std::find_if( materials.begin( ), materials.end( ), [usedMaterial]( const Material& material ) { return material.name == usedMaterial; } );

				if( materialIter == materials.end( ) )
				{
					continue;
				}

				material = *materialIter;
			}


			aiMaterial* mat = new aiMaterial;

			aiString s;
			s.Set( material.name );
			mat->AddProperty( &s, AI_MATKEY_NAME );

			if( material.has_color )
			{
				// convert from 0-255 to 0-1.0
				aiColor3D color( material.color.red / 255., material.color.green / 255., material.color.blue / 255. );

				mat->AddProperty<aiColor3D>( &color, 1, AI_MATKEY_COLOR_DIFFUSE );
			}

			if( material.has_alpha )
			{
				float alpha = material.alpha;
				mat->AddProperty<float>( &alpha, 1, AI_MATKEY_OPACITY );
			}

			// Store material property info in material array in scene
			pScene->mMaterials[matIndex] = mat;
			matIndex++;
		}
	}
} // namespace AssimpSkpHelper
