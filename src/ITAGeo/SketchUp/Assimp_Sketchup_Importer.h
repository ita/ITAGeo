#ifndef INCLUDE_WATCHER_ITA_GEO_ASSIMP_SKP_IMPORTER
#define INCLUDE_WATCHER_ITA_GEO_ASSIMP_SKP_IMPORTER

#include "Assimp_Sketchup_Helper.h"

#include <SketchUpAPI/SketchUp.h>
#include <assimp/BaseImporter.h>
#include <assimp/mesh.h>
#include <mutex>
#include <unordered_map>

///
/// \brief Assimp SketchUp importer.
///
/// This implementation is partly based on the "skp_to_xml" example supplied with the 2022 SketchUp API and the OBJ and AC3D load from assimp.
///
/// The current implementation omits any node or mesh names.
/// For our purposes, the most important part is that the material names are loaded correctly and are assigned to the correct mesh, which this implementation fulfils.
///
class CSketchUpImporter : public Assimp::BaseImporter
{
public:
	///
	/// \brief Construct a new CSketchUpImporter object.
	///
	CSketchUpImporter( );

	///
	/// \brief Destroy the CSketchUpImporter object.
	///
	~CSketchUpImporter( ) override;

	///
	/// \brief Returns whether the class can handle the format of the given file.
	/// The implementation is expected to perform a full check of the file structure, possibly searching
	/// the first bytes of the file for magic identifiers or keywords.
	/// \param filename Path and file name of the file to be examined.
	/// \param pIOHandler The IO handler to use for accessing any file.
	/// \param checkSig Legacy; do not use.
	/// \return true if the class can read this file, false if not or if unsure.
	///
	bool CanRead( const std::string &filename, Assimp::IOSystem *pIOHandler, bool checkSig ) const override;

protected:
	///
	/// \brief Called by #Importer::GetImporterInfo to get a description of some loader features.
	///
	/// Importers must provide this information
	/// \return description of the importer.
	///
	const aiImporterDesc *GetInfo( ) const override;

	///
	/// \brief Imports the given file into the given scene structure.
	///
	/// \throws ImportErrorException if there is an error.
	/// \param pFile Path of the file to be imported.
	/// \param pScene The scene object to hold the imported data. \p nullptr is not a valid parameter.
	/// \param pIOHandler The IO handler to use for any file access. \p nullptr is not a valid parameter.
	///
	void InternReadFile( const std::string &pFile, aiScene *pScene, Assimp::IOSystem *pIOHandler ) override;

private:
	inline static std::mutex m_mSUAccessMutex;
};

#endif