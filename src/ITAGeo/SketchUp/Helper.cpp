#include <ITAGeo/SketchUp/Conversions.h>
#include <ITAGeo/SketchUp/Helper.h>

// ITABase includes
#include <ITAException.h>

// Constants
static const double g_dInchesToMeterScale = 39.3700787f;
static const double g_dFeetToMeterScale   = 3.28084f;

std::string SUStringToStdString( SUStringRef sSUString )
{
	size_t nStringLength = SU_INVALID;
	SU_EXC( SUStringGetUTF8Length( sSUString, &nStringLength ) );
	char* cStringUTF8 = new char[nStringLength + 1];
	SU_EXC( SUStringGetUTF8( sSUString, nStringLength + 1, cStringUTF8, &nStringLength ) );
	std::string sStdString( cStringUTF8 );
	delete[] cStringUTF8;
	return sStdString;
}

void SU2ITAException( SUResult iSUErrorCode, std::string sSUFunctionName )
{
	if( iSUErrorCode == SU_ERROR_NONE )
		return;

	std::stringstream ss;
	if( sSUFunctionName.empty( ) == false )
		ss << sSUFunctionName << ": ";

	switch( iSUErrorCode )
	{
		case SU_ERROR_GENERIC:
			ss << "SU_ERROR_GENERIC";
			break;
		case SU_ERROR_INSUFFICIENT_SIZE:
			ss << "SU_ERROR_INSUFFICIENT_SIZE";
			break;
		case SU_ERROR_NO_DATA:
			ss << "SU_ERROR_NO_DATA";
			break;
		case SU_ERROR_INVALID_INPUT:
			ss << "SU_ERROR_INVALID_INPUT";
			break;
		case SU_ERROR_NULL_POINTER_INPUT:
			ss << "SU_ERROR_NULL_POINTER_INPUT";
			break;
		case SU_ERROR_NULL_POINTER_OUTPUT:
			ss << "SU_ERROR_NULL_POINTER_OUTPUT";
			break;
		case SU_ERROR_INVALID_OUTPUT:
			ss << "SU_ERROR_INVALID_OUTPUT";
			break;
		case SU_ERROR_OVERWRITE_VALID:
			ss << "SU_ERROR_OVERWRITE_VALID";
			break;
		case SU_ERROR_SERIALIZATION:
			ss << "SU_ERROR_SERIALIZATION";
			break;
		case SU_ERROR_OUT_OF_RANGE:
			ss << "SU_ERROR_OUT_OF_RANGE";
			break;
		case SU_ERROR_UNKNOWN_EXCEPTION:
			ss << "SU_ERROR_UNKNOWN_EXCEPTION";
			break;
		case SU_ERROR_MODEL_INVALID:
			ss << "SU_ERROR_MODEL_INVALID";
			break;
		case SU_ERROR_MODEL_VERSION:
			ss << "SU_ERROR_MODEL_VERSION";
			break;
	}

	throw ITAException( ITAException::INVALID_PARAMETER, "SLAPI", ss.str( ) );
}
