#include <ITAGeo/SketchUp/Helper.h>
#include <ITAGeo/SketchUp/Materials.h>

// ITAGeo includes
#include <ITAGeo/Material/Material.h>
#include <ITAGeo/Material/MaterialManager.h>

// ITABase includes
#include <ITAException.h>

// OpenMesh includes
#include <OpenMesh/Core/Utils/PropertyManager.hh>


// Typedefs
typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;


bool ITAGeo::SketchUp::SUColorToOneDimAcousticMaterial( std::shared_ptr<ITAGeo::Material::CScalarMaterial> pMaterial, const SUColor& tColor )
{
	assert( pMaterial );
	pMaterial->cdTransmissionFactor = tColor.alpha / 255.0f; // T, complex
	pMaterial->cdReflectionFactor   = std::complex<double>( ( tColor.red + tColor.green + tColor.blue ) / ( 3 * 255.0f ), 0 ) * pMaterial->cdTransmissionFactor;

	return true;
}

bool ITAGeo::SketchUp::AcousticMaterialToSUMaterial( const CITAMesh* pMesh, CITAMesh::FaceHandle hFace, SUMaterialRef* pSUFaceMaterial,
                                                     std::shared_ptr<ITAGeo::Material::CMaterialManager> pManager )
{
	assert( pMesh );
	CITAMesh* pLocalMesh = const_cast<CITAMesh*>( pMesh );
	auto mMaterialPropertyManager =
	    OpenMesh::getOrMakeProperty<OpenMesh::FaceHandle, ITAGeo::Material::CAcousticFaceProperty>( *pLocalMesh, "Acoustics::Material::Face" );
	auto pMaterial = mMaterialPropertyManager[hFace].GetMaterial( );

	if( pMaterial == nullptr )
		return false;

	SUColor oSUColor;

	int iMaterialType = pMaterial->GetType( );
	switch( iMaterialType )
	{
		case Material::IMaterial::VISUALIZATION:
		{
			std::shared_ptr<Material::CVisualizationMaterial> pVisMaterial = std::dynamic_pointer_cast<Material::CVisualizationMaterial>( pMaterial );
			assert( pVisMaterial );
			oSUColor.red   = (unsigned char)( pVisMaterial->dRed * 255.0f );
			oSUColor.green = (unsigned char)( pVisMaterial->dGreen * 255.0f );
			oSUColor.blue  = (unsigned char)( pVisMaterial->dBlue * 255.0f );
			oSUColor.alpha = (unsigned char)( pVisMaterial->dAlpha * 255.0f );
			break;
		}
		case Material::IMaterial::NONE:
		case Material::IMaterial::SCALAR:
		case Material::IMaterial::THIRD_OCTAVE:
		case Material::IMaterial::WHOLE_OCTAVE:
		case Material::IMaterial::COMPLEX_SPECTRUM:
		case Material::IMaterial::MAGNITUDE_SPECTRUM:
		default:
		{
			pSUFaceMaterial = NULL;
			return false;
		}
	}

	SU_EXC( SUMaterialCreate( pSUFaceMaterial ) );

	SU_EXC( SUMaterialSetColor( *pSUFaceMaterial, &oSUColor ) );

	return true;
}

SUResult ITAGeo::SketchUp::SUMaterialToAcousticMaterial( CITAMesh* pMesh, CITAMesh::FaceHandle hFace, SUFaceRef rFace,
                                                         std::shared_ptr<ITAGeo::Material::CMaterialManager> pManager )
{
	// Without a manager, the material assignment is skipped.
	if( !pManager )
		return SU_ERROR_NONE;

	SUResult sur;

	SUMaterialRef rMaterial = SU_INVALID;
	if( ( sur = SUFaceGetFrontMaterial( rFace, &rMaterial ) ) != SU_ERROR_NONE )
		return sur;

	auto mMaterialPropertyManager = OpenMesh::getOrMakeProperty<OpenMesh::FaceHandle, ITAGeo::Material::CAcousticFaceProperty>( *pMesh, "Acoustics::Material::Face" );

	SUMaterialType tMaterial;
	if( SUMaterialGetType( rMaterial, &tMaterial ) == SU_ERROR_NONE )
	{
		std::string sMaterialName;
		SUStringRef rMaterialName = SU_INVALID;
		SUStringCreate( &rMaterialName );
		if( SUMaterialGetName( rMaterial, &rMaterialName ) == SU_ERROR_NONE )
			sMaterialName = SUStringToStdString( rMaterialName );

		mMaterialPropertyManager[hFace].sName = sMaterialName;

		// If the manager has an acoustic material with the name of the
		// face material, it will be assigned, otherwise created and assigned if possible

		if( pManager->HasMaterial( sMaterialName ) )
		{
			auto pMat = pManager->GetMaterial( sMaterialName );
			mMaterialPropertyManager[hFace].SetMaterial( pMat );
		}
		else
		{
			SUColor tColor;

			std::shared_ptr<Material::CScalarMaterial> pNewMat = nullptr;

			switch( tMaterial )
			{
				case( SUMaterialType_ColorizedTexture ): // Only entered if material by name not found
				case( SUMaterialType_Colored ):
				{
					sur = SUMaterialGetColor( rMaterial, &tColor );
					if( sur != SU_ERROR_NONE )
						return sur;

					// Create Material
					pNewMat = std::make_shared<Material::CScalarMaterial>( );
					SUColorToOneDimAcousticMaterial( pNewMat, tColor );

					break;
				}
			}

			if( pNewMat && pManager->AddMaterial( sMaterialName, pNewMat ) )
				mMaterialPropertyManager[hFace].SetMaterial( pManager->GetMaterial( sMaterialName ) );
		}
	}

	return SU_ERROR_NONE;
}
