#ifndef INCLUDE_WATCHER_ITA_GEO_ASSIMP_SKP_HELPER
#define INCLUDE_WATCHER_ITA_GEO_ASSIMP_SKP_HELPER

#include <SketchUpAPI/SketchUp.h>
#include <assimp/scene.h>
#include <set>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

namespace AssimpSkpHelper
{
	// A simple SUStringRef wrapper class which makes usage simpler from C++.
	// Taken from SketchIpAPI example "skp_to_xml"
	class CSUString
	{
	public:
		CSUString( )
		{
			SUSetInvalid( su_str_ );
			SUStringCreate( &su_str_ );
		}

		~CSUString( ) { SUStringRelease( &su_str_ ); }

		operator SUStringRef*( ) { return &su_str_; }

		std::string utf8( )
		{
			size_t length;
			SUStringGetUTF8Length( su_str_, &length );
			std::string string;
			string.resize( length + 1 );
			size_t returned_length;
			SUStringGetUTF8( su_str_, length, &string[0], &returned_length );
			return string;
		}

	private:
		// Disallow copying for simplicity
		CSUString( const CSUString& copy );
		CSUString& operator=( const CSUString& copy );

		SUStringRef su_str_;
	};

	struct Instance;
	struct Group;
	struct Face;

	struct Entity
	{
		std::string name;
		std::vector<Instance> instances;
		std::vector<Group> groups;
		std::vector<Face> faces;
	};

	struct Material
	{
		std::string name;
		bool has_color;
		SUColor color;
		bool has_alpha;
		double alpha;
		bool has_texture;
	};

	struct Instance
	{
		std::string name;
		std::string material_name;
		SUTransformation transform;
	};

	struct Group
	{
		Entity entity;
		SUTransformation transform;
	};

	struct Face
	{
		std::string front_material;
		std::string back_material;
		std::vector<SUPoint3D> vertices;
	};

	struct Component
	{
		std::string name;
		Entity entity;
	};

	void checkSUResult( SUResult err );

	std::string readModelNameFromSU( const SUModelRef& rModel );

	std::vector<Material> readAllMaterialsFromSU( const SUModelRef& rModel );

	Material readMaterialFromSU( const SUMaterialRef& rMaterial );

	std::string readMaterialNameFromSU( const SUMaterialRef& material );

	Entity readEntityFromSU( const SUEntitiesRef& rEntities );

	std::vector<Component> readComponentDefinitionsFromSU( const SUModelRef& rModel );

	void createAssimpDataFromSkpData( aiScene* pScene, const std::vector<Material>& materials, const std::vector<Component>& componentDefinitions, const Entity& entity );

	aiMesh* createAssimpMesh( const std::vector<const Face*>& faces, int materialIndex );

	aiNode* createAssimpNodeFromEntity( const Entity& entity, std::vector<aiMesh*>& meshes, aiScene* pScene,
	                                    const std::unordered_map<std::string, std::unique_ptr<aiNode>>& componentDefinitions,
	                                    const std::set<std::string>& usedMaterials );

	void appendChildToParentNode( aiNode* pParent, aiNode* pChild );

	aiMatrix4x4 convertSUTransformation( const SUTransformation& trans );

	void collectUsedComponentsAndMaterials( const Entity& entity, std::set<std::string>& usedComponents, std::set<std::string>& usedMaterials );

	std::unordered_map<std::string, std::unique_ptr<aiNode>> createComponentDefinitions( const std::vector<Component>& componentDefinitions,
	                                                                                     const std::set<std::string>& usedComponents,
	                                                                                     const std::set<std::string>& usedMaterials, std::vector<aiMesh*>& meshes,
	                                                                                     aiScene* pScene );

	std::list<std::string> buildComponentDefinitionOrder( const std::vector<Component>& componentDefinitions, const std::set<std::string>& usedComponents );

	std::list<std::string> componentDefinitionOrderHelper( const Component& component, const std::vector<Component>& componentDefinitions );

	aiNode* deepCopyNode( aiNode* source );

	void createAssimpMaterials( aiScene* pScene, const std::vector<Material>& materials, const std::set<std::string>& usedMaterials );
} // namespace AssimpSkpHelper

#endif