#include <ITAGeo/SketchUp/Conversions.h>
#include <ITAGeo/SketchUp/Helper.h>
#include <ITAGeo/SketchUp/Materials.h>
#include <ITAGeo/SketchUp/MeshConversions.h>

// ITABase includes
#include <ITAException.h>

// Sketchup includes
#include <SketchUpAPI/geometry.h>
#include <SketchUpAPI/initialize.h>
#include <SketchUpAPI/model/component_definition.h>
#include <SketchUpAPI/model/component_instance.h>
#include <SketchUpAPI/model/drawing_element.h>
#include <SketchUpAPI/model/edge.h>
#include <SketchUpAPI/model/face.h>
#include <SketchUpAPI/model/group.h>
#include <SketchUpAPI/model/material.h>
#include <SketchUpAPI/model/model.h>
#include <SketchUpAPI/model/texture.h>
#include <SketchUpAPI/model/vertex.h>
#include <SketchUpAPI/transformation.h>

SUResult ITAGeo::SketchUp::SUEntitiesToITAMesh( SUEntitiesRef rEntities, CITAMesh* pMesh, bool bExplodeGroups, std::vector<SUTransformation>& vtTransform,
                                                std::shared_ptr<ITAGeo::Material::CMaterialManager> pMaterialManager /* = nullptr */ )
{
	bool bTopLevelMesh = vtTransform.empty( );

	// Convert top-level entities
	size_t nNumFaces = 0;
	SU_RET( SUEntitiesGetNumFaces( rEntities, &nNumFaces ) );

	if( nNumFaces > 0 )
	{
		std::vector<SUFaceRef> vrFaces( nNumFaces );
		size_t nActualNumFaces = 0;
		SU_RET( SUEntitiesGetFaces( rEntities, nNumFaces, &vrFaces[0], &nActualNumFaces ) );
		assert( nActualNumFaces == nNumFaces );

		// Iterate over faces and translate into CITAMesh representation
		SUPoint3D vSUPoint3D;
		std::map<const void*, CITAMesh::VertexHandle> mTopLevelVertices;
		for( size_t n = 0; n < nNumFaces; n++ )
		{
			const SUFaceRef& rFace( vrFaces[n] );

			size_t nNumEdges = 0;
			SU_RET( SUFaceGetNumEdges( rFace, &nNumEdges ) );

			std::vector<SUEdgeRef> vrEdges( nNumEdges );
			size_t nActualEdgeCount = 0;
			if( nNumEdges )
				SU_RET( SUFaceGetEdges( rFace, nNumEdges, &vrEdges[0], &nActualEdgeCount ) );

			size_t nNumVertices = 0, nActualNumVertices = 0;
			SU_RET( SUFaceGetNumVertices( rFace, &nNumVertices ) );

			std::vector<SUVertexRef> vrVertices( nNumVertices );
			SU_RET( SUFaceGetVertices( rFace, nNumVertices, &vrVertices[0], &nActualNumVertices ) );

			std::vector<CITAMesh::VertexHandle> vhVertices( nNumVertices );
			for( size_t m = 0; m < nNumVertices; m++ )
			{
				const SUVertexRef& rVertex( vrVertices[m] ); // SU vertex
				CITAMesh::VertexHandle hVertex;              // OpenMesh vertex

				std::map<const void*, CITAMesh::VertexHandle>::key_type key( rVertex.ptr );
				std::map<const void*, CITAMesh::VertexHandle>::iterator it = mTopLevelVertices.find( key );
				if( it == mTopLevelVertices.end( ) )
				{
					// Add new vertex
					SU_RET( SUVertexGetPosition( rVertex, &vSUPoint3D ) );
					CITAMesh::Point vPoint3D;
					if( bTopLevelMesh == false )
						vPoint3D = SUPoint3DToITAMeshPointTransformed( vSUPoint3D, vtTransform );
					else
						vPoint3D = SUPoint3DToITAMeshPoint( vSUPoint3D );
					hVertex = pMesh->add_vertex( vPoint3D );
					mTopLevelVertices.insert( std::pair<const void*, CITAMesh::VertexHandle>( rVertex.ptr, hVertex ) );
				}
				else
				{
					// Connect already registered vertex
					hVertex = it->second;
				}
				vhVertices[m] = hVertex;
			}

			CITAMesh::FaceHandle hFace = pMesh->add_face( vhVertices ); // --> OPENMESH
			if( pMesh->is_valid_handle( hFace ) == false )
			{
				std::cerr << "Found invalid face with " << nNumVertices << " vertices. Skipping." << std::endl;
				continue;
			}

			if( ITAGeo::SketchUp::SUMaterialToAcousticMaterial( pMesh, hFace, rFace, pMaterialManager ) == SU_ERROR_NO_DATA )
			{
				/* jst: todo: und nun? Material m�sste schon auf face sein
				CITAOneDimAcousticMaterial* pMaterial = new CITAOneDimAcousticMaterial;
				pMaterial->SetDefaults();
				pMesh->property( tAcousticFaceProp, hFace ).SetMaterial( pMaterial );
				*/
			}
		}
	}

	// If groups and components shall be preserved, stop here.
	if( !bExplodeGroups )
		return SU_ERROR_NONE;

	// Explode groups
	size_t nNumGroups;
	SU_RET( SUEntitiesGetNumGroups( rEntities, &nNumGroups ) );

	if( nNumGroups > 0 )
	{
		size_t nActualGroups;
		std::vector<SUGroupRef> vrGroups( nNumGroups );
		SU_RET( SUEntitiesGetGroups( rEntities, nNumGroups, &vrGroups[0], &nActualGroups ) );

		for( size_t n = 0; n < nNumGroups; n++ )
		{
			const SUGroupRef& rGroup( vrGroups[n] );
			SUEntitiesRef rGroupEntities;
			SU_RET( SUGroupGetEntities( rGroup, &rGroupEntities ) );

			SUTransformation tTransform;
			SU_RET( SUGroupGetTransform( rGroup, &tTransform ) );


			// Recursion.
			vtTransform.push_back( tTransform );
			SU_RET( SUEntitiesToITAMesh( rGroupEntities, pMesh, true, vtTransform, pMaterialManager ) );
			vtTransform.pop_back( );
		}
	}

	// Convert components
	size_t nNumInstances = 0;
	SU_RET( SUEntitiesGetNumInstances( rEntities, &nNumInstances ) );

	if( nNumInstances > 0 )
	{
		size_t nActualInstances;
		std::vector<SUComponentInstanceRef> vrInstances( nNumInstances );
		SU_RET( SUEntitiesGetInstances( rEntities, nNumInstances, &vrInstances[0], &nActualInstances ) );

		for( size_t n = 0; n < nNumInstances; n++ )
		{
			const SUComponentInstanceRef& rInstance( vrInstances[n] );
			SUTransformation tTransform;
			SU_RET( SUComponentInstanceGetTransform( rInstance, &tTransform ) );
			std::vector<SUTransformation> vtExtendedTransform = vtTransform;
			vtExtendedTransform.push_back( tTransform );

			SUComponentDefinitionRef rDefinition = SU_INVALID;
			SU_RET( SUComponentInstanceGetDefinition( rInstance, &rDefinition ) );

			SUEntitiesRef rCompDefEntities;
			SU_RET( SUComponentDefinitionGetEntities( rDefinition, &rCompDefEntities ) );

			// Recursion.
			SU_RET( SUEntitiesToITAMesh( rCompDefEntities, pMesh, true, vtExtendedTransform, pMaterialManager ) );
		}
	}

	return SU_ERROR_NONE;
}

SUResult ITAGeo::SketchUp::SUEntitiesToITAMesh( SUEntitiesRef rEntities, CITAMesh* pMesh, bool bExplodeGroups,
                                                std::shared_ptr<ITAGeo::Material::CMaterialManager> pMaterialManager /* = nullptr */ )
{
	std::vector<SUTransformation> vtDummy;
	return ITAGeo::SketchUp::SUEntitiesToITAMesh( rEntities, pMesh, bExplodeGroups, vtDummy, pMaterialManager );
}

SUResult ITAGeo::SketchUp::SUComponentInstanceToITAMesh( SUComponentInstanceRef rCompInstance, CITAMesh* pMesh,
                                                         std::shared_ptr<ITAGeo::Material::CMaterialManager> pMaterialManager /* = nullptr */ )
{
	SUComponentDefinitionRef rComponentDef;
	SU_RET( SUComponentInstanceGetDefinition( rCompInstance, &rComponentDef ) );

	SUEntitiesRef rCompDefEntities;
	SU_RET( SUComponentDefinitionGetEntities( rComponentDef, &rCompDefEntities ) );

	return ITAGeo::SketchUp::SUEntitiesToITAMesh( rCompDefEntities, pMesh, true, pMaterialManager );
}

SUResult ITAGeo::SketchUp::ITAMeshToSUEntities( const CITAMesh* pMesh, SUEntitiesRef* rEntities, SULayerRef* pSULayer /* = NULL */ )
{
	SUGeometryInputRef rGeoInput = SU_INVALID;
	SU_RET( SUGeometryInputCreate( &rGeoInput ) );

	if( pMesh->n_vertices( ) == 0 )
		return SU_ERROR_NONE;

	std::vector<SUPoint3D> vSUPoints;
	CITAMesh::VertexIter v_it = pMesh->vertices_begin( );
	while( v_it != pMesh->vertices_end( ) )
	{
		CITAMesh::VertexHandle hVertex( *v_it++ );
		SUPoint3D rSUPoint;
		CITAMesh::Point pPoint = pMesh->point( hVertex );
		rSUPoint               = ITAMeshPointToSUPoint3D( pPoint );
		vSUPoints.push_back( rSUPoint );
	}

	SU_RET( SUGeometryInputSetVertices( rGeoInput, vSUPoints.size( ), &( vSUPoints[0] ) ) );

	int n1                  = 0;
	CITAMesh::FaceIter f_it = pMesh->faces_begin( );
	while( f_it != pMesh->faces_end( ) )
	{
		CITAMesh::FaceHandle hFace( *f_it++ );

		SULoopInputRef rLoop = SU_INVALID;
		SU_RET( SULoopInputCreate( &rLoop ) );

		std::vector<OpenMesh::VertexHandle> vhVertices;
		CITAMesh::ConstFaceVertexIter fv_it = pMesh->cfv_begin( hFace );
		while( fv_it != pMesh->cfv_end( hFace ) )
		{
			CITAMesh::VertexHandle hVertex( *fv_it++ );
			size_t iVertexIndex = hVertex.idx( );
			SUResult sur;
			if( ( sur = SULoopInputAddVertexIndex( rLoop, iVertexIndex ) ) == SU_ERROR_UNSUPPORTED )
			{
				std::cout << "skipping vertex " << iVertexIndex << std::endl;
				break;
			}
		}

		size_t iFaceIndex;
		SU_RET( SUGeometryInputAddFace( rGeoInput, &rLoop, &iFaceIndex ) );

		// No idea, but this currently only works in debug mode. Problems with acoustic face property in OpenMesh.
#ifdef _DEBUG
		// Material
		SUMaterialRef rFaceMaterial = SU_INVALID;
		if( ITAGeo::SketchUp::AcousticMaterialToSUMaterial( pMesh, hFace, &rFaceMaterial, nullptr ) )
		{
			SUMaterialInput oMatInput;
			oMatInput.num_uv_coords = 0;
			oMatInput.material      = rFaceMaterial;
			SU_RET( SUGeometryInputFaceSetFrontMaterial( rGeoInput, iFaceIndex, &oMatInput ) );
			SU_RET( SUGeometryInputFaceSetBackMaterial( rGeoInput, iFaceIndex, &oMatInput ) );
		}
#endif
		// Layer
		if( pSULayer )
		{
			SU_RET( SUGeometryInputFaceSetLayer( rGeoInput, iFaceIndex, *pSULayer ) );
		}
	}

	SU_RET( SUEntitiesFill( *rEntities, rGeoInput, true ) );

	SU_RET( SUGeometryInputRelease( &rGeoInput ) );

	return SU_ERROR_NONE;
}
