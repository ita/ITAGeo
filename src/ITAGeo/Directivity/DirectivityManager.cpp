#include <ITAException.h>
#include <ITAGeo/Directivity/DAFF_ImpulseResponse.h>
#include <ITAGeo/Directivity/DAFF_MagnitudeSpectrum.h>
#include <ITAGeo/Directivity/DirectivityManager.h>

// ITABase
#include <ITAConfigUtils.h>
#include <ITAConstants.h>
#include <ITAStringUtils.h>

// Vista
#include <VistaTools/VistaFileSystemDirectory.h>
#include <VistaTools/VistaFileSystemFile.h>

// STL
#include <filesystem>
#include <variant>


ITAGeo::Directivity::CDirectivityManager::CDirectivityManager( ) {}

ITAGeo::Directivity::CDirectivityManager::CDirectivityManager( const std::string& sFolderPath, const bool bRecursive /* = false */ )
{
	AddDirectivitiesFromFolder( sFolderPath, bRecursive );
}

ITAGeo::Directivity::CDirectivityManager::~CDirectivityManager( ) {}


void ITAGeo::Directivity::CDirectivityManager::AddDirectivitiesFromFolder( const std::string& sFolderPath, const bool bRecursive /* = false */ )
{
	std::filesystem::path fsPath( sFolderPath );

	if( !std::filesystem::exists( fsPath ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Directivity database could not be loaded. Path not found!" );

	if( bRecursive )
	{
		for( const auto& fsEntry: std::filesystem::recursive_directory_iterator( fsPath ) )
		{
			addPathEntry( fsEntry );
		}
	}
	else
	{
		for( const auto& fsEntry: std::filesystem::directory_iterator( fsPath ) )
		{
			addPathEntry( fsEntry );
		}
	}
}

bool ITAGeo::Directivity::CDirectivityManager::AddDirectivity( const std::string& sID, std::shared_ptr<IDirectivity> pDirectivity )
{
	if( HasDirectivity( sID ) )
		return false;
	m_mDirectivities[sID] = pDirectivity;
	return true;
}

bool ITAGeo::Directivity::CDirectivityManager::HasDirectivity( const std::string& sID ) const
{
	return ( GetDirectivity( sID ) != nullptr );
}


std::shared_ptr<ITAGeo::Directivity::IDirectivity> ITAGeo::Directivity::CDirectivityManager::GetDirectivity( const std::string& sID ) const
{
	if( m_mDirectivities.count( sID ) == 0 )
		return nullptr;
	else
		return m_mDirectivities.at( sID );
}

std::string ITAGeo::Directivity::CDirectivityManager::GetDirectivityID( std::shared_ptr<ITAGeo::Directivity::IDirectivity> pDir ) const
{
	for( auto it: m_mDirectivities )
		if( it.second.get( ) == pDir.get( ) )
			return it.first;

	return "";
}


int ITAGeo::Directivity::CDirectivityManager::GetNumDirectivities( ) const
{
	return m_mDirectivities.size( );
}


/// Private functions
inline void ITAGeo::Directivity::CDirectivityManager::addPathEntry( const std::filesystem::directory_entry& fsEntry )
{
	// skip if not a file or the file ending is not .daff
	if( !fsEntry.is_regular_file( ) || ( fsEntry.path( ).extension( ).string( ) != ".daff" ) )
		return;

	// For us, DAFF is either in ImpulseResponse format or MagnitudeSpectrum. However, I have to know which of the two to create the correct class but
	// (to my knowlege) it is not possible to know which format it is before loading the file into ram. Therefor we do this ugly try catch load.
	// Just try to load Impulse, if that throws try to load magnitude, if that throws we cannot load the file because it is either a different,
	// not supported type or some other problem. Either way we throw an exception.

	std::shared_ptr<ITAGeo::Directivity::CDAFF_ImpulseResponse> pIRDirectivity( new ITAGeo::Directivity::CDAFF_ImpulseResponse );
	std::shared_ptr<ITAGeo::Directivity::CDAFF_MagnitudeSpectrum> pMSDirectivity( new ITAGeo::Directivity::CDAFF_MagnitudeSpectrum );
	std::string sName = fsEntry.path( ).stem( ).string( );

	try
	{
		pIRDirectivity->LoadFromFile( fsEntry.path( ).string( ) );
	}
	catch( const ITAException& eIR )
	{
		try
		{
			pMSDirectivity->LoadFromFile( fsEntry.path( ).string( ) );
		}
		catch( const ITAException& eMS )
		{
			std::cout << "Could not load: " << fsEntry.path( ).filename( ).string( ) << ". Only Impulse Response and Magnitude Spectrum are supported." << std::endl
			          << "The following error was thrown for Impulse Response load: " << std::endl
			          << "\t - " << eIR.sReason << std::endl
			          << "The following error was thrown for Magnitude Specturm load: " << std::endl
			          << "\t - " << eMS.sReason << std::endl;

			ITA_EXCEPT_INVALID_PARAMETER( "Found file '" + fsEntry.path( ).string( ) + "' but could not load file." );
		}

		// if we are here the MS load was successful
		// skip if already exists, just to be sure
		if( m_mDirectivities.count( sName ) == 0 )
			m_mDirectivities[sName] = pMSDirectivity;
		return;
	}
	// if we are here the IR load was successful
	// skip if already exists, just to be sure
	if( m_mDirectivities.count( sName ) == 0 )
		m_mDirectivities[sName] = pIRDirectivity;
	return;
}
