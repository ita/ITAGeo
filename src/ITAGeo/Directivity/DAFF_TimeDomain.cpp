#include <DAFF.h>
#include <ITAException.h>
#include <ITAGeo/Directivity/DAFF_TimeDomain.h>

ITAGeo::Directivity::CDAFF_ImpulseResponse::CDAFF_ImpulseResponse( ) : CDAFF_Format( )
{
	SetDomain( ITADomain::ITA_TIME_DOMAIN );
}

ITAGeo::Directivity::CDAFF_ImpulseResponse::CDAFF_ImpulseResponse( const std::string& sFilePath ) : CDAFF_ImpulseResponse( )
{
	LoadFromFile( sFilePath );
}

float ITAGeo::Directivity::CDAFF_ImpulseResponse::GetFilterLengthSamples( ) const
{
	auto p = dynamic_cast<DAFFContentIR*>( m_pDAFFReader->getContent( ) );
	if( p )
		return (float)p->getFilterLength( );
	else
		return 0.0f;
}

ITAGeo::Directivity::CDAFF_ImpulseResponse::~CDAFF_ImpulseResponse( ) {}
