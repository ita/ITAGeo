#include <DAFF.h>
#include <ITAException.h>
#include <ITAGeo/Coordinates.h>
#include <ITAGeo/Directivity/DAFF_MagnitudeSpectrum.h>

ITAGeo::Directivity::CDAFF_MagnitudeSpectrum::CDAFF_MagnitudeSpectrum( ) : CDAFF_Format( )
{
	SetDomain( ITADomain::ITA_FREQUENCY_DOMAIN );
}

ITAGeo::Directivity::CDAFF_MagnitudeSpectrum::CDAFF_MagnitudeSpectrum( const std::string& sFilePath ) : CDAFF_MagnitudeSpectrum( )
{
	LoadFromFile( sFilePath );
}

ITAGeo::Directivity::CDAFF_MagnitudeSpectrum::~CDAFF_MagnitudeSpectrum( ) {}

int ITAGeo::Directivity::CDAFF_MagnitudeSpectrum::GetNumCenterFrequencies( ) const
{
	auto p = dynamic_cast<DAFFContentMS*>( m_pDAFFReader->getContent( ) );
	if( p )
		return p->getNumFrequencies( );
	else
		return 0;
}

void ITAGeo::Directivity::CDAFF_MagnitudeSpectrum::GetNearestNeighbourMagnitudeSpectrum( const ITAGeo::Coordinates::CSpherical& oDirection,
                                                                                         ITABase::CThirdOctaveMagnitudeSpectrum& oSpectrum,
                                                                                         const int iChannelIndex ) const
{
	auto p = dynamic_cast<DAFFContentMS*>( m_pDAFFReader->getContent( ) );

	if( p->getNumFrequencies( ) != oSpectrum.GetNumBands( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "DAFF magnitude spectrum and target third octave magnitude spectrum have mismatching number of bands / frequencies" );

	int iRecordIndex = -1;
	p->getNearestNeighbour( DAFF_VIEWS::DAFF_OBJECT_VIEW, oDirection.GetAzimuthDegree( ), oDirection.GetElevationDegree( ), iRecordIndex );
	p->getMagnitudes( iRecordIndex, iChannelIndex, &oSpectrum[0] );
}
