#include <DAFF.h>
#include <ITAException.h>
#include <ITAGeo/Directivity/DAFF_Format.h>

// STL
#include <filesystem>

ITAGeo::Directivity::CDAFF_Format::CDAFF_Format( ) : IDirectivity( )
{
	SetFormat( IDirectivity::OPENDAFF );
	SetDomain( ITADomain::ITA_UNKNOWN_DOMAIN );
	m_pDAFFReader = DAFFReader::create( );
}

ITAGeo::Directivity::CDAFF_Format::CDAFF_Format( const std::string& sFilePath ) : CDAFF_Format( )
{
	LoadFromFile( sFilePath );
}

ITAGeo::Directivity::CDAFF_Format::~CDAFF_Format( )
{
	delete m_pDAFFReader;
}

void ITAGeo::Directivity::CDAFF_Format::LoadFromFile( const std::string& sFilePath )
{
	int iError = m_pDAFFReader->openFile( sFilePath );
	if( iError != DAFF_ERROR::DAFF_NO_ERROR )
		ITA_EXCEPT1( INVALID_PARAMETER, "Error loading DAFF file from path '" + sFilePath + "': " + DAFFUtils::StrError( iError ) );

	std::filesystem::path fsPath( sFilePath );
	SetName( fsPath.stem( ).string( ) );
}

int ITAGeo::Directivity::CDAFF_Format::GetNumChannels( ) const
{
	if( !m_pDAFFReader->isFileOpened( ) )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Can not provide a channel count because no DAFF file has been loaded yet" );
	return m_pDAFFReader->getProperties( )->getNumberOfChannels( );
}
