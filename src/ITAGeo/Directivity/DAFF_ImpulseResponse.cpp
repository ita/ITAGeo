#include <DAFF.h>
#include <ITAException.h>
#include <ITAGeo/Directivity/DAFF_ImpulseResponse.h>

ITAGeo::Directivity::CDAFF_ImpulseResponse::CDAFF_ImpulseResponse( ) : CDAFF_Format( )
{
	SetDomain( ITADomain::ITA_TIME_DOMAIN );
}

ITAGeo::Directivity::CDAFF_ImpulseResponse::CDAFF_ImpulseResponse( const std::string& sFilePath ) : CDAFF_ImpulseResponse( )
{
	LoadFromFile( sFilePath );
}

float ITAGeo::Directivity::CDAFF_ImpulseResponse::GetFilterLengthSamples( ) const
{
	auto p = dynamic_cast<DAFFContentIR*>( m_pDAFFReader->getContent( ) );
	if( p )
		return (float)p->getFilterLength( );
	else
		return 0.0f;
}

ITAGeo::Directivity::CDAFF_ImpulseResponse::~CDAFF_ImpulseResponse( ) {}

void ITAGeo::Directivity::CDAFF_ImpulseResponse::GetNearestNeighbourImpulseResponse( const ITAGeo::Coordinates::CSpherical& oDirection,
                                                                                     ITABase::CFiniteImpulseResponse& oFIR, const int iChannelIndex ) const
{
	auto p = dynamic_cast<DAFFContentIR*>( m_pDAFFReader->getContent( ) );

	if( p->getSamplerate( ) != oFIR.GetSampleRate( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "DAFF impulse response and target finite impulse response filter have mismatching sampling ranges." );

	if( p->getFilterLength( ) > oFIR.GetLength( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "DAFF impulse response is longer than target finite impulse response filter." );

	int iRecordIndex = -1;
	p->getNearestNeighbour( DAFF_VIEWS::DAFF_OBJECT_VIEW, oDirection.GetAzimuthDegree( ), oDirection.GetElevationDegree( ), iRecordIndex );
	p->getFilterCoeffs( iRecordIndex, iChannelIndex, &oFIR[0] );
}

void ITAGeo::Directivity::CDAFF_ImpulseResponse::GetNearestNeighbourImpulseResponse( const ITAGeo::Coordinates::CSpherical& oDirection, ITASampleBuffer& oFIR,
                                                                                     const int iChannelIndex ) const
{
	auto p = dynamic_cast<DAFFContentIR*>( m_pDAFFReader->getContent( ) );

	if( p->getFilterLength( ) > oFIR.GetLength( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "DAFF impulse response is longer than target finite impulse response filter." );

	int iRecordIndex = -1;
	p->getNearestNeighbour( DAFF_VIEWS::DAFF_OBJECT_VIEW, oDirection.GetAzimuthDegree( ), oDirection.GetElevationDegree( ), iRecordIndex );
	p->getFilterCoeffs( iRecordIndex, iChannelIndex, &oFIR[0] );
}

void ITAGeo::Directivity::CDAFF_ImpulseResponse::GetNearestNeighbourImpulseResponse( const ITAGeo::Coordinates::CSpherical& oDirection,
                                                                                     ITABase::CMultichannelFiniteImpulseResponse& oFIR ) const
{
	for( int n = 0; n < m_pDAFFReader->getProperties( )->getNumberOfChannels( ); n++ )
	{
		GetNearestNeighbourImpulseResponse( oDirection, oFIR[n], n );
	}
}

float ITAGeo::Directivity::CDAFF_ImpulseResponse::GetMeanTimeOfArrival( ) const
{
	auto p = dynamic_cast<DAFFContentIR*>( m_pDAFFReader->getContent( ) );
	if( m_pDAFFReader->getMetadata( )->hasKey( "delay_samples" ) )
	{
		if( m_pDAFFReader->getMetadata( )->getKeyType( "delay_samples" ) == DAFFMetadata::DAFF_FLOAT )
			return m_pDAFFReader->getMetadata( )->getKeyFloat( "delay_samples" ) / p->getSamplerate( );
		else if( m_pDAFFReader->getMetadata( )->getKeyType( "delay_samples" ) == DAFFMetadata::DAFF_INT )
			return m_pDAFFReader->getMetadata( )->getKeyInt( "delay_samples" ) / p->getSamplerate( );
	}

	return p->getMinEffectiveFilterOffset( ) / p->getSamplerate( );
}
