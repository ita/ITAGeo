#include <ITAGeo/Directivity/DAFF_ImpulseResponse.h>
#include <ITAGeo/Directivity/Utils.h>


ITA_GEO_API float ITAGeo::Directivity::Utils::EstimateFilterLengthSamples( std::shared_ptr<const ITAGeo::Directivity::IDirectivity> pDirectivity, const float fSampleRate,
                                                                           const float fSpeedOfSound )
{
	switch( pDirectivity->GetDomain( ) )
	{
		case( ITADomain::ITA_TIME_DOMAIN ):
		{
			if( pDirectivity->GetFormat( ) == IDirectivity::OPENDAFF )
			{
				auto p = std::dynamic_pointer_cast<const CDAFF_ImpulseResponse>( pDirectivity );
				return p->GetFilterLengthSamples( );
			}
		}
		default:
			return 0.0f * fSampleRate * fSpeedOfSound; // dummy
			                                           // @todo more cases
	}

	// All other cases are considered to not extend any filter length, hence have to estimate
	return 0.0f;
}