target_sources (${LIB_TARGET} PRIVATE Base.cpp DirectivityManager.cpp Utils.cpp)

target_sources (${LIB_TARGET} PRIVATE DAFF_Format.cpp DAFF_ImpulseResponse.cpp DAFF_MagnitudeSpectrum.cpp)
