#include <ITAConstants.h>
#include <ITAException.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Material/Material.h>
#include <ITAGeo/Utils.h>
#include <VistaTools/VistaRandomNumberGenerator.h>
#include <cassert>
#include <memory>

bool ITAGeoUtils::CalculateDiffractionAperturePoint( const VistaRay& rInfiniteDiffractionEdge, const VistaVector3D& v3SourcePos, const VistaVector3D& v3TargetPos,
                                                     VistaVector3D& v3AperturePoint )
{
	// If source and target are both on diffraction edge, a single aperture point can not be defined
	float fDotProductSource = rInfiniteDiffractionEdge.GetDir( ).GetNormalized( ).Dot( ( v3SourcePos - rInfiniteDiffractionEdge.GetOrigin( ) ).GetNormalized( ) );
	float fDotProductTarget = rInfiniteDiffractionEdge.GetDir( ).GetNormalized( ).Dot( ( v3TargetPos - rInfiniteDiffractionEdge.GetOrigin( ) ).GetNormalized( ) );
	if( 1 - std::abs( fDotProductSource * fDotProductTarget ) < Vista::Epsilon )
		return false;

	// If only source on edge, coincident aperture point
	if( std::abs( 1 - fDotProductSource ) < Vista::Epsilon )
	{
		v3AperturePoint = v3SourcePos;
		return true;
	}

	// If only target on edge, coincident aperture point
	if( std::abs( 1 - fDotProductTarget ) < Vista::Epsilon )
	{
		v3AperturePoint = v3TargetPos;
		return true;
	}

	VistaVector3D v3SourceToTargetDir = ( v3TargetPos - v3SourcePos ).GetNormalized( );
	VistaVector3D v3Normal            = v3SourceToTargetDir.Cross( rInfiniteDiffractionEdge.GetDir( ).GetNormalized( ) );

	// Construct a plane with source, target and (yet unknown) apex point
	VistaPlane oPlaneOnSourceAndTarget;
	oPlaneOnSourceAndTarget.SetOrigin( v3SourcePos );
	oPlaneOnSourceAndTarget.SetNormVector( v3Normal.Cross( v3SourceToTargetDir ) );

	// Determine apex point
	return oPlaneOnSourceAndTarget.CalcIntersectionPoint( rInfiniteDiffractionEdge, v3AperturePoint );
}

bool ITAGeoUtils::CalculateDiffractionAperturePoint( const VistaVector3D& v3StartVertex, const VistaVector3D& v3EndVertex, const VistaVector3D& v3SourcePos,
                                                     const VistaVector3D& v3TargetPos, VistaVector3D& v3AperturePoint )
{
	const VistaRay rDiffractionEdge( v3StartVertex, v3EndVertex - v3StartVertex );
	if( !ITAGeoUtils::CalculateDiffractionAperturePoint( rDiffractionEdge, v3SourcePos, v3TargetPos, v3AperturePoint ) )
		return false;
	else
		return ITAGeoUtils::IsDiffractionAperturePointInRange( v3StartVertex, v3EndVertex, v3AperturePoint );
}

bool ITAGeoUtils::IsPointInConvexPolygon( const VistaVector3D& v3Point, VistaPolygon& oPolygon )
{
	std::vector<VistaVector3D*> vv3PolyPoints;
	oPolygon.GetPolyPoints( vv3PolyPoints );

	if( vv3PolyPoints.size( ) < 3 )
		return false;

	for( size_t i = 0; i < vv3PolyPoints.size( ) - 1; i++ )
	{
		const size_t iCurrentIdx         = i;
		const size_t iNextIdx            = ( i + 1 ) % vv3PolyPoints.size( );
		const size_t iNextNextIdx        = ( i + 2 ) % vv3PolyPoints.size( );
		const VistaVector3D v3CrossProd1 = ( *vv3PolyPoints[iNextIdx] - *vv3PolyPoints[iCurrentIdx] ).Cross( v3Point - *vv3PolyPoints[iCurrentIdx] );
		const VistaVector3D v3CrossProd2 = ( *vv3PolyPoints[iNextNextIdx] - *vv3PolyPoints[iNextIdx] ).Cross( v3Point - *vv3PolyPoints[iNextIdx] );

		// Compare signs between pairs
		if( v3CrossProd1.Dot( v3CrossProd2 ) <= 0.0f )
			return false;
	}

	VistaPlane oPlaneOnSourceAndTarget;
	oPlaneOnSourceAndTarget.SetOrigin( *vv3PolyPoints[0] );
	oPlaneOnSourceAndTarget.SetNormVector( oPolygon.GetUpVector( ) );

	if( oPlaneOnSourceAndTarget.CalcDistance( v3Point ) > ITAConstants::EPS_F_L )
		return false;

	return true;
}

bool ITAGeoUtils::IsPointInFace( const VistaVector3D& v3Point, const CITAMesh* pMesh, CITAMesh::FaceHandle hFace )
{
	// Number of intersection. If number of intersection is even, the point must be on the outside of the face
	// Else, the point must be on the inside. (Jordan method)
	uint iIntersectCount = 0;

	// Vertex iterator of hFace (for points  lying on a vertex point)
	CITAMesh::ConstFaceVertexIter cfv_it = pMesh->cfv_begin( hFace );
	while( cfv_it != pMesh->cfv_end( hFace ) )
	{
		// Current vertex handle
		CITAMesh::VertexHandle hVertex = CITAMesh::VertexHandle( *cfv_it++ );

		OpenMesh::Vec3f vertexPos = pMesh->point( hVertex );

		auto dAbsDifference = abs( vertexPos[0] - v3Point[0] ) + abs( vertexPos[1] - v3Point[1] ) + abs( vertexPos[2] - v3Point[2] );

		if( dAbsDifference < ITAConstants::EPS_D_L )
			return false;
	}


	// Halfedge iterator of hFace
	CITAMesh::ConstFaceHalfedgeIter cfh_it = pMesh->cfh_begin( hFace );

	// Point vector, defined by the first halfedge
	CITAMesh::HalfedgeHandle hHalfedge( *cfh_it++ ); // First halfedge never crosses itself and can therefore be ignored
	OpenMesh::Vec3f oPointVector = pMesh->calc_edge_vector( hHalfedge );
	while( cfh_it != pMesh->cfh_end( hFace ) )
	{
		// Current halfedge handle
		hHalfedge = CITAMesh::HalfedgeHandle( *cfh_it++ );

		// If point is on halfedge, it won't be on the face (limit case)
		if( IsPointOnHalfedge( v3Point, pMesh, hHalfedge ) )
			return false;

		// Start point of halfedge and halfedge vector
		auto hStartVertex    = pMesh->from_vertex_handle( hHalfedge );
		auto oHalfedgeStart  = pMesh->point( hStartVertex );
		auto oHalfedgeVector = pMesh->calc_edge_vector( hHalfedge );

		// Relative position for the intersection between the halfedge vector and the tangent vector in relation to the halfedge
		double relIntersectHalfedge = -1;

		// Relative position for the intersection between the halfedge vector and the Tangent vector in relation to the tangent
		double relIntersectPoint = -1;

		// Divisor for first vector element(cross product of halfedge and tangent vector)
		double divisor = oHalfedgeVector[1] * oPointVector[2] - oHalfedgeVector[2] * oPointVector[1];

		// If plane normal is independent of first axis, the divisor will be too small leading to wrong intersection parameter( division by zero)
		// Then, the divisor of the next element must be calculated.
		if( abs( divisor ) > ITAConstants::EPS_D_L )
		{
			relIntersectHalfedge = ( ( v3Point[1] - oHalfedgeStart[1] ) * oPointVector[2] - ( v3Point[2] - oHalfedgeStart[2] ) * oPointVector[1] ) / divisor;
			relIntersectPoint    = ( ( v3Point[1] - oHalfedgeStart[1] ) * oHalfedgeVector[2] - ( v3Point[2] - oHalfedgeStart[2] ) * oHalfedgeVector[1] ) / divisor;
		}
		else
		{
			// Divisor for second vector element
			divisor = oHalfedgeVector[2] * oPointVector[0] - oHalfedgeVector[0] * oPointVector[2];

			if( abs( divisor ) > ITAConstants::EPS_D_L )
			{
				relIntersectHalfedge = ( ( v3Point[2] - oHalfedgeStart[2] ) * oPointVector[0] - ( v3Point[0] - oHalfedgeStart[0] ) * oPointVector[2] ) / divisor;
				relIntersectPoint    = ( ( v3Point[2] - oHalfedgeStart[2] ) * oHalfedgeVector[0] - ( v3Point[0] - oHalfedgeStart[0] ) * oHalfedgeVector[2] ) / divisor;
			}
			else
			{
				// divisor for third element
				divisor = oHalfedgeVector[0] * oPointVector[1] - oHalfedgeVector[1] * oPointVector[0];

				if( abs( divisor ) > ITAConstants::EPS_D_L )
				{
					relIntersectHalfedge = ( ( v3Point[0] - oHalfedgeStart[0] ) * oPointVector[1] - ( v3Point[1] - oHalfedgeStart[1] ) * oPointVector[0] ) / divisor;
					relIntersectPoint = ( ( v3Point[0] - oHalfedgeStart[0] ) * oHalfedgeVector[1] - ( v3Point[1] - oHalfedgeStart[1] ) * oHalfedgeVector[0] ) / divisor;
				}
			}
		}
		// The relative intersection location in relation to the halfedge vector must be between 0 and 1.
		// In addition to this, the intersection location in relation to the point vector must be positive.
		if( ( relIntersectHalfedge < 1 - ITAConstants::EPS_D_L ) && ( relIntersectHalfedge > +ITAConstants::EPS_D_L ) &&
		    ( relIntersectPoint > -ITAConstants::EPS_D_L ) ) // 0->(0-eps)
			iIntersectCount++;
	}

	if( iIntersectCount % 2 == 1 )
		return true;
	else
		return false;
}

bool ITAGeoUtils::IsPointInFace( const VistaVector3D& v3Point, const std::vector<std::shared_ptr<VistaVector3D>>& vv3FaceVertices )
{
	// Number of intersection. If number of intersection is even, the point must be on the outside of the face
	// Else, the point must be on the inside. (Jordan method)
	uint iIntersectCount = 0;

	// First check, if point  lies on a vertex
	for( auto& v3Vertex: vv3FaceVertices )
	{
		auto dAbsDifference = abs( ( *v3Vertex )[0] - v3Point[0] ) + abs( ( *v3Vertex )[1] - v3Point[1] ) + abs( ( *v3Vertex )[2] - v3Point[2] );

		if( dAbsDifference < ITAConstants::EPS_D_L )
			return false;
	}

	// Set an arbitrary point direction on the plane.
	// Here, the halfedge constructed by the first and last vertex will be used.
	VistaVector3D v3PointDirection = *vv3FaceVertices.front( ) - *vv3FaceVertices.back( );


	for( int i = 0; i < vv3FaceVertices.size( ) - 1; i++ )
	{
		// If point is on halfedge, it won't be on the face (limit case)
		if( IsPointOnHalfedge( v3Point, *vv3FaceVertices[i], *vv3FaceVertices[i + 1] ) )
			return false;


		// Current halfedge start
		auto& v3HalfedgeStart = vv3FaceVertices[i];

		// Direction of the current halfedge (not normalized)
		VistaVector3D v3HalfedgeDirection = *vv3FaceVertices[i + 1] - *vv3FaceVertices[i];

		// Relative position for the intersection between the halfedge vector and the tangent vector in relation to the halfedge
		double relIntersectHalfedge = -1;

		// Relative position for the intersection between the halfedge vector and the Tangent vector in relation to the tangent
		double relIntersectPoint = -1;

		// Divisor for first vector element(cross product of halfedge and tangent direction vector)
		double divisor = v3HalfedgeDirection[1] * v3PointDirection[2] - v3HalfedgeDirection[2] * v3PointDirection[1];

		// If plane normal is independent of first axis, the divisor will be too small leading to wrong intersection parameter( division by zero)
		////Then, the divisor of the next element must be calculated.
		if( abs( divisor ) > ITAConstants::EPS_D_L )
		{
			relIntersectHalfedge =
			    ( ( v3Point[1] - ( *v3HalfedgeStart )[1] ) * v3PointDirection[2] - ( v3Point[2] - ( *v3HalfedgeStart )[2] ) * v3PointDirection[1] ) / divisor;
			relIntersectPoint =
			    ( ( v3Point[1] - ( *v3HalfedgeStart )[1] ) * v3HalfedgeDirection[2] - ( v3Point[2] - ( *v3HalfedgeStart )[2] ) * v3HalfedgeDirection[1] ) / divisor;
		}
		else
		{
			// Divisor for second vector element
			divisor = v3HalfedgeDirection[2] * v3PointDirection[0] - v3HalfedgeDirection[0] * v3PointDirection[2];

			if( abs( divisor ) > ITAConstants::EPS_D_L )
			{
				relIntersectHalfedge =
				    ( ( v3Point[2] - ( *v3HalfedgeStart )[2] ) * v3PointDirection[0] - ( v3Point[0] - ( *v3HalfedgeStart )[0] ) * v3PointDirection[2] ) / divisor;
				relIntersectPoint =
				    ( ( v3Point[2] - ( *v3HalfedgeStart )[2] ) * v3HalfedgeDirection[0] - ( v3Point[0] - ( *v3HalfedgeStart )[0] ) * v3HalfedgeDirection[2] ) / divisor;
			}
			else
			{
				// divisor for third element
				divisor = v3HalfedgeDirection[0] * v3PointDirection[1] - v3HalfedgeDirection[1] * v3PointDirection[0];

				if( abs( divisor ) > ITAConstants::EPS_D_L )
				{
					relIntersectHalfedge =
					    ( ( v3Point[0] - ( *v3HalfedgeStart )[0] ) * v3PointDirection[1] - ( v3Point[1] - ( *v3HalfedgeStart )[1] ) * v3PointDirection[0] ) / divisor;
					relIntersectPoint =
					    ( ( v3Point[0] - ( *v3HalfedgeStart )[0] ) * v3HalfedgeDirection[1] - ( v3Point[1] - ( *v3HalfedgeStart )[1] ) * v3HalfedgeDirection[0] ) /
					    divisor;
				}
			}
		}

		// The relative intersection location in relation to the halfedge vector must be between 0 and 1.
		// In addition to this, the intersection location in relation to the point vector must be positive.
		if( ( relIntersectHalfedge < 1 - ITAConstants::EPS_D_L ) && ( relIntersectHalfedge > +ITAConstants::EPS_D_L ) &&
		    ( relIntersectPoint > -ITAConstants::EPS_D_L ) ) // 0->(0-eps)
			iIntersectCount++;
	}


	if( iIntersectCount % 2 == 1 )
		return true;
	else
		return false;
}

bool ITAGeoUtils::IsPointInFace( const VistaVector3D& v3Point, const std::vector<VistaVector3D>& vv3FaceVertices )
{
	// Number of intersection. If number of intersection is even, the point must be on the outside of the face
	// Else, the point must be on the inside. (Jordan method)
	uint iIntersectCount = 0;

	// First check, if point  lies on a vertex
	for( auto& v3Vertex: vv3FaceVertices )
	{
		auto dAbsDifference = abs( ( v3Vertex )[0] - v3Point[0] ) + abs( ( v3Vertex )[1] - v3Point[1] ) + abs( ( v3Vertex )[2] - v3Point[2] );

		if( dAbsDifference < ITAConstants::EPS_D_L )
			return false;
	}

	// Set an arbitrary point direction on the plane.
	// Here, the halfedge constructed by the first and last vertex will be used.
	VistaVector3D v3PointDirection = vv3FaceVertices.front( ) - vv3FaceVertices.back( );


	for( int i = 0; i < vv3FaceVertices.size( ) - 1; i++ )
	{
		// If point is on halfedge, it won't be on the face (limit case)
		if( IsPointOnHalfedge( v3Point, vv3FaceVertices[i], vv3FaceVertices[i + 1] ) )
			return false;


		// Current halfedge start
		auto& v3HalfedgeStart = vv3FaceVertices[i];

		// Direction of the current halfedge (not normalized)
		VistaVector3D v3HalfedgeDirection = vv3FaceVertices[i + 1] - vv3FaceVertices[i];

		// Relative position for the intersection between the halfedge vector and the tangent vector in relation to the halfedge
		double relIntersectHalfedge = -1;

		// Relative position for the intersection between the halfedge vector and the Tangent vector in relation to the tangent
		double relIntersectPoint = -1;

		// Divisor for first vector element(cross product of halfedge and tangent direction vector)
		double divisor = v3HalfedgeDirection[1] * v3PointDirection[2] - v3HalfedgeDirection[2] * v3PointDirection[1];

		// If plane normal is independent of first axis, the divisor will be too small leading to wrong intersection parameter( division by zero)
		////Then, the divisor of the next element must be calculated.
		if( abs( divisor ) > ITAConstants::EPS_D_L )
		{
			relIntersectHalfedge =
			    ( ( v3Point[1] - ( v3HalfedgeStart )[1] ) * v3PointDirection[2] - ( v3Point[2] - ( v3HalfedgeStart )[2] ) * v3PointDirection[1] ) / divisor;
			relIntersectPoint =
			    ( ( v3Point[1] - ( v3HalfedgeStart )[1] ) * v3HalfedgeDirection[2] - ( v3Point[2] - ( v3HalfedgeStart )[2] ) * v3HalfedgeDirection[1] ) / divisor;
		}
		else
		{
			// Divisor for second vector element
			divisor = v3HalfedgeDirection[2] * v3PointDirection[0] - v3HalfedgeDirection[0] * v3PointDirection[2];

			if( abs( divisor ) > ITAConstants::EPS_D_L )
			{
				relIntersectHalfedge =
				    ( ( v3Point[2] - ( v3HalfedgeStart )[2] ) * v3PointDirection[0] - ( v3Point[0] - ( v3HalfedgeStart )[0] ) * v3PointDirection[2] ) / divisor;
				relIntersectPoint =
				    ( ( v3Point[2] - ( v3HalfedgeStart )[2] ) * v3HalfedgeDirection[0] - ( v3Point[0] - ( v3HalfedgeStart )[0] ) * v3HalfedgeDirection[2] ) / divisor;
			}
			else
			{
				// divisor for third element
				divisor = v3HalfedgeDirection[0] * v3PointDirection[1] - v3HalfedgeDirection[1] * v3PointDirection[0];

				if( abs( divisor ) > ITAConstants::EPS_D_L )
				{
					relIntersectHalfedge =
					    ( ( v3Point[0] - ( v3HalfedgeStart )[0] ) * v3PointDirection[1] - ( v3Point[1] - ( v3HalfedgeStart )[1] ) * v3PointDirection[0] ) / divisor;
					relIntersectPoint =
					    ( ( v3Point[0] - ( v3HalfedgeStart )[0] ) * v3HalfedgeDirection[1] - ( v3Point[1] - ( v3HalfedgeStart )[1] ) * v3HalfedgeDirection[0] ) / divisor;
				}
			}
		}

		// The relative intersection location in relation to the halfedge vector must be between 0 and 1.
		// In addition to this, the intersection location in relation to the point vector must be positive.
		if( ( relIntersectHalfedge < 1 - ITAConstants::EPS_D_L ) && ( relIntersectHalfedge > +ITAConstants::EPS_D_L ) &&
		    ( relIntersectPoint > -ITAConstants::EPS_D_L ) ) // 0->(0-eps)
			iIntersectCount++;
	}


	if( iIntersectCount % 2 == 1 )
		return true;
	else
		return false;
}

bool ITAGeoUtils::IsPointOnHalfedge( const VistaVector3D& v3Point, const VistaVector3D& v3HalfedgeStart, const VistaVector3D& v3HalfedgeEnd )
{
	std::vector<float> vfRelativeDifferences; // = (v3Point[0] - oHalfedgeStart[0]) / oHalfedgeVector[0];
	for( int i = 0; i < 3; i++ )
	{
		// If the halfedge has no component in the i-th direction, the i-th component of the point and the halfedge must be the same
		if( abs( v3HalfedgeStart[i] - v3HalfedgeEnd[i] ) < Vista::Epsilon )
		{
			if( abs( v3Point[i] - v3HalfedgeStart[i] ) > Vista::Epsilon )
				return false;
		}
		else
		{
			float fRelativeDifference = ( v3Point[i] - v3HalfedgeStart[i] ) / ( v3HalfedgeEnd[i] - v3HalfedgeStart[i] );

			// Relative difference must be 0 and 1
			if( abs( fRelativeDifference - 0.5 ) > 0.5 + Vista::Epsilon )
				return false;

			vfRelativeDifferences.push_back( fRelativeDifference );

			// All differences must be the same
			if( abs( vfRelativeDifferences[0] - fRelativeDifference ) > Vista::Epsilon )
				return false;
		}
	}

	return true;
}

bool ITAGeoUtils::IsPointOnHalfedge( const VistaVector3D& v3Point, const CITAMesh* pMesh, CITAMesh::HalfedgeHandle hHalfedge )
{
	// Start point of halfedge and halfedge vector
	auto hStartVertex    = pMesh->from_vertex_handle( hHalfedge );
	auto oHalfedgeStart  = pMesh->point( hStartVertex );
	auto oHalfedgeVector = pMesh->calc_edge_vector( hHalfedge );

	std::vector<float> vfRelativeDifferences; // = (v3Point[0] - oHalfedgeStart[0]) / oHalfedgeVector[0];
	for( int i = 0; i < 3; i++ )
	{
		// If the halfedge has no component in the i-th direction, the i-th component of the point and the halfedge must be the same
		if( abs( oHalfedgeVector[i] ) < Vista::Epsilon )
		{
			if( abs( v3Point[i] - oHalfedgeStart[i] ) > Vista::Epsilon )
				return false;
		}
		else
		{
			float fRelativeDifference = ( v3Point[i] - oHalfedgeStart[i] ) / oHalfedgeVector[i];

			// Relative difference must be 0 and 1
			if( abs( fRelativeDifference - 0.5 ) > 0.5 + Vista::Epsilon )
				return false;

			vfRelativeDifferences.push_back( fRelativeDifference );

			// All differences must be the same
			if( abs( vfRelativeDifferences[0] - fRelativeDifference ) > Vista::Epsilon )
				return false;
		}
	}

	return true;
}

bool ITAGeoUtils::RayConvexPolygonIntersectionTest( const VistaRay& rRay, VistaPolygon& oPolygon, VistaVector3D& v3IntersectionPoint,
                                                    ITAGeo::ECulling eFaceCulling /* = ITAGeo::ECulling::BACKFACE*/ )
{
	// Extract points of polygon
	std::vector<VistaVector3D*> vv3PolyPoints;
	oPolygon.GetPolyPoints( vv3PolyPoints );

	// Create plane
	VistaPlane oPlane;
	oPlane.SetOrigin( *vv3PolyPoints[0] );
	oPlane.SetNormVector( oPolygon.GetUpVector( ) );


	// Do not calculate intersection point for facing BACKFACE/FRONTFACE for set ECulling
	switch( eFaceCulling )
	{
		case( ITAGeo::ECulling::BACKFACE ):
			if( oPlane.GetNormVector( ) * rRay.GetDir( ) >= 0 )
				return false;
			break;
		case( ITAGeo::ECulling::FRONTFACE ):
			if( oPlane.GetNormVector( ) * rRay.GetDir( ) <= 0 )
				return false;
			break;
		default:
			break;
	}

	// the point of intersection in relation to the ray
	float fLambda;

	// Calc lambda. if no intersection occurs return false
	if( !oPlane.CalcIntersection( rRay, fLambda ) )
		return false;

	// return false if the intersection is in back of the ray (@todo: check if comparison against negative epsilon needed)
	if( fLambda < 0 ) // fLamba < eps
		return false;
	else
	{
		v3IntersectionPoint = rRay.GetDir( ) * fLambda + rRay.GetOrigin( );

		return IsPointInConvexPolygon( v3IntersectionPoint, oPolygon );
	}
}

bool ITAGeoUtils::RayFaceIntersectionTest( const VistaRay& rRay, const CITAMesh* pMesh, CITAMesh::FaceHandle hFace, VistaVector3D& v3IntersectionPoint,
                                           ITAGeo::ECulling eFaceCulling /* = ITAGeo::ECulling::BACKFACE*/ )
{
	// Create plane
	VistaPlane oPlane;
	VistaPlaneFromOpenMeshFace( pMesh, hFace, oPlane );

	// Do not calculate intersection point for facing BACKFACE/FRONTFACE if eFaceCulling is set
	switch( eFaceCulling )
	{
		case( ITAGeo::ECulling::BACKFACE ):
			if( oPlane.GetNormVector( ) * rRay.GetDir( ) >= 0 )
				return false;
			break;
		case( ITAGeo::ECulling::FRONTFACE ):
			if( oPlane.GetNormVector( ) * rRay.GetDir( ) <= 0 )
				return false;
			break;
		default:
			break;
	}

	// The point of intersection in relation to the ray
	float fLambda;

	// Calc lambda. if no intersection occurs return false
	if( !oPlane.CalcIntersection( rRay, fLambda ) )
		return false;

	// return false if the intersection is in back of the ray (@todo: check if comparison against negative epsilon needed)
	if( fLambda < -Vista::Epsilon ) // fLamba < eps
		return false;
	else
	{
		v3IntersectionPoint = rRay.GetDir( ) * fLambda + rRay.GetOrigin( );
		return IsPointInFace( v3IntersectionPoint, pMesh, hFace );
	}
}

bool ITAGeoUtils::RayDetectionSphereIntersectionTest( const VistaRay& rRay, std::shared_ptr<ITAGeo::CPropagationAnchor> pDestination, float fDetectionSphereRadius,
                                                      VistaVector3D& v3DetectionPoint )
{
	const auto v3RayOrigin2RcvPos = pDestination->v3InteractionPoint - rRay.GetOrigin( );
	v3DetectionPoint              = rRay.GetDir( ).Dot( v3RayOrigin2RcvPos ) * rRay.GetDir( ) + rRay.GetOrigin( );

	const auto v3DetectionOffset = v3DetectionPoint - pDestination->v3InteractionPoint;
	const auto fDetectedRadius   = v3DetectionOffset.GetLength( );
	const auto bInsideSphere     = fDetectedRadius <= fDetectionSphereRadius;

	return bInsideSphere;
}

VistaVector3D ITAGeoUtils::GetReflectedDirection( const CITAMesh* pMesh, CITAMesh::FaceHandle hFace, const VistaRay& rRay )
{
	assert( pMesh->has_face_normals( ) );

	CITAMesh::Normal normal( pMesh->normal( hFace ) );
	const VistaVector3D v3FaceNormal( normal.data( ) );

	VistaVector3D v3OrthogonalComponent = rRay.GetDir( ).Dot( v3FaceNormal ) * v3FaceNormal; // direction component of incident ray orthogonal to reflection face

	//! Change direction of ray orthogonal to reflection wall into the opposite direction
	return rRay.GetDir( ) - 2 * v3OrthogonalComponent;
}

void ITAGeoUtils::GetUniformlyDistributedSphericalCoordinateAngles( float& fRandomAzimuthAngle, float& fRandomElevationAngle )
{
	fRandomAzimuthAngle   = 2 * ITAConstants::PI_F * VistaRandomNumberGenerator::GetStandardRNG( )->GenerateFloat( 0.0f, 1.0f );
	fRandomElevationAngle = acos( 1.0f - 2.0f * VistaRandomNumberGenerator::GetStandardRNG( )->GenerateFloat( 0.0f, 1.0f ) );
}

VistaVector3D ITAGeoUtils::GetUniformlyDistributedSphericalRandomDirection( )
{
	float fPhi, fTheta;
	GetUniformlyDistributedSphericalCoordinateAngles( fPhi, fTheta );

	return VistaVector3D( cos( fPhi ) * sin( fTheta ), sin( fPhi ) * sin( fTheta ), cos( fTheta ) );
}

ITAGeo::EIntersecting ITAGeoUtils::IsLineIntersectingFace( const VistaVector3D& v3StartPoint, const VistaVector3D& v3EndPoint, const CITAMesh* pMesh,
                                                           CITAMesh::FaceHandle hFace )
{
	// The point of intersection in relation to the ray
	float fLambda;

	// Create plane
	VistaPlane oPlane;
	VistaPlaneFromOpenMeshFace( pMesh, hFace, oPlane );

	// Scalar multiplication of the line vector with the face normal. If line and plane are perpendicular, result is 0. Then, no intersection occurs.
	if( abs( ( v3EndPoint - v3StartPoint ) * oPlane.GetNormVector( ) ) < Vista::Epsilon )
		return ITAGeo::EIntersecting::NOINTERSECTION;

	// Calculate lambda. If no intersection occurs, return EIntersecting::NOINTERSECTION
	if( !oPlane.CalcIntersection( VistaRay( v3StartPoint, v3EndPoint - v3StartPoint ), fLambda ) )
		return ITAGeo::EIntersecting::NOINTERSECTION;

	// Return EIntersecting::NOINTERSECTION if fLambda is smaller than zero
	if( fLambda < 0 - Vista::Epsilon )
		return ITAGeo::EIntersecting::NOINTERSECTION;

	// Calc length of line segment
	double lineLength = ( v3EndPoint - v3StartPoint ).GetLength( );

	// The length of the intersection vector starting at v3StartPoint and ending at the point of intersection
	//(can also be negative)
	double intersectionLength = fLambda * lineLength;

	// Return EIntersecting::NOINTERSECTION if intersectionLength larger than line length
	if( intersectionLength > lineLength + ITAConstants::EPS_F_L )
		return ITAGeo::EIntersecting::NOINTERSECTION;

	// Calc the intersection point
	VistaVector3D v3IntersectionPoint = v3StartPoint + fLambda * ( v3EndPoint - v3StartPoint );

	if( !IsPointInFace( v3IntersectionPoint, pMesh, hFace ) )
		return ITAGeo::EIntersecting::NOINTERSECTION;

	if( abs( intersectionLength ) < 100 * ITAConstants::EPS_F_L ) // intersectionLength is around zero
		return ITAGeo::EIntersecting::ATSTART;
	else if( abs( lineLength - intersectionLength ) < 100 * ITAConstants::EPS_F_L ) // lineLength and intersectionLength are the same
		return ITAGeo::EIntersecting::ATEND;
	else // intersectionLength is between zero and lineLength. Therefore, the line segment is intersecting somewhere between the start and end point
		return ITAGeo::EIntersecting::BETWEEN;
}

void ITAGeoUtils::MirrorPointOverPlane( const VistaVector3D& v3Point, const VistaPlane& oPlane, VistaVector3D& v3MirroredPoint )
{
	const float fDistance = oPlane.CalcDistance( v3Point );
	if( std::abs( fDistance ) < ITAConstants::EPS_F_L )
	{
		v3MirroredPoint = v3Point;
	}
	else
	{
		v3MirroredPoint = v3Point - 2.0f * fDistance * oPlane.GetNormVector( );
	}
}

float ITAGeoUtils::GetWedgeMainFaceElevationToPointRad( const VistaVector3D& v3StartVertex, const VistaVector3D& v3EndVertex, const VistaVector3D& v3MainFaceNormal,
                                                        const VistaVector3D& v3Point )
{
	// Definition of a local coordinate system with origin at v3StartVertex
	const VistaVector3D v3YDir = v3MainFaceNormal.GetNormalized( );
	const VistaVector3D v3XDir = v3YDir.Cross( ( v3EndVertex - v3StartVertex ).GetNormalized( ) );

	// Position of point in new coordinate system
	const float fXPos = ( v3Point - v3StartVertex ).Dot( v3XDir );
	const float fYPos = ( v3Point - v3StartVertex ).Dot( v3YDir );

	// Calculation of angle
	float fAngle = atan2f( fYPos, fXPos );

	fAngle = fAngle >= 0 ? fAngle : fAngle + ITAConstants::TWO_PI_F_L;

	return fAngle;
}

bool ITAGeoUtils::IsPointOutsideDiffractionWedge( const VistaVector3D& v3StartVertex, const VistaVector3D& v3EndVertex, const VistaVector3D& v3FaceNormal1,
                                                  const VistaVector3D& v3FaceNormal2, const VistaVector3D& v3Point )
{
	VistaPlane oPlane1;
	oPlane1.SetOrigin( v3StartVertex );
	oPlane1.SetNormVector( v3FaceNormal1.GetNormalized( ) );
	assert( std::abs( oPlane1.CalcDistance( v3EndVertex ) ) < Vista::Epsilon );

	const float fLambda1 = oPlane1.CalcDistance( v3Point );

	VistaPlane oPlane2;
	oPlane2.SetOrigin( v3StartVertex );
	oPlane2.SetNormVector( v3FaceNormal2.GetNormalized( ) );
	assert( std::abs( oPlane2.CalcDistance( v3EndVertex ) ) < Vista::Epsilon );

	const float fLambda2 = oPlane2.CalcDistance( v3Point );

	return ( fLambda1 >= -Vista::Epsilon || fLambda2 >= -Vista::Epsilon );
}

bool ITAGeoUtils::CanFaceBeIlluminated( const CITAMesh& oMesh, CITAMesh::FaceHandle hFace, const VistaVector3D& v3Origin )
{
	VistaVector3D v3FaceNormal( oMesh.calc_face_normal( hFace ).data( ) );
	VistaVector3D v3FaceCentroid( oMesh.calc_face_centroid( hFace ).data( ) );

	VistaVector3D v3EmitterFaceDirection = ( v3FaceCentroid - v3Origin ).GetNormalized( );

	float fDotProduct = v3FaceNormal.Dot( v3EmitterFaceDirection );

	if( fDotProduct < 0 + 5 * Vista::Epsilon )
		return true;
	else
		return false;
}

bool ITAGeoUtils::CanFaceBeIlluminated( const CITAMesh& oMesh, CITAMesh::FaceHandle hFace, const VistaVector3D& v3Origin, const VistaVector3D& v3OriginNormal )
{
	// This function kinda assumes that a face illuminates another face, even thought we only test for a point to illuminate face.
	// Idea behind function is, that the origin face can only reflect in normal direction with 90 degree deviation at most. Therefore take originNormal vector
	// in and see if point to centroid hFace is larger than 90 degree. Cuz then it would point through the origin face.


	VistaVector3D v3FaceCentroid( oMesh.calc_face_centroid( hFace ).data( ) );
	VistaVector3D v3EmitterFaceDirection = ( v3FaceCentroid - v3Origin ).GetNormalized( );

	float temp = v3EmitterFaceDirection.Dot( v3OriginNormal );
	// NMK: I use positive eps intentional here. The idea is that the face cannot really be illumated at an incoming angle of 89 to 90 degree, right?!
	if( temp < 5 * Vista::Epsilon )
		return false;

	VistaVector3D v3FaceNormal( oMesh.calc_face_normal( hFace ).data( ) );

	float fDotProduct = v3FaceNormal.Dot( v3EmitterFaceDirection );

	if( fDotProduct < 0 + 5 * Vista::Epsilon )
		return true;
	else
		return false;
}

bool ITAGeoUtils::IsPointInFrontOfPlane( const VistaPlane& oPlane, const VistaVector3D& v3Point )
{
	// vector of point showing in direction of an arbitrary point on the plane
	VistaVector3D v3PointToFaceDirection = ( oPlane.GetOrigin( ) - v3Point );

	// Dot product of the norm vector of the plane and the above calculated direction
	// If the dot product is smaller 0 than the point lies in front of the plane
	float fDotProduct = oPlane.GetNormVector( ).Dot( v3PointToFaceDirection );

	if( fDotProduct < 0 + 5 * Vista::Epsilon )
		return true;
	else
		return false;
}


bool ITAGeoUtils::IsDiffractionAperturePointInRange( const VistaVector3D& v3StartVertex, const VistaVector3D& v3EndVertex, const VistaVector3D& v3PointOnEdgeRay )
{
	// Points at vertices are considered out of range.
	if( ( v3PointOnEdgeRay == v3StartVertex ) || ( v3PointOnEdgeRay == v3EndVertex ) )
		return false;

	// Construct normalized vectors from vertices to point and evaluate dot product, that should be +1
	const float fDotProduct1 = ( v3PointOnEdgeRay - v3StartVertex ).GetNormalized( ).Dot( ( v3EndVertex - v3StartVertex ).GetNormalized( ) );
	const float fDotProduct2 = ( v3PointOnEdgeRay - v3EndVertex ).GetNormalized( ).Dot( ( v3StartVertex - v3EndVertex ).GetNormalized( ) );

	// Both dot products should be exactly +1, but evaluate againts squared distance quantization
	// @todo jst: is this quantization well-defined, especially if right-on-edge point is considered false?
	const float fResidual = std::abs( 1 - fDotProduct1 * fDotProduct2 );
	if( fResidual < ITAConstants::EPS_F_L )
		return true;
	else
		return false;
}

bool ITAGeoUtils::CalculateDiffractionAngle( const VistaVector3D& v3StartPoint, const VistaVector3D& v3AperturePoint, const VistaVector3D& v3EndPoint, float& dAngle )
{
	const VistaVector3D v3StartToApexDir = ( v3AperturePoint - v3StartPoint ).GetNormalized( );
	const VistaVector3D v3ApexToEndDir   = ( v3EndPoint - v3AperturePoint ).GetNormalized( );

	dAngle = acos( v3StartToApexDir * v3ApexToEndDir );

	return true;
}

bool ITAGeoUtils::RojectPoint( const VistaVector3D& v3VertexStart, const VistaVector3D& v3VertexEnd, const VistaVector3D& v3TargetPlaneNormal,
                               const VistaVector3D& v3AnyPoint, VistaVector3D& v3RojectedPoint )
{
	// Algorithm A: use closest point on planes, check and correct amibuity and scale for constant Euklidean distance

	// Define target plane (target face of wedge around diffraction edge)
	VistaPlane oTargetPlane;
	oTargetPlane.SetOrigin( v3VertexStart );
	oTargetPlane.SetNormVector( v3TargetPlaneNormal );

	// Closest point on target plane
	const VistaVector3D v3ClosestPoint = oTargetPlane.CalcNearestPointOnPlane( v3AnyPoint );

	// Calculate closest aperture point
	VistaPlane oPerpendicularPlane;
	oPerpendicularPlane.SetOrigin( v3VertexStart );
	const VistaVector3D v3PerpendicularPlaneNormal = ( v3VertexEnd - v3VertexStart ).Cross( v3TargetPlaneNormal ).GetNormalized( );
	oPerpendicularPlane.SetNormVector( v3PerpendicularPlaneNormal );

	const VistaVector3D v3ClosestAperture = oPerpendicularPlane.CalcNearestPointOnPlane( v3ClosestPoint );

	const float fInitialRojectionRadius = VistaVector3D( v3ClosestAperture - v3AnyPoint ).GetLength( );

	// Get rojected point direction and invert sign, if necessary
	const VistaVector3D v3ClosestApertureToClosestPointDir = v3ClosestPoint - v3ClosestAperture;
	float fSign                                            = v3PerpendicularPlaneNormal.Dot( v3ClosestApertureToClosestPointDir ) > .0f ? -1.0f : 1.0f;

	if( v3ClosestApertureToClosestPointDir.GetLength( ) > Vista::Epsilon )
	{
		v3RojectedPoint = v3ClosestAperture + fSign * fInitialRojectionRadius * v3ClosestApertureToClosestPointDir.GetNormalized( );
		return true;
	}
	else
	{
		// Special case: aperture and closest point are the same
		v3RojectedPoint = v3ClosestAperture - fSign * fInitialRojectionRadius * v3PerpendicularPlaneNormal;
		return true;
	}

	/* Algorithm B: use edge and rotate around direction by opening angle between opposite plane and edge+position plane.
	VistaPlane oEdgeSourcePosPlane;
	oEdgeSourcePosPlane.SetOrigin( v3VertexStart );
	const VistaVector3D v3EdgeSourcePosPlaneNorm = ( v3VertexEnd - v3VertexStart ).Cross( v3Pos - v3VertexStart ).GetNormalized();
	oEdgeSourcePosPlane.SetNormVector( v3EdgeSourcePosPlaneNorm );

	const double dAngleRad = v3TargetPlaneNormal.Dot( v3EdgeSourcePosPlaneNorm );

	VistaAxisAndAngle oAAA( v3VertexEnd - v3VertexStart, float( dAngleRad ) );
	VistaQuaternion qRot( oAAA );

	// @todo: finish with Quaternion rotation

	return v3NextRotatedPosition;
	*/
}


unsigned long int ITAGeoUtils::CalculateNumberOfImages( const int iFaceCount, const int iOrder )
{
	if( iOrder <= 0 )
		return 0;

	if( iOrder == 1 )
		return iFaceCount;

	return CalculateNumberOfImages( iFaceCount, iOrder - 1 ) + iFaceCount * (unsigned long int)std::pow( ( iFaceCount - 1 ), ( iOrder - 1 ) );
}

std::string ITAGeoUtils::GetAcousticMaterialTypeString( const int iType )
{
	switch( iType )
	{
		case ITAGeo::Material::IMaterial::VISUALIZATION:
			return "Visualization";
		case ITAGeo::Material::IMaterial::SCALAR:
			return "Scalar";
		case ITAGeo::Material::IMaterial::COMPLEX_SPECTRUM:
			return "Complex spectrum";
		case ITAGeo::Material::IMaterial::MAGNITUDE_SPECTRUM:
			return "Magnitude spectrum";
		case ITAGeo::Material::IMaterial::THIRD_OCTAVE:
			return "Third octave band spectrum";
		case ITAGeo::Material::IMaterial::WHOLE_OCTAVE:
			return "Octave band spectrum";
		case ITAGeo::Material::IMaterial::TRI_BAND:
			return "Tri-band spectrum";
		case ITAGeo::Material::IMaterial::NONE:
		default:
			return "Unspecified";
	}
}

//! Creates a VistaPolygon from OpenMesh, unfortunately very slow
bool ITAGeoUtils::VistaPolygonFromOpenMeshFace( const CITAMesh* pMesh, CITAMesh::FaceHandle hFace, VistaPolygon& oPolygon )
{
	std::vector<VistaVector3D*> vv3PolygonVertices;
	CITAMesh::ConstFaceVertexIter cfvit = pMesh->cfv_begin( hFace );
	while( cfvit != pMesh->cfv_end( hFace ) )
	{
		CITAMesh::VertexHandle hVertex( *cfvit++ );
		vv3PolygonVertices.push_back( new VistaVector3D( pMesh->point( hVertex ).data( ) ) );
	}

	oPolygon.SetPolyPoints( vv3PolygonVertices );

	for( auto p: vv3PolygonVertices )
		delete p;

	return true;
}

//! Creates a VistaPlane from OpenMesh
bool ITAGeoUtils::VistaPlaneFromOpenMeshFace( const CITAMesh* pMesh, CITAMesh::FaceHandle hFace, VistaPlane& oPlane )
{
	// Set origin of plane
	CITAMesh::ConstFaceVertexIter cfvit = pMesh->cfv_begin( hFace );
	CITAMesh::VertexHandle hVertex( *cfvit );
	VistaVector3D v3Vertex( pMesh->point( hVertex ).data( ) );
	oPlane.SetOrigin( v3Vertex );

	// Set normal of face to plane
	VistaVector3D v3Normal( pMesh->calc_face_normal( hFace ).data( ) );
	oPlane.SetNormVector( v3Normal );


	return true;
}

bool ITAGeoUtils::VistaRaysFromOpenMeshFaceHalfedges( const CITAMesh* pMesh, CITAMesh::FaceHandle hFace, std::vector<VistaRay>& vRays )
{
	vRays.clear( );

	CITAMesh::ConstFaceHalfedgeIter cfh_it = pMesh->cfh_begin( hFace );

	while( cfh_it != pMesh->cfh_end( hFace ) )
	{
		CITAMesh::HalfedgeHandle hHalfedge( *cfh_it++ );

		auto hStartVertex = pMesh->from_vertex_handle( hHalfedge );
		auto hEndVertex   = pMesh->to_vertex_handle( hHalfedge );

		VistaVector3D oStartPoint( pMesh->point( hStartVertex ).data( ) );
		VistaVector3D oEndPoint( pMesh->point( hEndVertex ).data( ) );

		VistaRay rEdgeAsRay( oStartPoint, oEndPoint - oStartPoint );

		vRays.push_back( VistaRay( oStartPoint, oEndPoint - oStartPoint ) );
	}
	return true;
}

ITA_GEO_API std::string ITAGeoUtils::GenerateIdentifier( const ITAGeo::CPropagationPath& oPath )
{
	std::stringstream ssIdentifier;

	for( auto n = 0; n < oPath.size( ); n++ )
	{
		auto a( oPath[n] );

		if( n > 0 )
			ssIdentifier << "_";

		switch( a->iAnchorType )
		{
			case ITAGeo::CPropagationAnchor::EAnchorType::ACOUSTIC_EMITTER:
				ssIdentifier << "E-" << dynamic_cast<ITAGeo::CEmitter*>( a.get( ) )->sName;
				break;
			case ITAGeo::CPropagationAnchor::EAnchorType::ACOUSTIC_SENSOR:
				ssIdentifier << "S-" << dynamic_cast<ITAGeo::CSensor*>( a.get( ) )->sName;
				break;
			case ITAGeo::CPropagationAnchor::EAnchorType::SPECULAR_REFLECTION:
				ssIdentifier << "SR-" << n << "-" << dynamic_cast<ITAGeo::CSpecularReflection*>( a.get( ) )->iPolygonID;
				break;
			case ITAGeo::CPropagationAnchor::EAnchorType::DIFFRACTION_INNER_APEX:
			case ITAGeo::CPropagationAnchor::EAnchorType::DIFFRACTION_OUTER_APEX:
			{
				auto w = dynamic_cast<ITAGeo::CITADiffractionWedgeApertureBase*>( a.get( ) );
				ssIdentifier << "ED-" << n << "-" << w->iMainWedgeFaceID << "-" << w->iOppositeWedgeFaceID;
				break;
			}
			default:
				ssIdentifier << "?";
				break;
		}
	}

	return ssIdentifier.str( );
}

void ITAGeoUtils::AddIdentifier( ITAGeo::CPropagationPathList& oPathList )
{
	for( auto& p: oPathList )
		p.sIdentifier = GenerateIdentifier( p );
}

std::pair<unsigned int, unsigned int> ITAGeoUtils::GetPathOrders( const ITAGeo::CPropagationPath& oPath )
{
	unsigned int uiReflectionOrder  = 0;
	unsigned int uiDiffractionOrder = 0;

	for( const std::shared_ptr<ITAGeo::CPropagationAnchor>& oAnchor: oPath )
	{
		switch( oAnchor->iAnchorType )
		{
			case ITAGeo::CPropagationAnchor::EAnchorType::SPECULAR_REFLECTION:
			{
				uiReflectionOrder++;
				break;
			}
			case ITAGeo::CPropagationAnchor::EAnchorType::DIFFRACTION_INNER_APEX:
				[[fallthrough]];
			case ITAGeo::CPropagationAnchor::EAnchorType::DIFFRACTION_OUTER_APEX:
			{
				uiDiffractionOrder++;
				break;
			}
			default:
			{
				// Ignore everything else
				break;
			}
		}
	}

	return std::pair<unsigned int, unsigned int>( uiReflectionOrder, uiDiffractionOrder );
}
