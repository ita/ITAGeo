#include <ITAGeo/Urban/RTree.h>

using namespace ITAGeo;
using namespace ITAGeo::Urban::RTree;


//===RTree::CBuildings class=======================================================================================

// ---Constructor & Destructor-------------------------------------------------------------------------------------

CBuildings::CBuildings( ) {}

CBuildings::~CBuildings( ) {}

// ---Create function----------------------------------------------------------------------------------------------

void CBuildings::Create( const std::vector<Halfedge::CMeshModelShared>& vpBuildings )
{
	// Set buildings vector
	m_vpBuildings = vpBuildings;

	// Create start min and max
	m_vpBuildings[0]->GetBoundingBoxAxisAligned( m_v3TreeMin, m_v3TreeMax );

	// Set vertex vista vectors
	SetVertices( );

	// Create branches if number of buildings is more than one
	if( m_vpBuildings.size( ) > 1 )
		CreateBranches( );
}

// ---Get functions------------------------------------------------------------------------------------------------

void CBuildings::GetIlluminableBuildings( std::set<Halfedge::CMeshModelShared>& spBuildings, const VistaPlane& oPlane )
{
	bool isNoneIlluminable       = true;
	bool isCompletelyIlluminable = true;

	// Check if all or none vertices are illuminable
	for( auto& v3Vertex: m_v3TreeVertices )
	{
		// Vertex is illuminable if the distance to the plane is more than or equal zero
		bool isIlluminable = oPlane.CalcDistance( v3Vertex ) >= ( 0 - Vista::Epsilon );

		isCompletelyIlluminable = isCompletelyIlluminable && isIlluminable;
		isNoneIlluminable       = isNoneIlluminable && !isIlluminable;
	}

	// No building is illuminable
	if( isNoneIlluminable )
	{
		return;
	}
	// Add all buildings if tree is completely illuminable
	else if( isCompletelyIlluminable )
	{
		spBuildings.insert( m_vpBuildings.begin( ), m_vpBuildings.end( ) );
	}
	// Check branches if tree is partly illuminable. If Tree contains only one building, add the building
	else
	{
		if( m_vpBuildings.size( ) > 1 )
		{
			for( auto& pBranch: m_vpBranches )
			{
				pBranch->GetIlluminableBuildings( spBuildings, oPlane );
			}
		}
		else
		{
			spBuildings.insert( *m_vpBuildings.begin( ) );
		}
	}
}

void CBuildings::GetIlluminableBuildings( std::set<Halfedge::CMeshModelShared>& spBuildingsOut, CITAMesh* pMesh, CITAMesh::FaceHandle hFace )
{
	VistaPlane oPlane;
	ITAGeoUtils::VistaPlaneFromOpenMeshFace( pMesh, hFace, oPlane );

	GetIlluminableBuildings( spBuildingsOut, oPlane );
}

void CBuildings::GetIlluminableBuildnigs( std::set<Halfedge::CMeshModelShared>& spBuildingsOut, CITAMesh* pMesh, CITAMesh::HalfedgeHandle hHalfedge )
{
	VistaPlane oPlane;
	auto hFace         = pMesh->face_handle( hHalfedge );
	auto hOppositeFace = pMesh->opposite_face_handle( hHalfedge );

	// Get illuminable buildings from corresponding face
	ITAGeoUtils::VistaPlaneFromOpenMeshFace( pMesh, hFace, oPlane );
	GetIlluminableBuildings( spBuildingsOut, oPlane );

	// Get illuminable buildings from opposite face
	ITAGeoUtils::VistaPlaneFromOpenMeshFace( pMesh, hOppositeFace, oPlane );
	GetIlluminableBuildings( spBuildingsOut, oPlane );
}
