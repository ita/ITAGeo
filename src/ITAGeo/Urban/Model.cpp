// ITAGeo includes
#include <ITAGeo/Urban/Model.h>
#ifdef URBAN_MODEL_WITH_SKETCHUP_SUPPORT
#	include "ITAGeo/SketchUp/Model.h"
#endif


// ITABase includes
#include <ITAException.h>
#include <ITAStopWatch.h>

// Vista includes
#include <VistaTools/VistaFileSystemDirectory.h>
#include <VistaTools/VistaFileSystemFile.h>


// OpenMesh includes
#pragma warning( disable : 4512 4127 )
#include <OpenMesh/Core/IO/IOManager.hh>
#include <OpenMesh/Core/IO/importer/ImporterT.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#pragma warning( default : 4512 4127 )

// STL
#include <cassert>
#include <cmath>
#include <iostream>
#include <memory>


typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;


// namespaces
using namespace ITAGeo;
using namespace ITAGeo::Urban;

// ---Constructor & Destructor-----------------------------------------------------------------------------------

CModel::CModel( )
{
	m_pGroundSurface = NULL;
}

CModel::~CModel( ) {}

// ---Load & Store-----------------------------------------------------------------------------------------------

bool CModel::Load( const std::string& sUrbanModelFilePath )
{
	// Return value of function. If set to true, the loading of the file worked.
	bool bIsCorrectlyLoaded = false;

	// Load all buildings
	bIsCorrectlyLoaded = m_vpBuildings.Load( sUrbanModelFilePath, false );

	// Return false, if file is not correctly loaded
	if( !bIsCorrectlyLoaded )
		return false;


	// Iterate over all mesh models and search for "Surface".
	bool bSurfaceFound = false;
	for( int i = 0; i < m_vpBuildings.size( ); i++ )
	{
		if( m_vpBuildings[i]->GetName( ) == "GroundSurface" )
		{
			// Set ground surface and erase element from vector
			m_pGroundSurface = m_vpBuildings[i];
			m_vpBuildings.erase( m_vpBuildings.begin( ) + i );
		}
	}

	// If no surface is found and the first mesh is  "TopLevelMesh", set it to the surface member variable
	if( bSurfaceFound == false && ( m_vpBuildings[0]->GetName( ) == "TopLevelMesh" ) )
	{
		// Set ground surface and erase element from vector
		m_pGroundSurface = m_vpBuildings[0];
		m_vpBuildings.erase( m_vpBuildings.begin( ) );
	}
	return bIsCorrectlyLoaded;
}

bool CModel::Store( const std::string& sUrbanModelFilePath, bool bOverwrite /*= true */ ) const
{
	VistaFileSystemFile oFile( sUrbanModelFilePath );
	VistaFileSystemDirectory oParentDir( oFile.GetParentDirectory( ) );
	if( oParentDir.Exists( ) == false )
		ITA_EXCEPT1( INVALID_PARAMETER, "Target folder '" + oParentDir.GetName( ) + "' does not exist or is invalid" );

	if( oFile.Exists( ) && bOverwrite == false )
		ITA_EXCEPT1( MODAL_EXCEPTION, "File '" + oFile.GetName( ) + "' already exists and shall not be overwritten" );


	std::string sFileBaseName = oFile.GetLocalName( );
	size_t idx                = sFileBaseName.find_last_of( '.' );

	std::string sFileEnding = sFileBaseName.substr( idx + 1, sFileBaseName.size( ) - 1 );


#ifdef URBAN_MODEL_WITH_SKETCHUP_SUPPORT
	if( sFileBaseName.compare( "skp" ) )
	{
		SketchUp::CModel oSketchUpModel;

		for( auto mBuilding: m_vpBuildings )
			oSketchUpModel.AddGroupMeshModel( mBuilding.get( ) );

		oSketchUpModel.AddGroupMeshModel( m_pGroundSurface.get( ) );

		oSketchUpModel.Store( sUrbanModelFilePath, bOverwrite );

		return true;
	}
#endif

#ifdef URBAN_MODEL_WITH_CITYGML_SUPPORT
	if( sFileBaseName.compare( "gml" ) )
	{
		ITA_EXCEPT1( NOT_IMPLEMENTED, "Loading CityGML models as urban models is currently not supported" );
	}
#endif

	return false;
}

// ---Get functions----------------------------------------------------------------------------------------------

std::vector<const Halfedge::CMeshModel*> CModel::GetBuildings( ) const
{
	std::vector<const Halfedge::CMeshModel*> vpBuildings;

	for( auto& pBuilding: m_vpBuildings )
		vpBuildings.push_back( pBuilding.get( ) );

	return vpBuildings;
}

size_t CModel::GetNumBuildings( ) const
{
	return m_vpBuildings.size( );
}

std::vector<std::string> CModel::GetBuildingNames( ) const
{
	std::vector<std::string> vsNames( GetNumBuildings( ) );
	for( size_t n = 0; n < GetNumBuildings( ); n++ )
		vsNames[n] = m_vpBuildings[n]->GetName( );
	return vsNames;
}

const Halfedge::CMeshModel* CModel::GetGroundSurface( ) const
{
	return m_pGroundSurface.get( );
}

// ---Set functions----------------------------------------------------------------------------------------------

void CModel::SetGroundSurface( const Halfedge::CMeshModel* pMesh )
{
	m_pGroundSurface = std::make_shared<Halfedge::CMeshModel>( );
	m_pGroundSurface->CopyFrom( *pMesh );
}

void CModel::AddBuilding( const Halfedge::CMeshModel* pMesh )
{
	auto pBuilding = std::make_shared<CBuilding>( );
	pBuilding->CopyFrom( *pMesh );
	m_vpBuildings.push_back( pBuilding );
}
