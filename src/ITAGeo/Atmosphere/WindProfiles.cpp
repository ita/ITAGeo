// ITAGeo includes
#include <ITAGeo/Atmosphere/WindProfiles.h>

// ITABase includes
#include <ITABase/Math/Spline.h>
#include <ITAException.h>

// STL
#include <math.h>


//---Constant Profile---
//----------------------
#pragma region Constant Wind

ITAGeo::WindProfiles::CConstant::CConstant( const double& windVel, const VistaVector3D& windDir ) : IWindProfile( WeatherProfileBase::EClassType::CONSTANT )
{
	if( windDir[Vista::Z] != 0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Z-component of wind direction must be zero." );

	m_windVector = windVel * windDir.GetNormalized( );
}

ITAGeo::WindProfiles::CConstant::CConstant( const CConstant& oOther ) : IWindProfile( WeatherProfileBase::EClassType::CONSTANT ), m_windVector( oOther.m_windVector ) {}

ITAGeo::WindProfiles::CConstant& ITAGeo::WindProfiles::CConstant::operator=( const CConstant& oOther )
{
	this->m_windVector = oOther.m_windVector;
	return *this;
}


VistaVector3D ITAGeo::WindProfiles::CConstant::WindVector( const double& ) const
{
	return m_windVector;
}
VistaVector3D ITAGeo::WindProfiles::CConstant::WindVectorGradient( const double& ) const
{
	return VistaVector3D( 0, 0, 0 );
}

#pragma endregion


//---Zero Wind Profile---
//-----------------------
#pragma region No Wind( Zero )
ITAGeo::WindProfiles::CZero::CZero( ) : IWindProfile( WeatherProfileBase::EClassType::ZERO ) {}

ITAGeo::WindProfiles::CZero::CZero( const CZero& ) : IWindProfile( WeatherProfileBase::EClassType::ZERO ) {}

VistaVector3D ITAGeo::WindProfiles::CZero::WindVector( const double& ) const
{
	return VistaVector3D( 0, 0, 0 );
}
VistaVector3D ITAGeo::WindProfiles::CZero::WindVectorGradient( const double& ) const
{
	return VistaVector3D( 0, 0, 0 );
}
#pragma endregion


//-----Log Profile------
//----------------------
#pragma region Logarithmic Wind Profile

ITAGeo::WindProfiles::CLog::CLog( const double& frictionVelocity, const double& surfaceRough, const VistaVector3D& windDir )
    : IWindProfile( WeatherProfileBase::EClassType::LOG )
{
	if( windDir[Vista::Z] != 0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Z-component of wind direction must be zero." );

	m_surfaceRoughness = surfaceRough;
	m_windDirection    = windDir.GetNormalized( );

	m_velocityPrefactor = frictionVelocity / c_karmanConstant;
}

ITAGeo::WindProfiles::CLog::CLog( const CLog& oOther )
    : IWindProfile( WeatherProfileBase::EClassType::LOG )
    , m_velocityPrefactor( oOther.m_velocityPrefactor )
    , m_surfaceRoughness( oOther.m_surfaceRoughness )
    , m_windDirection( oOther.m_windDirection )
{
}

ITAGeo::WindProfiles::CLog& ITAGeo::WindProfiles::CLog::operator=( const CLog& oOther )
{
	this->m_velocityPrefactor = oOther.m_velocityPrefactor;
	this->m_surfaceRoughness  = oOther.m_surfaceRoughness;
	this->m_windDirection     = oOther.m_windDirection;
	return *this;
}


VistaVector3D ITAGeo::WindProfiles::CLog::WindVector( const double& altitude ) const
{
	double windVelocity = 0;
	if( altitude > m_surfaceRoughness )
		windVelocity = m_velocityPrefactor * log( altitude / m_surfaceRoughness );

	return m_windDirection * windVelocity;
}
VistaVector3D ITAGeo::WindProfiles::CLog::WindVectorGradient( const double& altitude ) const
{
	double windVelGradient = 0;
	if( altitude > m_surfaceRoughness )
		windVelGradient = m_velocityPrefactor / altitude;

	return m_windDirection * windVelGradient;
}

#pragma endregion


using namespace ITABase::Math;
//--------Imported Profile--------
//--------------------------------
#pragma region Imported Profile
std::vector<double> CalcXWindData( const std::vector<double>& vdWindVelocity, const std::vector<double>& vdWindAzimuthDeg )
{
	// Note, that azimuth angle is defined as 0� = wind from north, 90� = wind from east.
	// The convention for the resulting data is that x = wind towards east, y = wind towards north (East-North-Up system).
	if( vdWindVelocity.size( ) != vdWindAzimuthDeg.size( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Vectors with wind velocity and direction data must be equally long" );

	auto vdXWind = std::vector<double>( vdWindVelocity.size( ) );
	for( int idx = 0; idx < vdWindVelocity.size( ); idx++ )
		vdXWind[idx] = -vdWindVelocity[idx] * std::sinf( vdWindAzimuthDeg[idx] * M_PI / 180.0 );

	return vdXWind;
}
std::vector<double> CalcYWindData( const std::vector<double>& vdWindVelocity, const std::vector<double>& vdWindAzimuthDeg )
{
	// Note, that azimuth angle is defined as 0� = wind from north, 90� = wind from east.
	// The convention for the resulting data is that x = wind towards east, y = wind towards north (East-North-Up system).
	if( vdWindVelocity.size( ) != vdWindAzimuthDeg.size( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Vectors with wind velocity and direction data must be equally long" );

	auto vdYWind = std::vector<double>( vdWindVelocity.size( ) );
	for( int idx = 0; idx < vdWindVelocity.size( ); idx++ )
		vdYWind[idx] = -vdWindVelocity[idx] * std::cosf( vdWindAzimuthDeg[idx] * M_PI / 180.0 );

	return vdYWind;
}


ITAGeo::WindProfiles::CImport::CImport( ) : CImport( { 1.0, 2.0, 3.0, 4.0 }, { 1.0, 2.0, 3.0, 4.0 }, { 1.0, 2.0, 3.0, 4.0 } ) {}

ITAGeo::WindProfiles::CImport::CImport( const std::vector<double>& vdAltitude, const std::vector<double>& vdWindVelocity, const std::vector<double>& vdWindAzimuthDeg,
                                        bool bPrecalculate, int iMaxPrecalcAltitude )
    : CImport( vdAltitude, vdWindVelocity, CalcXWindData( vdWindVelocity, vdWindAzimuthDeg ), CalcYWindData( vdWindVelocity, vdWindAzimuthDeg ), bPrecalculate,
               iMaxPrecalcAltitude )
{
}

ITAGeo::WindProfiles::CImport::CImport( const std::vector<double>& vdAltitude, const std::vector<double>& vdWindVelocity, const std::vector<double>& vdWindDirX,
                                        const std::vector<double>& vdWindDirY, bool bPrecalculate, int iMaxPrecalcAltitude )
    : IWindProfile( WeatherProfileBase::EClassType::IMPORT )
    , IPrecalcProfileExtension( bPrecalculate, iMaxPrecalcAltitude )
    , m_vdAltitude( vdAltitude )
    , m_vdWindVelocity( vdWindVelocity )
    , m_vdWindDirectionX( vdWindDirX )
    , m_vdWindDirectionY( vdWindDirY )

    , m_polynomialWindX( CubicSpline( vdAltitude, m_vdWindDirectionX ) )
    , m_polynomialWindY( CubicSpline( vdAltitude, m_vdWindDirectionY ) )

    , m_polynomialWindGradientX( m_polynomialWindX.Derivation( ) )
    , m_polynomialWindGradientY( m_polynomialWindY.Derivation( ) )
{
	InitPrecalculation( );
}

ITAGeo::WindProfiles::CImport& ITAGeo::WindProfiles::CImport::operator=( const CImport& oOther )
{
	IPrecalcProfileExtension::operator=( oOther );

	m_vdAltitude       = oOther.m_vdAltitude;
	m_vdWindVelocity   = oOther.m_vdWindVelocity;
	m_vdWindDirectionX = oOther.m_vdWindDirectionX;
	m_vdWindDirectionY = oOther.m_vdWindDirectionY;

	m_polynomialWindX = oOther.m_polynomialWindX;
	m_polynomialWindY = oOther.m_polynomialWindY;

	m_polynomialWindGradientX = oOther.m_polynomialWindGradientX;
	m_polynomialWindGradientY = oOther.m_polynomialWindGradientY;

	m_vv3PrecalcWindVector         = oOther.m_vv3PrecalcWindVector;
	m_vv3PrecalcWindVectorGradient = oOther.m_vv3PrecalcWindVectorGradient;

	return *this;
}

VistaVector3D ITAGeo::WindProfiles::CImport::WindVector( const double& altitude ) const
{
	if( UsePrecalculatedData( ) )
	{
		const int iAltitude = NearestPrecalcAltitude( altitude );
		return m_vv3PrecalcWindVector[iAltitude];
	}
	return VistaVector3D( m_polynomialWindX.Evaluate( altitude, true ), m_polynomialWindY.Evaluate( altitude, true ), 0.0 );
}

VistaVector3D ITAGeo::WindProfiles::CImport::WindVectorGradient( const double& altitude ) const
{
	if( UsePrecalculatedData( ) )
	{
		const int iAltitude = NearestPrecalcAltitude( altitude );
		return m_vv3PrecalcWindVectorGradient[iAltitude];
	}
	return VistaVector3D( m_polynomialWindGradientX.Evaluate( altitude, true ), m_polynomialWindGradientY.Evaluate( altitude, true ), 0.0 );
}
void ITAGeo::WindProfiles::CImport::Precalculate( )
{
	const int iNumMaxValues = PrecalculationMaxAltitude( ) + 1;
	m_vv3PrecalcWindVector.resize( iNumMaxValues );
	m_vv3PrecalcWindVectorGradient.resize( iNumMaxValues );
	for( int iAltitude = 0; iAltitude < m_vv3PrecalcWindVector.size( ); iAltitude++ )
	{
		m_vv3PrecalcWindVector[iAltitude]         = WindVector( (double)iAltitude );
		m_vv3PrecalcWindVectorGradient[iAltitude] = WindVectorGradient( (double)iAltitude );
	}
}
#pragma endregion
