// ITAGeo includes
#include <ITAGeo/Atmosphere/TemperatureProfiles.h>

// ITABase includes
#include <ITABase/Math/Spline.h>
#include <ITAException.h>

// STL
#include <math.h>


//---Constant Profile---
//----------------------
#pragma region Constant Temperature

ITAGeo::TemperatureProfiles::CConstant::CConstant( const double& constTemperature, const double& staticPressure )
    : ITemperatureProfile( WeatherProfileBase::EClassType::CONSTANT )
    , m_temperature( constTemperature )
    , m_staticPressure( staticPressure )
{
}

ITAGeo::TemperatureProfiles::CConstant::CConstant( const CConstant& oOther )
    : ITemperatureProfile( WeatherProfileBase::EClassType::CONSTANT )
    , m_temperature( oOther.m_staticPressure )
    , m_staticPressure( oOther.m_temperature )
{
}

ITAGeo::TemperatureProfiles::CConstant& ITAGeo::TemperatureProfiles::CConstant::operator=( const CConstant& oOther )
{
	this->m_staticPressure = oOther.m_staticPressure;
	this->m_temperature    = oOther.m_temperature;
	return *this;
}

double ITAGeo::TemperatureProfiles::CConstant::StaticPressure( const double& ) const
{
	return m_staticPressure;
}
double ITAGeo::TemperatureProfiles::CConstant::Temperature( const double& ) const
{
	return m_temperature;
}
double ITAGeo::TemperatureProfiles::CConstant::TemperatureGradient( const double& ) const
{
	return 0;
}

#pragma endregion


//-------Room-------
//------------------
#pragma region Room Temperature
double ITAGeo::TemperatureProfiles::CRoom::StaticPressure( const double& ) const
{
	return 101325;
}
double ITAGeo::TemperatureProfiles::CRoom::Temperature( const double& ) const
{
	return 293.15;
}
double ITAGeo::TemperatureProfiles::CRoom::TemperatureGradient( const double& ) const
{
	return 0;
}
#pragma endregion


//-------ISA-------
//-----------------
#pragma region International Standard Atmosphere( ISA )

double ITAGeo::TemperatureProfiles::CISA::StaticPressure( const double& altitude ) const
{
	double p0 = 101325;
	if( altitude > 0 )
	{
		double T0 = 288.15;
		p0 *= pow( 1 - 0.0065 * altitude / T0, 5.2561 );
	}
	return p0;
}
double ITAGeo::TemperatureProfiles::CISA::Temperature( const double& altitude ) const
{
	double T0 = 288.15;
	if( altitude > 0 )
	{
		T0 -= 0.0065 * altitude;
	}
	return T0;
}
double ITAGeo::TemperatureProfiles::CISA::TemperatureGradient( const double& ) const
{
	return -0.0065;
}
#pragma endregion


using namespace ITABase::Math;
//-------Import-------
//--------------------
#pragma region Import

ITAGeo::TemperatureProfiles::CImport::CImport( ) : CImport( { 1.0, 2.0, 3.0, 4.0 }, { 1.0, 2.0, 3.0, 4.0 }, { 1.0, 2.0, 3.0, 4.0 } ) {}

ITAGeo::TemperatureProfiles::CImport::CImport( const std::vector<double>& vdAltitude, const std::vector<double>& vdTemperature,
                                               const std::vector<double>& vdStaticPressure, bool bPrecalculate, int iMaxPrecalcAltitude )
    : ITemperatureProfile( WeatherProfileBase::EClassType::IMPORT )
    , IPrecalcProfileExtension( bPrecalculate, iMaxPrecalcAltitude )
    , m_vdAltitude( vdAltitude )
    , m_vdTemperature( vdTemperature )
    , m_vdStaticPressure( vdStaticPressure )

    , m_polynomialStaticPressure( CubicSpline( vdAltitude, vdStaticPressure ) )
    , m_polynomialTemperature( CubicSpline( vdAltitude, vdTemperature ) )
    , m_polynomialTemperatureGradient( m_polynomialTemperature.Derivation( ) )
{
	InitPrecalculation( );
}

ITAGeo::TemperatureProfiles::CImport& ITAGeo::TemperatureProfiles::CImport::operator=( const CImport& oOther )
{
	IPrecalcProfileExtension::operator=( oOther );

	m_vdAltitude       = oOther.m_vdAltitude;
	m_vdTemperature    = oOther.m_vdTemperature;
	m_vdStaticPressure = oOther.m_vdStaticPressure;

	m_polynomialStaticPressure      = oOther.m_polynomialStaticPressure;
	m_polynomialTemperature         = oOther.m_polynomialTemperature;
	m_polynomialTemperatureGradient = oOther.m_polynomialTemperatureGradient;

	m_vdPrecalcStaticPressure      = oOther.m_vdPrecalcStaticPressure;
	m_vdPrecalcTemperature         = oOther.m_vdPrecalcTemperature;
	m_vdPrecalcTemperatureGradient = oOther.m_vdPrecalcTemperatureGradient;

	return *this;
}

double ITAGeo::TemperatureProfiles::CImport::StaticPressure( const double& altitude ) const
{
	if( UsePrecalculatedData( ) )
	{
		const int iAltitude = NearestPrecalcAltitude( altitude );
		return m_vdPrecalcStaticPressure[iAltitude];
	}
	return m_polynomialStaticPressure.Evaluate( altitude, true );
}

double ITAGeo::TemperatureProfiles::CImport::Temperature( const double& altitude ) const
{
	if( UsePrecalculatedData( ) )
	{
		const int iAltitude = NearestPrecalcAltitude( altitude );
		return m_vdPrecalcTemperature[iAltitude];
	}
	return m_polynomialTemperature.Evaluate( altitude, true );
}

double ITAGeo::TemperatureProfiles::CImport::TemperatureGradient( const double& altitude ) const
{
	if( UsePrecalculatedData( ) )
	{
		const int iAltitude = NearestPrecalcAltitude( altitude );
		return m_vdPrecalcTemperatureGradient[iAltitude];
	}
	return m_polynomialTemperatureGradient.Evaluate( altitude, true );
}


void ITAGeo::TemperatureProfiles::CImport::Precalculate( )
{
	const int iNumMaxValues = PrecalculationMaxAltitude( ) + 1;
	m_vdPrecalcStaticPressure.resize( iNumMaxValues );
	m_vdPrecalcTemperature.resize( iNumMaxValues );
	m_vdPrecalcTemperatureGradient.resize( iNumMaxValues );
	for( int iAltitude = 0; iAltitude < m_vdPrecalcStaticPressure.size( ); iAltitude++ )
	{
		m_vdPrecalcStaticPressure[iAltitude]      = StaticPressure( (double)iAltitude );
		m_vdPrecalcTemperature[iAltitude]         = Temperature( (double)iAltitude );
		m_vdPrecalcTemperatureGradient[iAltitude] = TemperatureGradient( (double)iAltitude );
	}
}
#pragma endregion
