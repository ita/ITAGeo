// ITAGeo includes
#include <ITAGeo/Atmosphere/StratifiedAtmosphere.h>
#include <ITAGeo/Atmosphere/WeatherProfileBase.h>

// Vista includes
#include <VistaTools/VistaFileSystemFile.h>

// STD includes
#include <fstream>

using namespace ITAGeo::WeatherProfileBase;

ITAGeo::CStratifiedAtmosphere::CStratifiedAtmosphere( )
{
	pHumidityProfile    = std::make_shared<HumidityProfiles::CConstant>( 50 );
	pTemperatureProfile = std::make_shared<TemperatureProfiles::CISA>( );
	pWindProfile        = std::make_shared<WindProfiles::CLog>( 0.6, 0.1, VistaVector3D( 1, 0, 0 ) );
}

ITAGeo::CStratifiedAtmosphere::CStratifiedAtmosphere( std::shared_ptr<TemperatureProfiles::ITemperatureProfile> temperature,
                                                      std::shared_ptr<WindProfiles::IWindProfile> wind, std::shared_ptr<HumidityProfiles::IHumidityProfile> humidity )
    : pTemperatureProfile( temperature )
    , pWindProfile( wind )
    , pHumidityProfile( humidity )
{
}

double ITAGeo::CStratifiedAtmosphere::RelativeHumidity( double altitude ) const
{
	return pHumidityProfile->RelativeHumidity( altitude );
}


double ITAGeo::CStratifiedAtmosphere::StaticPressure( double altitude ) const
{
	return pTemperatureProfile->StaticPressure( altitude );
}
double ITAGeo::CStratifiedAtmosphere::Temperature( double altitude ) const
{
	return pTemperatureProfile->Temperature( altitude );
}
double ITAGeo::CStratifiedAtmosphere::TemperatureGradient( double altitude ) const
{
	return pTemperatureProfile->TemperatureGradient( altitude );
}

VistaVector3D ITAGeo::CStratifiedAtmosphere::WindVector( double altitude ) const
{
	return pWindProfile->WindVector( altitude );
}
VistaVector3D ITAGeo::CStratifiedAtmosphere::WindVectorGradient( double altitude ) const
{
	return pWindProfile->WindVectorGradient( altitude );
}

bool ITAGeo::CStratifiedAtmosphere::InsideBounds( double altitude ) const
{
	double dMaxAltitude;
	if( pTemperatureProfile->HasMaximumAltitude( dMaxAltitude ) && altitude > dMaxAltitude )
		return false;
	if( pWindProfile->HasMaximumAltitude( dMaxAltitude ) && altitude > dMaxAltitude )
		return false;
	if( pHumidityProfile->HasMaximumAltitude( dMaxAltitude ) && altitude > dMaxAltitude )
		return false;

	return true;
}


double ITAGeo::CStratifiedAtmosphere::SpeedOfSound( double altitude ) const
{
	return sqrt( airGasConstant_times_ratioSpecificHeats * Temperature( altitude ) );
}
double ITAGeo::CStratifiedAtmosphere::SpeedOfSoundGradient( double altitude ) const
{
	return 0.5 * sqrt( airGasConstant_times_ratioSpecificHeats / Temperature( altitude ) ) * TemperatureGradient( altitude );
}