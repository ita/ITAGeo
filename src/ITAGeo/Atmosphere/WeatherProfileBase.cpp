// ITAGeo includes
#include <ITAGeo/Atmosphere/WeatherProfileBase.h>
#include <StratifiedAtmosphere_instrumentation.h>

// STL
#include <algorithm>
#include <cmath>

std::string ITAGeo::WeatherProfileBase::IWeatherProfile::GetProfileTypeStr( const EProfileType& iType )
{
	switch( iType )
	{
		case EProfileType::HUMIDITY:
			return "humidity_profile";
		case EProfileType::TEMPERATURE:
			return "temperature_profile";
		case EProfileType::WIND:
			return "wind_profile";
		default:
			return "Unknown";
	}
}

std::string ITAGeo::WeatherProfileBase::IWeatherProfile::GetProfileClassStr( const EClassType& iType )
{
	switch( iType )
	{
		case EClassType::CONSTANT:
			return "constant";
		case EClassType::IMPORT:
			return "import";
		case EClassType::ROOM:
			return "room";
		case EClassType::ISA:
			return "isa";
		case EClassType::ZERO:
			return "zero";
		case EClassType::LOG:
			return "log";
		default:
			return "Unknown";
	}
}

ITAGeo::WeatherProfileBase::EProfileType ITAGeo::WeatherProfileBase::IWeatherProfile::ParseProfileType( const std::string& sTypeStr )
{
	for( size_t i = 0; i < EProfileType::NUM_PROFILE_TYPES; i++ )
	{
		if( sTypeStr == GetProfileTypeStr( (EProfileType)i ) )
		{
			return (EProfileType)i;
		}
	}

	return EProfileType::INVALID_PROFILE;
}

ITAGeo::WeatherProfileBase::EClassType ITAGeo::WeatherProfileBase::IWeatherProfile::ParseProfileClassType( const std::string& sTypeStr )
{
	for( size_t i = 0; i < EClassType::NUM_PROFILE_CLASS_TYPES; i++ )
	{
		if( sTypeStr == GetProfileClassStr( (EClassType)i ) )
		{
			return (EClassType)i;
		}
	}

	return EClassType::INVALID_PROFILE_CLASS;
}


ITAGeo::WeatherProfileBase::IPrecalcProfileExtension::IPrecalcProfileExtension( bool bPrecalculate, int iMaxPrecalcAltitude )
    : bPrecalculate( bPrecalculate )
    , iMaxPrecalcAltitude( iMaxPrecalcAltitude )
{
}

void ITAGeo::WeatherProfileBase::IPrecalcProfileExtension::InitPrecalculation( )
{
	if( bPrecalcInitialized )
		return;

	if( bPrecalculate )
		Precalculate( );
	bPrecalcInitialized = true;
}

int ITAGeo::WeatherProfileBase::IPrecalcProfileExtension::NearestPrecalcAltitude( const double& dAltitude ) const
{
	const int iAltitude = std::max( 0, (int)std::round( dAltitude ) );
	return std::min( iMaxPrecalcAltitude, iAltitude );
}

bool ITAGeo::WeatherProfileBase::IPrecalcProfileExtension::MaximumAltitudeRelevant( double& dMaxAltitude ) const
{
	const bool bHasMaxAltitude = PrecalculationEnabled( );
	if( bHasMaxAltitude )
		dMaxAltitude = PrecalculationMaxAltitude( );
	return bHasMaxAltitude;
}

ITAGeo::WeatherProfileBase::IPrecalcProfileExtension& ITAGeo::WeatherProfileBase::IPrecalcProfileExtension::operator=( const IPrecalcProfileExtension& oOther )
{
	this->bPrecalculate       = oOther.bPrecalculate;
	this->iMaxPrecalcAltitude = oOther.iMaxPrecalcAltitude;
	this->bPrecalcInitialized = oOther.bPrecalcInitialized;
	return *this;
}
