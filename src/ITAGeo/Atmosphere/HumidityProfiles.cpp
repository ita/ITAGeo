// ITAGeo includes
#include <ITAGeo/Atmosphere/HumidityProfiles.h>

// ITABase includes
#include <ITABase/Math/Spline.h>


//---Constant Profile---
//----------------------
#pragma region Constant Humidity

ITAGeo::HumidityProfiles::CConstant::CConstant( const double& relativeHumidityPerc )
    : IHumidityProfile( WeatherProfileBase::EClassType::CONSTANT )
    , m_relativeHumidity( relativeHumidityPerc )
{
	if( !( 0.0f <= relativeHumidityPerc && relativeHumidityPerc <= 100.0f ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Relative humidity must be a value in percent between 0 and 100." );
}

ITAGeo::HumidityProfiles::CConstant::CConstant( const CConstant& oOther )
    : IHumidityProfile( WeatherProfileBase::EClassType::CONSTANT )
    , m_relativeHumidity( oOther.m_relativeHumidity )
{
}

ITAGeo::HumidityProfiles::CConstant& ITAGeo::HumidityProfiles::CConstant::operator=( const CConstant& oOther )
{
	m_relativeHumidity = oOther.m_relativeHumidity;
	return *this;
};

double ITAGeo::HumidityProfiles::CConstant::RelativeHumidity( const double& ) const
{
	return m_relativeHumidity;
}

#pragma endregion

using namespace ITABase::Math;
//--------Import--------
//----------------------
#pragma region Import

ITAGeo::HumidityProfiles::CImport::CImport( ) : CImport( { 1.0, 2.0, 3.0, 4.0 }, { 1.0, 2.0, 3.0, 4.0 } ) {}

ITAGeo::HumidityProfiles::CImport::CImport( const std::vector<double>& vdAltitude, const std::vector<double>& vdHumidity, bool bPrecalculate, int iMaxPrecalcAltitude )
    : IHumidityProfile( WeatherProfileBase::EClassType::IMPORT )
    , IPrecalcProfileExtension( bPrecalculate, iMaxPrecalcAltitude )
    , m_vdAltitude( vdAltitude )
    , m_vdHumidity( vdHumidity )
    , m_polynomialRelativeHumidity( CubicSpline( vdAltitude, vdHumidity ) )
{
	InitPrecalculation( );
}

ITAGeo::HumidityProfiles::CImport& ITAGeo::HumidityProfiles::CImport::operator=( const CImport& oOther )
{
	IPrecalcProfileExtension::operator=( oOther );
	m_vdAltitude                 = oOther.m_vdAltitude;
	m_vdHumidity                 = oOther.m_vdHumidity;
	m_polynomialRelativeHumidity = oOther.m_polynomialRelativeHumidity;
	m_vdPrecalcHumidity          = oOther.m_vdPrecalcHumidity;
	return *this;
}

double ITAGeo::HumidityProfiles::CImport::RelativeHumidity( const double& altitude ) const
{
	if( UsePrecalculatedData( ) )
	{
		const int iAltitude = NearestPrecalcAltitude( altitude );
		return m_vdPrecalcHumidity[iAltitude];
	}
	return m_polynomialRelativeHumidity.Evaluate( altitude, true );
}

void ITAGeo::HumidityProfiles::CImport::Precalculate( )
{
	const int iNumMaxValues = PrecalculationMaxAltitude( ) + 1;
	m_vdPrecalcHumidity.resize( iNumMaxValues );
	for( int iAltitude = 0; iAltitude < m_vdPrecalcHumidity.size( ); iAltitude++ )
	{
		m_vdPrecalcHumidity[iAltitude] = RelativeHumidity( (double)iAltitude );
	}
}

#pragma endregion