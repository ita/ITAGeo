#include <ITAException.h>
#include <ITAGeo/ResourceManager.h>

ITAGeo::CResourceManager::CResourceManager( std::shared_ptr<Material::CMaterialManager> pMM, std::shared_ptr<Directivity::CDirectivityManager> pDM )
{
	if( !pMM || !pDM )
		ITA_EXCEPT_INVALID_PARAMETER( "Could not create a resource manager with invalid or nullpointer material or directivity manager" );

	m_pMaterialManager    = pMM;
	m_pDirectivityManager = pDM;
}

ITAGeo::CResourceManager::CResourceManager( std::shared_ptr<Material::CMaterialManager> pMM )
    : CResourceManager( pMM, std::make_shared<Directivity::CDirectivityManager>( ) )
{
}
ITAGeo::CResourceManager::CResourceManager( std::shared_ptr<Directivity::CDirectivityManager> pDM )
    : CResourceManager( std::make_shared<Material::CMaterialManager>( ), pDM )
{
}

std::shared_ptr<ITAGeo::Material::CMaterialManager> ITAGeo::CResourceManager::GetMaterialManager( ) const
{
	return m_pMaterialManager;
}

std::shared_ptr<ITAGeo::Directivity::CDirectivityManager> ITAGeo::CResourceManager::GetDirectivityManager( ) const
{
	return m_pDirectivityManager;
}

bool ITAGeo::CResourceManager::HasMaterial( const std::string& sMaterialIdentifier ) const
{
	return m_pMaterialManager->HasMaterial( sMaterialIdentifier );
}

std::shared_ptr<ITAGeo::Material::IMaterial> ITAGeo::CResourceManager::GetMaterial( const std::string& sMaterialIdentifier ) const
{
	return m_pMaterialManager->GetMaterial( sMaterialIdentifier );
}

bool ITAGeo::CResourceManager::HasDirectivity( const std::string& sDirectivityIdentifier ) const
{
	return m_pDirectivityManager->HasDirectivity( sDirectivityIdentifier );
}

std::shared_ptr<ITAGeo::Directivity::IDirectivity> ITAGeo::CResourceManager::GetDirectivity( const std::string& sID ) const
{
	return m_pDirectivityManager->GetDirectivity( sID );
}

std::string ITAGeo::CResourceManager::GetDirectivityID( std::shared_ptr<ITAGeo::Directivity::IDirectivity> pDirectivity ) const
{
	return m_pDirectivityManager->GetDirectivityID( pDirectivity );
}

bool ITAGeo::CResourceManager::AddDirectivity( const std::string& sIdentifier, std::shared_ptr<ITAGeo::Directivity::IDirectivity> pDirectivity )
{
	return m_pDirectivityManager->AddDirectivity( sIdentifier, pDirectivity );
}

bool ITAGeo::CResourceManager::AddMaterial( const std::string& sIdentifier, std::shared_ptr<ITAGeo::Material::IMaterial> pMat )
{
	return m_pMaterialManager->AddMaterial( sIdentifier, pMat );
}