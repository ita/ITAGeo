﻿/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_GEO_UTILS_JSON_VISTA
#define IW_ITA_GEO_UTILS_JSON_VISTA

#include <ITAGeo/Definitions.h>
#include <VistaBase/VistaQuaternion.h>
#include <VistaBase/VistaVector3D.h>
#include <nlohmann/json.hpp>
using namespace nlohmann;

namespace ITAGeo
{
	namespace Utils
	{
		namespace JSON
		{
			//---BASIC CLASSES ENCODING/DECODING---

			//! Create json array from given Vista3DVector
			/**
			 * @param[in] v3Vector Vector to be encoded
			 * @param[in] encodeW If enabled, the fourth parameter of the VistaVector3D (W) is encoded (default:off), otherwise not
			 */
			inline json CreateNode( const VistaVector3D& v3Vector, bool encodeW = false )
			{
				json jsonArray  = json::array( );
				const int iSize = encodeW ? 4 : 3;
				for( int idx = 0; idx < iSize; idx++ )
					jsonArray.push_back( v3Vector[idx] );

				return jsonArray;
			};

			//! Parse properties from json array to given Vista3DVector
			/**
			 * @param[in] v3Vector Target vector for decoding
			 * @param[in] jsonArray json array with data to be decoded
			 * If the fourth variable (W) did not get encoded, it is set to the default value of 1.
			 */
			inline void Decode( VistaVector3D& v3Vector, const json& jsonArray )
			{
				assert( jsonArray.size( ) >= 3 );

				double dW = 1;
				if( jsonArray.size( ) > 3 )
					dW = jsonArray[4];

				v3Vector.SetValues( jsonArray[0], jsonArray[1], jsonArray[2], dW );
			};


			//! Create json array from given VistaQuaternion
			/**
			 * @param[in] qQuat VistaQuaternion to be encoded
			 */
			inline json CreateNode( const VistaQuaternion& qQuat )
			{
				json jsonArray = json::array( );
				for( int idx = 0; idx < 4; idx++ )
					jsonArray.push_back( qQuat[idx] );

				return jsonArray;
			};

			//! Parse properties from json array to given VistaQuaternion
			/**
			 * @param[in] qQuat Target quaternion for decoding
			 * @param[in] jsonArray json array with data to be decoded
			 */
			inline void Decode( VistaQuaternion& qQuat, const json& jsonArray )
			{
				assert( jsonArray.size( ) == 4 );
				for( int idx = 0; idx < 4; idx++ )
					qQuat[idx] = jsonArray[idx];
			};
		} // namespace JSON
	} // namespace Utils
} // namespace ITAGeo

#endif // IW_ITA_GEO_UTILS_JSON_VISTA
