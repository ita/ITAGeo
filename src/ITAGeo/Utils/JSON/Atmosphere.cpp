﻿#include "StdJSON.h"
#include "VistaJSON.h"

#include <ITAException.h>
#include <ITAGeo/Utils/JSON/Atmosphere.h>

// Vista includes
#include <VistaTools/VistaFileSystemFile.h>
#include <fstream>
#include <nlohmann/json.hpp>
using namespace nlohmann;
#define JSON_INDENT 3

// TODO: Check which Decode / Encode are really required to have a public interface.
//		Maybe it would be good to move code to inline functions that only create or parse json nodes which is then used by the Decode / Encode functions (see
// PropagationPath_Impl.h)

#pragma region ATMOSPHERE - EXPORT / IMPORT
void ITAGeo::Utils::JSON::Export( const CStratifiedAtmosphere& cAtmosphere, const std::string& sTargetFilePath, bool bOverwrite /* = false*/ )
{
	VistaFileSystemFile oFile( sTargetFilePath );
	if( oFile.Exists( ) && !bOverwrite )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Rejected to store stratified atmosphere to file '" + sTargetFilePath + "', overwrite was protected" );

	std::ofstream fsOut( sTargetFilePath );
	fsOut << Encode( cAtmosphere ).str( );
	fsOut.close( );
}

void ITAGeo::Utils::JSON::Import( CStratifiedAtmosphere& cAtmosphere, const std::string& sFilePath )
{
	VistaFileSystemFile oFile( sFilePath );
	if( !oFile.Exists( ) || !oFile.IsFile( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Could not load stratified atmosphere, file '" + sFilePath + "' not found or not a file" );

	std::ifstream inFile( sFilePath );
	std::string sContent( ( std::istreambuf_iterator<char>( inFile ) ), ( std::istreambuf_iterator<char>( ) ) );

	Decode( cAtmosphere, sContent );
}

#pragma region ATMOSPHERE - ENCODE / DECODE
std::stringstream ITAGeo::Utils::JSON::Encode( const CStratifiedAtmosphere& cAtmosphere )
{
	json jnRoot;

	if( !cAtmosphere.HumidityProfile( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Humidity Profile is not defined" );
	if( !cAtmosphere.TemperatureProfile( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Temperature Profile is not defined" );
	if( !cAtmosphere.WindProfile( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Wind Profile is not defined" );

	jnRoot[ITA_HUMIDITY_PROFILE_STR]    = json::parse( FromHumidityProfile( *cAtmosphere.HumidityProfile( ) ).str( ) );
	jnRoot[ITA_TEMPERATURE_PROFILE_STR] = json::parse( FromTemperatureProfile( *cAtmosphere.TemperatureProfile( ) ).str( ) );
	jnRoot[ITA_WIND_PROFILE_STR]        = json::parse( FromWindProfile( *cAtmosphere.WindProfile( ) ).str( ) );

	std::stringstream ss;
	ss << jnRoot.dump( JSON_INDENT );
	return ss;
}
void ITAGeo::Utils::JSON::Decode( CStratifiedAtmosphere& oAtmosphere, const std::string& sContent )
{
	json jnRoot = json::parse( sContent );

	if( jnRoot.find( ITA_HUMIDITY_PROFILE_STR ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Humidity profile data not found. Missing key: '" + ITA_HUMIDITY_PROFILE_STR + "'" );
	if( jnRoot.find( ITA_TEMPERATURE_PROFILE_STR ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Temperature profile data not found. Missing key: '" + ITA_TEMPERATURE_PROFILE_STR + "'" );
	if( jnRoot.find( ITA_WIND_PROFILE_STR ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Wind profile data not found. Missing key: '" + ITA_WIND_PROFILE_STR + "'" );

	json jnHumidity = jnRoot[ITA_HUMIDITY_PROFILE_STR];
	oAtmosphere.SetHumidityProfile( ToHumidityProfile( jnHumidity.dump( ) ) );

	json jnTemperature = jnRoot[ITA_TEMPERATURE_PROFILE_STR];
	oAtmosphere.SetTemperatureProfile( ToTemperatureProfile( jnTemperature.dump( ) ) );

	json jnWind = jnRoot[ITA_WIND_PROFILE_STR];
	oAtmosphere.SetWindProfile( ToWindProfile( jnWind.dump( ) ) );
}
#pragma endregion

#pragma region WEATHER PROFILES - FACTORY FUNCTIONS
// ---Humidity Profiles---
// -----------------------
ITA_GEO_API std::stringstream ITAGeo::Utils::JSON::FromHumidityProfile( const HumidityProfiles::IHumidityProfile& iProfile )
{
	switch( iProfile.iClassType )
	{
		case WeatherProfileBase::CONSTANT:
		{
			auto cProfile = static_cast<const HumidityProfiles::CConstant&>( iProfile );
			return Encode( cProfile );
		}
		case WeatherProfileBase::IMPORT:
		{
			auto cProfile = static_cast<const HumidityProfiles::CImport&>( iProfile );
			return Encode( cProfile );
		}
		default:
			break;
	}
	ITA_EXCEPT_INVALID_PARAMETER( "Cannot convert humidity profile to JSON string. No valid class type found." );
}
std::shared_ptr<ITAGeo::HumidityProfiles::IHumidityProfile> ITAGeo::Utils::JSON::ToHumidityProfile( const std::string& sContent )
{
	json jnRoot = json::parse( sContent );
	if( jnRoot.find( "class" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'class'" );
	WeatherProfileBase::EClassType classType = WeatherProfileBase::IWeatherProfile::ParseProfileClassType( jnRoot["class"] );

	switch( classType )
	{
		case WeatherProfileBase::CONSTANT:
		{
			auto cProfile = std::make_shared<HumidityProfiles::CConstant>( );
			Decode( *cProfile, sContent );
			return cProfile;
		}
		case WeatherProfileBase::IMPORT:
		{
			auto cProfile = std::make_shared<HumidityProfiles::CImport>( );
			Decode( *cProfile, sContent );
			return cProfile;
		}
		default:
			break;
	}
	ITA_EXCEPT_INVALID_PARAMETER( "Cannot conver JSON string to humidity profile. No valid class type found." );
}

// ---Temperature Profiles---
// --------------------------
ITA_GEO_API std::stringstream ITAGeo::Utils::JSON::FromTemperatureProfile( const TemperatureProfiles::ITemperatureProfile& iProfile )
{
	switch( iProfile.iClassType )
	{
		case WeatherProfileBase::CONSTANT:
		{
			auto cProfile = static_cast<const TemperatureProfiles::CConstant&>( iProfile );
			return Encode( cProfile );
		}
		case WeatherProfileBase::ROOM:
		{
			auto cProfile = static_cast<const TemperatureProfiles::CRoom&>( iProfile );
			return Encode( cProfile );
		}
		case WeatherProfileBase::ISA:
		{
			auto cProfile = static_cast<const TemperatureProfiles::CISA&>( iProfile );
			return Encode( cProfile );
		}
		case WeatherProfileBase::IMPORT:
		{
			auto cProfile = static_cast<const TemperatureProfiles::CImport&>( iProfile );
			return Encode( cProfile );
		}
		default:
			break;
	}
	ITA_EXCEPT_INVALID_PARAMETER( "Cannot conver humidity Profile to JSON string. No valid class type found." );
}
std::shared_ptr<ITAGeo::TemperatureProfiles::ITemperatureProfile> ITAGeo::Utils::JSON::ToTemperatureProfile( const std::string& sContent )
{
	json jnRoot = json::parse( sContent );
	if( jnRoot.find( "class" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'class'" );
	WeatherProfileBase::EClassType classType = WeatherProfileBase::IWeatherProfile::ParseProfileClassType( jnRoot["class"] );

	switch( classType )
	{
		case WeatherProfileBase::CONSTANT:
		{
			auto cProfile = std::make_shared<TemperatureProfiles::CConstant>( 0, 0 );
			Decode( *cProfile, sContent );
			return cProfile;
		}
		case WeatherProfileBase::ROOM:
		{
			auto cProfile = std::make_shared<TemperatureProfiles::CRoom>( );
			Decode( *cProfile, sContent );
			return cProfile;
		}
		break;
		case WeatherProfileBase::ISA:
		{
			auto cProfile = std::make_shared<TemperatureProfiles::CISA>( );
			Decode( *cProfile, sContent );
			return cProfile;
		}
		case WeatherProfileBase::IMPORT:
		{
			auto cProfile = std::make_shared<TemperatureProfiles::CImport>( );
			Decode( *cProfile, sContent );
			return cProfile;
		}
		break;
		default:
			break;
	}
	ITA_EXCEPT_INVALID_PARAMETER( "Cannot conver JSON string to humidity profile. No valid class type found." );
}

// ---Wind Profiles---
// -------------------
std::stringstream ITAGeo::Utils::JSON::FromWindProfile( const WindProfiles::IWindProfile& iProfile )
{
	switch( iProfile.iClassType )
	{
		case WeatherProfileBase::CONSTANT:
		{
			auto cProfile = static_cast<const WindProfiles::CConstant&>( iProfile );
			return Encode( cProfile );
		}
		case WeatherProfileBase::ZERO:
		{
			auto cProfile = static_cast<const WindProfiles::CZero&>( iProfile );
			return Encode( cProfile );
		}
		case WeatherProfileBase::LOG:
		{
			auto cProfile = static_cast<const WindProfiles::CLog&>( iProfile );
			return Encode( cProfile );
		}
		case WeatherProfileBase::IMPORT:
		{
			auto cProfile = static_cast<const WindProfiles::CImport&>( iProfile );
			return Encode( cProfile );
		}
		default:
			break;
	}
	ITA_EXCEPT_INVALID_PARAMETER( "Cannot conver wind profile to JSON string. No valid class type found." );
}
std::shared_ptr<ITAGeo::WindProfiles::IWindProfile> ITAGeo::Utils::JSON::ToWindProfile( const std::string& sContent )
{
	json jnRoot = json::parse( sContent );
	if( jnRoot.find( "class" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'class'" );
	WeatherProfileBase::EClassType classType = WeatherProfileBase::IWeatherProfile::ParseProfileClassType( jnRoot["class"] );

	switch( classType )
	{
		case WeatherProfileBase::CONSTANT:
		{
			auto cProfile = std::make_shared<WindProfiles::CConstant>( 0, VistaVector3D( 1, 0, 0 ) );
			Decode( *cProfile, sContent );
			return cProfile;
		}
		case WeatherProfileBase::ZERO:
		{
			auto cProfile = std::make_shared<WindProfiles::CZero>( );
			Decode( *cProfile, sContent );
			return cProfile;
		}
		case WeatherProfileBase::LOG:
		{
			auto cProfile = std::make_shared<WindProfiles::CLog>( 0, 0, VistaVector3D( 1, 0, 0 ) );
			Decode( *cProfile, sContent );
			return cProfile;
		}
		case WeatherProfileBase::IMPORT:
		{
			auto cProfile = std::make_shared<WindProfiles::CImport>( );
			Decode( *cProfile, sContent );
			return cProfile;
		}
		default:
			break;
	}
	ITA_EXCEPT_INVALID_PARAMETER( "Cannot conver JSON string to humidity profile. No valid class type found." );
}
#pragma endregion

#pragma region WEATHER PROFILES - ENCODING / DECODING
// ---Weather Profile Base---
// --------------------------
#pragma region Weather Profile Base
std::stringstream ITAGeo::Utils::JSON::Encode( const ITAGeo::WeatherProfileBase::IWeatherProfile& iProfile )
{
	json jnRoot;
	jnRoot["type"]  = iProfile.GetProfileTypeStr( );
	jnRoot["class"] = iProfile.GetProfileClassStr( );

	std::stringstream ss;
	ss << jnRoot.dump( JSON_INDENT );
	return ss;
}
void ITAGeo::Utils::JSON::Decode( const WeatherProfileBase::IWeatherProfile& iProfile, const std::string& sContent )
{
	json jnRoot = json::parse( sContent );

	if( jnRoot.find( "type" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'type'" );
	if( jnRoot.find( "class" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'class'" );

	if( jnRoot["type"] != iProfile.GetProfileTypeStr( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Expecting a JSON node with type " + iProfile.GetProfileTypeStr( ) );

	const std::string sClassName = jnRoot["class"];
	if( sClassName != iProfile.GetProfileClassStr( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "The json string doesn't contain properties of class " + iProfile.GetProfileClassStr( ) + ". It contains the properties of " +
		                              sClassName + "." );
}
void EncodePrecalcProfileExtention( const ITAGeo::WeatherProfileBase::IPrecalcProfileExtension& iProfile, json& jnRoot )
{
	jnRoot["precalculation_enabled"] = iProfile.PrecalculationEnabled( );
	if( iProfile.PrecalculationEnabled( ) )
		jnRoot["precalculation_max_altitude"] = iProfile.PrecalculationMaxAltitude( );
}
void DecodePrecalcProfileExtension( const json& jnRoot, bool& bPrecalculate, int& iMaxPrecalcAltitude )
{
	if( jnRoot.find( "precalculation_enabled" ) != jnRoot.end( ) )
		bPrecalculate = jnRoot["precalculation_enabled"];
	else
		bPrecalculate = false;
	if( jnRoot.find( "precalculation_max_altitude" ) != jnRoot.end( ) )
		iMaxPrecalcAltitude = jnRoot["precalculation_max_altitude"];
	else
		iMaxPrecalcAltitude = ITA_WEATHERPROFILE_PRECALC_MAXALTITUDE_DEFAULT;
}
#pragma endregion

// ---Humidity Profiles---
// -----------------------
#pragma region Humidity Profiles
std::stringstream ITAGeo::Utils::JSON::Encode( const HumidityProfiles::CConstant& iProfile )
{
	std::string sRoot = Encode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ) ).str( );
	json jnRoot       = json::parse( sRoot );

	jnRoot["relative_humidity"] = iProfile.m_relativeHumidity;

	std::stringstream ss;
	ss << jnRoot.dump( JSON_INDENT );
	return ss;
}
void ITAGeo::Utils::JSON::Decode( HumidityProfiles::CConstant& iProfile, const std::string& sContent )
{
	Decode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ), sContent );

	json jnRoot = json::parse( sContent );
	if( jnRoot.find( "relative_humidity" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'relative_humidity'" );
	iProfile.m_relativeHumidity = jnRoot["relative_humidity"];
}

std::stringstream ITAGeo::Utils::JSON::Encode( const HumidityProfiles::CImport& iProfile )
{
	std::string sRoot = Encode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ) ).str( );
	json jnRoot       = json::parse( sRoot );

	jnRoot["altitude_values"] = CreateNode( iProfile.m_vdAltitude );
	jnRoot["humidity_values"] = CreateNode( iProfile.m_vdHumidity );

	EncodePrecalcProfileExtention( iProfile, jnRoot );

	std::stringstream ss;
	ss << jnRoot.dump( JSON_INDENT );
	return ss;
}
void ITAGeo::Utils::JSON::Decode( HumidityProfiles::CImport& iProfile, const std::string& sContent )
{
	Decode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ), sContent );

	std::vector<double> vfAltitude, vfHumidity;
	json jnRoot = json::parse( sContent );
	if( jnRoot.find( "altitude_values" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'altitude_values'" );
	if( jnRoot.find( "humidity_values" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'humidity_values'" );

	Decode( vfAltitude, jnRoot["altitude_values"] );
	Decode( vfHumidity, jnRoot["humidity_values"] );

	bool bPrecalculate;
	int iMaxPrecalcAltitude;
	DecodePrecalcProfileExtension( jnRoot, bPrecalculate, iMaxPrecalcAltitude );

	iProfile = HumidityProfiles::CImport( vfAltitude, vfHumidity, bPrecalculate, iMaxPrecalcAltitude );
}
#pragma endregion

// ---Temperature Profiles---
// --------------------------
#pragma region Temperature Profiles
std::stringstream ITAGeo::Utils::JSON::Encode( const TemperatureProfiles::CConstant& iProfile )
{
	std::string sRoot = Encode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ) ).str( );
	json jnRoot       = json::parse( sRoot );

	jnRoot["temperature"]     = iProfile.m_temperature;
	jnRoot["static_pressure"] = iProfile.m_staticPressure;

	std::stringstream ss;
	ss << jnRoot.dump( JSON_INDENT );
	return ss;
}
void ITAGeo::Utils::JSON::Decode( TemperatureProfiles::CConstant& iProfile, const std::string& sContent )
{
	Decode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ), sContent );

	json jnRoot = json::parse( sContent );
	if( jnRoot.find( "temperature" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'temperature'" );
	if( jnRoot.find( "static_pressure" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'static_pressure'" );

	iProfile.m_temperature    = jnRoot["temperature"];
	iProfile.m_staticPressure = jnRoot["static_pressure"];
}

std::stringstream ITAGeo::Utils::JSON::Encode( const TemperatureProfiles::CImport& iProfile )
{
	std::string sRoot = Encode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ) ).str( );
	json jnRoot       = json::parse( sRoot );

	jnRoot["altitude_values"]        = CreateNode( iProfile.m_vdAltitude );
	jnRoot["static_pressure_values"] = CreateNode( iProfile.m_vdStaticPressure );
	jnRoot["temperature_values"]     = CreateNode( iProfile.m_vdTemperature );

	EncodePrecalcProfileExtention( iProfile, jnRoot );

	std::stringstream ss;
	ss << jnRoot.dump( JSON_INDENT );
	return ss;
}
void ITAGeo::Utils::JSON::Decode( TemperatureProfiles::CImport& iProfile, const std::string& sContent )
{
	Decode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ), sContent );

	std::vector<double> vdAltitude, vdStaticPressure, vdTemperature;
	json jnRoot = json::parse( sContent );
	if( jnRoot.find( "altitude_values" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'altitude_values'" );
	if( jnRoot.find( "static_pressure_values" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'static_pressure_values'" );
	if( jnRoot.find( "temperature_values" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'temperature_values'" );

	Decode( vdAltitude, jnRoot["altitude_values"] );
	Decode( vdStaticPressure, jnRoot["static_pressure_values"] );
	Decode( vdTemperature, jnRoot["temperature_values"] );

	bool bPrecalculate;
	int iMaxPrecalcAltitude;
	DecodePrecalcProfileExtension( jnRoot, bPrecalculate, iMaxPrecalcAltitude );

	iProfile = TemperatureProfiles::CImport( vdAltitude, vdTemperature, vdStaticPressure, bPrecalculate, iMaxPrecalcAltitude );
}
#pragma endregion

// ---Wind Profiles---
// -------------------
#pragma region Wind Profiles
std::stringstream ITAGeo::Utils::JSON::Encode( const WindProfiles::CConstant& iProfile )
{
	std::string sRoot = Encode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ) ).str( );
	json jnRoot       = json::parse( sRoot );

	jnRoot["wind_vector"] = CreateNode( iProfile.m_windVector, false );

	std::stringstream ss;
	ss << jnRoot.dump( JSON_INDENT );
	return ss;
}
void ITAGeo::Utils::JSON::Decode( WindProfiles::CConstant& iProfile, const std::string& sContent )
{
	Decode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ), sContent );

	json jnRoot = json::parse( sContent );
	if( jnRoot.find( "wind_vector" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'wind_vector'" );

	json jnWindVector = jnRoot["wind_vector"];
	Decode( iProfile.m_windVector, jnWindVector );
}

std::stringstream ITAGeo::Utils::JSON::Encode( const WindProfiles::CLog& iProfile )
{
	std::string sRoot = Encode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ) ).str( );
	json jnRoot       = json::parse( sRoot );

	jnRoot["friction_velocity"] = iProfile.m_velocityPrefactor * iProfile.c_karmanConstant;
	jnRoot["surface_roughness"] = iProfile.m_surfaceRoughness;
	jnRoot["wind_direction"]    = CreateNode( iProfile.m_windDirection, false );

	std::stringstream ss;
	ss << jnRoot.dump( JSON_INDENT );
	return ss;
}
void ITAGeo::Utils::JSON::Decode( WindProfiles::CLog& iProfile, const std::string& sContent )
{
	Decode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ), sContent );

	json jnRoot = json::parse( sContent );
	if( jnRoot.find( "friction_velocity" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'friction_velocity'" );
	if( jnRoot.find( "surface_roughness" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'surface_roughness'" );
	if( jnRoot.find( "wind_direction" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'wind_direction'" );

	const double frictionVelocity = jnRoot["friction_velocity"];
	iProfile.m_velocityPrefactor  = frictionVelocity / iProfile.c_karmanConstant;
	iProfile.m_surfaceRoughness   = jnRoot["surface_roughness"];
	json jnWindDirection          = jnRoot["wind_direction"];
	Decode( iProfile.m_windDirection, jnWindDirection );
}
std::stringstream ITAGeo::Utils::JSON::Encode( const WindProfiles::CImport& iProfile )
{
	std::string sRoot = Encode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ) ).str( );
	json jnRoot       = json::parse( sRoot );

	json jnAltitude = CreateNode( iProfile.m_vdAltitude );
	json jnVelocity = CreateNode( iProfile.m_vdWindVelocity );

	json jnWindDir = json::array( );
	for( int idx = 0; idx < iProfile.m_vdWindDirectionX.size( ); idx++ )
	{
		jnWindDir.push_back( { iProfile.m_vdWindDirectionX[idx], iProfile.m_vdWindDirectionY[idx] } );
	}

	jnRoot["altitude_values"]   = jnAltitude;
	jnRoot["velocity_values"]   = jnVelocity;
	jnRoot["direction_vectors"] = jnWindDir;

	EncodePrecalcProfileExtention( iProfile, jnRoot );

	std::stringstream ss;
	ss << jnRoot.dump( JSON_INDENT );
	return ss;
}
void ITAGeo::Utils::JSON::Decode( WindProfiles::CImport& iProfile, const std::string& sContent )
{
	Decode( static_cast<WeatherProfileBase::IWeatherProfile>( iProfile ), sContent );

	std::vector<double> vdAltitude, vdWindVel, vdWindDirX, vdWindDirY;

	json jnRoot = json::parse( sContent );
	if( jnRoot.find( "altitude_values" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'altitude_values'" );
	if( jnRoot.find( "velocity_values" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'velocity_values'" );
	if( jnRoot.find( "direction_vectors" ) == jnRoot.end( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Missing JSON node with key: 'direction_vectors'" );

	Decode( vdAltitude, jnRoot["altitude_values"] );
	Decode( vdWindVel, jnRoot["velocity_values"] );

	json jnDirectionVectors = jnRoot["direction_vectors"];
	vdWindDirX.resize( jnDirectionVectors.size( ) );
	vdWindDirY.resize( jnDirectionVectors.size( ) );
	for( int idx = 0; idx < jnDirectionVectors.size( ); idx++ )
	{
		json jnVector   = jnDirectionVectors[idx];
		vdWindDirX[idx] = jnVector[0];
		vdWindDirY[idx] = jnVector[1];
	}

	bool bPrecalculate;
	int iMaxPrecalcAltitude;
	DecodePrecalcProfileExtension( jnRoot, bPrecalculate, iMaxPrecalcAltitude );

	iProfile = WindProfiles::CImport( vdAltitude, vdWindVel, vdWindDirX, vdWindDirY, bPrecalculate, iMaxPrecalcAltitude );
}
#pragma endregion

#pragma endregion