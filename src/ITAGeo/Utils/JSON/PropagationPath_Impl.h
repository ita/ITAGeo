﻿/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_GEO_UTILS_JSON_PROPAGATIONPATH_IMPL
#define IW_ITA_GEO_UTILS_JSON_PROPAGATIONPATH_IMPL

#include <ITAGeo/Base.h>

// External
#include <nlohmann/json.hpp>
using namespace nlohmann;

#include <ITAGeo/ResourceManager.h>
#include <memory>

namespace ITAGeo
{
	namespace Utils
	{
		namespace JSON
		{
			//---PROPAGATION ANCHORS---

			json CreateNode( const CPropagationAnchor& anchor );
			void Decode( CPropagationAnchor& anchor, const json& jnRoot );

			json CreateNode( const CEmitter& anchor, std::shared_ptr<CResourceManager> pResourceManager );
			void Decode( CEmitter& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager );

			json CreateNode( const CSensor& anchor, std::shared_ptr<CResourceManager> pResourceManager );
			void Decode( CSensor& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager );

			json CreateNode( const CMirrorImage& anchor );
			void Decode( CMirrorImage& anchor, const json& jnRoot );

			json CreateNode( const CSpecularReflection& anchor, std::shared_ptr<CResourceManager> pResourceManager );
			void Decode( CSpecularReflection& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager );

			inline json CreateNode( const CDetectionPoint& anchor )
			{
				return CreateNode( (CPropagationAnchor&)( anchor ) );
			}
			inline void Decode( CDetectionPoint& anchor, const json& jnRoot )
			{
				Decode( (CPropagationAnchor&)( anchor ), jnRoot );
			}

			//---PROPAGATION ANCHORS - INHOMOGENOUS MEDIA---

			void ExtendNode( const CInhomogeneousAnchorExtension& anchorExtension, json& jnRoot );
			void Decode( CInhomogeneousAnchorExtension& anchorExtension, const json& jnRoot );

			json CreateNode( const CInhomogeneity& anchor );
			void Decode( CInhomogeneity& anchor, const json& jnRoot );

			json CreateNode( const CEmitterInhomogeneous& anchor, std::shared_ptr<CResourceManager> pResourceManager );
			void Decode( CEmitterInhomogeneous& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager );

			json CreateNode( const CSensorInhomogeneous& anchor, std::shared_ptr<CResourceManager> pResourceManager );
			void Decode( CSensorInhomogeneous& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager );

			json CreateNode( const CSpecularReflectionInhomogeneous& anchor, std::shared_ptr<CResourceManager> pResourceManager );
			void Decode( CSpecularReflectionInhomogeneous& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager );

			//---PROPAGATION ANCHORS - DIFFRACTION---

			json CreateNode( const CITADiffractionWedgeApertureBase& anchor );
			void Decode( CITADiffractionWedgeApertureBase& anchor, const json& jnRoot );

			//---MATERIALS---

			json CreateNode( ITAGeo::Material::IMaterial* material );

			json CreateDatabaseNode( const CPropagationPathList& pathList );

			//---PROPAGATION PATH---

			json CreateNode( const CPropagationPath& path, std::shared_ptr<CResourceManager> pResourceManager );
			void Decode( CPropagationPath& path, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager );

			//---PROPAGATION PATH LIST---
			json CreateNode( const CPropagationPathList& pathList, std::shared_ptr<CResourceManager> pResourceManager );
			void Decode( CPropagationPathList& pathList, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager );
		} // namespace JSON
	} // namespace Utils
} // namespace ITAGeo

#endif // IW_ITA_GEO_UTILS_JSON_PROPAGATIONPATH_IMPL
