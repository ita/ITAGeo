﻿#include "PropagationPath_Impl.h"

#include "VistaJSON.h"

#include <ITAException.h>

// STD
#include <cmath>
#include <set>
#include <stdexcept>


using namespace ITAGeo;
using namespace ITAGeo::Utils;


// --------PROPAGATION ANCHORS - BASIC--------
// -------------------------------------------
#pragma region Propagation Anchors
json JSON::CreateNode( const CPropagationAnchor& anchor )
{
	json jnRoot;
	jnRoot["class"]             = anchor.GetTypeStr( );
	jnRoot["anchor_type"]       = anchor.GetAnchorTypeStr( );
	jnRoot["interaction_point"] = CreateNode( anchor.v3InteractionPoint );

	return jnRoot;
}

void JSON::Decode( CPropagationAnchor& anchor, const json& jnRoot )
{
	const std::string sClass = jnRoot["class"];
	if( sClass != anchor.GetTypeStr( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "The json string doesn't contain properties of type" + anchor.GetTypeStr( ) + ". It contains the properties of " + sClass + "." );

	json jnPosition = jnRoot.at( "interaction_point" );
	Decode( anchor.v3InteractionPoint, jnPosition );

	anchor.iAnchorType = anchor.ParseAnchorType( jnRoot["anchor_type"] );
}

json JSON::CreateNode( const CEmitter& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	json jnRoot           = CreateNode( CPropagationAnchor( anchor ) );
	jnRoot["name"]        = anchor.sName;
	jnRoot["orientation"] = CreateNode( anchor.qOrient );
	if( pResourceManager && anchor.pDirectivity )
		jnRoot["directivity_id"] = anchor.pDirectivity->GetName( );
	return jnRoot;
}

void JSON::Decode( CEmitter& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( (CPropagationAnchor&)( anchor ), jnRoot );

	if( jnRoot.find( "name" ) != jnRoot.end( ) )
		anchor.sName = jnRoot["name"];

	if( jnRoot.find( "orientation" ) != jnRoot.end( ) )
		Decode( anchor.qOrient, jnRoot["orientation"] );

	if( jnRoot.find( "directivity_id" ) != jnRoot.end( ) )
	{
		const std::string sDirectivityID = jnRoot["directivity_id"];
		if( pResourceManager && pResourceManager->HasDirectivity( sDirectivityID ) )
			anchor.pDirectivity = pResourceManager->GetDirectivity( sDirectivityID );
	}
}

json JSON::CreateNode( const CSensor& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	json jnRoot           = CreateNode( (CPropagationAnchor&)( anchor ) );
	jnRoot["name"]        = anchor.sName;
	jnRoot["orientation"] = CreateNode( anchor.qOrient );
	if( pResourceManager && anchor.pDirectivity )
		jnRoot["directivity_id"] = anchor.pDirectivity->GetName( );

	return jnRoot;
}

void JSON::Decode( CSensor& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( (CPropagationAnchor&)( anchor ), jnRoot );

	if( jnRoot.find( "name" ) != jnRoot.end( ) )
		anchor.sName = jnRoot["name"];

	if( jnRoot.find( "orientation" ) != jnRoot.end( ) )
		Decode( anchor.qOrient, jnRoot["orientation"] );

	if( jnRoot.find( "directivity_id" ) != jnRoot.end( ) )
	{
		const std::string sDirectivityID = jnRoot["directivity_id"];
		if( pResourceManager && pResourceManager->HasDirectivity( sDirectivityID ) )
			anchor.pDirectivity = pResourceManager->GetDirectivity( sDirectivityID );
	}
}

json JSON::CreateNode( const CMirrorImage& anchor )
{
	json jnRoot             = CreateNode( (CPropagationAnchor&)( anchor ) );
	jnRoot["mirror_normal"] = CreateNode( anchor.v3MirrorNormal );
	jnRoot["polygon_index"] = anchor.iPolygonIndex;
	jnRoot["order"]         = anchor.iOrder;

	return jnRoot;
}

void JSON::Decode( CMirrorImage& anchor, const json& jnRoot )
{
	Decode( (CPropagationAnchor&)( anchor ), jnRoot );

	Decode( anchor.v3MirrorNormal, jnRoot["orientation"] );

	if( jnRoot.find( "polygon_index" ) != jnRoot.end( ) )
		anchor.iPolygonIndex = jnRoot["polygon_index"];

	if( jnRoot.find( "order" ) != jnRoot.end( ) )
		anchor.iOrder = jnRoot["order"];
}

json JSON::CreateNode( const CSpecularReflection& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	json jnRoot          = CreateNode( (CPropagationAnchor&)( anchor ) );
	jnRoot["polygon_id"] = anchor.iPolygonID;

	if( !anchor.v3FaceNormal.GetIsZeroVector( ) )
		jnRoot["face_normal"] = CreateNode( anchor.v3FaceNormal );

	if( anchor.pMaterial && pResourceManager )
		jnRoot["material_id"] = anchor.pMaterial->GetIdentifier( );

	return jnRoot;
}

void JSON::Decode( CSpecularReflection& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( (CPropagationAnchor&)( anchor ), jnRoot );

	if( jnRoot.find( "polygon_id" ) != jnRoot.end( ) )
		anchor.iPolygonID = jnRoot["polygon_id"];

	if( jnRoot.find( "face_normal" ) != jnRoot.end( ) )
		Decode( anchor.v3FaceNormal, jnRoot["face_normal"] );

	if( jnRoot.find( "material_id" ) != jnRoot.end( ) )
	{
		std::string sMaterialID = jnRoot["material_id"];
		if( pResourceManager && pResourceManager->HasMaterial( sMaterialID ) )
			anchor.pMaterial = pResourceManager->GetMaterial( sMaterialID );
	}
}
#pragma endregion


// --------PROPAGATION ANCHORS - INHOMOGENEOUS MEDIA--------
// ---------------------------------------------------------
#pragma region Propagation Anchors - Inhomogeneous Media
void JSON::ExtendNode( const CInhomogeneousAnchorExtension& anchorExtension, json& jnRoot )
{
	jnRoot["wavefront_normal"] = CreateNode( anchorExtension.v3WavefrontNormal );
	jnRoot["time_stamp"]       = anchorExtension.dTimeStamp;
}

void JSON::Decode( CInhomogeneousAnchorExtension& anchorExtension, const json& jnRoot )
{
	Decode( anchorExtension.v3WavefrontNormal, jnRoot["wavefront_normal"] );
	anchorExtension.dTimeStamp = jnRoot["time_stamp"];
}

json JSON::CreateNode( const CInhomogeneity& anchor )
{
	json jnRoot = CreateNode( (CPropagationAnchor&)( anchor ) );
	ExtendNode( CInhomogeneousAnchorExtension( anchor ), jnRoot );
	return jnRoot;
}

void JSON::Decode( CInhomogeneity& anchor, const json& jnRoot )
{
	Decode( (CPropagationAnchor&)( anchor ), jnRoot );
	Decode( (CInhomogeneousAnchorExtension&)( anchor ), jnRoot );
}

json JSON::CreateNode( const CEmitterInhomogeneous& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	json jnRoot = CreateNode( (CPropagationAnchor&)( anchor ) );
	ExtendNode( (CInhomogeneousAnchorExtension&)( anchor ), jnRoot );
	return jnRoot;
}

void JSON::Decode( CEmitterInhomogeneous& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( (CPropagationAnchor&)( anchor ), jnRoot );
	Decode( (CInhomogeneousAnchorExtension&)( anchor ), jnRoot );
}

json JSON::CreateNode( const CSensorInhomogeneous& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	json jnRoot = CreateNode( (CPropagationAnchor&)( anchor ) );
	ExtendNode( (CInhomogeneousAnchorExtension&)( anchor ), jnRoot );
	if( anchor.dSpreadingLoss > 0 )
	{
		// Old JSON library (libjson) could not parse values below 0.000001. Storing in dB still seems reasonable.
		jnRoot["spreading_loss_db"] = 20 * std::log10( anchor.dSpreadingLoss );
		// jnRoot["spreading_loss"] = anchor.dSpreadingLoss;
	}
	jnRoot["is_eigenray"]            = anchor.bEigenray;
	jnRoot["receiver_hit"]           = anchor.bReceiverHit;
	jnRoot["ray_zooming_iterations"] = anchor.iRayZoomingIterations;

	return jnRoot;
}

void JSON::Decode( CSensorInhomogeneous& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( (CPropagationAnchor&)( anchor ), jnRoot );
	Decode( (CInhomogeneousAnchorExtension&)( anchor ), jnRoot );
	if( jnRoot.find( "spreading_loss" ) != jnRoot.end( ) )
		anchor.dSpreadingLoss = jnRoot["spreading_loss"];
	else if( jnRoot.find( "spreading_loss_db" ) != jnRoot.end( ) )
		anchor.dSpreadingLoss = std::pow( 10, jnRoot["spreading_loss_db"] / 20.0 );

	if( jnRoot.find( "is_eigenray" ) != jnRoot.end( ) )
		anchor.bEigenray = jnRoot["is_eigenray"];
	if( jnRoot.find( "receiver_hit" ) != jnRoot.end( ) )
		anchor.bReceiverHit = jnRoot["receiver_hit"];
	if( jnRoot.find( "ray_zooming_iterations" ) != jnRoot.end( ) )
		anchor.iRayZoomingIterations = jnRoot["ray_zooming_iterations"];
}

json JSON::CreateNode( const CSpecularReflectionInhomogeneous& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	json jnRoot = CreateNode( (CPropagationAnchor&)( anchor ) );
	ExtendNode( (CInhomogeneousAnchorExtension&)( anchor ), jnRoot );
	return jnRoot;
}

void JSON::Decode( CSpecularReflectionInhomogeneous& anchor, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( (CPropagationAnchor&)( anchor ), jnRoot );
	Decode( (CInhomogeneousAnchorExtension&)( anchor ), jnRoot );
}
#pragma endregion


// --------PROPAGATION ANCHORS - DIFFRACTION--------
// -------------------------------------------------
#pragma region Propagation Anchors - Diffraction
json JSON::CreateNode( const CITADiffractionWedgeApertureBase& anchor )
{
	json jnRoot                          = CreateNode( (CPropagationAnchor&)( anchor ) );
	jnRoot["vertex_start"]               = CreateNode( anchor.v3VertextStart );
	jnRoot["vertex_end"]                 = CreateNode( anchor.v3VertextEnd );
	jnRoot["main_wedge_face_normal"]     = CreateNode( anchor.v3MainWedgeFaceNormal );
	jnRoot["opposite_wedge_face_normal"] = CreateNode( anchor.v3OppositeWedgeFaceNormal );
	jnRoot["main_wedge_face_id"]         = anchor.iMainWedgeFaceID;
	jnRoot["opposite_wedge_face_id"]     = anchor.iOppositeWedgeFaceID;

	return jnRoot;
}
void JSON::Decode( CITADiffractionWedgeApertureBase& anchor, const json& jnRoot )
{
	Decode( (CPropagationAnchor&)( anchor ), jnRoot );

	Decode( anchor.v3VertextStart, jnRoot["vertex_start"] );
	Decode( anchor.v3VertextEnd, jnRoot["vertex_end"] );
	Decode( anchor.v3MainWedgeFaceNormal, jnRoot["main_wedge_face_normal"] );
	Decode( anchor.v3OppositeWedgeFaceNormal, jnRoot["opposite_wedge_face_normal"] );

	if( jnRoot.find( "main_wedge_face_id" ) != jnRoot.end( ) )
		anchor.iMainWedgeFaceID = jnRoot["main_wedge_face_id"];
	if( jnRoot.find( "opposite_wedge_face_id" ) != jnRoot.end( ) )
		anchor.iOppositeWedgeFaceID = jnRoot["opposite_wedge_face_id"];
}

#pragma endregion


// --------PROPAGATION PATH--------
// --------------------------------
#pragma region Propagation Path

json JSON::CreateNode( const CPropagationPath& path, std::shared_ptr<CResourceManager> pResourceManager )
{
	json jnPropagationPath;
	jnPropagationPath["class"]      = path.GetTypeStr( );
	jnPropagationPath["identifier"] = path.sIdentifier;

	json jnAnchorArray = json::array( );
	for( auto pAnchor: path )
	{
		json jnAnchor;
		switch( pAnchor->iAnchorType )
		{
			case CPropagationAnchor::EAnchorType::ACOUSTIC_EMITTER:
			{
				std::shared_ptr<CEmitter> pEmitter = std::static_pointer_cast<CEmitter>( pAnchor );
				jnAnchor                           = CreateNode( *pEmitter, pResourceManager );
			}
			break;
			case CPropagationAnchor::EAnchorType::ACOUSTIC_SENSOR:
			{
				std::shared_ptr<CSensor> pSensor = std::static_pointer_cast<CSensor>( pAnchor );
				jnAnchor                         = CreateNode( *pSensor, pResourceManager );
			}
			break;
			case CPropagationAnchor::EAnchorType::MIRROR_IMAGE:
			{
				std::shared_ptr<CMirrorImage> pMirrorImage = std::static_pointer_cast<CMirrorImage>( pAnchor );
				jnAnchor                                   = CreateNode( *pMirrorImage );
			}
			break;
			case CPropagationAnchor::EAnchorType::SPECULAR_REFLECTION:
			{
				std::shared_ptr<CSpecularReflection> pSpecularReflection = std::static_pointer_cast<CSpecularReflection>( pAnchor );
				jnAnchor                                                 = CreateNode( *pSpecularReflection, pResourceManager );
			}
			break;
			case CPropagationAnchor::EAnchorType::INHOMOGENEITY:
			{
				std::shared_ptr<CInhomogeneity> pInhomogeneity = std::static_pointer_cast<CInhomogeneity>( pAnchor );
				jnAnchor                                       = CreateNode( *pInhomogeneity );
			}
			break;
			case CPropagationAnchor::EAnchorType::INHOMOGENEITY_EMITTER:
			{
				std::shared_ptr<CEmitterInhomogeneous> pInhomogeneity = std::static_pointer_cast<CEmitterInhomogeneous>( pAnchor );
				jnAnchor                                              = CreateNode( *pInhomogeneity, pResourceManager );
			}
			break;
			case CPropagationAnchor::EAnchorType::INHOMOGENEITY_SENSOR:
			{
				std::shared_ptr<CSensorInhomogeneous> pInhomogeneity = std::static_pointer_cast<CSensorInhomogeneous>( pAnchor );
				jnAnchor                                             = CreateNode( *pInhomogeneity, pResourceManager );
			}
			break;
			case CPropagationAnchor::EAnchorType::INHOMOGENEITY_SPECULAR_REFLECTION:
			{
				std::shared_ptr<CSpecularReflectionInhomogeneous> pInhomogeneity = std::static_pointer_cast<CSpecularReflectionInhomogeneous>( pAnchor );
				jnAnchor                                                         = CreateNode( *pInhomogeneity, pResourceManager );
			}
			break;
			case CPropagationAnchor::EAnchorType::DIFFRACTION_OUTER_APEX:
			case CPropagationAnchor::EAnchorType::DIFFRACTION_INNER_APEX:
			{
				std::shared_ptr<CITADiffractionWedgeApertureBase> pDiffractionApex = std::static_pointer_cast<CITADiffractionWedgeApertureBase>( pAnchor );
				jnAnchor                                                           = CreateNode( *pDiffractionApex );
			}
			break;
			default:
			{
				jnAnchor = CreateNode( *pAnchor );
			}
			break;
		}

		jnAnchorArray.push_back( jnAnchor );
	}
	jnPropagationPath["propagation_anchors"] = jnAnchorArray;

	return jnPropagationPath;
}

void JSON::Decode( CPropagationPath& path, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager )
{
	// Check for correct class type of the content
	const std::string sClass = jnRoot["class"];
	if( sClass != path.GetTypeStr( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "The json string doesn't contain properties of type " + path.GetTypeStr( ) + ". It contains the properties of " + sClass + "." );

	path.sIdentifier = jnRoot["identifier"];


	json jnPropagationAnchors = jnRoot["propagation_anchors"];
	path.reserve( jnPropagationAnchors.size( ) );
	for( json jnAnchor: jnPropagationAnchors )
	{
		CPropagationAnchor::EAnchorType eAnchorType = CPropagationAnchor::ParseAnchorType( jnAnchor["anchor_type"] );

		switch( eAnchorType )
		{
			case CPropagationAnchor::EAnchorType::ACOUSTIC_EMITTER:
			{
				auto pEmitter = std::make_shared<CEmitter>( );
				Decode( *pEmitter, jnAnchor, pResourceManager );
				path.push_back( pEmitter );
			}
			break;
			case CPropagationAnchor::EAnchorType::ACOUSTIC_SENSOR:
			{
				auto pSensor = std::make_shared<CSensor>( );
				Decode( *pSensor, jnAnchor, pResourceManager );
				path.push_back( pSensor );
			}
			break;
			case CPropagationAnchor::EAnchorType::MIRROR_IMAGE:
			{
				auto pMirrorImage = std::make_shared<CMirrorImage>( );
				Decode( *pMirrorImage, jnAnchor );
				path.push_back( pMirrorImage );
			}
			break;
			case CPropagationAnchor::EAnchorType::SPECULAR_REFLECTION:
			{
				auto pSpecularReflection = std::make_shared<CSpecularReflection>( );
				Decode( *pSpecularReflection, jnAnchor, pResourceManager );
				path.push_back( pSpecularReflection );
			}
			break;
			case CPropagationAnchor::EAnchorType::INHOMOGENEITY:
			{
				auto pInhomogeneity = std::make_shared<CInhomogeneity>( );
				Decode( *pInhomogeneity, jnAnchor );
				path.push_back( pInhomogeneity );
			}
			break;
			case CPropagationAnchor::EAnchorType::INHOMOGENEITY_EMITTER:
			{
				auto pInhomogeneity = std::make_shared<CEmitterInhomogeneous>( );
				Decode( *pInhomogeneity, jnAnchor, pResourceManager );
				path.push_back( pInhomogeneity );
			}
			break;
			case CPropagationAnchor::EAnchorType::INHOMOGENEITY_SENSOR:
			{
				auto pInhomogeneity = std::make_shared<CSensorInhomogeneous>( );
				Decode( *pInhomogeneity, jnAnchor, pResourceManager );
				path.push_back( pInhomogeneity );
			}
			break;
			case CPropagationAnchor::EAnchorType::INHOMOGENEITY_SPECULAR_REFLECTION:
			{
				auto pInhomogeneity = std::make_shared<CSpecularReflectionInhomogeneous>( );
				Decode( *pInhomogeneity, jnAnchor, pResourceManager );
				path.push_back( pInhomogeneity );
			}
			break;
			case CPropagationAnchor::EAnchorType::DIFFRACTION_OUTER_APEX:
			{
				auto pDiffraction = std::make_shared<CITADiffractionOuterWedgeAperture>( );
				Decode( *pDiffraction, jnAnchor );
				path.push_back( pDiffraction );
			}
			break;
			case CPropagationAnchor::EAnchorType::DIFFRACTION_INNER_APEX:
			{
				auto pDiffraction = std::make_shared<CITADiffractionInnerWedgeAperture>( );
				Decode( *pDiffraction, jnAnchor );
				path.push_back( pDiffraction );
			}
			break;
			default:
			{
				auto pAnchor = std::make_shared<CPropagationAnchor>( );
				Decode( *pAnchor, jnAnchor );
				path.push_back( pAnchor );
			}
			break;
		}
	}
}
#pragma endregion


// --------MATERIALS--------
// -------------------------
#pragma region Materials
json JSON::CreateNode( ITAGeo::Material::IMaterial* material )
{
	int iType             = material->GetType( );
	std::string sClassStr = material->GetTypeStr( );
	std::string sTypeStr  = material->GetMaterialTypeStr( );

	json jnMaterial;
	jnMaterial["class"]     = sClassStr;
	jnMaterial["type_id"]   = iType;
	jnMaterial["type_name"] = sTypeStr;

	switch( iType )
	{
		case ITAGeo::Material::IMaterial::SCALAR:
		{
			auto* scalarMaterial            = dynamic_cast<ITAGeo::Material::CScalarMaterial*>( material );
			double fAbsorptionCoefficient   = scalarMaterial->GetAbsorptionCoefficient( );
			double dScatterCoefficient_real = scalarMaterial->cdScatteringCoefficient.real( );
			double dScatterCoefficient_imag = scalarMaterial->cdScatteringCoefficient.imag( );

			jnMaterial["absorption"]   = fAbsorptionCoefficient;
			jnMaterial["scatter_real"] = dScatterCoefficient_real;
			jnMaterial["scatter_imag"] = dScatterCoefficient_imag;
		}
		break;

		case ITAGeo::Material::IMaterial::THIRD_OCTAVE:
		{
			auto* thirdOctaveMaterial       = dynamic_cast<ITAGeo::Material::CThirdOctaveMaterial*>( material );
			auto vAbsorptionCoefficients    = thirdOctaveMaterial->oAbsorptionCoefficients.GetValues( );
			auto vScatterCoefficients       = thirdOctaveMaterial->oScatteringCoefficients.GetValues( );
			auto vImpedanceRealCoefficients = thirdOctaveMaterial->vfImpedanceRealCoefficients;
			auto vImpedanceImagCoefficients = thirdOctaveMaterial->vfImpedanceImagCoefficients;

			json jnAbsoprtionList( vAbsorptionCoefficients );
			json jnScatterList( vScatterCoefficients );
			json jnImpedanceRealList( vImpedanceRealCoefficients );
			json jnImpedanceImageList( vImpedanceImagCoefficients );

			jnMaterial["absorption"]    = jnAbsoprtionList;
			jnMaterial["scatter"]       = jnScatterList;
			jnMaterial["impedanceReal"] = jnImpedanceRealList;
			jnMaterial["impedanceImag"] = jnImpedanceImageList;
		}
		break;

		default:
			throw std::invalid_argument( "Material export failed. Class type '" + sTypeStr + "' not recognised." );
			break;
	}

	return jnMaterial;
}

json JSON::CreateDatabaseNode( const CPropagationPathList& pathList )
{
	json jnMaterials;

	// Get all Materials that are used and write them to json if not already written
	std::set<std::string> sUsedMaterials;
	for( auto path: pathList )
	{
		for( auto pAnchor: path )
		{
			if( pAnchor->iAnchorType != ITAGeo::CPropagationAnchor::SPECULAR_REFLECTION )
				continue;

			ITAGeo::CSpecularReflection& reflectionAnchor = dynamic_cast<ITAGeo::CSpecularReflection&>( *pAnchor );
			ITAGeo::Material::IMaterial* pMaterial        = reflectionAnchor.pMaterial.get( );
			// no material given
			if( pMaterial == nullptr )
				continue;
			std::string sName = pMaterial->GetName( );

			if( sUsedMaterials.count( sName ) == 0 )
			{
				jnMaterials[sName] = JSON::CreateNode( pMaterial );
				sUsedMaterials.insert( sName );
			}
		}
	}

	return jnMaterials;
}
#pragma endregion


// --------PROPAGATION PATH LIST--------
// --------------------------------
#pragma region Propagation Path List
json JSON::CreateNode( const CPropagationPathList& pathList, std::shared_ptr<CResourceManager> pResourceManager )
{
	json jnPropagationPathList;
	jnPropagationPathList["class"]      = pathList.GetTypeStr( );
	jnPropagationPathList["identifier"] = pathList.sIdentifier;

	json jnPathArray = json::array( );
	for( const CPropagationPath& pPath: pathList )
		jnPathArray.push_back( JSON::CreateNode( pPath, pResourceManager ) );

	// Add Material Database if available
	if( pResourceManager != nullptr )
	{
		auto pMaterialManager = pResourceManager->GetMaterialManager( );
		if( pMaterialManager != nullptr )
			jnPropagationPathList["material_database"] = CreateDatabaseNode( pathList );
	}
	jnPropagationPathList["propagation_paths"] = jnPathArray;

	return jnPropagationPathList;
}
void JSON::Decode( CPropagationPathList& pathList, const json& jnRoot, std::shared_ptr<CResourceManager> pResourceManager )
{
	const std::string sClass = jnRoot["class"];
	if( sClass != pathList.GetTypeStr( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "The json string doesn't contain properties of type" + pathList.GetTypeStr( ) + ". It contains the properties of " + sClass + "." );

	pathList.sIdentifier = jnRoot["identifier"];

	// Get the propagation anchors of the path
	json jnPropagationPaths = jnRoot["propagation_paths"];
	pathList.reserve( jnPropagationPaths.size( ) );
	for( json jnPath: jnPropagationPaths )
	{
		// Initialization of the emitter as a shared pointer
		CPropagationPath oPath;
		Decode( oPath, jnPath, pResourceManager );
		pathList.push_back( oPath );
	}

	// Reduce duplicates of BASIS emitters and sensors
	std::vector<std::shared_ptr<CEmitter>> vpEmitters;
	std::vector<std::shared_ptr<CSensor>> vpSensors;
	for( CPropagationPath path: pathList )
	{
		// Look for emitter at front and for sensor at back.
		// Do not filter emitters and sensors for inhomogeneous media

		bool isEmitterInhomogeneous = std::static_pointer_cast<CEmitterInhomogeneous>( path.front( ) ) != nullptr;
		bool isSensorInhomogeneous  = std::static_pointer_cast<CSensorInhomogeneous>( path.back( ) ) != nullptr;

		auto pCurrentEmitter = std::static_pointer_cast<CEmitter>( path.front( ) );
		auto pCurrentSensor  = std::static_pointer_cast<CSensor>( path.back( ) );

		bool bContainsEmitter = false, bContainsSensor = false;

		if( !isEmitterInhomogeneous && pCurrentEmitter != nullptr )
		{
			for( auto& pEmitter: vpEmitters )
			{
				if( pEmitter->v3InteractionPoint == pCurrentEmitter->v3InteractionPoint )
				{
					path.front( ) = pEmitter;

					bContainsEmitter = true;
					break;
				}
			}
			if( bContainsEmitter == false )
				vpEmitters.push_back( pCurrentEmitter );
		}

		if( !isSensorInhomogeneous && pCurrentSensor != nullptr )
		{
			for( auto& pSensor: vpSensors )
			{
				if( pSensor->v3InteractionPoint == pCurrentSensor->v3InteractionPoint )
				{
					path.back( ) = pSensor;

					bContainsSensor = true;
					break;
				}
			}
			if( bContainsSensor == false )
				vpSensors.push_back( pCurrentSensor );
		}
	}
	// TODO:
	// @todo get resource
}
#pragma endregion
