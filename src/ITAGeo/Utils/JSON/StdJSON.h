﻿/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_GEO_UTILS_JSON_STD
#define IW_ITA_GEO_UTILS_JSON_STD

// #include <ITAGeo/Definitions.h>

// STD
#include <nlohmann/json.hpp>
#include <vector>
using namespace nlohmann;

namespace ITAGeo
{
	namespace Utils
	{
		namespace JSON
		{
			//---BASIC CLASSES ENCODING/DECODING---

			//! Creates a json array from given std::vector<float>
			/**
			 * @param[in] vfArray Data vector to be encoded
			 */
			inline json CreateNode( const std::vector<float>& vfArray )
			{
				json jsonArray = json::array( );
				for( const float& fValue: vfArray )
					jsonArray.push_back( fValue );

				return jsonArray;
			};

			//! Creates a json array from given std::vector<double>
			/**
			 * @param[in] vdArray Data vector to be encoded
			 */
			inline json CreateNode( const std::vector<double>& vdArray )
			{
				json jsonArray = json::array( );
				for( const double& dValue: vdArray )
					jsonArray.push_back( dValue );

				return jsonArray;
			};

			//! Parse properties from json array to given std::vector<float>
			/**
			 * @param[in] vfArray Target vector for decoding (Note: old data will be lost)
			 * @param[in] jsonArray json array with data to be decoded
			 */
			inline void Decode( std::vector<float>& vfArray, const json& jsonArray )
			{
				vfArray.resize( jsonArray.size( ) );
				for( int idx = 0; idx < jsonArray.size( ); idx++ )
					vfArray[idx] = jsonArray[idx];
			};

			//! Parse properties from json array to given std::vector<double>
			/**
			 * @param[in] vdArray Target vector for decoding (Note: old data will be lost)
			 * @param[in] jsonArray json array with data to be decoded
			 */
			inline void Decode( std::vector<double>& vdArray, const json& jsonArray )
			{
				vdArray.resize( jsonArray.size( ) );
				for( int idx = 0; idx < jsonArray.size( ); idx++ )
					vdArray[idx] = jsonArray[idx];
			};
		} // namespace JSON
	} // namespace Utils
} // namespace ITAGeo

#endif // IW_ITA_GEO_UTILS_JSON_STD
