﻿#include "PropagationPath_Impl.h"

#include <ITAGeo/Utils/JSON/PropagationPath.h>

// #include "VistaVector3D.h"

// #include <ITAException.h>

// Vista includes
#include <VistaTools/VistaFileSystemFile.h>

// STD
#include <fstream>
// #include <memory>


#define JSON_INDENT 3
using namespace nlohmann;
using namespace ITAGeo;
using namespace ITAGeo::Utils;


// --------PROPAGATION ANCHORS - BASIC--------
// -------------------------------------------
#pragma region Propagation Anchor
std::string JSON::Encode( const CPropagationAnchor& anchor )
{
	return CreateNode( anchor ).dump( JSON_INDENT );
}
void JSON::Decode( CPropagationAnchor& anchor, const std::string& sContent )
{
	Decode( anchor, json::parse( sContent ) );
}
std::string JSON::Encode( const CEmitter& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	return CreateNode( anchor, pResourceManager ).dump( JSON_INDENT );
}
void JSON::Decode( CEmitter& anchor, const std::string& sContent, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( anchor, json::parse( sContent ), pResourceManager );
}
std::string JSON::Encode( const CSensor& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	return CreateNode( anchor, pResourceManager ).dump( JSON_INDENT );
}
void JSON::Decode( CSensor& anchor, const std::string& sContent, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( anchor, json::parse( sContent ), pResourceManager );
}
std::string JSON::Encode( const CMirrorImage& anchor )
{
	return CreateNode( anchor ).dump( JSON_INDENT );
}
void JSON::Decode( CMirrorImage& anchor, const std::string& sContent )
{
	Decode( anchor, json::parse( sContent ) );
}
std::string JSON::Encode( const CSpecularReflection& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	return CreateNode( anchor, pResourceManager ).dump( JSON_INDENT );
}
void JSON::Decode( CSpecularReflection& anchor, const std::string& sContent, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( anchor, json::parse( sContent ), pResourceManager );
}
std::string JSON::Encode( const CDetectionPoint& anchor )
{
	return CreateNode( anchor ).dump( JSON_INDENT );
}
void JSON::Decode( CDetectionPoint& anchor, const std::string& sContent )
{
	Decode( anchor, json::parse( sContent ) );
}
#pragma endregion


// --------PROPAGATION ANCHORS - INHOMOGENEOUS MEDIA--------
// ---------------------------------------------------------
#pragma region Propagation Anchors - Inhomogeneous Media
std::string JSON::Encode( const CInhomogeneity& anchor )
{
	return CreateNode( anchor ).dump( JSON_INDENT );
}
void JSON::Decode( CInhomogeneity& anchor, const std::string& sContent )
{
	Decode( anchor, json::parse( sContent ) );
}
std::string JSON::Encode( const CEmitterInhomogeneous& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	return CreateNode( anchor, pResourceManager ).dump( JSON_INDENT );
}
void JSON::Decode( CEmitterInhomogeneous& anchor, const std::string& sContent, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( anchor, json::parse( sContent ), pResourceManager );
}
std::string JSON::Encode( const CSensorInhomogeneous& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	return CreateNode( anchor, pResourceManager ).dump( JSON_INDENT );
}
void JSON::Decode( CSensorInhomogeneous& anchor, const std::string& sContent, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( anchor, json::parse( sContent ), pResourceManager );
}
std::string JSON::Encode( const CSpecularReflectionInhomogeneous& anchor, std::shared_ptr<CResourceManager> pResourceManager )
{
	return CreateNode( anchor, pResourceManager ).dump( JSON_INDENT );
}
void JSON::Decode( CSpecularReflectionInhomogeneous& anchor, const std::string& sContent, std::shared_ptr<CResourceManager> pResourceManager )
{
	Decode( anchor, json::parse( sContent ), pResourceManager );
}
#pragma endregion


// --------PROPAGATION ANCHORS - DIFFRACTION--------
// -------------------------------------------------
#pragma region Propagation Anchors - Diffraction
std::string JSON::Encode( const CITADiffractionWedgeApertureBase& anchor )
{
	return CreateNode( anchor ).dump( JSON_INDENT );
}
void JSON::Decode( CITADiffractionWedgeApertureBase& anchor, const std::string& sContent )
{
	Decode( anchor, json::parse( sContent ) );
}
#pragma endregion


// ------CPropagationPath------
// ----------------------------
#pragma region Propagation Path
void JSON::Export( const CPropagationPath& path, const std::string& sFilePath, bool bOverwrite, std::shared_ptr<ITAGeo::CResourceManager> pResourceManager )
{
	VistaFileSystemFile oFile( sFilePath );
	if( oFile.Exists( ) && !bOverwrite )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Rejected to store propagation path to file '" + sFilePath + "', overwrite was protected" );

	// Store propagation path to json file located in sFilePath
	std::ofstream out( sFilePath );

	out << Encode( path, pResourceManager );
	out.close( );
}
void JSON::Import( CPropagationPath& path, const std::string& sFilePath, std::shared_ptr<ITAGeo::CResourceManager> pResourceManager )
{
	VistaFileSystemFile oFile( sFilePath );
	if( !oFile.Exists( ) || !oFile.IsFile( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Could not load propagation path, file '" + sFilePath + "' not found or not a file" );

	std::ifstream inFile( sFilePath );

	//--Get content of file-----------------------------
	std::string sContent( ( std::istreambuf_iterator<char>( inFile ) ), ( std::istreambuf_iterator<char>( ) ) );

	Decode( path, sContent, pResourceManager );
}

std::string JSON::Encode( const CPropagationPath& path, std::shared_ptr<ITAGeo::CResourceManager> pResourceManager )
{
	return CreateNode( path, pResourceManager ).dump( JSON_INDENT );
}
void JSON::Decode( CPropagationPath& path, const std::string& sContent, std::shared_ptr<ITAGeo::CResourceManager> pResourceManager )
{
	return Decode( path, json::parse( sContent ), pResourceManager );
}

#pragma endregion

// ------CPropagationPathList------
// --------------------------------
#pragma region Propagation Path List
void JSON::Export( const CPropagationPathList& pathList, const std::string& sFilePath, bool bOverwrite, std::shared_ptr<ITAGeo::CResourceManager> pResourceManager )
{
	VistaFileSystemFile oFile( sFilePath );
	if( oFile.Exists( ) && !bOverwrite )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Rejected to store propagation path to file '" + sFilePath + "', overwrite was protected" );

	// Store propagation path to json file located in sFilePath
	std::ofstream out( sFilePath );

	out << Encode( pathList, pResourceManager ).c_str( );
	out.close( );
}
void JSON::Import( CPropagationPathList& pathList, const std::string& sFilePath, std::shared_ptr<ITAGeo::CResourceManager> pResourceManager )
{
	VistaFileSystemFile oFile( sFilePath );
	if( !oFile.Exists( ) || !oFile.IsFile( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Could not load propagation path list, file '" + sFilePath + "' not found or not a file" );

	std::ifstream inFile( sFilePath );

	//--Get content of file-----------------------------
	std::string sContent( ( std::istreambuf_iterator<char>( inFile ) ), ( std::istreambuf_iterator<char>( ) ) );

	Decode( pathList, sContent, pResourceManager );
}
std::string JSON::Encode( const CPropagationPathList& pathList, std::shared_ptr<CResourceManager> pResourceManager )
{
	return CreateNode( pathList, pResourceManager ).dump( JSON_INDENT );
}
void JSON::Decode( CPropagationPathList& pathList, const std::string& sContent, std::shared_ptr<CResourceManager> pResourceManager )
{
	return Decode( pathList, json::parse( sContent ), pResourceManager );
}
#pragma endregion