#include <ITAException.h>
#include <ITAGeo/Base.h>
#include <ITAGeo/Utils.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <algorithm>
#include <cassert>

#ifdef WITH_XML_SUPPORT
#	include <tinyxml.h>
#endif


//---CPropagationAnchor-------------------------------------------------------------------------------------------------------------------

ITAGeo::CPropagationAnchor::CPropagationAnchor( ) : iAnchorType( GENERIC_ANCHOR ) {}

ITAGeo::CPropagationAnchor::CPropagationAnchor( const VistaVector3D& v3InteractionPoint ) : iAnchorType( GENERIC_ANCHOR ), v3InteractionPoint( v3InteractionPoint ) {}

ITAGeo::CPropagationAnchor::~CPropagationAnchor( ) {}

std::string ITAGeo::CPropagationAnchor::GetAnchorTypeStr( ITAGeo::CPropagationAnchor::EAnchorType iType )
{
	switch( iType )
	{
		case INVALID:
			return "invalid_anchor";
		case ACOUSTIC_EMITTER:
			return "source";
		case ACOUSTIC_SENSOR:
			return "receiver";
		case SPECULAR_REFLECTION:
			return "specular_reflection";
		case ITA_DIFFUSE_REFLECTION:
			return "diffuse_reflection";
		case TRANSMISSION_APEX:
			return "transmission";
		case DIFFRACTION_OUTER_APEX:
			return "outer_edge_diffraction";
		case DIFFRACTION_INNER_APEX:
			return "inner_edge_diffraction";
		case INHOMOGENEITY:
			return "inhomogeneity";
		case INHOMOGENEITY_EMITTER:
			return "inhomogeneity_source";
		case INHOMOGENEITY_SENSOR:
			return "inhomogeneity_receiver";
		case INHOMOGENEITY_SPECULAR_REFLECTION:
			return "inhomogeneity_specular_reflection";
		case GENERIC_ANCHOR:
			return "generic_anchor";
		default:
			return "Unkown";
	}
}

ITAGeo::CPropagationAnchor::EAnchorType ITAGeo::CPropagationAnchor::ParseAnchorType( const std::string& sTypeStr )
{
	for( size_t i = 0; i < EAnchorType::LAST_ELEMENT; i++ )
	{
		if( sTypeStr == GetAnchorTypeStr( (EAnchorType)i ) )
		{
			return (EAnchorType)i;
		}
	}

	return EAnchorType::INVALID;
}

std::string ITAGeo::CPropagationAnchor::GetTypeStr( )
{
	return "propagation_anchor";
}

std::string ITAGeo::CPropagationAnchor::ToString( ) const
{
	std::stringstream ss;
	ss << "[ " << std::setw( 10 ) << GetAnchorTypeStr( iAnchorType ) << " ] Interaction point : " << v3InteractionPoint;
	return ss.str( );
}

//---CEmitter-----------------------------------------------------------------------------------------------------------------------------

ITAGeo::CEmitter::CEmitter( VistaVector3D vPos_ ) : CPropagationAnchor( vPos_ ), vPos( v3InteractionPoint )
{
	CPropagationAnchor::iAnchorType = CPropagationAnchor::ACOUSTIC_EMITTER;
}

ITAGeo::CEmitter::CEmitter( ) : vPos( v3InteractionPoint )
{
	CPropagationAnchor::iAnchorType = CPropagationAnchor::ACOUSTIC_EMITTER;
}

const ITAGeo::CEmitter& ITAGeo::CEmitter::operator=( const CEmitter& oOther )
{
	sName              = oOther.sName;
	v3InteractionPoint = oOther.v3InteractionPoint;
	qOrient            = oOther.qOrient;
	pDirectivity       = oOther.pDirectivity;
	return *this;
};

std::string ITAGeo::CEmitter::ToString( ) const
{
	std::stringstream ss;
	ss << "[ " << std::setw( 10 ) << GetAnchorTypeStr( iAnchorType ) << " ] Name : \"" << sName << "\", Interaction point : " << v3InteractionPoint
	   << ", Orientation : " << qOrient << ", Directivity : " << pDirectivity;
	return ss.str( );
}

int ITAGeo::CEmitter::GetNumChannels( ) const
{
	if( pDirectivity )
		return pDirectivity->GetNumChannels( );
	else
		return 1;
}

//---CSensor-----------------------------------------------------------------------------------------------------------------------------

ITAGeo::CSensor::CSensor( ) : vPos( v3InteractionPoint )
{
	CPropagationAnchor::iAnchorType = CPropagationAnchor::ACOUSTIC_SENSOR;
}

ITAGeo::CSensor::CSensor( VistaVector3D vPos_ ) : CPropagationAnchor( vPos_ ), vPos( v3InteractionPoint )
{
	CPropagationAnchor::iAnchorType = CPropagationAnchor::ACOUSTIC_SENSOR;
}

const ITAGeo::CSensor& ITAGeo::CSensor::operator=( const CSensor& oOther )
{
	sName              = oOther.sName;
	v3InteractionPoint = oOther.v3InteractionPoint;
	qOrient            = oOther.qOrient;
	pDirectivity       = oOther.pDirectivity;
	return *this;
}

int ITAGeo::CSensor::GetNumChannels( ) const
{
	if( pDirectivity )
		return pDirectivity->GetNumChannels( );
	else
		return 1;
}

std::string ITAGeo::CSensor::ToString( ) const
{
	std::stringstream ss;
	ss << "[ " << std::setw( 10 ) << GetAnchorTypeStr( iAnchorType ) << " ] Name : \"" << sName << "\", Interaction point : " << v3InteractionPoint
	   << ", Orientation : " << qOrient << ", Directivity : " << pDirectivity;
	return ss.str( );
}


//---CMirrorImage-------------------------------------------------------------------------------------------------------------------------

ITAGeo::CMirrorImage::CMirrorImage( ) : iPolygonIndex( -1 ), pUserData( nullptr ), iOrder( -1 ), v3MirrorPosition( v3InteractionPoint )
{
	CPropagationAnchor::iAnchorType = CPropagationAnchor::MIRROR_IMAGE;
}

ITAGeo::CMirrorImage::CMirrorImage( const CPropagationAnchor& oAnchor )
    : CPropagationAnchor( oAnchor )
    , iPolygonIndex( -1 )
    , iOrder( -1 )
    , pUserData( nullptr )
    , v3MirrorPosition( v3InteractionPoint )
{
	CPropagationAnchor::iAnchorType = CPropagationAnchor::SPECULAR_REFLECTION;
}


void ITAGeo::CMirrorImage::CopyFromAnchor( std::shared_ptr<const ITAGeo::CPropagationAnchor> pOther )
{
	v3InteractionPoint              = pOther->v3InteractionPoint;
	CPropagationAnchor::iAnchorType = pOther->iAnchorType;
}

std::string ITAGeo::CMirrorImage::ToString( ) const
{
	std::stringstream ss;
	ss << "[ " << std::setw( 10 ) << GetAnchorTypeStr( iAnchorType ) << " ] "
	   << ", Interaction point : " << v3InteractionPoint << ", Mirror normal : " << v3MirrorNormal << ", Polygon index: " << iPolygonIndex << ", Order: " << iOrder;
	return ss.str( );
}


//---CSpecularReflection------------------------------------------------------------------------------------------------------------------

ITAGeo::CSpecularReflection::CSpecularReflection( )
{
	iAnchorType = CPropagationAnchor::SPECULAR_REFLECTION;
	v3FaceNormal.SetToZeroVector( );
	iPolygonID = -1;
}

ITAGeo::CSpecularReflection::CSpecularReflection( const VistaVector3D& v3SpecularReflectionPoint ) : CPropagationAnchor( v3SpecularReflectionPoint )
{
	iAnchorType = CPropagationAnchor::SPECULAR_REFLECTION;
	iPolygonID  = -1;
}

std::string ITAGeo::CSpecularReflection::ToString( ) const
{
	std::stringstream ss;
	ss << "[ " << std::setw( 10 ) << GetAnchorTypeStr( iAnchorType ) << " ] "
	   << ", Interaction point : " << v3InteractionPoint;
	return ss.str( );
}


//---CDetectionPoint----------------------------------------------------------------------------------------------------------------------

ITAGeo::CDetectionPoint::CDetectionPoint( )
{
	iAnchorType = CPropagationAnchor::DETECTION_POINT;
}
ITAGeo::CDetectionPoint::CDetectionPoint( const VistaVector3D& v3DetectionPoint ) : CPropagationAnchor( v3DetectionPoint )
{
	iAnchorType = CPropagationAnchor::DETECTION_POINT;
}

std::string ITAGeo::CDetectionPoint::ToString( ) const
{
	std::stringstream ss;
	ss << "[ " << std::setw( 10 ) << GetAnchorTypeStr( iAnchorType ) << " ] "
	   << ", Interaction point : " << v3InteractionPoint;
	return ss.str( );
}


//---CInhomogeneousAnchorExtension---------------------------------------------------------------------------------------------------------------------

ITAGeo::CInhomogeneousAnchorExtension::CInhomogeneousAnchorExtension( )
    : v3WavefrontNormal( VistaVector3D( ) ) // ZeroVector
    , dTimeStamp( -1.0 )
{
}
ITAGeo::CInhomogeneousAnchorExtension::CInhomogeneousAnchorExtension( const VistaVector3D& v3Normal, const double& dTimeStamp )
    : v3WavefrontNormal( v3Normal )
    , dTimeStamp( dTimeStamp )
{
}

std::string ITAGeo::CInhomogeneousAnchorExtension::ToString( ) const
{
	std::stringstream ss;
	ss << ", Time stamp : " << dTimeStamp << ", Wavefront normal : " << v3WavefrontNormal;
	return ss.str( );
}


//---CInhomogeneity----------------------------------------------------------------------------------------------------------------------

ITAGeo::CInhomogeneity::CInhomogeneity( ) : CPropagationAnchor( ), CInhomogeneousAnchorExtension( )
{
	iAnchorType = CPropagationAnchor::INHOMOGENEITY;
}
ITAGeo::CInhomogeneity::CInhomogeneity( const VistaVector3D& v3Pos, const VistaVector3D& v3Normal, const double& dTimeStamp )
    : CPropagationAnchor( v3Pos )
    , CInhomogeneousAnchorExtension( v3Normal, dTimeStamp )
{
	iAnchorType = CPropagationAnchor::INHOMOGENEITY;
}

std::string ITAGeo::CInhomogeneity::ToString( ) const
{
	std::stringstream ss;
	ss << CPropagationAnchor::ToString( ) << CInhomogeneousAnchorExtension::ToString( );
	return ss.str( );
}


//---CEmitterInhomogeneous----------------------------------------------------------------------------------------------------------------------
ITAGeo::CEmitterInhomogeneous::CEmitterInhomogeneous( ) : CEmitter( ), CInhomogeneousAnchorExtension( )
{
	iAnchorType = CPropagationAnchor::INHOMOGENEITY_EMITTER;
	dTimeStamp  = 0.0;
}
ITAGeo::CEmitterInhomogeneous::CEmitterInhomogeneous( const VistaVector3D& v3Pos, const VistaVector3D& v3Normal )
    : CEmitter( v3Pos )
    , CInhomogeneousAnchorExtension( v3Normal, 0.0 )
{
	iAnchorType = CPropagationAnchor::INHOMOGENEITY_EMITTER;
}

std::string ITAGeo::CEmitterInhomogeneous::ToString( ) const
{
	std::stringstream ss;
	ss << CEmitter::ToString( ) << CInhomogeneousAnchorExtension::ToString( );
	return ss.str( );
}

//---CSensorInhomogeneous----------------------------------------------------------------------------------------------------------------------
ITAGeo::CSensorInhomogeneous::CSensorInhomogeneous( ) : CSensor( ), CInhomogeneousAnchorExtension( )
{
	iAnchorType           = CPropagationAnchor::INHOMOGENEITY_SENSOR;
	dSpreadingLoss        = -1.0;
	bEigenray             = false;
	bReceiverHit          = true;
	iRayZoomingIterations = -1;
}
ITAGeo::CSensorInhomogeneous::CSensorInhomogeneous( const VistaVector3D& v3Pos, const VistaVector3D& v3Normal, const double& dTimeStamp, const double& spreadingLoss )
    : CSensor( v3Pos )
    , CInhomogeneousAnchorExtension( v3Normal, dTimeStamp )
    , dSpreadingLoss( spreadingLoss )
{
	iAnchorType           = CPropagationAnchor::INHOMOGENEITY_SENSOR;
	bEigenray             = false;
	bReceiverHit          = true;
	iRayZoomingIterations = -1;
}

std::string ITAGeo::CSensorInhomogeneous::ToString( ) const
{
	std::stringstream ss;
	ss << CSensor::ToString( ) << CInhomogeneousAnchorExtension::ToString( ) << ", Spreading loss : " << dSpreadingLoss;
	return ss.str( );
}

//---CSpecularReflectionInhomogeneous----------------------------------------------------------------------------------------------------------------------
ITAGeo::CSpecularReflectionInhomogeneous::CSpecularReflectionInhomogeneous( ) : CSpecularReflection( ), CInhomogeneousAnchorExtension( )
{
	iAnchorType = CPropagationAnchor::INHOMOGENEITY_SPECULAR_REFLECTION;
}
ITAGeo::CSpecularReflectionInhomogeneous::CSpecularReflectionInhomogeneous( const VistaVector3D& v3Pos, const VistaVector3D& v3Normal, const double& dTimeStamp )
    : CSpecularReflection( v3Pos )
    , CInhomogeneousAnchorExtension( v3Normal, dTimeStamp )
{
	iAnchorType = CPropagationAnchor::INHOMOGENEITY_SPECULAR_REFLECTION;
}

std::string ITAGeo::CSpecularReflectionInhomogeneous::ToString( ) const
{
	std::stringstream ss;
	ss << CSpecularReflection::ToString( ) << CInhomogeneousAnchorExtension::ToString( );
	return ss.str( );
}


//---CITADiffractionWedgeAperture---------------------------------------------------------------------------------------------------------

ITAGeo::CITADiffractionWedgeApertureBase::CITADiffractionWedgeApertureBase( )
    : v3AperturePoint( ITAGeo::CPropagationAnchor::v3InteractionPoint )
    , iMainWedgeFaceID( -1 )
    , iOppositeWedgeFaceID( -1 )
{
}

VistaVector3D ITAGeo::CITADiffractionWedgeApertureBase::GetApertureDirection( ) const
{
	return ( v3VertextEnd - v3VertextStart ).GetNormalized( );
}

float ITAGeo::CITADiffractionWedgeApertureBase::GetApertureLength( ) const
{
	return ( v3VertextEnd - v3VertextStart ).GetLength( );
}

bool ITAGeo::CITADiffractionWedgeApertureBase::IsOccluding( const VistaVector3D& v3SourcePos, const VistaVector3D& v3TargetPos ) const
{
	// Inversed wedges can not occlude
	if( GetWedgeAngleRad( ) > ITAConstants::PI_D )
		return false;

	// We use Hesse normal form with signed distance

	VistaVector3D v3SrcToApex( v3SourcePos - v3AperturePoint );

	double dSignedDistanceSourceMainFace     = v3MainWedgeFaceNormal.Dot( v3SrcToApex );
	double dSignedDistanceSourceOppositeFace = v3OppositeWedgeFaceNormal.Dot( v3SrcToApex );

	VistaVector3D vrDest2Apex( v3TargetPos - v3AperturePoint );

	double dSignedDistanceDestMainFace     = v3MainWedgeFaceNormal.Dot( vrDest2Apex );
	double dSignedDistanceDestOppositeFace = v3OppositeWedgeFaceNormal.Dot( vrDest2Apex );

	// Gracing angle where connecting ray is touching wedge aperture is considered non-occluding

	if( dSignedDistanceSourceMainFace < 0.0f && dSignedDistanceSourceOppositeFace >= 0.0f && dSignedDistanceDestOppositeFace <= 0.0f &&
	    dSignedDistanceDestMainFace > 0.0f )
		return true;

	if( dSignedDistanceSourceMainFace >= 0.0f && dSignedDistanceSourceOppositeFace < 0.0f && dSignedDistanceDestOppositeFace > 0.0f &&
	    dSignedDistanceDestMainFace <= 0.0f )
		return true;

	return false;
}

bool ITAGeo::CITADiffractionWedgeApertureBase::IsOccluding( std::shared_ptr<const ITAGeo::CPropagationAnchor> pSource,
                                                            std::shared_ptr<const ITAGeo::CPropagationAnchor> pDest ) const
{
	return IsOccluding( pSource->v3InteractionPoint, pDest->v3InteractionPoint );
}

bool ITAGeo::CITADiffractionWedgeApertureBase::IsOutsideWedge( const VistaVector3D& v3Pos ) const
{
	// We use Hesse normal form with signed distance

	VistaVector3D v3PosToApex( v3AperturePoint - v3Pos );

	double dSignedDistanceMainFace     = v3MainWedgeFaceNormal.Dot( v3PosToApex );
	double dSignedDistanceOppositeFace = v3OppositeWedgeFaceNormal.Dot( v3PosToApex );

	if( dSignedDistanceOppositeFace < 0.0f && dSignedDistanceMainFace < 0.0f )
		return false;

	return true;
}

float ITAGeo::CITADiffractionWedgeApertureBase::GetIncidenceWaveAngleRad( const VistaVector3D& v3SourcePoint ) const
{
	const float fDotProduct = fabs( ( v3VertextEnd - v3VertextStart ).GetNormalized( ).Dot( ( v3AperturePoint - v3SourcePoint ).GetNormalized( ) ) );
	return acos( fDotProduct );
}

float ITAGeo::CITADiffractionWedgeApertureBase::GetReciprocalIncidenceWaveAngleRad( const VistaVector3D& v3TargetPoint ) const
{
	return GetIncidenceWaveAngleRad( v3TargetPoint );
}

VistaRay ITAGeo::CITADiffractionWedgeApertureBase::GetEdgeRay( ) const
{
	return VistaRay( v3VertextStart, ( v3VertextEnd - v3VertextStart ).GetNormalized( ) );
}

float ITAGeo::CITADiffractionWedgeApertureBase::GetShortestPathLength( const VistaVector3D& v3SourcePoint, const VistaVector3D& v3TargetPoint ) const
{
	const float fShortestPathLength = ( v3AperturePoint - v3SourcePoint ).GetLength( ) + ( v3TargetPoint - v3AperturePoint ).GetLength( );
	return fShortestPathLength;
}

float ITAGeo::CITADiffractionWedgeApertureBase::GetMinimumWavefrontDelayTime( const VistaVector3D& v3SourcePoint, const VistaVector3D& v3TargetPoint,
                                                                              const float fSpeedOfSound /*= ITAConstants::SPEED_OF_SOUND_F */ ) const
{
	if( fSpeedOfSound <= 0.0f )
		ITA_EXCEPT_INVALID_PARAMETER( "Speed of sound must be greater zero" );

	VistaVector3D v3RegardedAperturePoint;
	if( !ITAGeoUtils::CalculateDiffractionAperturePoint( GetEdgeRay( ), v3SourcePoint, v3TargetPoint, v3RegardedAperturePoint ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Could not calculate aperture point for this situation" );

	if( !ITAGeoUtils::IsDiffractionAperturePointInRange( v3VertextStart, v3VertextEnd, v3RegardedAperturePoint ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Aperture point is outside edge range, can not determine minimum wavefront delay time" );

	const float fMinimumPropagationPathLength = GetShortestPathLength( v3SourcePoint, v3TargetPoint );
	return ( fMinimumPropagationPathLength / fSpeedOfSound );
}

float ITAGeo::CITADiffractionWedgeApertureBase::GetMaximumWavefrontDelayTime( const VistaVector3D& v3SourcePoint, const VistaVector3D& v3TargetPoint,
                                                                              const float fSpeedOfSound /*= ITAConstants::SPEED_OF_SOUND_F */ ) const
{
	if( fSpeedOfSound <= 0.0f )
		ITA_EXCEPT_INVALID_PARAMETER( "Speed of sound must be greater zero" );

	VistaVector3D v3RegardedAperturePoint;
	if( !ITAGeoUtils::CalculateDiffractionAperturePoint( GetEdgeRay( ), v3SourcePoint, v3TargetPoint, v3RegardedAperturePoint ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Could not calculate aperture point for this situation" );

	if( !ITAGeoUtils::IsDiffractionAperturePointInRange( v3VertextStart, v3VertextEnd, v3RegardedAperturePoint ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Aperture point is outside edge range, can not determine minimum wavefront delay time" );

	const float fPropagationPathLengthApertureStart = ( v3VertextStart - v3SourcePoint ).GetLength( ) + ( v3TargetPoint - v3VertextStart ).GetLength( );
	const float fPropagationPathLengthApertureEnd   = ( v3VertextEnd - v3SourcePoint ).GetLength( ) + ( v3TargetPoint - v3VertextEnd ).GetLength( );

	return ( std::max( fPropagationPathLengthApertureStart, fPropagationPathLengthApertureEnd ) / fSpeedOfSound );
}

bool ITAGeo::CITADiffractionWedgeApertureBase::IsValid( ) const
{
	// Validate vertex length
	VistaVector3D v3Vertex = v3VertextStart - v3VertextEnd;
	if( v3Vertex.GetLength( ) < ITAConstants::EPS_D_L )
		return false;

	// Validate vertex opening angle not (exactly) \pi and not greater 2 * \pi)
	double dCrossLength = v3MainWedgeFaceNormal.Cross( v3OppositeWedgeFaceNormal ).GetLength( );
	if( std::abs( dCrossLength ) < ITAConstants::EPS_D_L )
		return false;

	return true;
}

ITAGeo::CITADiffractionWedgeApertureBase& ITAGeo::CITADiffractionWedgeApertureBase::operator=( const CITADiffractionWedgeApertureBase& oRHS )
{
	this->v3AperturePoint           = oRHS.v3AperturePoint;
	this->v3MainWedgeFaceNormal     = oRHS.v3MainWedgeFaceNormal;
	this->v3OppositeWedgeFaceNormal = oRHS.v3OppositeWedgeFaceNormal;
	this->v3VertextEnd              = oRHS.v3VertextEnd;
	this->v3VertextStart            = oRHS.v3VertextStart;
	this->iMainWedgeFaceID          = oRHS.iMainWedgeFaceID;
	this->iOppositeWedgeFaceID      = oRHS.iOppositeWedgeFaceID;

	return *this;
}

std::string ITAGeo::CITADiffractionWedgeApertureBase::ToString( ) const
{
	std::stringstream ss;
	ss << "[ " << std::setw( 10 ) << GetAnchorTypeStr( iAnchorType ) << " ] "
	   << ", Interaction point : " << v3InteractionPoint << ", Vertex start : " << v3VertextStart << ", Vertex end : " << v3VertextEnd
	   << ", Main wedge face normal : " << v3MainWedgeFaceNormal << ", Opposite wedge face normal: " << v3OppositeWedgeFaceNormal;
	return ss.str( );
}


//---CITADiffractionInnerWedgeAperture------------------------------------------------------------------------------------------------------------------
double ITAGeo::CITADiffractionInnerWedgeAperture::GetWedgeAngleRad( ) const
{
	ITA_EXCEPT_NOT_IMPLEMENTED;
}

double ITAGeo::CITADiffractionInnerWedgeAperture::GetOpeningAngleRad( ) const
{
	ITA_EXCEPT_NOT_IMPLEMENTED;
}

//---CITADiffractionOuterWedgeAperture------------------------------------------------------------------------------------------------------------------
double ITAGeo::CITADiffractionOuterWedgeAperture::GetWedgeAngleRad( ) const
{
	if( !IsValid( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Can not calculate opening angle on invalid diffraction wedge" );

	assert( std::abs( v3MainWedgeFaceNormal.GetLength( ) - 1.0f ) < ITAConstants::EPS_D_L );
	assert( std::abs( v3OppositeWedgeFaceNormal.GetLength( ) - 1.0f ) < ITAConstants::EPS_D_L );

	const float fDotProduct = v3MainWedgeFaceNormal.Dot( v3OppositeWedgeFaceNormal );

	/* Project to 0 .. 2 * \pi, where
	1. \pi is planar
	2. 0 is steepest valley possible
	3. 2 * \pi is sharpest wedge possible (thin plate)
	*/

	// Special case, we do not know if the wedge is an inner or outer corner (1.5*\pi vs. 0.5*\pi)
	if( fDotProduct == 0.0f )
	{
		VistaVector3D v3EdgeMainFaceNormalCrossNormal = ( v3VertextEnd - v3VertextStart ).GetNormalized( ).Cross( v3MainWedgeFaceNormal );
		float fDotProductCrossNormal                  = v3EdgeMainFaceNormalCrossNormal.Dot( v3OppositeWedgeFaceNormal );
		if( fDotProductCrossNormal < 0.0f )
			return 3 * ITAConstants::PI_D / 2.0f;
		else
			return ITAConstants::PI_D / 2.0f;
	}

	const double dWedgeAngleRad = acos( fDotProduct );
	return dWedgeAngleRad;
}

double ITAGeo::CITADiffractionOuterWedgeAperture::GetOpeningAngleRad( ) const
{
	if( !IsValid( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Can not calculate opening angle on invalid diffraction wedge" );

	const double dWedgeAngle = GetWedgeAngleRad( );
	const double dAngleRad   = 2.0f * ITAConstants::PI_D - dWedgeAngle;

	return dAngleRad;
}


//---CPropagationPath----------------------------------------------------------------------------------------------------------------------

double ITAGeo::CPropagationPath::GetLength( ) const
{
	double dLength = 0.0;

	for( size_t i = 1; i < this->size( ); i++ )
	{
		auto& v3From( at( i - 1 )->v3InteractionPoint );
		auto& v3To( at( i )->v3InteractionPoint );
		const double dSegmentLength = ( v3To - v3From ).GetLength( );

		// Add length of sub path
		dLength += dSegmentLength;
	}

	return dLength;
}

int ITAGeo::CPropagationPath::GetNumReflections( ) const
{
	int iNumReflections = 0;
	for( auto& pAnchor: *this )
	{
		if( pAnchor->iAnchorType == ITAGeo::CPropagationAnchor::SPECULAR_REFLECTION )
			iNumReflections++;
	}

	return iNumReflections;
}

int ITAGeo::CPropagationPath::GetNumRaySteps( ) const
{
	int iNumRaySteps = 0;
	for( auto& pAnchor: *this )
	{
		if( pAnchor->iAnchorType == ITAGeo::CPropagationAnchor::INHOMOGENEITY || pAnchor->iAnchorType == ITAGeo::CPropagationAnchor::INHOMOGENEITY_EMITTER ||
		    pAnchor->iAnchorType == ITAGeo::CPropagationAnchor::INHOMOGENEITY_SENSOR ||
		    pAnchor->iAnchorType == ITAGeo::CPropagationAnchor::INHOMOGENEITY_SPECULAR_REFLECTION )
			iNumRaySteps++;
	}

	return iNumRaySteps;
}

int ITAGeo::CPropagationPath::GetNumDiffractions( ) const
{
	int iNumDiffractions = 0;
	for( auto& pAnchor: *this )
	{
		if( pAnchor->iAnchorType == ITAGeo::CPropagationAnchor::DIFFRACTION_OUTER_APEX || pAnchor->iAnchorType == ITAGeo::CPropagationAnchor::DIFFRACTION_INNER_APEX )
		{
			iNumDiffractions++;
		}
	}

	return iNumDiffractions;
}

double ITAGeo::CPropagationPath::GetAccumulatedDiffractionAnglesRad( ) const
{
	const ITAGeo::CPropagationPath& oPropPath( *this );

	if( size( ) < 2 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Propagation path needs at least 2 anchors to get accumulated angle" );

	auto pAnchorDirectLazy( oPropPath[0] );
	if( pAnchorDirectLazy->iAnchorType == ITAGeo::CPropagationAnchor::INVALID )
		ITA_EXCEPT1( INVALID_PARAMETER, "First geo propagation anchor is invalid, please purge first." );

	auto pAnchorTail( oPropPath[oPropPath.size( ) - 1] );
	if( pAnchorTail->iAnchorType == ITAGeo::CPropagationAnchor::INVALID )
		ITA_EXCEPT1( INVALID_PARAMETER, "Last geo propagation anchor is invalid, please purge first." );

	// We don't care for first and last anchor's type (except invalid)

	double dIncrementalAngle = 0.0f;

	for( size_t i = 1; i < oPropPath.size( ) - 1; i++ )
	{
		auto pAnchorCur( oPropPath[i] );

		if( pAnchorCur->iAnchorType == ITAGeo::CPropagationAnchor::INVALID )
			ITA_EXCEPT1( INVALID_PARAMETER, "Geo propagation path has invalid anchors, please purge first." );

		if( pAnchorCur->iAnchorType == ITAGeo::CPropagationAnchor::DIFFRACTION_OUTER_APEX ||
		    pAnchorCur->iAnchorType == ITAGeo::CPropagationAnchor::DIFFRACTION_INNER_APEX )
		{
			auto pWedge = std::dynamic_pointer_cast<ITAGeo::CITADiffractionWedgeApertureBase>( pAnchorCur );
			dIncrementalAngle += pWedge->GetOpeningAngleRad( );
		}
	}

	return dIncrementalAngle;
}

VistaVector3D ITAGeo::CPropagationPath::GetSourceWaveFrontNormal( ) const
{
	assert( size( ) >= 2 );
	return ( this->at( 1 )->v3InteractionPoint - this->at( 0 )->v3InteractionPoint ).GetNormalized( );
}

VistaVector3D ITAGeo::CPropagationPath::GetReceiverWaveFrontNormal( ) const
{
	assert( size( ) >= 2 );
	return ( this->at( size( ) - 1 )->v3InteractionPoint - this->at( size( ) - 2 )->v3InteractionPoint ).GetNormalized( );
}

//---CPropagationPathList--------------------------------------------------------------------------------------------------------
double ITAGeo::CPropagationPathList::GetMaxLength( )
{
	double dMaxLength = 0;
	for( int i = 0; i < GetNumPaths( ); i++ )
	{
		double dCurrentLength = this->at( i ).GetLength( );
		dMaxLength            = fmax( dMaxLength, dCurrentLength );
	}
	return dMaxLength;
}
