#include <ITAGeo/Material/MaterialManager.h>

// ITABase
#include <ITAConfigUtils.h>
#include <ITAConstants.h>
#include <ITAStringUtils.h>

// Vista
#include <VistaTools/VistaFileSystemDirectory.h>
#include <VistaTools/VistaFileSystemFile.h>
#include <memory>


// Helper struct to read .ini files
struct CMATFileMaterial
{
	inline CMATFileMaterial( ) {};
	CMATFileMaterial( const std::string& sFilePath );
	void LoadFromFile( const std::string& sFilePath );

	std::string sName;
	std::string sNotes;
	std::vector<float> vfAbsorptionCoefficients    = std::vector<float>( ITAConstants::THIRD_OCTAVE_CENTER_FREQUENCIES_ISO_F.size( ) );
	std::vector<float> vfScatteringCoefficients    = std::vector<float>( ITAConstants::THIRD_OCTAVE_CENTER_FREQUENCIES_ISO_F.size( ) );
	std::vector<float> vfImpedanceRealCoefficients = std::vector<float>( ITAConstants::THIRD_OCTAVE_CENTER_FREQUENCIES_ISO_F.size( ) );
	std::vector<float> vfImpedanceImagCoefficients = std::vector<float>( ITAConstants::THIRD_OCTAVE_CENTER_FREQUENCIES_ISO_F.size( ) );
};


ITAGeo::Material::CMaterialManager::CMaterialManager( ) {}

ITAGeo::Material::CMaterialManager::CMaterialManager( const std::string& sFolderPath, const bool bRecursive /* = false */ )
{
	AddMaterialsFromFolder( sFolderPath, bRecursive );
}

ITAGeo::Material::CMaterialManager::~CMaterialManager( ) {}


void ITAGeo::Material::CMaterialManager::AddMaterialsFromFolder( const std::string& sFolderPath, const bool bRecursive /*= false*/ )
{
	std::filesystem::path fsPath( sFolderPath );

	if( !std::filesystem::exists( fsPath ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Material database could not be loaded. Path not found!" );

	if( bRecursive )
	{
		for( const std::filesystem::directory_entry& fsEntry: std::filesystem::recursive_directory_iterator( fsPath ) )
		{
			addPathEntry( fsEntry );
		}
	}
	else
	{
		for( const std::filesystem::directory_entry& fsEntry: std::filesystem::directory_iterator( fsPath ) )
		{
			addPathEntry( fsEntry );
		}
	}
}

bool ITAGeo::Material::CMaterialManager::AddMaterial( const std::string& sName, std::shared_ptr<ITAGeo::Material::IMaterial> pMaterial )
{
	if( m_mMaterials.count( sName ) > 0 )
		return false;

	m_mMaterials[sName] = pMaterial;
	return true;
}


std::shared_ptr<ITAGeo::Material::IMaterial> ITAGeo::Material::CMaterialManager::GetMaterial( const std::string& sMaterialIdentifier ) const
{
	if( m_mMaterials.find( sMaterialIdentifier ) == m_mMaterials.end( ) )
		return nullptr;
	else
		return m_mMaterials.find( sMaterialIdentifier )->second;
}

int ITAGeo::Material::CMaterialManager::GetNumMaterials( ) const
{
	return (int)m_mMaterials.size( );
}

bool ITAGeo::Material::CMaterialManager::HasMaterial( const std::string& sMaterialIdentifier ) const
{
	return ( GetMaterial( sMaterialIdentifier ) != nullptr );
}

/// Private functions
inline void ITAGeo::Material::CMaterialManager::addPathEntry( const std::filesystem::directory_entry& fsEntry )
{
	// skip if not a file or the file ending is not .mat
	if( !fsEntry.is_regular_file( ) || ( fsEntry.path( ).extension( ).string( ) != ".mat" ) )
		return;

	CMATFileMaterial oMaterial( fsEntry.path( ).string( ) );

	auto pMaterial    = std::make_shared<ITAGeo::Material::CThirdOctaveMaterial>( );
	std::string sName = oMaterial.sName;
	pMaterial->sName  = sName;

	pMaterial->oAbsorptionCoefficients.SetMagnitudes( oMaterial.vfAbsorptionCoefficients );
	pMaterial->oScatteringCoefficients.SetMagnitudes( oMaterial.vfScatteringCoefficients );

	pMaterial->vfImpedanceRealCoefficients = oMaterial.vfImpedanceRealCoefficients;
	pMaterial->vfImpedanceImagCoefficients = oMaterial.vfImpedanceImagCoefficients;

	// skip if already exists, just to be sure
	if( m_mMaterials.count( sName ) == 0 )
		m_mMaterials[sName] = pMaterial;
}


// Helper functions
CMATFileMaterial::CMATFileMaterial( const std::string& sFilePath )
{
	LoadFromFile( sFilePath );
}

void CMATFileMaterial::LoadFromFile( const std::string& sFilePath )
{
	INIFileUseFile( sFilePath );
	if( INIFileUseSectionIfExists( "Material" ) )
	{
		sName  = INIFileReadStringExplicit( "name" );
		sNotes = INIFileReadString( "notes" );

		// We at least need either Impedance or Absorption coeffs. Store both if both are available.
		if( !INIFileKeyExists( "absorp" ) && !( INIFileKeyExists( "impedanceReal" ) && INIFileKeyExists( "impedanceImag" ) ) )
			ITA_EXCEPT_INVALID_PARAMETER( "Material '" + sName + "' could not be loaded. There is no impdeance or absorption coefficients." );

		if( INIFileKeyExists( "absorp" ) )
			vfAbsorptionCoefficients = INIFileReadFloatList( "absorp" );

		if( INIFileKeyExists( "impedanceReal" ) )
			vfImpedanceRealCoefficients = INIFileReadFloatList( "impedanceReal" );

		if( INIFileKeyExists( "impedanceImag" ) )
			vfImpedanceImagCoefficients = INIFileReadFloatList( "impedanceImag" );

		if( INIFileKeyExists( "scatter" ) ) // some material files have no scatter coeffs
			vfScatteringCoefficients = INIFileReadFloatList( "scatter" );
	}
	else
	{
		ITA_EXCEPT_INVALID_PARAMETER( "Material file '" + sFilePath + "' did not contain an INI section named 'Material'." );
	}
}
