#include <ITAException.h>
#include <ITAGeo/Material/Utils.h>
#include <cassert>

float ITAGeo::Material::Utils::EstimateFilterLengthSamples( std::shared_ptr<const ITAGeo::Material::IMaterial> pMaterial, const float fSampleRate,
                                                            const float fSpeedOfSound )
{
	// Allow for the lowest frequency a phase shift of one cycle / return samples of one wave length
	switch( pMaterial->GetType( ) )
	{
		case( ITAGeo::Material::IMaterial::THIRD_OCTAVE ):
		{
			auto p                            = std::dynamic_pointer_cast<const ITAGeo::Material::CThirdOctaveMaterial>( pMaterial );
			const float fFirstCenterFrequency = p->oAbsorptionCoefficients.GetCenterFrequencies( )[0];
			assert( fFirstCenterFrequency != 0.0f );
			const float fLambda = fSpeedOfSound / fFirstCenterFrequency;
			return fLambda * fSampleRate;
		}
	}

	// All other cases are considered to not extend any filter length, hence have to estimate
	return 0.0f;
}
