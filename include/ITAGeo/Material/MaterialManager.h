/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_GEO_MATERIAL_MANAGER
#define IW_ITA_GEO_MATERIAL_MANAGER

// ITAGeo includes
#include <ITAGeo/Definitions.h>
#include <ITAGeo/Material/Material.h>

// STL includes
#include <filesystem>
#include <map>
#include <string>
#include <vector>


namespace ITAGeo
{
	namespace Material
	{
		//! @brief Acoustic material manager that retrieves data from a folder (checks file endings).
		//! IMPORTANT: Can currently only handle Third Octave Material!
		class ITA_GEO_API CMaterialManager
		{
		public:
			CMaterialManager( );
			//! @brief Directly loads material database
			//! @throw Throws ITAException::INVALID_PARAMETER if path does not exist
			//! @param sFolderPath path to materials
			//! @param bRecursive bool to indicate if subfolders containing material files are loaded as well (default: false).
			CMaterialManager( const std::string& sFolderPath, const bool bRecursive = false );
			virtual ~CMaterialManager( );

			//! @brief Adds material files (.mat) files from path to the manager. Throws ITAException::INVALID_PARAMETER if path does not exist or material could not be
			//! read.
			//! @throw Throws ITAException::INVALID_PARAMETER if path does not exist or material file could not be read.
			//! @param sFolderPath path to materials
			//! @param bRecursive bool to indicate if subfolders containing material files are loaded as well (default: false).
			void AddMaterialsFromFolder( const std::string& sFolderPath, const bool bRecursive = false );
			bool AddMaterial( const std::string& sName, std::shared_ptr<IMaterial> pMaterial );

			bool HasMaterial( const std::string& sMaterialIdentifier ) const;

			std::shared_ptr<IMaterial> GetMaterial( const std::string& sMaterialIdentifier ) const;
			int GetNumMaterials( ) const;

		protected:
			std::map<std::string, std::shared_ptr<IMaterial> > m_mMaterials; //! Name - Material map

		private:
			inline void addPathEntry( const std::filesystem::directory_entry& fsEntry );
		};

	} // namespace Material
} // namespace ITAGeo

#endif // IW_ITA_GEO_MATERIAL_MANAGER
