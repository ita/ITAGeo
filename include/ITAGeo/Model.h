/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_GEO_MODEL
#define INCLUDE_WATCHER_ITA_GEO_MODEL

// ITAGeo includes
#include <ITAGeo/Base.h>
#include <ITAGeo/Definitions.h>
#include <ITAGeo/Material/MaterialManager.h>

// Assimp includes
#include <assimp/scene.h>

// OpenMesh includes
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

// STL includes
#include <memory>
#include <optional>

using CITAMesh = OpenMesh::PolyMesh_ArrayKernelT<>;
// map for the visualisation
using LineHierachyMap = std::map<unsigned int, std::map<std::pair<unsigned int, unsigned int>, std::vector<aiMesh*>>>;

namespace ITAGeo
{
	//! @brief Geometric model for acoustics
	//! @details This model based on geometrical data can be used for acoustic simulation. It holds an ASSIMP scene which is loaded from a CAD model.
	//! The scene can be converted to an OpenMesh mesh. If a material manager is present the mesh will have face properties holding
	//! respective materials from the material manager. They can be access and manipulated via
	//! 'auto faceManager = OpenMesh::getProperty<CITAMesh::FaceHandle, ITAGeo::Material::CAcousticFaceProperty>( *YOUR_MESH, "Acoustics::Material::Face" );
	//! std::shared_ptr<ITAGeo::Material::IMaterial> pMaterial = faceManager[YOUR_CITA_FACEHANDLE]'.
	//! The scene can be exported with visualisation if visualisation parameters were provided.
	class ITA_GEO_API CModel
	{
	public:
		CModel( )  = default;
		~CModel( ) = default;

		// Copy constructor has to copy the whole scene and additionaly adds "_copy" to name
		CModel( const CModel& other );
		// Copy assignment has to copy the whole scene and additionaly adds "_copy" to name
		CModel& operator=( const CModel& other );


		//! @brief Loads CAD model from 'sPath'
		//! @details Tries to load a model from a given path. If file cannot be found or file format is not supported the function just returns false. If the Assimp
		//! importer fails an exception is thrown. Model name is set based on filename.
		//! @param sPath location of model.
		//! @return returns true if successful and false if not.
		//! @throws ITAException::PARSE_ERROR if the assimp import fails.
		//! @warning Your model to load currently MUST be in a right-handed coordinate system (y up, -z forward). Additionally, the unit of 1 represents 1 meter. Please
		//! make sure that your model fullfils these criterions or unwanted side-effects may occur.
		//! @note When exporting your model with materials make sure that the names are not changed while exporting, as the whole name is used to search for an equivilant
		//! material in the MaterialManager
		bool Load( const std::string& sPath );

		//! @brief Stores internal scene with visualization if available. Output Format is determined by file ending.
		//! @details Tries to store the internal scene with visualization, if a propagationpath, emitter, and sensor are given. Returns false if directory cannot be found
		//! or file format is not supported. If the assimp exporter fails and exception is thrown.
		//! @param sPath location to store model.
		//! @param sFormatExtension (default = "") file format. Has to EXCLUDE the dot at the beginning of an extension. If kept empty the extension
		//! is derived from the output path (extension after the last dot). For example, if you want to load a collada file you have to put "collada" as sFormatExtension
		//! and "YOUR_PATH/file.dae" as sPath.
		//! @throw ITAException::PARSE_ERROR if the assimp export fails.
		//! @return returns true if successful. False if not.
		bool Store( const std::string& sPath, const std::string& sFormatExtension = "" ) const;

		//! @brief Converts the internal assimp scene to an openmesh mesh and adds material face properties if materialManager is available.
		//! @details Converts a loaded assimp scene to the openmesh representation. If no assimp scenen is loaded a ITAException::CONFIG_ERROR is thrown. Lines and Points
		//! are ignored. Assigns a face property if a MaterialManager is given and the material name of a mesh is found in the manager.
		//! @return unique_ptr of openmesh
		//! @throw ITAException::CONFIG_ERROR when no scene is found.
		//! @note If materials are not assigned to faces make sure that the material names of your import model were not changed while exporting.
		std::unique_ptr<CITAMesh> GetOpenMesh( ) const;


		//! @brief Adds parameters needed for visualisation
		//! @param vPropagationPaths PropagationPathList which was created by a pathfinder
		//! @param aEmitter vector of sound emitters
		//! @param aSensor vector of sound sensors
		void AddVisualization( ITAGeo::CPropagationPathList& vPropagationPaths, std::vector<ITAGeo::CEmitter>& vEmitters, std::vector<ITAGeo::CSensor>& vSensors );
		void SetMaterialManager( std::shared_ptr<ITAGeo::Material::CMaterialManager> pMaterialManager );

		// getter
		inline std::string GetName( ) const noexcept { return this->m_sName; };
		inline std::shared_ptr<ITAGeo::Material::CMaterialManager> GetMaterialManager( ) const noexcept { return this->m_pMaterialManager; };

	private: /// FUNCTIONS
		/// GET OPENMESH FUNCTIONS

		// openmesh creation
		// add currentNode to mesh, recursively add child nodes to mesh
		void RecursivelyAddNodesToMesh( const aiNode* aiCurrentNode, CITAMesh* pMesh, aiMatrix4x4 m4CurrentTransformation,
		                                std::map<aiVector3D, CITAMesh::VertexHandle>* pGlobalMeshVertexHandleMap ) const;

		// add vertex and faces to the input pMesh, current rotation is applied
		void AddToMesh( CITAMesh* pMesh, std::vector<unsigned int>& vMeshIndices, aiMatrix4x4 m4CurrentTransformation,
		                std::map<aiVector3D, CITAMesh::VertexHandle>* pGlobalMeshVertexHandleMap ) const;


		/// VISUALISATION FUNCTIONS

		//! @brief THE function that handles the visualisation
		//! @return returns an assimp aiScene ptr to a visualisation scene.
		aiScene* const CreateVisualizationScene( ) const;

		const std::vector<aiMesh*> CreateVisualizationCubes( const std::vector<VistaVector3D>& vOrigins ) const;
		//! @brief Sorts input meshes based on their overall reflection/diffraction order
		//! @detail								combined order
		//!			0 (order)			1 (order)					2 (order)				...
		//!				|					|							|
		//!			  (0,0)			 (1,0)    (0,1)			 (2,0)    (1,1)    (0,2)		...		<- local order (specular reflection order, diffraction order)
		//!		  [Mesh*, ...]	[Mesh*, ...][Mesh*, ...]   [Mesh*, ...]     ...
		//!
		//! Each unique combination of reflection and diffraction order holds a list of line meshes. This enables us later to export the lines in the same assimp
		//! scene hierachy as this map monstrosity.
		//! @return Map with combined order as first param and a map as second param. The second map splits up the global order into reflections and/or diffractions
		const std::map<unsigned int, std::map<std::pair<unsigned int, unsigned int>, std::vector<aiMesh*>>> CreateVisulizationLines( ) const;

		//! @brief Create simple aiMaterial for emitter, sensor, and lines. RGB values have to be between 0 and 1
		//! @return aiMaterial with input colour
		aiMaterial* CreateAiMaterial( float fRed, float fGreen, float fBlue ) const;

		//! @brief Creates all materials needed in our visualisation scene
		//! @param uiCombinedOrder largest combined order.
		//! @return aiMaterial* array that has first all materials, one for each combination of reflection and diffraction combi, and last two materials, one for emitters
		//! one for sensors
		aiMaterial** CreateSceneMaterials( const unsigned int uiCombinedOrder ) const;

		//! @brief Returns a material index for a group of lines with a certain reflection and diffraction order.
		//! @detail The idea is, that each possible combined order pair has a material even if we do not use it.
		//! The combined order of 0 has only one possible material, for the pair 0,0,
		//! The combined order of 1 has two possible materials, for 1,0 and 0,1
		//! The combined order of 2 has three possible materials, for 2,0 and 1,1 and 0,2
		//! And so forth. Each order has one more material then the one before.
		//! @param pairPathOrders pair of reflection and diffraction order
		//! @return index of material in assimp visualisation scene.
		const unsigned int GetMaterialIndex( const std::pair<unsigned int, unsigned int>& pairPathOrders ) const;


		unsigned int GetSumUpTo( const unsigned int uiCombinedOrder ) const;

	private:                        /// VARIABLES
		std::string m_sName { "" }; // Set in load function
		std::unique_ptr<aiScene> m_pScene { nullptr };
		std::shared_ptr<ITAGeo::Material::CMaterialManager> m_pMaterialManager { nullptr };

		// For Visualization
		struct SVisHelper
		{
			ITAGeo::CPropagationPathList vPropagationPaths;
			std::vector<ITAGeo::CEmitter> vEmitters;
			std::vector<ITAGeo::CSensor> vSensors;
		};
		std::optional<SVisHelper> m_oVisualisationObjects;
	};
} // namespace ITAGeo

#endif // INCLUDE_WATCHER_ITA_GEO_MODEL