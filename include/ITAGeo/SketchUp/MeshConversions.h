/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_GEO_SU_MESH_CONVERSIONS
#define INCLUDE_WATCHER_ITA_GEO_SU_MESH_CONVERSIONS

// ITAGeo includes
#include <ITAGeo/Material/MaterialManager.h>

// Sketchup includes
#include <SketchUpAPI/model/component_definition.h>
#include <SketchUpAPI/model/component_instance.h>
#include <SketchUpAPI/model/entities.h>
#include <SketchUpAPI/model/layer.h>
#include <SketchUpAPI/transformation.h>

// OpenMesh includes
#pragma warning( disable : 4512 4127 )
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#pragma warning( default : 4512 4127 )

// STL includes
#include <vector>

// Typedefs
typedef OpenMesh::PolyMesh_ArrayKernelT<> CITAMesh;

namespace ITAGeo
{
	namespace SketchUp
	{
		//! Add entities to the mesh
		ITA_GEO_API SUResult SUEntitiesToITAMesh( SUEntitiesRef rEntities, CITAMesh* pMesh, bool bExplodeGroups,
		                                          std::shared_ptr<Material::CMaterialManager> pMaterialManager = nullptr );

		//! Add entities to the mesh with transformation chain
		ITA_GEO_API SUResult SUEntitiesToITAMesh( SUEntitiesRef rEntities, CITAMesh* pMesh, bool bExplodeGroups, std::vector<SUTransformation>& vtTransform,
		                                          std::shared_ptr<Material::CMaterialManager> pMaterialManager = nullptr );

		//! Convert from ITAMesh to SketchUp entities (and add to a layer if not NULL)
		ITA_GEO_API SUResult ITAMeshToSUEntities( const CITAMesh* pMesh, SUEntitiesRef* rEntities, SULayerRef* pSULayer = NULL );

		//! Converts from SketchUp component instante to mesh
		ITA_GEO_API SUResult SUComponentInstanceToITAMesh( SUComponentInstanceRef rCompInstance, CITAMesh* pMesh,
		                                                   std::shared_ptr<Material::CMaterialManager> pMaterialManager = nullptr );
	} // namespace SketchUp
} // namespace ITAGeo

#endif // INCLUDE_WATCHER_ITA_GEO_SU_MESH_CONVERSIONS
