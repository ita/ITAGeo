/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_GEO_RESOURCE_MANAGER
#define IW_ITA_GEO_RESOURCE_MANAGER

// ITAGeo includes
#include <ITAGeo/Definitions.h>
#include <ITAGeo/Directivity/DirectivityManager.h>
#include <ITAGeo/Material/MaterialManager.h>

// STL includes
#include <string>


namespace ITAGeo
{
	//! Resource manager
	class ITA_GEO_API CResourceManager
	{
	public:
		CResourceManager( ) = default;
		CResourceManager( std::shared_ptr<Material::CMaterialManager> );
		CResourceManager( std::shared_ptr<Directivity::CDirectivityManager> );
		CResourceManager( std::shared_ptr<Material::CMaterialManager> pMM, std::shared_ptr<Directivity::CDirectivityManager> pDM );
		virtual ~CResourceManager( ) = default;

		std::shared_ptr<Material::CMaterialManager> GetMaterialManager( ) const;
		std::shared_ptr<Directivity::CDirectivityManager> GetDirectivityManager( ) const;

		//! Checks if material can be found
		virtual bool HasMaterial( const std::string& sMaterialIdentifier ) const;

		//! Retrieve acoustic material, if available
		/**
		 * @param[in] sMaterialIdentifier Text-based material identifier (usually the name of the material)
		 * @return Pointer to material or nullptr, if not available
		 */
		virtual std::shared_ptr<Material::IMaterial> GetMaterial( const std::string& sMaterialIdentifier ) const;

		//! Checks if directivity can be found
		virtual bool HasDirectivity( const std::string& sDirectivityIdentifier ) const;

		//! Retrieve directivity, if available
		/**
		 * @param[in] sID Text-based directivity identifier (usually the name of the directivity file)
		 * @return Pointer to directivity or nullptr, if not available
		 *
		 * @note Will first query default manager and then in order of manager list
		 */
		virtual std::shared_ptr<Directivity::IDirectivity> GetDirectivity( const std::string& sID ) const;

		//! Returns the identifier of a material pointer from the resource mapping
		virtual std::string GetDirectivityID( std::shared_ptr<Directivity::IDirectivity> pDirectivity ) const;

		//! Add a material copy, if not already existant
		/**
		 * @return True, if possible, false if already existing or manager is read-only
		 */
		virtual bool AddMaterial( const std::string& sIdentifier, std::shared_ptr<Material::IMaterial> pMat );

		//! Add a material copy, if not already existant
		/**
		 * @return True, if possible, false if already existing or manager is read-only
		 */
		virtual bool AddDirectivity( const std::string& sIdentifier, std::shared_ptr<Directivity::IDirectivity> pDirectivity );

	private:
		std::shared_ptr<Material::CMaterialManager> m_pMaterialManager;
		std::shared_ptr<Directivity::CDirectivityManager> m_pDirectivityManager;
	};

} // namespace ITAGeo

#endif // IW_ITA_GEO_RESOURCE_MANAGER
