/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_GEO_DIRECTIVITY_MANAGER
#define IW_ITA_GEO_DIRECTIVITY_MANAGER

// ITAGeo includes
#include <ITAGeo/Definitions.h>
#include <ITAGeo/Directivity/Base.h>

// STL includes
#include <filesystem>
#include <map>
#include <memory>
#include <string>
#include <vector>


namespace ITAGeo
{
	namespace Directivity
	{
		//! Acoustic directivity manager that retrieves data from a folder (checks file endings)
		class ITA_GEO_API CDirectivityManager
		{
		public:
			CDirectivityManager( );
			//! @brief Directly load directivities from path.
			//! @throw ITAExcetion::INVALID_PARAMETER if path wrong or .daff format not supported.
			//! @param sFolderPath path to directivity files.
			//! @param bRecursive if true includes subfolders (default: false).
			CDirectivityManager( const std::string& sFolderPath, const bool bRecursive = false );
			virtual ~CDirectivityManager( );

			//! @brief Loads directivities from a given directory and subfolder if recursive is set true.
			//! @details Loads directivites from a directory. If recursive is set to true all subfolders are searched for directivites as well. Currently only opendaff
			//! (.daff) files with Version 1.7 are supported. Additionally, only Impulse Response and Magnitude Spectrum opendaff formats can be loaded. If path does not
			//! exist or the .daff format is not supported an ITAException::INVALID_PARAMETER exception is thrown.
			//! @throw ITAExcetion::INVALID_PARAMETER if path wrong or .daff format not supported.
			//! @param sFolderPath path to directivity files.
			//! @param bRecursive if true includes subfolders (default: false).
			void AddDirectivitiesFromFolder( const std::string& sFolderPath, const bool bRecursive = false );
			bool AddDirectivity( const std::string& sID, std::shared_ptr<IDirectivity> pDirectivity );

			bool HasDirectivity( const std::string& sID ) const;

			std::shared_ptr<IDirectivity> GetDirectivity( const std::string& sDirectivityIdentifier ) const;
			std::string GetDirectivityID( const std::shared_ptr<IDirectivity> pDirectivity ) const;
			int GetNumDirectivities( ) const;

		protected:
			std::map<std::string, std::shared_ptr<IDirectivity> > m_mDirectivities { };

		private:
			//! @brief Actual directivity load
			inline void addPathEntry( const std::filesystem::directory_entry& fsEntry );
		};

	} // namespace Directivity
} // namespace ITAGeo

#endif // IW_ITA_GEO_DIRECTIVITY_MANAGER
