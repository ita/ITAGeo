/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 * 				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_GEO_WEATHERPROFILEBASE
#define INCLUDE_WATCHER_ITA_GEO_WEATHERPROFILEBASE

#include <ITAGeo/Definitions.h>
#include <VistaBase/VistaVector3D.h>

// ITABase includes
#include <ITAException.h>

// STL includes
#include <sstream>


// Helper Makros
#define ITA_HUMIDITY_PROFILE_STR    ITAGeo::WeatherProfileBase::IWeatherProfile::GetProfileTypeStr( ITAGeo::WeatherProfileBase::EProfileType::HUMIDITY )
#define ITA_TEMPERATURE_PROFILE_STR ITAGeo::WeatherProfileBase::IWeatherProfile::GetProfileTypeStr( ITAGeo::WeatherProfileBase::EProfileType::TEMPERATURE )
#define ITA_WIND_PROFILE_STR        ITAGeo::WeatherProfileBase::IWeatherProfile::GetProfileTypeStr( ITAGeo::WeatherProfileBase::EProfileType::WIND )

#define ITA_WEATHERPROFILE_PRECALC_MAXALTITUDE_DEFAULT 15000

namespace ITAGeo
{
	namespace WeatherProfileBase
	{
		enum EProfileType
		{
			INVALID_PROFILE = -1,
			HUMIDITY        = 0,
			TEMPERATURE,
			WIND,

			NUM_PROFILE_TYPES //!< Last element of EClassType to iterate over EClassType
		};

		enum EClassType
		{
			INVALID_PROFILE_CLASS = -1,
			CONSTANT              = 0,
			IMPORT,
			ROOM,
			ISA,
			ZERO,
			LOG,

			NUM_PROFILE_CLASS_TYPES //!< Last element of EClassType to iterate over EClassType
		};

		//! Base class for all weather profile interfaces
		/**
		 *	Implements many JSON related functions and introduces enums for the different class types.
		 */
		class ITA_GEO_API IWeatherProfile
		{
		public:
			const EProfileType iProfileType; // Type of weather profile
			const EClassType iClassType;     // Type of weather profile class

		protected:
			inline IWeatherProfile( const EProfileType& iType, const EClassType& iClassType ) : iProfileType( iType ), iClassType( iClassType ) {};

		public:
			IWeatherProfile& operator=( const IWeatherProfile& oOther ) = delete;

		public:
			static std::string GetProfileTypeStr( const EProfileType& iType );
			static std::string GetProfileClassStr( const EClassType& iType );

			static EProfileType ParseProfileType( const std::string& sTypeStr );
			static EClassType ParseProfileClassType( const std::string& sTypeStr );

			inline std::string GetProfileTypeStr( ) const { return GetProfileTypeStr( iProfileType ); };
			inline std::string GetProfileClassStr( ) const { return GetProfileClassStr( iClassType ); };

			//! Returns true if this profile has a maximum altitude for valid data (above data might not be properly specified, e.g. being constant)
			inline virtual bool HasMaximumAltitude( double& dMaxAltitude ) const { return false; };
		};


		class ITA_GEO_API IPrecalcProfileExtension
		{
		private:
			bool bPrecalcInitialized = false; // Indicates whether precalculation was initialized
			bool bPrecalculate;               // Disables/Enables precalculation of weather data using an altitude resolution of 1m
			int iMaxPrecalcAltitude;          // Maximum altitude [m] used for precalculation of weather data

		protected:
			IPrecalcProfileExtension( bool bPrecalculate = false, int iMaxPrecalcAltitude = ITA_WEATHERPROFILE_PRECALC_MAXALTITUDE_DEFAULT );
			// Initializes precalculation data, to be called in constructors of derived classes
			void InitPrecalculation( );
			// Precalculates data for fixed altitude vector
			// MUST BE OVERLOADED IN DERIVED CLASSES
			virtual void Precalculate( ) = 0;
			// Returns the nearest altitude used in the vector for precalculated data
			int NearestPrecalcAltitude( const double& dAltitude ) const;

			bool UsePrecalculatedData( ) const { return bPrecalculate && bPrecalcInitialized; };

			bool MaximumAltitudeRelevant( double& dMaxAltitude ) const;

		public:
			bool PrecalculationEnabled( ) const { return bPrecalculate; };
			int PrecalculationMaxAltitude( ) const { return iMaxPrecalcAltitude; };
			IPrecalcProfileExtension& operator=( const IPrecalcProfileExtension& oOther );
		};
	} // namespace WeatherProfileBase
} // namespace ITAGeo

#endif // INCLUDE_WATCHER_ITA_GEO_WEATHERPROFILEBASE