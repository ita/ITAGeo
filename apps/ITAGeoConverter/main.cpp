/*
 * ----------------------------------------------------------------
 *
 *		ITA geometrical acoustics
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Loads an ITAGeo halfedge mesh model from file using internal
 * IO reader (obj, ac, skp, gml, etc.) and stores it using the internal IO
 * writer (skp, ac etc.)
 *
 */

#include <ITAException.h>
#include <ITAGeo/Halfedge/MeshModel.h>
#include <assert.h>
#include <iostream>

using namespace std;
using namespace ITAGeo;

int main( int argc, char** argv )
{
	// Convert string from supported file formats
	string s   = Halfedge::CMeshModel::GetSupportedInputFormats( );
	size_t idx = s.find_first_of( ";;" );
	if( idx < 13 + 2 )
		cerr << "Could not retrieve supported input formats from OpenMesh" << endl;
	string sSupportedImportFiles = s.substr( 13, idx - 2 - 13 );
	s                            = Halfedge::CMeshModel::GetSupportedOutputFormats( );
	idx                          = s.find_first_of( ";;" );
	if( idx < 13 + 2 )
		cerr << "Could not retrieve supported input formats from OpenMesh" << endl;
	string sSupportedExportFiles = s.substr( 13, idx - 2 - 13 );

	// Check arguments
	if( argc < 3 )
	{
		cerr << "Syntax error occured, usage: ITAGeoConverter in_file_name.??? out_file_name.??? [flip_normals]" << endl;
		cout << "Supported import formats: " << sSupportedImportFiles << endl;
		cout << "Supported output formats: " << sSupportedExportFiles << endl;
		return 255;
	}


	string sModelFilePathIn  = argv[1];
	string sModelFilePathOut = argv[2];
	bool bFlipNormals        = ( argc > 3 ) ? true : false;

	cout << "Converting from input file '" << sModelFilePathIn << "' to '" << sModelFilePathOut << "'" << endl;

	Halfedge::CMeshModel oHEModel;

	try
	{
		bool bError = false;
		if( oHEModel.Load( sModelFilePathIn ) )
			cout << "Sucessfully loaded file '" << sModelFilePathIn << "' as an halfedge model" << endl;
		else
			bError = true;

		if( bFlipNormals )
			if( oHEModel.InvertFaceNormals( ) )
				cout << "Sucessfully inverted face normals" << endl;

		if( oHEModel.Store( sModelFilePathOut ) )
			cout << "Sucessfully stored file '" << sModelFilePathOut << "'" << endl;
		else
			bError = true;

		if( bError )
		{
			cerr << "An unknown error occured during import/export." << endl;
			cout << "Supported import formats: " << sSupportedImportFiles << endl;
			cout << "Supported output formats: " << sSupportedExportFiles << endl;
		}
	}
	catch( const ITAException& e )
	{
		cerr << "An error occured: " << e << endl;
		return 255;
	}

	return 0;
}
