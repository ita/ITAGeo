%% Load

json_file_path = 'CombinatedModelTest_SimpleUrbanEnvironment.skp.json';
txt = fileread( json_file_path );
json_content = jsondecode( txt );

N = numel( json_content.propagation_paths );
fprintf( 'Found %i propagation paths\n', N )


%% Calculations

prop_path_length = zeros( 1, N );
for n = 1:N
    prop_path = json_content.propagation_paths( n );
    assert( strcmpi( prop_path.class, 'propagation_path' ) )
    
    for a = 2:numel( prop_path.propagation_anchors )
        
        if iscell( prop_path.propagation_anchors( a - 1 ) )
            prop_anchor_prev = prop_path.propagation_anchors{ a - 1 };
        else
            prop_anchor_prev = prop_path.propagation_anchors( a - 1 );
        end
        
        if iscell( prop_path.propagation_anchors( a ) )
            prop_anchor_curr = prop_path.propagation_anchors{ a };
        else
            prop_anchor_curr = prop_path.propagation_anchors( a );
        end
        
        prop_segment_vec = prop_anchor_prev.interaction_point - prop_anchor_curr.interaction_point;
        prop_path_length( n ) = prop_path_length( n ) + norm( prop_segment_vec );
    end
end


%% Statistics

fprintf( 'Mean propagation path length: %.1fm\n', mean( prop_path_length ) )

c = 341; % speed of sound
prop_path_length_sorted = sort( prop_path_length );
plot( prop_path_length_sorted ./ c, -20 * log10( prop_path_length_sorted ), '-o' )
title( 'propagation decay' )
xlabel( 'time / s')
ylabel( 'amplitude after spreading loss / dB re 1m' )


%% Histogram

histogram( prop_path_length_sorted )

